﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BearMatch
{
    public class UiManager : MonoBehaviour
    {
        //private const string POPUP_RESOURCE_PATH = "Popups/";
        private const string POPUP_RESOURCE_PATH = "";

        [SerializeField] private Canvas rootCanvas;
        [SerializeField] private SuperImage superImage;
        [SerializeField] private RectTransform gamePanelParent;
        [SerializeField] private RectTransform gamePopupParent;

        [SerializeField] private Transform coin;

        public static UiManager Instance { get; private set; }

        //public Color SuperImageColor => superImage.color;
        public SuperImage SuperImage => superImage;

        public Canvas RootCanvas => rootCanvas;

        public float SizeScale { get; private set; }

        public Vector3 CoinPosition => coin.position;

        private Dictionary<string, GamePopup> gamePopups = new Dictionary<string, GamePopup>();
        private GamePopup currentPopup;

        private void Awake()
        {
            Instance = this;
        }

        private IEnumerator Start()
        {
            yield return null;

            // get canvas height
            CanvasScaler canvasScaler = GetComponent<CanvasScaler>();
            if (canvasScaler != null)
            {
                float panelWidth = Screen.width;
                float panelHeight = Screen.height;
                Debug.unityLogger.Log("panelWidth: " + panelWidth);
                Debug.unityLogger.Log("panelHeight: " + panelHeight);

                SizeScale = canvasScaler.referenceResolution.y / panelHeight;
                if (SizeScale * panelWidth < canvasScaler.referenceResolution.x)
                {
                    SizeScale = canvasScaler.referenceResolution.x / panelWidth;
                }
                Debug.unityLogger.Log("sizeScale: " + SizeScale);
            }
            else
            {
                Debug.unityLogger.Log("Cannot get sizeScale. canvasScaler is NULL");
            }
        }

        public T ShowPanel<T>(object data = null, UnityAction closeCallback = null, bool isHideLastPopup = true, bool isShowSuperImage = true) where T : GamePopup
        {
            string popupName = typeof(T).Name;
            if (!gamePopups.ContainsKey(popupName))
            {
                gamePopups.Add(popupName, Instantiate(Resources.Load<GamePopup>(POPUP_RESOURCE_PATH + popupName), gamePanelParent));
            }

            currentPopup = gamePopups[popupName];

            superImage.transform.SetAsLastSibling();
            currentPopup.transform.SetAsLastSibling();

            currentPopup.Show(currentPopup, isHideLastPopup, data, closeCallback, isShowSuperImage);

            coin.SetAsLastSibling();
            return (T)currentPopup;
        }

        public T ShowPopup<T>(object data = null, UnityAction closeCallback = null, bool isHideLastPopup = true, bool isShowSuperImage = true) where T : GamePopup
        {
            string popupName = typeof(T).Name;
            if (!gamePopups.ContainsKey(popupName))
            {
                gamePopups.Add(popupName, Instantiate(Resources.Load<GamePopup>(POPUP_RESOURCE_PATH + popupName), gamePopupParent));
            }

            currentPopup = gamePopups[popupName];
            currentPopup.transform.SetAsLastSibling();
            currentPopup.Show(currentPopup, isHideLastPopup, data, closeCallback, isShowSuperImage);

            return (T)currentPopup;
        }

        public void ClosePopup<T>() where T : GamePopup
        {
            if (currentPopup is T)
            {
                currentPopup.Click_DefaultClosePopup();
            }
        }

        public T GetPopup<T>() where T : GamePopup
        {
            string popupName = typeof(T).Name;
            if (!gamePopups.ContainsKey(popupName))
            {
                gamePopups.Add(popupName, Instantiate(Resources.Load<GamePopup>(POPUP_RESOURCE_PATH + popupName), gamePopupParent));
            }

            return (T)gamePopups[popupName];
        }

        public bool HasShowPopup<T>() where T : GamePopup
        {
            string popupName = typeof(T).Name;
            return gamePopups.ContainsKey(popupName) && gamePopups[popupName].gameObject.activeInHierarchy;
        }

        public void ShowSuperImage(RectTransform rect, Vector4 margin)
        {
            superImage.gameObject.SetActive(true);
            //superImage.UpdateVertices(rect, rect == null ? Vector4.zero : margin);
        }

        public void HideSuperImage()
        {
            superImage.gameObject.SetActive(false);
        }

        public void ShowCoin()
        {
            coin.SetAsLastSibling();
        }
    }
}
