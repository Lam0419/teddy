﻿using UnityEngine;
using System.Collections.Generic;

namespace BearMatch
{
    public class EventDispatcher : MonoBehaviour
    {
        #region Singleton
        public static EventDispatcher Instance { get; private set; }

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
            Instance = this;
            Logger.Log("Awake EventDispatcher");
        }

        private void OnDestroy()
        {
            // reset this static var to null if it's the singleton instance
            if (Instance == this)
            {
                ClearAllListener();
            }
        }
        #endregion


        #region Fields
        public delegate void ActionCallback(object obj = null);
        /// Store all "listener"
        readonly Dictionary<EventID, ActionCallback> _listeners = new();
        #endregion


        #region Add Listeners, Post events, Remove listener
        /// <summary>
        /// Register to listen for eventID
        /// </summary>
        /// <param name="eventID">EventID that object want to listen</param>
        /// <param name="callback">Callback will be invoked when this eventID be raised</param>
        public void AddListener(EventID eventID, ActionCallback callback)
        {
            // check if listener exist in distionary
            if (_listeners.ContainsKey(eventID))
            {
                // add callback to our collection
                _listeners[eventID] += callback;
            }
            else
            {
                // add new key-value pair
                _listeners.Add(eventID, null);
                _listeners[eventID] += callback;
            }
        }

        /// <summary>
        /// Posts the event with param. This will notify all listener that register for this event
        /// </summary>
        /// <param name="eventID">EventID.</param>
        /// <param name="param">Parameter. Can be anything (struct, class ...), Listener will make a cast to get the data</param>
        public void PostEvent(EventID eventID, object param)
        {
            if (!_listeners.ContainsKey(eventID))
                return;

            _listeners[eventID]?.Invoke(param ?? eventID);
        }

        /// <summary>
        /// Removes the listener has param. Use to Unregister listener
        /// </summary>
        /// <param name="eventID">EventID.</param>
        /// <param name="callback">Callback.</param>
        public void RemoveListener(EventID eventID, ActionCallback callback)
        {
            if (_listeners.ContainsKey(eventID))
                _listeners[eventID] -= callback;
        }

        /// <summary>
        /// Removes the listener has param. Use to Unregister listener
        /// </summary>
        /// <param name="eventID">EventID.</param>
        /// <param name="callback">Callback.</param>
        public void RemoveAllListener(EventID eventID)
        {
            if (_listeners.ContainsKey(eventID))
                _listeners[eventID] = null;
        }

        /// <summary>
        /// Clears all the listener.
        /// </summary>
        public void ClearAllListener()
        {
            _listeners.Clear();
        }
        #endregion
    }


    #region Extension class
    /// <summary>
    /// Delare some "shortcut" for using EventDispatcher easier
    /// </summary>
    public static class EventDispatcherExtension
    {
        /// Post event with param
        public static void PostEvent(EventID eventID, object param = null)
        {
            // instance not exist, then create new one
            if (EventDispatcher.Instance == null)
            {
                new GameObject("EventDispatcher", typeof(EventDispatcher));
            }

            EventDispatcher.Instance.PostEvent(eventID, param);
        }

        /// Use for registering with EventsManager with param
        public static void AddListener(EventID eventID, EventDispatcher.ActionCallback callback)
        {
            // instance not exist, then create new one
            if (EventDispatcher.Instance == null)
            {
                new GameObject("EventDispatcher", typeof(EventDispatcher));
            }

            EventDispatcher.Instance.AddListener(eventID, callback);
        }

        /// Use for registering with EventsManager with param
        public static void RemoveListener(EventID eventID, EventDispatcher.ActionCallback callback)
        {
            EventDispatcher.Instance.RemoveListener(eventID, callback);
        }

        /// Use for registering with EventsManager with param
        public static void RemoveAllListener(EventID eventID)
        {
            EventDispatcher.Instance.RemoveAllListener(eventID);
        }

        public static void ClearAllListener()
        {
            EventDispatcher.Instance.ClearAllListener();
        }
    }
    #endregion
}
