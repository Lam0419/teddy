using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;

namespace BearMatch
{
    public class StoreManager : MonoBehaviour, IStoreListener
    {
        public static StoreManager Instance { get; private set; }

        private static IStoreController m_StoreController;          // The Unity Purchasing system.
        private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

        public const string product_no_ad = "iap.remove.ads";

        private UnityAction<bool, string> buySuccessCallback;

        private void Awake()
        {
            if (Instance != null)
            {
                return;
            }

            Debug.unityLogger.Log("Awake Store Manager");
            Instance = this;
        }

        private void Start()
        {
            if (m_StoreController == null)
            {
                InitializePurchasing();
            }
        }

        private void InitializePurchasing()
        {
            if (IsInitialized())
            {
                return;
            }

            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance())
                .AddProduct(product_no_ad, ProductType.NonConsumable);

            UnityPurchasing.Initialize(this, builder);
        }

        private bool IsInitialized()
        {
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }



        #region Buy Function
        public void BuyItem(string productId, UnityAction<bool, string> buyCallback = null)
        {
            buySuccessCallback = buyCallback;

            if (IsInitialized())
            {
                Product product = m_StoreController.products.WithID(productId);

                if (product != null && product.availableToPurchase)
                {
                    Debug.unityLogger.Log($"Purchasing product asychronously: '{product.definition.id}'");

                    AdsManager.Instance.IsPausedBySystem = true;
                    m_StoreController.InitiatePurchase(product);
                }
                else
                {
                    Debug.unityLogger.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");

                    buySuccessCallback?.Invoke(false, productId);
                }
            }
            else
            {
                Debug.unityLogger.Log("BuyProductID FAIL. Not initialized.");

                InitializePurchasing();
                buySuccessCallback?.Invoke(false, productId);
            }
        }

        public string GetPrice(string productID)
        {
            if (IsInitialized())
            {
                return m_StoreController.products.WithID(productID).metadata.localizedPriceString;
            }
            else
            {
                Debug.unityLogger.Log("GetPrice FAIL. Not initialized.");
                InitializePurchasing();
                return "";
            }
        }
        #endregion



        public void RestorePurchases()
        {
            if (IsInitialized())
            {
                AdsManager.Instance.IsPausedBySystem = true;

                if (Application.platform == RuntimePlatform.IPhonePlayer
                    || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    Debug.Log("RestorePurchases started ...");
                    m_StoreExtensionProvider.GetExtension<IAppleExtensions>().RestoreTransactions(OnTransactionsRestored);
                }
                else
                {
                    Debug.LogWarning("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
                }
            }
            else
            {
                Debug.unityLogger.Log("RestorePurchases FAIL. Not initialized.");
            }
        }

        private void OnTransactionsRestored(bool success)
        {
            Debug.unityLogger.Log("Transactions restored: " + success);
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            Debug.unityLogger.Log("OnInitialized: PASS");

            m_StoreController = controller;
            m_StoreExtensionProvider = extensions;
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.unityLogger.Log("OnInitializeFailed InitializationFailureReason:" + error);
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            switch (args.purchasedProduct.definition.id)
            {
                case product_no_ad:
                    SaveLoadData.Data.IsBoughtNoAds = true;
                    SaveLoadData.SaveData();
                    EventDispatcherExtension.PostEvent(EventID.Bought_No_Ads);
                    break;

                default:
                    Debug.unityLogger.Log($"ProcessPurchase: PASS. Product: '{args.purchasedProduct.definition.id}' but DO NOTHING");
                    break;
            }

            buySuccessCallback?.Invoke(true, args.purchasedProduct.definition.id);

            return PurchaseProcessingResult.Complete;
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            Debug.unityLogger.Log($"OnPurchaseFailed: FAIL. Product: '{product.definition.storeSpecificId}', PurchaseFailureReason: {failureReason}");
            buySuccessCallback?.Invoke(false, product.definition.id);
        }
    }
}
