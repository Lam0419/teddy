﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void ExampleGUI::OnGUI()
extern void ExampleGUI_OnGUI_m7F98D722B7B9B570BBAA43D6E62AA1C9ADAB9514 (void);
// 0x00000002 System.Void ExampleGUI::ShowGUI(System.Int32)
extern void ExampleGUI_ShowGUI_m3A21CF5132CCE304624990EEB9311B27FC974279 (void);
// 0x00000003 System.Void ExampleGUI::HandleGooglePlayId(System.String)
extern void ExampleGUI_HandleGooglePlayId_m3E5E31CFA1DACF07F0E799F5C9D79A0AAE15ABE0 (void);
// 0x00000004 System.Void ExampleGUI::AttributionChangedCallback(com.adjust.sdk.AdjustAttribution)
extern void ExampleGUI_AttributionChangedCallback_mF94C25FEB4A93C591437B1B7B980C4FEA040BA73 (void);
// 0x00000005 System.Void ExampleGUI::EventSuccessCallback(com.adjust.sdk.AdjustEventSuccess)
extern void ExampleGUI_EventSuccessCallback_m31F74382525AD79CE9078C9E3DBA80527C24124D (void);
// 0x00000006 System.Void ExampleGUI::EventFailureCallback(com.adjust.sdk.AdjustEventFailure)
extern void ExampleGUI_EventFailureCallback_m3E6FA431F4BD7E4B85E03C02B841950D78AF35EE (void);
// 0x00000007 System.Void ExampleGUI::SessionSuccessCallback(com.adjust.sdk.AdjustSessionSuccess)
extern void ExampleGUI_SessionSuccessCallback_m0900AAA8351E1B38AA866EC6BC8D138165E2146C (void);
// 0x00000008 System.Void ExampleGUI::SessionFailureCallback(com.adjust.sdk.AdjustSessionFailure)
extern void ExampleGUI_SessionFailureCallback_mE56B4A25ADA21A2A3CE51C1CC1B9DED1CDC93059 (void);
// 0x00000009 System.Void ExampleGUI::DeferredDeeplinkCallback(System.String)
extern void ExampleGUI_DeferredDeeplinkCallback_m0490F45A5F0CAB4623C4DE98C9056D175511544B (void);
// 0x0000000A System.Void ExampleGUI::.ctor()
extern void ExampleGUI__ctor_m781F1113ADEDC46C79DCD9CDEF57E6A92867A83D (void);
// 0x0000000B System.Void ExampleGUI/<>c::.cctor()
extern void U3CU3Ec__cctor_m822E5D6A5457A73E7B23238FFDDFDE17EE74C196 (void);
// 0x0000000C System.Void ExampleGUI/<>c::.ctor()
extern void U3CU3Ec__ctor_m4795792C3C133DF92B1D9761F0AF23E6BE3D3FB6 (void);
// 0x0000000D System.Void ExampleGUI/<>c::<OnGUI>b__6_0(System.String)
extern void U3CU3Ec_U3COnGUIU3Eb__6_0_m44C8CA7B767C4F1F34EBEA0E08A25658315F0219 (void);
// 0x0000000E System.Void JoystickPlayerExample::FixedUpdate()
extern void JoystickPlayerExample_FixedUpdate_m9AEDBA111F95D67A006A5D3821956048224541B7 (void);
// 0x0000000F System.Void JoystickPlayerExample::.ctor()
extern void JoystickPlayerExample__ctor_m702422E0AE29402330CF41FDDBEE76F0506342E2 (void);
// 0x00000010 System.Void JoystickSetterExample::ModeChanged(System.Int32)
extern void JoystickSetterExample_ModeChanged_m35AF30EE3E6C8CEBF064A7AB80F5795E9AF06D23 (void);
// 0x00000011 System.Void JoystickSetterExample::AxisChanged(System.Int32)
extern void JoystickSetterExample_AxisChanged_m5CA220FEA14E06BD8A445E31C5B66E4601C5E404 (void);
// 0x00000012 System.Void JoystickSetterExample::SnapX(System.Boolean)
extern void JoystickSetterExample_SnapX_m25A77C3DE4C6DBBE3B4A58E2DE8CD44B1773D6A1 (void);
// 0x00000013 System.Void JoystickSetterExample::SnapY(System.Boolean)
extern void JoystickSetterExample_SnapY_m54FE8DCB2CE8D2BF5D2CDF84C68DE263F0E25B1B (void);
// 0x00000014 System.Void JoystickSetterExample::Update()
extern void JoystickSetterExample_Update_m99B2432D22FE669B4DC3209696AD4A62096C7D41 (void);
// 0x00000015 System.Void JoystickSetterExample::.ctor()
extern void JoystickSetterExample__ctor_m2A3D66E05BCDF78D0F116348094717BEBA73EC91 (void);
// 0x00000016 System.Single Joystick::get_Horizontal()
extern void Joystick_get_Horizontal_m78CF4472B86063E54254AC8AE0A52126E4008AFA (void);
// 0x00000017 System.Single Joystick::get_Vertical()
extern void Joystick_get_Vertical_mA2B0917896CF9CE47A6D342D1734E43441C3D4BE (void);
// 0x00000018 UnityEngine.Vector2 Joystick::get_Direction()
extern void Joystick_get_Direction_m52502695D42BDAB6075089BDE442ABE72EAC81EC (void);
// 0x00000019 System.Single Joystick::get_HandleRange()
extern void Joystick_get_HandleRange_mB38F0D3B6ADE2D1557D7A3D6759C469F17509D77 (void);
// 0x0000001A System.Void Joystick::set_HandleRange(System.Single)
extern void Joystick_set_HandleRange_m686B579A1F02EFCD4878BEA27EA606FC23CD2505 (void);
// 0x0000001B System.Single Joystick::get_DeadZone()
extern void Joystick_get_DeadZone_mCE52B635A8CF24F6DD867C14E34094515DE6AEFC (void);
// 0x0000001C System.Void Joystick::set_DeadZone(System.Single)
extern void Joystick_set_DeadZone_mD5699A14E5284026F303C8AF8D3457DFA9920F19 (void);
// 0x0000001D AxisOptions Joystick::get_AxisOptions()
extern void Joystick_get_AxisOptions_mA74F5FEE31C158E5179F0B108272ED28A661E388 (void);
// 0x0000001E System.Void Joystick::set_AxisOptions(AxisOptions)
extern void Joystick_set_AxisOptions_m541692280806ECF76F7C2C710973AF9D8AB334C6 (void);
// 0x0000001F System.Boolean Joystick::get_SnapX()
extern void Joystick_get_SnapX_m51CAFDCC399606BA82986908700AAA45668A0F40 (void);
// 0x00000020 System.Void Joystick::set_SnapX(System.Boolean)
extern void Joystick_set_SnapX_mB2090989F6AC933B30823751D74E799FC8D9B87A (void);
// 0x00000021 System.Boolean Joystick::get_SnapY()
extern void Joystick_get_SnapY_m35AFC1AD1DF5EDE5FCE8BAFEBE91AD51D7451613 (void);
// 0x00000022 System.Void Joystick::set_SnapY(System.Boolean)
extern void Joystick_set_SnapY_m7419D5EB972285A9E5E446CD668BEC266D11CD86 (void);
// 0x00000023 System.Void Joystick::Start()
extern void Joystick_Start_m5E46F090910AB69BE9838BFDB91A4F6E6934480C (void);
// 0x00000024 System.Void Joystick::OnEnable()
extern void Joystick_OnEnable_m9DAE2F453BA576C3E2248CEF1990F60D3E6A5970 (void);
// 0x00000025 System.Void Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerDown_mF176903D532D9129C90BBBD00FD7714BA3A0D8E6 (void);
// 0x00000026 System.Void Joystick::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnBeginDrag_m61F6E3409DA1D701467A10355CD99D20415E5387 (void);
// 0x00000027 System.Void Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnDrag_m39E69636AEDC0E471EAD1371A99F4694ECDBA1E9 (void);
// 0x00000028 System.Void Joystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void Joystick_HandleInput_m15A4E86369A1AF0A4A5727DEC0FD93212A99901C (void);
// 0x00000029 System.Void Joystick::FormatInput()
extern void Joystick_FormatInput_mDDF7AF40138CF227F0106811C8749180FBF45342 (void);
// 0x0000002A System.Single Joystick::SnapFloat(System.Single,AxisOptions)
extern void Joystick_SnapFloat_mADE5AF21C67A2298A08CD12F9A8ED73AFA866987 (void);
// 0x0000002B System.Void Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerUp_mEDED4DA77D954CBAC11CF82B57AB7A4DBFCDE22C (void);
// 0x0000002C UnityEngine.Vector2 Joystick::ScreenPointToAnchoredPosition(UnityEngine.Vector2)
extern void Joystick_ScreenPointToAnchoredPosition_mC1EB7560E844BF682674E4E7BD640604BC12B024 (void);
// 0x0000002D System.Void Joystick::.ctor()
extern void Joystick__ctor_m9BBE494CA4714F24227F33CB54C10B4DA78BF06B (void);
// 0x0000002E System.Single DynamicJoystick::get_MoveThreshold()
extern void DynamicJoystick_get_MoveThreshold_m16C670C1DA0A45E83F6F87C4304F459EDDEEDD5A (void);
// 0x0000002F System.Void DynamicJoystick::set_MoveThreshold(System.Single)
extern void DynamicJoystick_set_MoveThreshold_m000C1AE325C0B9C33172E4202F2AFB59820517F9 (void);
// 0x00000030 System.Void DynamicJoystick::Start()
extern void DynamicJoystick_Start_mFE16C6CE0B753F08E79A2AEC75782DEEF3B96F72 (void);
// 0x00000031 System.Void DynamicJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void DynamicJoystick_OnPointerDown_mBFA3026A0DA4A6B53C0E747A1E892CBC7F43E136 (void);
// 0x00000032 System.Void DynamicJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void DynamicJoystick_OnPointerUp_m10389907A9D3362F6B75FDC5F35AF37A5DD5AE7C (void);
// 0x00000033 System.Void DynamicJoystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void DynamicJoystick_HandleInput_m3F157F4825BE6682228C8E135581C6404D268506 (void);
// 0x00000034 System.Void DynamicJoystick::.ctor()
extern void DynamicJoystick__ctor_m9DDA6263314BD7B97679DF14A4664358BD3E58CB (void);
// 0x00000035 System.Void FixedJoystick::.ctor()
extern void FixedJoystick__ctor_m8C8BB74E5EA8CA2C3DD2AE084301EC91F519AD24 (void);
// 0x00000036 System.Void FloatingJoystick::Start()
extern void FloatingJoystick_Start_mB22059CD82AF77A8F94AC72E81A8BAE969399E81 (void);
// 0x00000037 System.Void FloatingJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void FloatingJoystick_OnPointerDown_mFE5B4F54D5BBCA72F9729AB64765F558FA5C7A54 (void);
// 0x00000038 System.Void FloatingJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void FloatingJoystick_OnPointerUp_m80ABA9C914E1953F91DBF74853CE84879352280D (void);
// 0x00000039 System.Void FloatingJoystick::.ctor()
extern void FloatingJoystick__ctor_m6B72425996D46B025F9E9D22121E9D01BEC6BD3C (void);
// 0x0000003A System.Single VariableJoystick::get_MoveThreshold()
extern void VariableJoystick_get_MoveThreshold_m8C9D3A63DB3B6CF1F0139C3504EC2E7AC4E7CF99 (void);
// 0x0000003B System.Void VariableJoystick::set_MoveThreshold(System.Single)
extern void VariableJoystick_set_MoveThreshold_m23DC4187B405EB690D297379E2113568B40C705A (void);
// 0x0000003C System.Void VariableJoystick::SetMode(JoystickType)
extern void VariableJoystick_SetMode_mB9D0B9B6E2E4F431E36AED6F5AC989305ADDB1EE (void);
// 0x0000003D System.Void VariableJoystick::Start()
extern void VariableJoystick_Start_m21743760641EA0317ACCD95949B99825446FE74D (void);
// 0x0000003E System.Void VariableJoystick::OnDisable()
extern void VariableJoystick_OnDisable_m28E1A01EC1C3E09951F78D24CEA99EEAED625E17 (void);
// 0x0000003F System.Void VariableJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void VariableJoystick_OnPointerDown_m8ABE5C8EFBC8DB3A2EE135FFF3C0F67C533AF4B5 (void);
// 0x00000040 System.Void VariableJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void VariableJoystick_OnPointerUp_m65296D82A6C2E1BDC2D622B9C922FAE8E4544526 (void);
// 0x00000041 System.Void VariableJoystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void VariableJoystick_HandleInput_mD1BCF9A068737A9C057EE8CEB7E6DEB682CC03AB (void);
// 0x00000042 System.Void VariableJoystick::.ctor()
extern void VariableJoystick__ctor_m6C7B41973BE20A94F16DB5DCC9AA805C5D8DF852 (void);
// 0x00000043 GoogleMobileAds.Common.Mediation.AppLovin.IAppLovinClient GoogleMobileAds.Mediation.AppLovinClientFactory::AppLovinInstance()
extern void AppLovinClientFactory_AppLovinInstance_mE49124170E0B40B5A7DA90F722F4EDF3D95E2B0B (void);
// 0x00000044 System.Void GoogleMobileAds.Mediation.AppLovinClientFactory::.ctor()
extern void AppLovinClientFactory__ctor_mE4BF0A3809E04795759A668A1525FB5BE17308DE (void);
// 0x00000045 System.Void GoogleMobileAds.Android.Mediation.AppLovin.AppLovinClient::.ctor()
extern void AppLovinClient__ctor_m1F78AD3047E4A1BC42B0D09885C9DFBA745A4F4A (void);
// 0x00000046 GoogleMobileAds.Android.Mediation.AppLovin.AppLovinClient GoogleMobileAds.Android.Mediation.AppLovin.AppLovinClient::get_Instance()
extern void AppLovinClient_get_Instance_m299C432C799DE4D562F54BF166C7D5C7C6E8F349 (void);
// 0x00000047 System.Void GoogleMobileAds.Android.Mediation.AppLovin.AppLovinClient::Initialize()
extern void AppLovinClient_Initialize_mA295EBBAE9C66A82395CB050D491F550E324C10A (void);
// 0x00000048 System.Void GoogleMobileAds.Android.Mediation.AppLovin.AppLovinClient::SetHasUserConsent(System.Boolean)
extern void AppLovinClient_SetHasUserConsent_m63C9B4AC68146CFD047A6C894C0897A0D0BFA2D8 (void);
// 0x00000049 System.Void GoogleMobileAds.Android.Mediation.AppLovin.AppLovinClient::SetIsAgeRestrictedUser(System.Boolean)
extern void AppLovinClient_SetIsAgeRestrictedUser_mDDABEF99C2B1E351CA04D19BAE18A6AC850E7F9A (void);
// 0x0000004A System.Void GoogleMobileAds.Android.Mediation.AppLovin.AppLovinClient::.cctor()
extern void AppLovinClient__cctor_m79901FDE51452FB2DF90D4BD26DD8F26B9676D9B (void);
// 0x0000004B System.Void GoogleMobileAds.Common.Mediation.AppLovin.DummyClient::.ctor()
extern void DummyClient__ctor_m4B3AAD2D317866316100150FE739319A27219D84 (void);
// 0x0000004C System.Void GoogleMobileAds.Common.Mediation.AppLovin.DummyClient::Initialize()
extern void DummyClient_Initialize_m40E5DE4B66A169169654DCC99E5E1467063DF2EB (void);
// 0x0000004D System.Void GoogleMobileAds.Common.Mediation.AppLovin.DummyClient::SetHasUserConsent(System.Boolean)
extern void DummyClient_SetHasUserConsent_m4DD67FB305D5A20966A1B7F541C2B7AD844B85E3 (void);
// 0x0000004E System.Void GoogleMobileAds.Common.Mediation.AppLovin.DummyClient::SetIsAgeRestrictedUser(System.Boolean)
extern void DummyClient_SetIsAgeRestrictedUser_m112FF92EC432AA1C7DD2FC1FE2FF9754DE23F3D4 (void);
// 0x0000004F System.Void GoogleMobileAds.Common.Mediation.AppLovin.IAppLovinClient::Initialize()
// 0x00000050 System.Void GoogleMobileAds.Common.Mediation.AppLovin.IAppLovinClient::SetHasUserConsent(System.Boolean)
// 0x00000051 System.Void GoogleMobileAds.Common.Mediation.AppLovin.IAppLovinClient::SetIsAgeRestrictedUser(System.Boolean)
// 0x00000052 System.Void GoogleMobileAds.Api.Mediation.AppLovin.AppLovin::Initialize()
extern void AppLovin_Initialize_mCE699071BCBC1C6F8BC78F0DF444C106AC2A0121 (void);
// 0x00000053 System.Void GoogleMobileAds.Api.Mediation.AppLovin.AppLovin::SetHasUserConsent(System.Boolean)
extern void AppLovin_SetHasUserConsent_m46E30DA73CF2EE6864A308F43EED8A32D53743E0 (void);
// 0x00000054 System.Void GoogleMobileAds.Api.Mediation.AppLovin.AppLovin::SetIsAgeRestrictedUser(System.Boolean)
extern void AppLovin_SetIsAgeRestrictedUser_m9A370550AA1664C4B7683C13EE2FB3CB65E64323 (void);
// 0x00000055 GoogleMobileAds.Common.Mediation.AppLovin.IAppLovinClient GoogleMobileAds.Api.Mediation.AppLovin.AppLovin::GetAppLovinClient()
extern void AppLovin_GetAppLovinClient_m567E3A189A980D79B59337FB2859CDE43953EA0B (void);
// 0x00000056 System.Void GoogleMobileAds.Api.Mediation.AppLovin.AppLovin::.ctor()
extern void AppLovin__ctor_mEFDBAF3971E66989A5CF3031985C031EE32296B6 (void);
// 0x00000057 System.Void GoogleMobileAds.Api.Mediation.AppLovin.AppLovin::.cctor()
extern void AppLovin__cctor_m26A8AE7E9F360DC5C80644CDFA79C6DE20BED908 (void);
// 0x00000058 System.Int32 Facebook.Unity.Example.ConsoleBase::get_ButtonHeight()
extern void ConsoleBase_get_ButtonHeight_mF81259A974C9E3C4B24AFB8D95A18F0A03EE3396 (void);
// 0x00000059 System.Int32 Facebook.Unity.Example.ConsoleBase::get_MainWindowWidth()
extern void ConsoleBase_get_MainWindowWidth_m9D6DB2FC65DC5D7FA781E5CF2FDB945C760D3C1E (void);
// 0x0000005A System.Int32 Facebook.Unity.Example.ConsoleBase::get_MainWindowFullWidth()
extern void ConsoleBase_get_MainWindowFullWidth_m7238BF9690E4397727DF038E6B185AFC2F7A1BF8 (void);
// 0x0000005B System.Int32 Facebook.Unity.Example.ConsoleBase::get_MarginFix()
extern void ConsoleBase_get_MarginFix_m286762B25357CC839197F05CC557CCFA376B7DB3 (void);
// 0x0000005C System.Collections.Generic.Stack`1<System.String> Facebook.Unity.Example.ConsoleBase::get_MenuStack()
extern void ConsoleBase_get_MenuStack_mA1E88A5414EA46C243855D30F28FB58F507F7597 (void);
// 0x0000005D System.Void Facebook.Unity.Example.ConsoleBase::set_MenuStack(System.Collections.Generic.Stack`1<System.String>)
extern void ConsoleBase_set_MenuStack_mDC687D0701EEDAD9D55A44AE9449326C5E6ADFDC (void);
// 0x0000005E System.String Facebook.Unity.Example.ConsoleBase::get_Status()
extern void ConsoleBase_get_Status_m71AFE45D2F68B01846188F212D65923B5B341728 (void);
// 0x0000005F System.Void Facebook.Unity.Example.ConsoleBase::set_Status(System.String)
extern void ConsoleBase_set_Status_mFD36D24FF2D3D6554F11859251BB325B750776D2 (void);
// 0x00000060 UnityEngine.Texture2D Facebook.Unity.Example.ConsoleBase::get_LastResponseTexture()
extern void ConsoleBase_get_LastResponseTexture_m02F8A694917953E5B184AC489DBE600009451DCE (void);
// 0x00000061 System.Void Facebook.Unity.Example.ConsoleBase::set_LastResponseTexture(UnityEngine.Texture2D)
extern void ConsoleBase_set_LastResponseTexture_m8ED57EA554542EB683B1484F5DDE13ED983A146D (void);
// 0x00000062 System.String Facebook.Unity.Example.ConsoleBase::get_LastResponse()
extern void ConsoleBase_get_LastResponse_m5BCC50E8189B285B0AE6244435A8A9F6E3D4CE85 (void);
// 0x00000063 System.Void Facebook.Unity.Example.ConsoleBase::set_LastResponse(System.String)
extern void ConsoleBase_set_LastResponse_m847D45A344585AFCFCE231CF8993E2A750C76547 (void);
// 0x00000064 UnityEngine.Vector2 Facebook.Unity.Example.ConsoleBase::get_ScrollPosition()
extern void ConsoleBase_get_ScrollPosition_m489494F56C1E8E36B530B9CEBF1973640D39A47D (void);
// 0x00000065 System.Void Facebook.Unity.Example.ConsoleBase::set_ScrollPosition(UnityEngine.Vector2)
extern void ConsoleBase_set_ScrollPosition_m7C2D74B4FF2432875A98B2F5A70F4D02494A9C84 (void);
// 0x00000066 System.Single Facebook.Unity.Example.ConsoleBase::get_ScaleFactor()
extern void ConsoleBase_get_ScaleFactor_m54D2E9E038B321F36372AB6284799232475B37F2 (void);
// 0x00000067 System.Int32 Facebook.Unity.Example.ConsoleBase::get_FontSize()
extern void ConsoleBase_get_FontSize_mBA25F85EA4D6916062FE3D049BDE8BF618D5189F (void);
// 0x00000068 UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_TextStyle()
extern void ConsoleBase_get_TextStyle_m08AB60C7174363AE850D9F15A5C606E73D766704 (void);
// 0x00000069 UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_ButtonStyle()
extern void ConsoleBase_get_ButtonStyle_m28EF2E33654CE9FC915EBECA78D93778517322E2 (void);
// 0x0000006A UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_TextInputStyle()
extern void ConsoleBase_get_TextInputStyle_m99CA0D3F64E54CCF5DED958ACB141D07EEFB0C13 (void);
// 0x0000006B UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_LabelStyle()
extern void ConsoleBase_get_LabelStyle_m97006D9788464D58A9BA885AD2348D784699C346 (void);
// 0x0000006C System.Void Facebook.Unity.Example.ConsoleBase::Awake()
extern void ConsoleBase_Awake_m8068C221C280D202F4051E0CBFBF91C77568CCD7 (void);
// 0x0000006D System.Boolean Facebook.Unity.Example.ConsoleBase::Button(System.String)
extern void ConsoleBase_Button_m8787498ECF036B9B3B5EB6B2FF49AFA154282D10 (void);
// 0x0000006E System.Void Facebook.Unity.Example.ConsoleBase::LabelAndTextField(System.String,System.String&)
extern void ConsoleBase_LabelAndTextField_m72E1BBF75934CE747195CB87737A2D7EFC503A8A (void);
// 0x0000006F System.Boolean Facebook.Unity.Example.ConsoleBase::IsHorizontalLayout()
extern void ConsoleBase_IsHorizontalLayout_m90873C8D21DF82E9733E95CD43BFB72A54912B1D (void);
// 0x00000070 System.Void Facebook.Unity.Example.ConsoleBase::SwitchMenu(System.Type)
extern void ConsoleBase_SwitchMenu_mA4A6A237A2D4EE813268204D38391A9D2AB7BC34 (void);
// 0x00000071 System.Void Facebook.Unity.Example.ConsoleBase::GoBack()
extern void ConsoleBase_GoBack_mA682B2317560D91C931680AA95C1AD390611304A (void);
// 0x00000072 System.Void Facebook.Unity.Example.ConsoleBase::.ctor()
extern void ConsoleBase__ctor_m2295736CFFF716F434C20994E95820657BA9C751 (void);
// 0x00000073 System.Void Facebook.Unity.Example.ConsoleBase::.cctor()
extern void ConsoleBase__cctor_mE537FEF66506B08BAF97CD2B40D29210C530F6BB (void);
// 0x00000074 System.Void Facebook.Unity.Example.LogView::AddLog(System.String)
extern void LogView_AddLog_mF87B865A91948F6D3F59502861BF67B6354B4A0E (void);
// 0x00000075 System.Void Facebook.Unity.Example.LogView::OnGUI()
extern void LogView_OnGUI_m6B78F78EC906C89DA0BE45F5D3C5571126542376 (void);
// 0x00000076 System.Void Facebook.Unity.Example.LogView::.ctor()
extern void LogView__ctor_m23A534F52CC925EE10260A5EC32DECBDADE2F35D (void);
// 0x00000077 System.Void Facebook.Unity.Example.LogView::.cctor()
extern void LogView__cctor_m112A3FBD481C9FC1A99F57426E9D79AC9E36F226 (void);
// 0x00000078 System.Void Facebook.Unity.Example.MenuBase::GetGui()
// 0x00000079 System.Boolean Facebook.Unity.Example.MenuBase::ShowDialogModeSelector()
extern void MenuBase_ShowDialogModeSelector_mB52DA9E57B0AF80ED97B7951B1815424F939A5D6 (void);
// 0x0000007A System.Boolean Facebook.Unity.Example.MenuBase::ShowBackButton()
extern void MenuBase_ShowBackButton_m33BAD66D85A5FA9E564FF8CF85E8920842238BAE (void);
// 0x0000007B System.Void Facebook.Unity.Example.MenuBase::HandleResult(Facebook.Unity.IResult)
extern void MenuBase_HandleResult_m0CB446ED4B8BDA605B140E61A4A1C6B442765E65 (void);
// 0x0000007C System.Void Facebook.Unity.Example.MenuBase::HandleLimitedLoginResult(Facebook.Unity.IResult)
extern void MenuBase_HandleLimitedLoginResult_m5A42F3F12FA695D59588DCC12004870D55C4ECB9 (void);
// 0x0000007D System.Void Facebook.Unity.Example.MenuBase::OnGUI()
extern void MenuBase_OnGUI_m2EDD4E22A69914B99CBDF039142129959C363D97 (void);
// 0x0000007E System.Void Facebook.Unity.Example.MenuBase::AddStatus()
extern void MenuBase_AddStatus_mA9B44C51B6D228B6EB901EA6FDEC17B6524FF98E (void);
// 0x0000007F System.Void Facebook.Unity.Example.MenuBase::AddBackButton()
extern void MenuBase_AddBackButton_m546C0ED7A454CD1CC80C9BCE27E56D924E6FCDC6 (void);
// 0x00000080 System.Void Facebook.Unity.Example.MenuBase::AddLogButton()
extern void MenuBase_AddLogButton_mDCB6003139C36CE1485D8F6C70C23BF7A13FB812 (void);
// 0x00000081 System.Void Facebook.Unity.Example.MenuBase::AddDialogModeButtons()
extern void MenuBase_AddDialogModeButtons_mCEA0B4B8B4C88A0A8F9A7B4737B4595B0FE5B208 (void);
// 0x00000082 System.Void Facebook.Unity.Example.MenuBase::AddDialogModeButton(Facebook.Unity.ShareDialogMode)
extern void MenuBase_AddDialogModeButton_m8F24FD4F294616E73BEC80B082FADB4D73BF1AC2 (void);
// 0x00000083 System.Void Facebook.Unity.Example.MenuBase::.ctor()
extern void MenuBase__ctor_mD1A153F6EBCE35B5990669A20787B391A5DEC0E2 (void);
// 0x00000084 System.Void Facebook.Unity.Example.AccessTokenMenu::GetGui()
extern void AccessTokenMenu_GetGui_mAFFC7AADE6A3C5A549A15922B3933F868690FBB7 (void);
// 0x00000085 System.Void Facebook.Unity.Example.AccessTokenMenu::.ctor()
extern void AccessTokenMenu__ctor_mA506557143C1CEB6F2D52ED9EECC44FB9058804C (void);
// 0x00000086 System.Void Facebook.Unity.Example.AppEvents::GetGui()
extern void AppEvents_GetGui_m4D6548C60C8A0B6B81EB16E4F20416013669AE09 (void);
// 0x00000087 System.Void Facebook.Unity.Example.AppEvents::.ctor()
extern void AppEvents__ctor_m633F317D3E69F6B62705498B7790C36075F3C09B (void);
// 0x00000088 System.Void Facebook.Unity.Example.AppLinks::GetGui()
extern void AppLinks_GetGui_mEF6BABE28307C19F823198102718BF2DCBE91FC2 (void);
// 0x00000089 System.Void Facebook.Unity.Example.AppLinks::.ctor()
extern void AppLinks__ctor_m2408DE5760F132BFD7437824410EE9FA14FE7F02 (void);
// 0x0000008A System.Void Facebook.Unity.Example.AppRequests::GetGui()
extern void AppRequests_GetGui_mF21A92393C08E39EA4404B8AD9DE1076A42A7060 (void);
// 0x0000008B System.Nullable`1<Facebook.Unity.OGActionType> Facebook.Unity.Example.AppRequests::GetSelectedOGActionType()
extern void AppRequests_GetSelectedOGActionType_m350F98087C7EB39851FC4562007A9EC26261BA1A (void);
// 0x0000008C System.Void Facebook.Unity.Example.AppRequests::.ctor()
extern void AppRequests__ctor_m22F0E79DD38013C0846E20AC2E9F96FCCE6356D8 (void);
// 0x0000008D System.Boolean Facebook.Unity.Example.DialogShare::ShowDialogModeSelector()
extern void DialogShare_ShowDialogModeSelector_mCF0A9C2537886392CC684FBD4939F14C6B8A1F87 (void);
// 0x0000008E System.Void Facebook.Unity.Example.DialogShare::GetGui()
extern void DialogShare_GetGui_m5A303622B0B9250A1C0E9570683CE6F5DC894783 (void);
// 0x0000008F System.Void Facebook.Unity.Example.DialogShare::.ctor()
extern void DialogShare__ctor_m18343E0A2E25DD13D50F65E99F855EE5CBB4A44F (void);
// 0x00000090 System.Void Facebook.Unity.Example.GraphRequest::GetGui()
extern void GraphRequest_GetGui_m04AC165B02447897B6210A4EEA01341C54A18E7E (void);
// 0x00000091 System.Void Facebook.Unity.Example.GraphRequest::ProfilePhotoCallback(Facebook.Unity.IGraphResult)
extern void GraphRequest_ProfilePhotoCallback_m377E44982A0745303405A4B2792BEAEA617DC962 (void);
// 0x00000092 System.Collections.IEnumerator Facebook.Unity.Example.GraphRequest::TakeScreenshot()
extern void GraphRequest_TakeScreenshot_mB6D07D0BD896A3FDA1B3A0CECD1F41A82BB71153 (void);
// 0x00000093 System.Void Facebook.Unity.Example.GraphRequest::.ctor()
extern void GraphRequest__ctor_m1893756902051EDCA6A648EFF3F8C79482D72F17 (void);
// 0x00000094 System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::.ctor(System.Int32)
extern void U3CTakeScreenshotU3Ed__4__ctor_mC5022BC2D6F58E67E8D34863FDDD01DB102192A9 (void);
// 0x00000095 System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::System.IDisposable.Dispose()
extern void U3CTakeScreenshotU3Ed__4_System_IDisposable_Dispose_m024CE39BF1690FA910DDA8A774BFC512D437756F (void);
// 0x00000096 System.Boolean Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::MoveNext()
extern void U3CTakeScreenshotU3Ed__4_MoveNext_m1BC8573BD27ED010119E112AA3BE6CA166C05D31 (void);
// 0x00000097 System.Object Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTakeScreenshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF4F64034B62BD451ED5DB8EF972FC4B40806FC23 (void);
// 0x00000098 System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::System.Collections.IEnumerator.Reset()
extern void U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_Reset_m99D229E478D52F853F19ADCBCFFDAF25C148D899 (void);
// 0x00000099 System.Object Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_get_Current_m13E1B2CBD35D3D58C012334F0A4FD17B3930C030 (void);
// 0x0000009A System.Boolean Facebook.Unity.Example.MainMenu::ShowBackButton()
extern void MainMenu_ShowBackButton_m0AA293F10C340E46851503EF2B29E235FE61BBE8 (void);
// 0x0000009B System.Void Facebook.Unity.Example.MainMenu::GetGui()
extern void MainMenu_GetGui_mBEAC140C4E2DC7BEE3B2F1FA3AA744770B6BE1BF (void);
// 0x0000009C System.Void Facebook.Unity.Example.MainMenu::CallFBLogin(Facebook.Unity.LoginTracking,Facebook.Unity.Example.MainMenu/Scope)
extern void MainMenu_CallFBLogin_m8C94D27865F5C4197AF2C2A90D5E60BBA9D08269 (void);
// 0x0000009D System.Void Facebook.Unity.Example.MainMenu::CallFBLoginForPublish()
extern void MainMenu_CallFBLoginForPublish_m5519242C5D4241EF52CA37F78A5BCEEF18973A19 (void);
// 0x0000009E System.Void Facebook.Unity.Example.MainMenu::CallFBLogout()
extern void MainMenu_CallFBLogout_mEE96298E9614D3F79009BB034E4DB1047A7855C3 (void);
// 0x0000009F System.Void Facebook.Unity.Example.MainMenu::OnInitComplete()
extern void MainMenu_OnInitComplete_m36800A94B891CDDCC9C4DAED0CC5639190C2E207 (void);
// 0x000000A0 System.Void Facebook.Unity.Example.MainMenu::OnHideUnity(System.Boolean)
extern void MainMenu_OnHideUnity_mDE312A6C6A2FE6F756E72A64388E31F5BE626D84 (void);
// 0x000000A1 System.Void Facebook.Unity.Example.MainMenu::.ctor()
extern void MainMenu__ctor_m15C7560C4A0296BB1D433FA3D8B1C82568B7179F (void);
// 0x000000A2 System.Void Facebook.Unity.Example.Pay::GetGui()
extern void Pay_GetGui_m38BDD0E07950F97403560C5AAD4D35F4ABE590AA (void);
// 0x000000A3 System.Void Facebook.Unity.Example.Pay::CallFBPay()
extern void Pay_CallFBPay_m6C1243E4AA248F715366AA5E8996D25431AA2A58 (void);
// 0x000000A4 System.Void Facebook.Unity.Example.Pay::.ctor()
extern void Pay__ctor_mF524FF0E64FFA62C166094B9CFE640D276161A26 (void);
// 0x000000A5 System.Boolean BearMatch.BotController::get_IsDied()
extern void BotController_get_IsDied_m63415A22D604ABA1F5B2968E873A62C650FEF7F1 (void);
// 0x000000A6 System.Void BearMatch.BotController::set_IsDied(System.Boolean)
extern void BotController_set_IsDied_m58AB128CD3900EE06594A77EF3DCB8DC4DD9882C (void);
// 0x000000A7 System.Void BearMatch.BotController::Start()
extern void BotController_Start_mECFD2A31A4CF7B879A4D78BA6B0A3190D955EC55 (void);
// 0x000000A8 System.Void BearMatch.BotController::OnDisable()
extern void BotController_OnDisable_m1BE5DD7FE3B0F170AE190439A1FD883906DDF706 (void);
// 0x000000A9 System.Void BearMatch.BotController::OnValidate()
extern void BotController_OnValidate_m612D3A047D552240918EC20DD4425CAB0C23A9A9 (void);
// 0x000000AA System.Void BearMatch.BotController::InitializeNewGame(UnityEngine.Vector3)
extern void BotController_InitializeNewGame_m926980AA77770138EB5F4CBD8A71EADB160593E8 (void);
// 0x000000AB System.Void BearMatch.BotController::StartRunToTargetPoint()
extern void BotController_StartRunToTargetPoint_m851299EAB633424DCFB90AD045DBDB6C949820E4 (void);
// 0x000000AC System.Void BearMatch.BotController::RandomTargetPosition()
extern void BotController_RandomTargetPosition_m05F8B41F1960585DEC4367BBFC970EB1F3351E35 (void);
// 0x000000AD System.Void BearMatch.BotController::Update()
extern void BotController_Update_mED0D75A52B193110501D81E40611A53D3F979BDF (void);
// 0x000000AE System.Void BearMatch.BotController::TriggerUnderground()
extern void BotController_TriggerUnderground_mC7EBDDC4AB48468C70FD54D8B1A9437BC673F0F3 (void);
// 0x000000AF System.Void BearMatch.BotController::RunToGridResult(UnityEngine.Vector3)
extern void BotController_RunToGridResult_mA77C9006DE3BCB45361AEC623C72C4A804E8E2EC (void);
// 0x000000B0 System.Void BearMatch.BotController::OnPlayerDie(System.Object)
extern void BotController_OnPlayerDie_mE671BCCB097FCACEE46606A630E871120268EDE5 (void);
// 0x000000B1 System.Void BearMatch.BotController::PushBack(System.Single,System.Single)
extern void BotController_PushBack_mDA610250EBA5609413A8E25BF34F73A79BD09F9B (void);
// 0x000000B2 System.Collections.IEnumerator BearMatch.BotController::IEPushBack(UnityEngine.Vector3)
extern void BotController_IEPushBack_m3AF2D1848E79185FFDDAC3D41D2A56B392387D61 (void);
// 0x000000B3 System.Void BearMatch.BotController::ToggleRagdoll(System.Boolean)
extern void BotController_ToggleRagdoll_mF3EB813D81F4394A366CDBB64C452F4E36A0F18B (void);
// 0x000000B4 System.Void BearMatch.BotController::.ctor()
extern void BotController__ctor_m39D3F896495D9FEF2BAD2C8B2B33B974CB2A3EAB (void);
// 0x000000B5 System.Void BearMatch.BotController/<IEPushBack>d__36::.ctor(System.Int32)
extern void U3CIEPushBackU3Ed__36__ctor_mEB5D3F0500DE66401C18D8FA98541EE13D880EC4 (void);
// 0x000000B6 System.Void BearMatch.BotController/<IEPushBack>d__36::System.IDisposable.Dispose()
extern void U3CIEPushBackU3Ed__36_System_IDisposable_Dispose_m70E4C09F1848215B1B0D4EFEA8DF4A8DF07648B7 (void);
// 0x000000B7 System.Boolean BearMatch.BotController/<IEPushBack>d__36::MoveNext()
extern void U3CIEPushBackU3Ed__36_MoveNext_m0F95494B9A3A0561A7CB870F69B55D61E7084465 (void);
// 0x000000B8 System.Object BearMatch.BotController/<IEPushBack>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIEPushBackU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87DF9A1E775B3564035D7623D59C88419CD9F687 (void);
// 0x000000B9 System.Void BearMatch.BotController/<IEPushBack>d__36::System.Collections.IEnumerator.Reset()
extern void U3CIEPushBackU3Ed__36_System_Collections_IEnumerator_Reset_m0E82361595DE77C97B44EAD650917D238AD6D5E9 (void);
// 0x000000BA System.Object BearMatch.BotController/<IEPushBack>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CIEPushBackU3Ed__36_System_Collections_IEnumerator_get_Current_m3B511B8033F95E603EF7A93194B00E8D7F8BB27A (void);
// 0x000000BB BearMatch.CameraController BearMatch.CameraController::get_Instance()
extern void CameraController_get_Instance_mFD916D72E823F41D1BB917486DE0DA7E3601AFBB (void);
// 0x000000BC System.Void BearMatch.CameraController::set_Instance(BearMatch.CameraController)
extern void CameraController_set_Instance_m8C6DD53EB4753EA7B8E1A51FB92F7A2D5E575E52 (void);
// 0x000000BD System.Void BearMatch.CameraController::Awake()
extern void CameraController_Awake_m228DFEE758BBA7002ACB717A2DFED29959B64943 (void);
// 0x000000BE System.Void BearMatch.CameraController::Start()
extern void CameraController_Start_m1711365E7028C2B26425FCCB569E1527B83E8513 (void);
// 0x000000BF System.Void BearMatch.CameraController::OnDestroy()
extern void CameraController_OnDestroy_mF4CA27701B6AF26D2EF4F6327F30A84862F018C7 (void);
// 0x000000C0 System.Void BearMatch.CameraController::InitializeNewGame()
extern void CameraController_InitializeNewGame_mE2510481FB56966A564EDAD54C1A9064AE58B3C2 (void);
// 0x000000C1 System.Void BearMatch.CameraController::StartNewLevel()
extern void CameraController_StartNewLevel_m02D6901CCFD058C0CA57B7AB881CFA9E90D2D41E (void);
// 0x000000C2 System.Void BearMatch.CameraController::LateUpdate()
extern void CameraController_LateUpdate_mE04957AF2CA40A36A4AB546E82EAB5E3F491DCE2 (void);
// 0x000000C3 System.Void BearMatch.CameraController::OnPlayerVictory(System.Object)
extern void CameraController_OnPlayerVictory_m5AE2D204ED9D2D03E902535B19B69032531E08D1 (void);
// 0x000000C4 System.Void BearMatch.CameraController::.ctor()
extern void CameraController__ctor_m3E4D0BC1FD1534718F6D4AD1FBADF0A24CD1ABBC (void);
// 0x000000C5 BearMatch.AdsManager BearMatch.AdsManager::get_Instance()
extern void AdsManager_get_Instance_m89C7BFBC5CE29F77AB53A243F1BA80A6ABB636BD (void);
// 0x000000C6 System.Void BearMatch.AdsManager::set_Instance(BearMatch.AdsManager)
extern void AdsManager_set_Instance_mC7ECC2C57232DB9AAFD3E2DE6D87429DB4DF9A34 (void);
// 0x000000C7 System.Boolean BearMatch.AdsManager::get_IsBannerLoaded()
extern void AdsManager_get_IsBannerLoaded_mC52A5A2A8217ECE54B22ECD045BD6ADAFF469A7D (void);
// 0x000000C8 System.Void BearMatch.AdsManager::set_IsBannerLoaded(System.Boolean)
extern void AdsManager_set_IsBannerLoaded_mA04DB9C019C849AB487E11E0465F1BC9986BD815 (void);
// 0x000000C9 System.Boolean BearMatch.AdsManager::get_IsInterstitialLoaded()
extern void AdsManager_get_IsInterstitialLoaded_m8AFF32ECA1FEC8AD5B94D250489A4340FE828C08 (void);
// 0x000000CA System.Void BearMatch.AdsManager::set_IsInterstitialLoaded(System.Boolean)
extern void AdsManager_set_IsInterstitialLoaded_m0CC59AEB2A5785CDDFDA39E6409D236E4E0DA976 (void);
// 0x000000CB System.Boolean BearMatch.AdsManager::get_IsRewardVideoLoaded()
extern void AdsManager_get_IsRewardVideoLoaded_mC22ECF4F9BE721BDCCFFA2E8594F529D1DDCF4F2 (void);
// 0x000000CC System.Void BearMatch.AdsManager::set_IsRewardVideoLoaded(System.Boolean)
extern void AdsManager_set_IsRewardVideoLoaded_m7ACD487A9E0EEEB3682D64A5A2EFE699D0DCC2F8 (void);
// 0x000000CD System.Void BearMatch.AdsManager::Awake()
extern void AdsManager_Awake_mF8BF8E9CA2461217CE98A394093C8C7644321843 (void);
// 0x000000CE System.Void BearMatch.AdsManager::Start()
extern void AdsManager_Start_m26609DE96EE4F19DE80B214DF00308F998793585 (void);
// 0x000000CF System.Void BearMatch.AdsManager::InitMax()
extern void AdsManager_InitMax_m8D467ADB40AC0CE9D0CAD61C5BF0DF4C3DCD4DB9 (void);
// 0x000000D0 System.Void BearMatch.AdsManager::LoadAds()
extern void AdsManager_LoadAds_mA1C70597CAC67BAC92801EB50DCDB3270921FA50 (void);
// 0x000000D1 System.Void BearMatch.AdsManager::OnBoughtNoAds(System.Object)
extern void AdsManager_OnBoughtNoAds_mF719B5A3A042512FE3BCFB39B0C35A44916A88E3 (void);
// 0x000000D2 System.Void BearMatch.AdsManager::RequestBanner()
extern void AdsManager_RequestBanner_mA8C7E65E97C95169E73E5CA2F03A5FE926BD7631 (void);
// 0x000000D3 System.Void BearMatch.AdsManager::OnBannerAdLoadedEvent(System.String,MaxSdkBase/AdInfo)
extern void AdsManager_OnBannerAdLoadedEvent_mDF31E97ED7A9809CB132BE6ED5066E691BA9E78F (void);
// 0x000000D4 System.Void BearMatch.AdsManager::OnBannerAdFailedEvent(System.String,MaxSdkBase/ErrorInfo)
extern void AdsManager_OnBannerAdFailedEvent_m06C360AF48A1F9413FF92DAA254682D3EC43B01E (void);
// 0x000000D5 System.Void BearMatch.AdsManager::OnBannerAdClickedEvent(System.String,MaxSdkBase/AdInfo)
extern void AdsManager_OnBannerAdClickedEvent_m422D599DBC614E8A2722B15082BA5E3665E87A72 (void);
// 0x000000D6 System.Void BearMatch.AdsManager::OnBannerAdRevenuePaidEvent(System.String,MaxSdkBase/AdInfo)
extern void AdsManager_OnBannerAdRevenuePaidEvent_mD2085C5C50DF3362B0FF03AF15C568DAEB8BFC03 (void);
// 0x000000D7 System.Collections.IEnumerator BearMatch.AdsManager::IEShowBanner()
extern void AdsManager_IEShowBanner_mA46D4B5B1D6CE4CE0F7E87CA34743555D16270EF (void);
// 0x000000D8 System.Void BearMatch.AdsManager::RequestInterstitial()
extern void AdsManager_RequestInterstitial_m495BABEC3EC3CD01333D3A6FD3596F4830D4239B (void);
// 0x000000D9 System.Void BearMatch.AdsManager::LoadInterstitial()
extern void AdsManager_LoadInterstitial_m80E4C77A45F2D9CB1B3247D292D52CE466F8A001 (void);
// 0x000000DA System.Void BearMatch.AdsManager::OnInterstitialLoadedEvent(System.String,MaxSdkBase/AdInfo)
extern void AdsManager_OnInterstitialLoadedEvent_m3C4A4FD6E3C6211D66FB2DE480BF09482C1100AC (void);
// 0x000000DB System.Void BearMatch.AdsManager::InterstitialFailedToDisplayEvent(System.String,MaxSdkBase/ErrorInfo,MaxSdkBase/AdInfo)
extern void AdsManager_InterstitialFailedToDisplayEvent_m9846B122B0E4D869F5C16073F723865E465F6B25 (void);
// 0x000000DC System.Void BearMatch.AdsManager::OnInterstitialFailedEvent(System.String,MaxSdkBase/ErrorInfo)
extern void AdsManager_OnInterstitialFailedEvent_m16DF579ECCC9BD282D850DEED25BEAEF77C60FC7 (void);
// 0x000000DD System.Void BearMatch.AdsManager::OnInterstitialDismissedEvent(System.String,MaxSdkBase/AdInfo)
extern void AdsManager_OnInterstitialDismissedEvent_mF82B9884F8915D4767B5DD957BD98C62F3EC58EB (void);
// 0x000000DE System.Void BearMatch.AdsManager::OnInterstitialClickedEvent(System.String,MaxSdkBase/AdInfo)
extern void AdsManager_OnInterstitialClickedEvent_m1BE81BB0F12297A115BFD71BA5A8BCCA8A4D79D2 (void);
// 0x000000DF System.Void BearMatch.AdsManager::OnInterstitialRevenuePaidEvent(System.String,MaxSdkBase/AdInfo)
extern void AdsManager_OnInterstitialRevenuePaidEvent_mE861343869179709D3F671BBDCE6B497F0E050B3 (void);
// 0x000000E0 System.Void BearMatch.AdsManager::RequestRewardVideo()
extern void AdsManager_RequestRewardVideo_mB021A0C7F69D1B1298171799BEC802AA01D1F3F6 (void);
// 0x000000E1 System.Void BearMatch.AdsManager::OnRewardedAdLoadedEvent(System.String,MaxSdkBase/AdInfo)
extern void AdsManager_OnRewardedAdLoadedEvent_mE60652E69DB32F073156DFBD925DCBA6D5CF7098 (void);
// 0x000000E2 System.Void BearMatch.AdsManager::OnRewardedAdFailedEvent(System.String,MaxSdkBase/ErrorInfo)
extern void AdsManager_OnRewardedAdFailedEvent_m6CF4A8BE667F7ECBCE64F47732D719548CD5100D (void);
// 0x000000E3 System.Void BearMatch.AdsManager::OnRewardedAdFailedToDisplayEvent(System.String,MaxSdkBase/ErrorInfo,MaxSdkBase/AdInfo)
extern void AdsManager_OnRewardedAdFailedToDisplayEvent_mA1405D0D50C49F2A2D4977642A450E2EBB9C793E (void);
// 0x000000E4 System.Void BearMatch.AdsManager::OnRewardedAdDisplayedEvent(System.String,MaxSdkBase/AdInfo)
extern void AdsManager_OnRewardedAdDisplayedEvent_m53F0186B452E9C28E2FCA6DF41425DB72D75A791 (void);
// 0x000000E5 System.Void BearMatch.AdsManager::OnRewardedAdClickedEvent(System.String,MaxSdkBase/AdInfo)
extern void AdsManager_OnRewardedAdClickedEvent_m81ED193C031DC3945996E542A86CCF2AB6CA0FBC (void);
// 0x000000E6 System.Void BearMatch.AdsManager::OnRewardVideoOpened(System.Object,System.EventArgs)
extern void AdsManager_OnRewardVideoOpened_m520618A2E440A4CD0F1695C40718942E5D185DC6 (void);
// 0x000000E7 System.Void BearMatch.AdsManager::OnRewardedAdDismissedEvent(System.String,MaxSdkBase/AdInfo)
extern void AdsManager_OnRewardedAdDismissedEvent_m793AF672292A9B05D3F4F28B4657CA9C20F3277E (void);
// 0x000000E8 System.Void BearMatch.AdsManager::OnRewardedAdReceivedRewardEvent(System.String,MaxSdkBase/Reward,MaxSdkBase/AdInfo)
extern void AdsManager_OnRewardedAdReceivedRewardEvent_m77FC56B5A307E4A4A5554F05AA9BB565A6CD8975 (void);
// 0x000000E9 System.Void BearMatch.AdsManager::OnRewardedAdRevenuePaidEvent(System.String,MaxSdkBase/AdInfo)
extern void AdsManager_OnRewardedAdRevenuePaidEvent_mBC41E4F65C3F586719C203C639A4A41DB42FDD11 (void);
// 0x000000EA System.Collections.IEnumerator BearMatch.AdsManager::IEDoRewardVideoRewardedCallback()
extern void AdsManager_IEDoRewardVideoRewardedCallback_m4D8A05812E25F41280794D5286F5DDCAD50BC2AF (void);
// 0x000000EB System.Collections.IEnumerator BearMatch.AdsManager::IEDoRewardVideoClosedCallback()
extern void AdsManager_IEDoRewardVideoClosedCallback_mCF6608A95685EBE846158817F6AA7161D054C9B5 (void);
// 0x000000EC System.Void BearMatch.AdsManager::LoadRewardedAd()
extern void AdsManager_LoadRewardedAd_mD815A3B22E2044D2956D2B0B40A3A8E9BF3C4C83 (void);
// 0x000000ED System.Void BearMatch.AdsManager::ShowInterstitialAd(UnityEngine.Events.UnityAction)
extern void AdsManager_ShowInterstitialAd_m9E2C121CBE5CB80578EFF257FD04DA00D67BA6AE (void);
// 0x000000EE System.Void BearMatch.AdsManager::ShowRewardVideo(UnityEngine.Events.UnityAction,UnityEngine.Events.UnityAction,UnityEngine.Events.UnityAction)
extern void AdsManager_ShowRewardVideo_m68ED33958B7FC3A76D21DEFBB7D768A42F2C48B4 (void);
// 0x000000EF System.Void BearMatch.AdsManager::OnApplicationPause(System.Boolean)
extern void AdsManager_OnApplicationPause_mAF1142F41624784E6407B3C52719AB8FB5A4AB60 (void);
// 0x000000F0 System.Void BearMatch.AdsManager::TrackAdRevenue(MaxSdkBase/AdInfo)
extern void AdsManager_TrackAdRevenue_m4FF79E040FC2F8B5AA06DF0A2B0981B0D7A0DD34 (void);
// 0x000000F1 System.Void BearMatch.AdsManager::Requestbanner()
extern void AdsManager_Requestbanner_mDEA572F6C9A0EAD55488E6BF1E47D6F06AFC310C (void);
// 0x000000F2 System.Void BearMatch.AdsManager::ShowBanner()
extern void AdsManager_ShowBanner_mBE83916DAD40B7B9EB34079BA3C0E8DE692AD99E (void);
// 0x000000F3 System.Boolean BearMatch.AdsManager::InterstitialIsLoaded()
extern void AdsManager_InterstitialIsLoaded_m990B2C257ABFF88EFFDF411888099F4A69D2EAD8 (void);
// 0x000000F4 System.Boolean BearMatch.AdsManager::VideoRewardIsLoaded()
extern void AdsManager_VideoRewardIsLoaded_m93EEF85A59B42EFA939C81D6BCAF62BF67166A83 (void);
// 0x000000F5 System.Void BearMatch.AdsManager::.ctor()
extern void AdsManager__ctor_m8F03684B891950A46800EE2DC0707469D0B3FF7F (void);
// 0x000000F6 System.Void BearMatch.AdsManager::<InitMax>b__46_0(MaxSdkBase/SdkConfiguration)
extern void AdsManager_U3CInitMaxU3Eb__46_0_mF3422E16B740FDA20935BFE8B8E2A9D59FA23BBC (void);
// 0x000000F7 System.Void BearMatch.AdsManager/<IEShowBanner>d__54::.ctor(System.Int32)
extern void U3CIEShowBannerU3Ed__54__ctor_mE55F6BC876B5A446AC412BA26D62052EE64ECB89 (void);
// 0x000000F8 System.Void BearMatch.AdsManager/<IEShowBanner>d__54::System.IDisposable.Dispose()
extern void U3CIEShowBannerU3Ed__54_System_IDisposable_Dispose_m24CAC7454DB6D0CC46B715B3D159A668166CDEBF (void);
// 0x000000F9 System.Boolean BearMatch.AdsManager/<IEShowBanner>d__54::MoveNext()
extern void U3CIEShowBannerU3Ed__54_MoveNext_m107505AF578A310406DB47521A1554E82D10C11A (void);
// 0x000000FA System.Object BearMatch.AdsManager/<IEShowBanner>d__54::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIEShowBannerU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCEFEA230C2D3CB9E998A340F0377BF1C94916C54 (void);
// 0x000000FB System.Void BearMatch.AdsManager/<IEShowBanner>d__54::System.Collections.IEnumerator.Reset()
extern void U3CIEShowBannerU3Ed__54_System_Collections_IEnumerator_Reset_m2509B81FDED6AF0AED1315F2BEB9E216625463F3 (void);
// 0x000000FC System.Object BearMatch.AdsManager/<IEShowBanner>d__54::System.Collections.IEnumerator.get_Current()
extern void U3CIEShowBannerU3Ed__54_System_Collections_IEnumerator_get_Current_mDA82D84F5A2DADA15888C7BC0E0AC32A3FC579F2 (void);
// 0x000000FD System.Void BearMatch.AdsManager/<IEDoRewardVideoRewardedCallback>d__73::.ctor(System.Int32)
extern void U3CIEDoRewardVideoRewardedCallbackU3Ed__73__ctor_m9110C0ADB8476971785C3E519D4EDC2221D547E9 (void);
// 0x000000FE System.Void BearMatch.AdsManager/<IEDoRewardVideoRewardedCallback>d__73::System.IDisposable.Dispose()
extern void U3CIEDoRewardVideoRewardedCallbackU3Ed__73_System_IDisposable_Dispose_m44EF9687E17F93CEB21B300BCED3D4EAEC1A4F52 (void);
// 0x000000FF System.Boolean BearMatch.AdsManager/<IEDoRewardVideoRewardedCallback>d__73::MoveNext()
extern void U3CIEDoRewardVideoRewardedCallbackU3Ed__73_MoveNext_m82B2072D95948CAEAE3FE79A1E808E3E6DB7E20E (void);
// 0x00000100 System.Object BearMatch.AdsManager/<IEDoRewardVideoRewardedCallback>d__73::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIEDoRewardVideoRewardedCallbackU3Ed__73_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1F999B6B2D27D361338F586D9F8FD1FA35A5BFF7 (void);
// 0x00000101 System.Void BearMatch.AdsManager/<IEDoRewardVideoRewardedCallback>d__73::System.Collections.IEnumerator.Reset()
extern void U3CIEDoRewardVideoRewardedCallbackU3Ed__73_System_Collections_IEnumerator_Reset_m76D4FAA44521CCA25BE77E30DB848E5EE8577C79 (void);
// 0x00000102 System.Object BearMatch.AdsManager/<IEDoRewardVideoRewardedCallback>d__73::System.Collections.IEnumerator.get_Current()
extern void U3CIEDoRewardVideoRewardedCallbackU3Ed__73_System_Collections_IEnumerator_get_Current_m7B3DFA1F48FE26F10649CF64402E8D7425A20693 (void);
// 0x00000103 System.Void BearMatch.AdsManager/<IEDoRewardVideoClosedCallback>d__74::.ctor(System.Int32)
extern void U3CIEDoRewardVideoClosedCallbackU3Ed__74__ctor_m40418B31F69800DF23D1E7657FD1217F38EA91E0 (void);
// 0x00000104 System.Void BearMatch.AdsManager/<IEDoRewardVideoClosedCallback>d__74::System.IDisposable.Dispose()
extern void U3CIEDoRewardVideoClosedCallbackU3Ed__74_System_IDisposable_Dispose_m8A2F41A17DD37CF86C8B39F443D74C2009A76CE8 (void);
// 0x00000105 System.Boolean BearMatch.AdsManager/<IEDoRewardVideoClosedCallback>d__74::MoveNext()
extern void U3CIEDoRewardVideoClosedCallbackU3Ed__74_MoveNext_m5A02997D2ADBD15586FB5DA3E6B4206396B1AD61 (void);
// 0x00000106 System.Object BearMatch.AdsManager/<IEDoRewardVideoClosedCallback>d__74::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIEDoRewardVideoClosedCallbackU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m529E9135EF65E6488A93D9622C0D074EB108DD37 (void);
// 0x00000107 System.Void BearMatch.AdsManager/<IEDoRewardVideoClosedCallback>d__74::System.Collections.IEnumerator.Reset()
extern void U3CIEDoRewardVideoClosedCallbackU3Ed__74_System_Collections_IEnumerator_Reset_m2562181F949759F38E571607B6CB10ED231FF43E (void);
// 0x00000108 System.Object BearMatch.AdsManager/<IEDoRewardVideoClosedCallback>d__74::System.Collections.IEnumerator.get_Current()
extern void U3CIEDoRewardVideoClosedCallbackU3Ed__74_System_Collections_IEnumerator_get_Current_m8BE854EDD23ECC0FB02974523FB2E0E9C6316E92 (void);
// 0x00000109 System.Void BearMatch.AutoRotation::Update()
extern void AutoRotation_Update_m2D83DCDC263AD25A68CFE5BD966A7C7A77840CB0 (void);
// 0x0000010A System.Void BearMatch.AutoRotation::.ctor()
extern void AutoRotation__ctor_m60EA534D955B452A3D2B4E8B5C0730855AEE6703 (void);
// 0x0000010B System.Void BearMatch.Constants::.ctor()
extern void Constants__ctor_m46B3DF6C0B62C5925652A8D37CBDD11E3A57F8D5 (void);
// 0x0000010C System.Void BearMatch.Constants::.cctor()
extern void Constants__cctor_mB80636127E24EB163DD67F6C97D52507E173BA55 (void);
// 0x0000010D BearMatch.EventDispatcher BearMatch.EventDispatcher::get_Instance()
extern void EventDispatcher_get_Instance_m046F93FB07E8F0E75912861DE66BA0A65B52F3F5 (void);
// 0x0000010E System.Void BearMatch.EventDispatcher::set_Instance(BearMatch.EventDispatcher)
extern void EventDispatcher_set_Instance_m649143682312DB945292D7553B9E5D776DDF51CC (void);
// 0x0000010F System.Void BearMatch.EventDispatcher::Awake()
extern void EventDispatcher_Awake_m8455EA8E92FF1DE15009CBBEC86AD071FFEA38E2 (void);
// 0x00000110 System.Void BearMatch.EventDispatcher::OnDestroy()
extern void EventDispatcher_OnDestroy_mCCBEF07567F855D5D4B633E59AE243E48856CB48 (void);
// 0x00000111 System.Void BearMatch.EventDispatcher::AddListener(BearMatch.EventID,BearMatch.EventDispatcher/ActionCallback)
extern void EventDispatcher_AddListener_m8307E3C820300B29DC1A739F0CB1C214C48E8D14 (void);
// 0x00000112 System.Void BearMatch.EventDispatcher::PostEvent(BearMatch.EventID,System.Object)
extern void EventDispatcher_PostEvent_mBAB648658AE85BC0DDD7F1F85216E6A40049BF76 (void);
// 0x00000113 System.Void BearMatch.EventDispatcher::RemoveListener(BearMatch.EventID,BearMatch.EventDispatcher/ActionCallback)
extern void EventDispatcher_RemoveListener_m869BC7202F8FA11ED18AF10BDF5A092D895D9DC5 (void);
// 0x00000114 System.Void BearMatch.EventDispatcher::RemoveAllListener(BearMatch.EventID)
extern void EventDispatcher_RemoveAllListener_m35D3FF04596C9E905C19513A94ADB43537C8A936 (void);
// 0x00000115 System.Void BearMatch.EventDispatcher::ClearAllListener()
extern void EventDispatcher_ClearAllListener_mB99A53A0F9642321188094AA81C7784150305937 (void);
// 0x00000116 System.Void BearMatch.EventDispatcher::.ctor()
extern void EventDispatcher__ctor_m173DB974E29B69AC3C07775F67AC78FF1052875F (void);
// 0x00000117 System.Void BearMatch.EventDispatcher/ActionCallback::.ctor(System.Object,System.IntPtr)
extern void ActionCallback__ctor_m00C66FE39E2F6E97C8D96C873DC01DF42B545D37 (void);
// 0x00000118 System.Void BearMatch.EventDispatcher/ActionCallback::Invoke(System.Object)
extern void ActionCallback_Invoke_m1C3A2774148F9843BF87AE50288B94BC684874E6 (void);
// 0x00000119 System.IAsyncResult BearMatch.EventDispatcher/ActionCallback::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern void ActionCallback_BeginInvoke_mF0491E24B2356315595C893D695C1E5473E67528 (void);
// 0x0000011A System.Void BearMatch.EventDispatcher/ActionCallback::EndInvoke(System.IAsyncResult)
extern void ActionCallback_EndInvoke_m9E2EF563D4131C58C84E1F48232C26285C819303 (void);
// 0x0000011B System.Void BearMatch.EventDispatcherExtension::PostEvent(BearMatch.EventID,System.Object)
extern void EventDispatcherExtension_PostEvent_m92BF43BC0FF6F17D2AEF94A2F14296C7196DF1CA (void);
// 0x0000011C System.Void BearMatch.EventDispatcherExtension::AddListener(BearMatch.EventID,BearMatch.EventDispatcher/ActionCallback)
extern void EventDispatcherExtension_AddListener_mD04C17591104474F01F7F4861EAB9C1E100546C9 (void);
// 0x0000011D System.Void BearMatch.EventDispatcherExtension::RemoveListener(BearMatch.EventID,BearMatch.EventDispatcher/ActionCallback)
extern void EventDispatcherExtension_RemoveListener_m06BBEEF5F1559824CC42CB11B1699B847B9A3CB1 (void);
// 0x0000011E System.Void BearMatch.EventDispatcherExtension::RemoveAllListener(BearMatch.EventID)
extern void EventDispatcherExtension_RemoveAllListener_mF5692345464600A9F01DB031D97B34A5C2BB7CDE (void);
// 0x0000011F System.Void BearMatch.EventDispatcherExtension::ClearAllListener()
extern void EventDispatcherExtension_ClearAllListener_m66D02C1E36E2C25EB978AC7BC7237ED64337D090 (void);
// 0x00000120 System.String BearMatch.FirebaseManager::get_TestGameValue()
extern void FirebaseManager_get_TestGameValue_m04C9A77827BB3FBFC7E1A041E997C2211597FE53 (void);
// 0x00000121 System.Void BearMatch.FirebaseManager::set_TestGameValue(System.String)
extern void FirebaseManager_set_TestGameValue_m710A5B19A3D01FACB94ACA61211CE9D8104DC41D (void);
// 0x00000122 BearMatch.FirebaseManager BearMatch.FirebaseManager::get_Instance()
extern void FirebaseManager_get_Instance_mDEB6781B7A6D8508DE5A88B092D6D69FB3B38DF2 (void);
// 0x00000123 System.Void BearMatch.FirebaseManager::set_Instance(BearMatch.FirebaseManager)
extern void FirebaseManager_set_Instance_m462121C1D44304C7059D224BCB1D75E69639AD4A (void);
// 0x00000124 System.Boolean BearMatch.FirebaseManager::get_IsFirebaseReady()
extern void FirebaseManager_get_IsFirebaseReady_mF97530F5C4E97E9D502161D341BF1EDE49A3E476 (void);
// 0x00000125 System.Void BearMatch.FirebaseManager::set_IsFirebaseReady(System.Boolean)
extern void FirebaseManager_set_IsFirebaseReady_mA33B3004BAD0575BF9CD348B108615A633F0F547 (void);
// 0x00000126 System.Void BearMatch.FirebaseManager::Awake()
extern void FirebaseManager_Awake_m7CE26934278B19D64C61B2B991477EA53DD03EC2 (void);
// 0x00000127 System.Void BearMatch.FirebaseManager::Start()
extern void FirebaseManager_Start_m85C19F19BFED89551E1365FFC383C63DF1662CAA (void);
// 0x00000128 System.Void BearMatch.FirebaseManager::InitializeFirebaseDone()
extern void FirebaseManager_InitializeFirebaseDone_mA64EE226D65A0891DE085A083A930E2937D08198 (void);
// 0x00000129 System.Void BearMatch.FirebaseManager::FetchFirebaseConfigDone()
extern void FirebaseManager_FetchFirebaseConfigDone_mF559AB5474414289E1208306C75AD553EC3D972B (void);
// 0x0000012A System.Void BearMatch.FirebaseManager::LogEvent(System.String)
extern void FirebaseManager_LogEvent_m888365F2FA2964090AE6A3C7FA589414CAA15C69 (void);
// 0x0000012B System.Void BearMatch.FirebaseManager::.ctor()
extern void FirebaseManager__ctor_mD38384E2549151C86E3EFACF2CB126FD4AC40DA0 (void);
// 0x0000012C System.Void BearMatch.FirebaseManager::.cctor()
extern void FirebaseManager__cctor_m1EEB766C1B997FB960358DB42CD47DC51BFACFF4 (void);
// 0x0000012D System.Void BearMatch.FirebaseManager::<Start>b__16_0(System.Threading.Tasks.Task`1<Firebase.DependencyStatus>)
extern void FirebaseManager_U3CStartU3Eb__16_0_mD3658BF021506790330BB855AC753623DDD88086 (void);
// 0x0000012E System.Void BearMatch.FirebaseManager::<InitializeFirebaseDone>b__17_0(System.Threading.Tasks.Task)
extern void FirebaseManager_U3CInitializeFirebaseDoneU3Eb__17_0_mD6BBE91BE5DBDFC634B27D8533DCD8DC85D8BEA4 (void);
// 0x0000012F System.Void BearMatch.FPSCounter::Start()
extern void FPSCounter_Start_m8D9BBC3654CFB2F7311CE1B006EB3545F4D799D5 (void);
// 0x00000130 System.Void BearMatch.FPSCounter::Update()
extern void FPSCounter_Update_mAC0E80A7667F828266DBD5ACC4547799F1988A28 (void);
// 0x00000131 System.Void BearMatch.FPSCounter::.ctor()
extern void FPSCounter__ctor_mDF524F4E7C59782F1CCEB236F28170E9189EE9ED (void);
// 0x00000132 System.Int32 BearMatch.GamePopup::get_NumberPopupShow()
extern void GamePopup_get_NumberPopupShow_m511AF17578AAE2F4293ED2A231229794D34E5F8A (void);
// 0x00000133 System.Boolean BearMatch.GamePopup::get_IsHavePopupOpen()
extern void GamePopup_get_IsHavePopupOpen_m8ADC1B3498F63C07EA28E09499DB323D43225370 (void);
// 0x00000134 UnityEngine.RectTransform BearMatch.GamePopup::get_RectTransform()
extern void GamePopup_get_RectTransform_mA44FD26BFB42F48F30F5EF04F3832635C1033087 (void);
// 0x00000135 BearMatch.GamePopup BearMatch.GamePopup::get_CurrentPopup()
extern void GamePopup_get_CurrentPopup_mFA0A3C885D802F94A15CFD854967A07CDAADDF3F (void);
// 0x00000136 System.Void BearMatch.GamePopup::Show(BearMatch.GamePopup,System.Boolean,System.Object,UnityEngine.Events.UnityAction,System.Boolean)
extern void GamePopup_Show_m91082F6455FD2DF47A99AB5619605584B2BDA3E5 (void);
// 0x00000137 System.Void BearMatch.GamePopup::Hide(System.Boolean)
extern void GamePopup_Hide_m4A7FCABB603C0E335E13FCEB6446EF2C70380CE9 (void);
// 0x00000138 System.Void BearMatch.GamePopup::OnEscapeKeyClicked()
extern void GamePopup_OnEscapeKeyClicked_m544E16A11C2E8D5D8F6168E0B0CABB04F82BA783 (void);
// 0x00000139 System.Void BearMatch.GamePopup::Click_DefaultClosePopup()
extern void GamePopup_Click_DefaultClosePopup_mA2CB677911B46A439E3226ED0B2792444BBD4225 (void);
// 0x0000013A System.Void BearMatch.GamePopup::AnimationCall_Hide()
extern void GamePopup_AnimationCall_Hide_m00E0678AEAA75BA3A5C0B3BEE5201DDA34504BEA (void);
// 0x0000013B System.Void BearMatch.GamePopup::ClearAllPopup()
extern void GamePopup_ClearAllPopup_mB04D0631AAE78200AA24321D1AED8F64053C2437 (void);
// 0x0000013C System.Void BearMatch.GamePopup::.ctor()
extern void GamePopup__ctor_mE345689B603B61BF23CEFA75CA1E1A5D705834B2 (void);
// 0x0000013D System.Void BearMatch.GamePopup::.cctor()
extern void GamePopup__cctor_m89986E3ECA697666E8B15DF65E15127BD9229D13 (void);
// 0x0000013E System.Void BearMatch.Logger::Log(System.String)
extern void Logger_Log_m6F3EE6B05EF1CB73DECF048100777487B1CDD7A1 (void);
// 0x0000013F System.Void BearMatch.Logger::LogWarning(System.String)
extern void Logger_LogWarning_m28CD5C06586810B23CE56185D2215A638F75EA1F (void);
// 0x00000140 System.Void BearMatch.Logger::LogError(System.String)
extern void Logger_LogError_m985236BF9496AF7746D9CD47050E7BBB86B6E6B4 (void);
// 0x00000141 System.Void BearMatch.SaveLoadData::.ctor()
extern void SaveLoadData__ctor_m1FEFCB4735CEAE3FB1F6AC8B97B7676336A7325D (void);
// 0x00000142 System.String BearMatch.SaveLoadData::Md5Sum(System.String)
extern void SaveLoadData_Md5Sum_m535D40EF0E7E6CD92B96E7D98020B6879565B261 (void);
// 0x00000143 System.Void BearMatch.SaveLoadData::UpdateDataToLatestVersion()
extern void SaveLoadData_UpdateDataToLatestVersion_m9F19121C8E3E920BB74864424605E99A5870FAA9 (void);
// 0x00000144 BearMatch.SaveLoadData BearMatch.SaveLoadData::get_Data()
extern void SaveLoadData_get_Data_mC66D57C2327DD2788B265D8751F01DA0B624FA28 (void);
// 0x00000145 System.Void BearMatch.SaveLoadData::set_Data(BearMatch.SaveLoadData)
extern void SaveLoadData_set_Data_mB59DA1260C0EC6B33733B3FCF7F18DACD47B6BA3 (void);
// 0x00000146 System.Void BearMatch.SaveLoadData::LoadData()
extern void SaveLoadData_LoadData_mF8AFA3998CFF7B6AE5F97E1FAD966C583D48AE1A (void);
// 0x00000147 System.Void BearMatch.SaveLoadData::SaveData()
extern void SaveLoadData_SaveData_mE87CF48DA01B6D624DB1C977ED354E7BB502A547 (void);
// 0x00000148 System.Void BearMatch.SaveLoadData::AddCoin(System.Int32)
extern void SaveLoadData_AddCoin_m992B8B74AE1F16E1B548D95CA4611AB6D47E729A (void);
// 0x00000149 System.Void BearMatch.SaveLoadData::NextLevel()
extern void SaveLoadData_NextLevel_mC1F6C1CD83369373306C8BE267A79423E252C587 (void);
// 0x0000014A System.Void BearMatch.SaveLoadData::RandomIndexCharacterNeedUnlock()
extern void SaveLoadData_RandomIndexCharacterNeedUnlock_mA807994A9051556099226A0884D349944CBFCE66 (void);
// 0x0000014B System.Void BearMatch.SaveLoadData/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m2A052FBC6E3B96516E6F215B3596CD9E581AB22D (void);
// 0x0000014C System.Void BearMatch.SaveLoadData/<>c__DisplayClass15_0::<Md5Sum>b__0(System.Byte)
extern void U3CU3Ec__DisplayClass15_0_U3CMd5SumU3Eb__0_mC1A89EA328E0E9F1EC7B466A6179DEA461F2F887 (void);
// 0x0000014D BearMatch.SoundManager BearMatch.SoundManager::get_Instance()
extern void SoundManager_get_Instance_m3AEC930B80711741CC64E5DBBD46E884026E1A34 (void);
// 0x0000014E System.Void BearMatch.SoundManager::set_Instance(BearMatch.SoundManager)
extern void SoundManager_set_Instance_mB9B4EFC6B14C6880343E91E568F53821226081CE (void);
// 0x0000014F System.Void BearMatch.SoundManager::Awake()
extern void SoundManager_Awake_mB231859CF84A895E30D233A88A288830C858C705 (void);
// 0x00000150 System.Void BearMatch.SoundManager::Play_ClickButton(System.Single)
extern void SoundManager_Play_ClickButton_m59BB00CCFC5CEF1C2E41E8B15ED95D792170978F (void);
// 0x00000151 System.Void BearMatch.SoundManager::Play_Firework(System.Single)
extern void SoundManager_Play_Firework_m2C0A6626625138847CC3004C77201FCF218A52B3 (void);
// 0x00000152 System.Void BearMatch.SoundManager::Play_GetCoin(System.Single)
extern void SoundManager_Play_GetCoin_m4F0249D246BB2D2C4891C5AA1BF713D8A6B4B6A4 (void);
// 0x00000153 System.Void BearMatch.SoundManager::Play_HitBot(System.Single)
extern void SoundManager_Play_HitBot_mE9F4B117042C50EAA7B61C9EAD738F42C9D40D29 (void);
// 0x00000154 System.Void BearMatch.SoundManager::Play_GameOver(System.Single)
extern void SoundManager_Play_GameOver_m8D51C4FA5941934F4CCC56D1EDCA50A99FBBC09E (void);
// 0x00000155 System.Void BearMatch.SoundManager::Play_Victory(System.Single)
extern void SoundManager_Play_Victory_m8378F266D7475B90EB13C16E533318CEE766C50C (void);
// 0x00000156 System.Void BearMatch.SoundManager::.ctor()
extern void SoundManager__ctor_mF50352EFDC43BCD2B58E8F0301C91C361C0AF1C6 (void);
// 0x00000157 BearMatch.StoreManager BearMatch.StoreManager::get_Instance()
extern void StoreManager_get_Instance_mD8D3E05EFDD07AEE406FB4C9A397D8FC53887881 (void);
// 0x00000158 System.Void BearMatch.StoreManager::set_Instance(BearMatch.StoreManager)
extern void StoreManager_set_Instance_m1F1387F316149E0C835CFE29E55DF5DE837F8198 (void);
// 0x00000159 System.Void BearMatch.StoreManager::Awake()
extern void StoreManager_Awake_mCECB207C77BBBD79617DB4C17607CB0B30959412 (void);
// 0x0000015A System.Void BearMatch.StoreManager::Start()
extern void StoreManager_Start_m7430C8F7627AE60462D6F59B4D3DEBFAD2D3EFFB (void);
// 0x0000015B System.Void BearMatch.StoreManager::InitializePurchasing()
extern void StoreManager_InitializePurchasing_m02DDF4E2B70879D39EE8B982535E9C8AD3891661 (void);
// 0x0000015C System.Boolean BearMatch.StoreManager::IsInitialized()
extern void StoreManager_IsInitialized_m2CB2893D9CBB226FA95BE5D48EECA8526FD9D4B9 (void);
// 0x0000015D System.Void BearMatch.StoreManager::BuyItem(System.String,UnityEngine.Events.UnityAction`2<System.Boolean,System.String>)
extern void StoreManager_BuyItem_m45A9EBDAE616C06FADFA937E9D1099A1B0777AD6 (void);
// 0x0000015E System.String BearMatch.StoreManager::GetPrice(System.String)
extern void StoreManager_GetPrice_mF0B76EEDB419BA8092131CE2CA5BAB4FAD413D5E (void);
// 0x0000015F System.Void BearMatch.StoreManager::RestorePurchases()
extern void StoreManager_RestorePurchases_m056D3BB26AA2625C25DAA1DB889F892C9DB05C57 (void);
// 0x00000160 System.Void BearMatch.StoreManager::OnTransactionsRestored(System.Boolean)
extern void StoreManager_OnTransactionsRestored_m5D371CFB0B4CECCFB5FAFE1532D8E4A993F51DD0 (void);
// 0x00000161 System.Void BearMatch.StoreManager::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern void StoreManager_OnInitialized_m206C6DC94709C9F5E49FB6151FC86D5BA13BA041 (void);
// 0x00000162 System.Void BearMatch.StoreManager::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern void StoreManager_OnInitializeFailed_m640D5685F4BC7CD6F076717CC975661CE7B98357 (void);
// 0x00000163 UnityEngine.Purchasing.PurchaseProcessingResult BearMatch.StoreManager::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern void StoreManager_ProcessPurchase_m8C29E4489FCBAA26A74AD634FED39DC994C5F2C2 (void);
// 0x00000164 System.Void BearMatch.StoreManager::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern void StoreManager_OnPurchaseFailed_mC3055CAD71C86533AE30A26D8974477A918790EA (void);
// 0x00000165 System.Void BearMatch.StoreManager::.ctor()
extern void StoreManager__ctor_mB47881B0785F4B7B00809FD0A2CD3DC3A06E7992 (void);
// 0x00000166 UnityEngine.Texture BearMatch.SuperImage::get_mainTexture()
extern void SuperImage_get_mainTexture_mDE93967675478B3984E1D11AC7D2F4F5937339F5 (void);
// 0x00000167 System.Void BearMatch.SuperImage::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void SuperImage_OnPopulateMesh_m613343577F7F882145FFAACB63E6102A12CA7FDA (void);
// 0x00000168 System.Void BearMatch.SuperImage::UpdateVertices()
extern void SuperImage_UpdateVertices_m1478012E3D36C2D55C2F67DC0F3747A0B99B9BC1 (void);
// 0x00000169 System.Void BearMatch.SuperImage::UpdateVertices(UnityEngine.RectTransform,UnityEngine.Vector4)
extern void SuperImage_UpdateVertices_m483BD64D5AD07EC38F6CF88B5ADC03F1328F74E1 (void);
// 0x0000016A System.Void BearMatch.SuperImage::.ctor()
extern void SuperImage__ctor_mA8A96EF0912B3272F52ED3A984B566F4D1D46107 (void);
// 0x0000016B BearMatch.UiManager BearMatch.UiManager::get_Instance()
extern void UiManager_get_Instance_m0F69E0B2F3E56FB25404FD9695D1B37A0EE81870 (void);
// 0x0000016C System.Void BearMatch.UiManager::set_Instance(BearMatch.UiManager)
extern void UiManager_set_Instance_m158AB08B22E591609E59D166661A61F3F3AF2D15 (void);
// 0x0000016D BearMatch.SuperImage BearMatch.UiManager::get_SuperImage()
extern void UiManager_get_SuperImage_m29922B5B66B403C81C8FECA07FDD57DAC996F8AE (void);
// 0x0000016E UnityEngine.Canvas BearMatch.UiManager::get_RootCanvas()
extern void UiManager_get_RootCanvas_mEABA6028D8BBE0BADBEF3830D1EDB44F8F26585B (void);
// 0x0000016F System.Single BearMatch.UiManager::get_SizeScale()
extern void UiManager_get_SizeScale_m30D6A4BDDCF12558A32B3624E802A49FA7C58AD6 (void);
// 0x00000170 System.Void BearMatch.UiManager::set_SizeScale(System.Single)
extern void UiManager_set_SizeScale_mDC8928E8957908A14CB0352D1E950DF550B07570 (void);
// 0x00000171 UnityEngine.Vector3 BearMatch.UiManager::get_CoinPosition()
extern void UiManager_get_CoinPosition_mE33FE79367B35A2319D359C8AB1C1D02A0AA8A8F (void);
// 0x00000172 System.Void BearMatch.UiManager::Awake()
extern void UiManager_Awake_m66267834B7A3A6F590F1C5908CA6E533BD848BFD (void);
// 0x00000173 System.Collections.IEnumerator BearMatch.UiManager::Start()
extern void UiManager_Start_m04854C3B25D17BB036BE8AC9BEFCF3C066D81F77 (void);
// 0x00000174 T BearMatch.UiManager::ShowPanel(System.Object,UnityEngine.Events.UnityAction,System.Boolean,System.Boolean)
// 0x00000175 T BearMatch.UiManager::ShowPopup(System.Object,UnityEngine.Events.UnityAction,System.Boolean,System.Boolean)
// 0x00000176 System.Void BearMatch.UiManager::ClosePopup()
// 0x00000177 T BearMatch.UiManager::GetPopup()
// 0x00000178 System.Boolean BearMatch.UiManager::HasShowPopup()
// 0x00000179 System.Void BearMatch.UiManager::ShowSuperImage(UnityEngine.RectTransform,UnityEngine.Vector4)
extern void UiManager_ShowSuperImage_mCAA23A83EFA7347E01D3BAE0AF75BD38E54E5925 (void);
// 0x0000017A System.Void BearMatch.UiManager::HideSuperImage()
extern void UiManager_HideSuperImage_mACF1CC7B66DFBA12DFBF6EB2C6C473893D9AB3F3 (void);
// 0x0000017B System.Void BearMatch.UiManager::ShowCoin()
extern void UiManager_ShowCoin_m8994865C16CFFE20A28B4B93B03D1AAEEC3A08F0 (void);
// 0x0000017C System.Void BearMatch.UiManager::.ctor()
extern void UiManager__ctor_m483258F0D8DC96CD38FE86F254AE0F33FAA2F8A0 (void);
// 0x0000017D System.Void BearMatch.UiManager/<Start>d__23::.ctor(System.Int32)
extern void U3CStartU3Ed__23__ctor_mB1E0AA28F3BE6CC004BE229ADEA18831F3036196 (void);
// 0x0000017E System.Void BearMatch.UiManager/<Start>d__23::System.IDisposable.Dispose()
extern void U3CStartU3Ed__23_System_IDisposable_Dispose_m87B3CC49A5C4568140EC610F4A17DB16D733662E (void);
// 0x0000017F System.Boolean BearMatch.UiManager/<Start>d__23::MoveNext()
extern void U3CStartU3Ed__23_MoveNext_m03241495E437ACA94B4DF532A2998D6FFBD40857 (void);
// 0x00000180 System.Object BearMatch.UiManager/<Start>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7247B803EA76009541937A2B21E7A1A5EF17398F (void);
// 0x00000181 System.Void BearMatch.UiManager/<Start>d__23::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__23_System_Collections_IEnumerator_Reset_mC0C98B770819D6CFA5B63022A0914CE56E0EDF49 (void);
// 0x00000182 System.Object BearMatch.UiManager/<Start>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__23_System_Collections_IEnumerator_get_Current_m90F037AB99A992F6380DDE60F98AB9B4F94FCA3F (void);
// 0x00000183 BearMatch.GameManager BearMatch.GameManager::get_Instance()
extern void GameManager_get_Instance_mA33B8A03D44543B6721A6B4E6232FD3745FAEAC9 (void);
// 0x00000184 System.Void BearMatch.GameManager::set_Instance(BearMatch.GameManager)
extern void GameManager_set_Instance_m194FEBEF6D0DDE5F32F4818C90DAD1ADCF6ACBC7 (void);
// 0x00000185 System.Void BearMatch.GameManager::Awake()
extern void GameManager_Awake_mF4EF1B6F6B1B260FDC5953909D67D0B6E906E1B8 (void);
// 0x00000186 System.Void BearMatch.GameManager::.ctor()
extern void GameManager__ctor_m6107A41B2117199E92B9A14649119414EE437789 (void);
// 0x00000187 BearMatch.GameplayController BearMatch.GameplayController::get_Instance()
extern void GameplayController_get_Instance_m3AD44C84B6AAF71930D1779B4BA83098E9D42318 (void);
// 0x00000188 System.Void BearMatch.GameplayController::set_Instance(BearMatch.GameplayController)
extern void GameplayController_set_Instance_m226E911D6E1C4636D9A00BF26EBFB284B754F0D7 (void);
// 0x00000189 BearMatch.GameplayState BearMatch.GameplayController::get_State()
extern void GameplayController_get_State_m79E017B68A5568FB4A42352BF6663FCBA6917D18 (void);
// 0x0000018A System.Void BearMatch.GameplayController::set_State(BearMatch.GameplayState)
extern void GameplayController_set_State_m274BC55B97FF08BAE18C49B66F2F277538BD8388 (void);
// 0x0000018B System.Int32 BearMatch.GameplayController::get_TestGameType()
extern void GameplayController_get_TestGameType_m3E3ACA8257CBCCCB1393BC0E5F548888105A5D53 (void);
// 0x0000018C System.Void BearMatch.GameplayController::set_TestGameType(System.Int32)
extern void GameplayController_set_TestGameType_m046CE058F92ADA3CBC2B80826B13CC383714AFED (void);
// 0x0000018D System.Void BearMatch.GameplayController::Awake()
extern void GameplayController_Awake_m8796CAA118409CE6F767DD53A10184B860464DD9 (void);
// 0x0000018E System.Void BearMatch.GameplayController::Update()
extern void GameplayController_Update_m932DAD3BD66DAF071603EF962FC1442241000326 (void);
// 0x0000018F System.Void BearMatch.GameplayController::SpawnPlayer()
extern void GameplayController_SpawnPlayer_m32C4EB7D5E3BAF883AB616392BCA64F149039143 (void);
// 0x00000190 System.Void BearMatch.GameplayController::InitializeNewGame()
extern void GameplayController_InitializeNewGame_mFF8CCB91A3F45CE3D9EF3F5CB38CA3493B17DC1B (void);
// 0x00000191 System.Void BearMatch.GameplayController::StartNewLevel()
extern void GameplayController_StartNewLevel_m65D25F4F60B2A4F183CDDDF5EF5AEFE7F2A9F812 (void);
// 0x00000192 System.Void BearMatch.GameplayController::ClearAllGridItem()
extern void GameplayController_ClearAllGridItem_mA1049343BA8D9D1D793F587CE561A883CD0E3418 (void);
// 0x00000193 System.Void BearMatch.GameplayController::RandomDecal()
extern void GameplayController_RandomDecal_m8F6F021279755B2B463D3EED3A2B3EE2FF20F266 (void);
// 0x00000194 System.Void BearMatch.GameplayController::ShowDecalResult()
extern void GameplayController_ShowDecalResult_m9FEB502ACB8136F6ED6BD2C6A9DDAB1A198D4B2D (void);
// 0x00000195 System.Collections.IEnumerator BearMatch.GameplayController::IECheckPlayerNearGridResult()
extern void GameplayController_IECheckPlayerNearGridResult_m7B7C0289B5992953AD0FAEF6E132959AA91E655A (void);
// 0x00000196 System.Collections.IEnumerator BearMatch.GameplayController::IECheckPushBot()
extern void GameplayController_IECheckPushBot_mD9AB204331079A1273013E933CF49DA8AF86BEC9 (void);
// 0x00000197 System.Void BearMatch.GameplayController::OnCountdownDone()
extern void GameplayController_OnCountdownDone_mB844D07ACBDECB7660DE9E8EC766D24A18C1CB34 (void);
// 0x00000198 System.Void BearMatch.GameplayController::GridItemMoveUpDone()
extern void GameplayController_GridItemMoveUpDone_m0DFBE29093B8AFB9C614F4B430D351AB32BF78EA (void);
// 0x00000199 System.Void BearMatch.GameplayController::PlayerWin()
extern void GameplayController_PlayerWin_m5AFC70381AC54F2842D584B8297ADD895B6AEB3C (void);
// 0x0000019A System.Void BearMatch.GameplayController::OnPlayerDie(System.Object)
extern void GameplayController_OnPlayerDie_m51C5237CAD474B1E8FF2FCB2E64C29CE2D711A37 (void);
// 0x0000019B UnityEngine.Vector3 BearMatch.GameplayController::GetRandomGridItemActive()
extern void GameplayController_GetRandomGridItemActive_m441762DDCB9FB9E06E79F54D574744A6D62C8A70 (void);
// 0x0000019C System.Void BearMatch.GameplayController::HaveBotKilled()
extern void GameplayController_HaveBotKilled_m82385C73D1018299B2D0A354FFEB520DAC761043 (void);
// 0x0000019D System.Void BearMatch.GameplayController::.ctor()
extern void GameplayController__ctor_m3C8705B3712ED60635C1943848C8D04E02622626 (void);
// 0x0000019E System.Void BearMatch.GameplayController/<IECheckPlayerNearGridResult>d__45::.ctor(System.Int32)
extern void U3CIECheckPlayerNearGridResultU3Ed__45__ctor_mC4F6C25B2DE9D2850EBC31B7B68D26512A23FB72 (void);
// 0x0000019F System.Void BearMatch.GameplayController/<IECheckPlayerNearGridResult>d__45::System.IDisposable.Dispose()
extern void U3CIECheckPlayerNearGridResultU3Ed__45_System_IDisposable_Dispose_m31E332BECB03640F84E9A338967F49338BB88FC3 (void);
// 0x000001A0 System.Boolean BearMatch.GameplayController/<IECheckPlayerNearGridResult>d__45::MoveNext()
extern void U3CIECheckPlayerNearGridResultU3Ed__45_MoveNext_mF68BFFA10CB7407795CADD1C8C04D845B6929343 (void);
// 0x000001A1 System.Object BearMatch.GameplayController/<IECheckPlayerNearGridResult>d__45::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIECheckPlayerNearGridResultU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB826C1D388E9E12928B896852A772469677DF368 (void);
// 0x000001A2 System.Void BearMatch.GameplayController/<IECheckPlayerNearGridResult>d__45::System.Collections.IEnumerator.Reset()
extern void U3CIECheckPlayerNearGridResultU3Ed__45_System_Collections_IEnumerator_Reset_m6ADE6777E5672069452A8BFE6CB200C8E171A88D (void);
// 0x000001A3 System.Object BearMatch.GameplayController/<IECheckPlayerNearGridResult>d__45::System.Collections.IEnumerator.get_Current()
extern void U3CIECheckPlayerNearGridResultU3Ed__45_System_Collections_IEnumerator_get_Current_mA5F976BE4F1B7E36EDF27648D3122BF89A953F35 (void);
// 0x000001A4 System.Void BearMatch.GameplayController/<IECheckPushBot>d__46::.ctor(System.Int32)
extern void U3CIECheckPushBotU3Ed__46__ctor_m466EB7CCBF1335A1F55C38F0E34E46A59CE3C202 (void);
// 0x000001A5 System.Void BearMatch.GameplayController/<IECheckPushBot>d__46::System.IDisposable.Dispose()
extern void U3CIECheckPushBotU3Ed__46_System_IDisposable_Dispose_mA6D328E54083A4CFAD09680985FABA3B1253E861 (void);
// 0x000001A6 System.Boolean BearMatch.GameplayController/<IECheckPushBot>d__46::MoveNext()
extern void U3CIECheckPushBotU3Ed__46_MoveNext_m4C59BE8407653B3ED1809F3F8E0C47FE5845B27B (void);
// 0x000001A7 System.Object BearMatch.GameplayController/<IECheckPushBot>d__46::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIECheckPushBotU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F1B7458C355DF0AED6A74E059707D90C5048A6D (void);
// 0x000001A8 System.Void BearMatch.GameplayController/<IECheckPushBot>d__46::System.Collections.IEnumerator.Reset()
extern void U3CIECheckPushBotU3Ed__46_System_Collections_IEnumerator_Reset_mFCADB12534CA1CF277414BB659505F0DF152899D (void);
// 0x000001A9 System.Object BearMatch.GameplayController/<IECheckPushBot>d__46::System.Collections.IEnumerator.get_Current()
extern void U3CIECheckPushBotU3Ed__46_System_Collections_IEnumerator_get_Current_mFFF5A1A8B3F9CE0F471744A8F96B990A867B313E (void);
// 0x000001AA System.Void BearMatch.GameplayController/<>c::.cctor()
extern void U3CU3Ec__cctor_mC26489CA69666053EBAC7380439A312BE90535B1 (void);
// 0x000001AB System.Void BearMatch.GameplayController/<>c::.ctor()
extern void U3CU3Ec__ctor_m88BC53F773CA9C6C2B2BC083ADBDA154769315E3 (void);
// 0x000001AC System.Int32 BearMatch.GameplayController/<>c::<HaveBotKilled>b__52_0(BearMatch.BotController,BearMatch.BotController)
extern void U3CU3Ec_U3CHaveBotKilledU3Eb__52_0_m89FCE30705FB9A437E9588F537D15FB8DC1C3531 (void);
// 0x000001AD BearMatch.GameplayUi BearMatch.GameplayUi::get_Instance()
extern void GameplayUi_get_Instance_m2C600C696E55AA47C11A09C510D1ABE0C5F0D0E5 (void);
// 0x000001AE System.Void BearMatch.GameplayUi::set_Instance(BearMatch.GameplayUi)
extern void GameplayUi_set_Instance_m4CF915559AFA718E104720E2445D121F2A2E7652 (void);
// 0x000001AF System.Void BearMatch.GameplayUi::Awake()
extern void GameplayUi_Awake_mAE3C793D7D39422BD42F63412409E7A4077D0E19 (void);
// 0x000001B0 System.Void BearMatch.GameplayUi::OnDestroy()
extern void GameplayUi_OnDestroy_mC5BA36169A8C2E2434021CE9A9BE4AA80C2EB589 (void);
// 0x000001B1 System.Void BearMatch.GameplayUi::Start()
extern void GameplayUi_Start_mD587709DC9812D10387A77EE0310D3A66880554F (void);
// 0x000001B2 System.Void BearMatch.GameplayUi::InitializeNewGame()
extern void GameplayUi_InitializeNewGame_mF18977DD6886D72A14E608518137491F12A8A8FA (void);
// 0x000001B3 System.Void BearMatch.GameplayUi::StartGame()
extern void GameplayUi_StartGame_mFAA3C1268CD47C7E5C2B2A02A0EA283512281697 (void);
// 0x000001B4 System.Void BearMatch.GameplayUi::ShowCountdown(UnityEngine.Events.UnityAction)
extern void GameplayUi_ShowCountdown_m294D751C20D2689F5DCF92620AD748B82CCD88D4 (void);
// 0x000001B5 System.Void BearMatch.GameplayUi::Countdown()
extern void GameplayUi_Countdown_mCF8BC21DAD417831AC75EC2B4B70E298664B4846 (void);
// 0x000001B6 System.Void BearMatch.GameplayUi::UpdateLevelText()
extern void GameplayUi_UpdateLevelText_m2EC07DE4432D90BBA4E1A9FE83B2E5145F6A4BE8 (void);
// 0x000001B7 System.Void BearMatch.GameplayUi::OnPlayerDie(System.Object)
extern void GameplayUi_OnPlayerDie_m50680823EF72045B9D73CEDB389FFF8EB4BBF861 (void);
// 0x000001B8 System.Void BearMatch.GameplayUi::OnCoinChanged(System.Object)
extern void GameplayUi_OnCoinChanged_m3B5BF036A0BB7FB74F3804DACEB9876375112C29 (void);
// 0x000001B9 System.Void BearMatch.GameplayUi::TouchStartMovePlayer()
extern void GameplayUi_TouchStartMovePlayer_mF90074BA38DF57285DD19456B15D106ECF5DD138 (void);
// 0x000001BA System.Void BearMatch.GameplayUi::StartMovePlayer()
extern void GameplayUi_StartMovePlayer_mF829C4B8255A7EE66FDC4D6A23917FADEAECC4EE (void);
// 0x000001BB System.Void BearMatch.GameplayUi::UpdatePlayerVelocity(UnityEngine.Vector2)
extern void GameplayUi_UpdatePlayerVelocity_m1DCF9607722C9B76EE8ACE8DCCC973C4B0568593 (void);
// 0x000001BC System.Void BearMatch.GameplayUi::EndMovePlayer()
extern void GameplayUi_EndMovePlayer_m70EAA2F50F24E27901FD27FFC09A7B4C2FD344B2 (void);
// 0x000001BD System.Void BearMatch.GameplayUi::ShowGameOverPopup()
extern void GameplayUi_ShowGameOverPopup_m0225E33C23CB41215BF9945AC5CF1F68E0D5C7BB (void);
// 0x000001BE System.Void BearMatch.GameplayUi::ReplayLevel()
extern void GameplayUi_ReplayLevel_m7232C8598301DA77BF360E12CE7425260DF9057A (void);
// 0x000001BF System.Void BearMatch.GameplayUi::ShowVictory()
extern void GameplayUi_ShowVictory_m7A6E93997A80B9B6A7586390347B290FAA447366 (void);
// 0x000001C0 System.Void BearMatch.GameplayUi::ShowVictoryPopup()
extern void GameplayUi_ShowVictoryPopup_m8E5F28579A032EDDDA300BFFA5BC0A2E6AD88F0F (void);
// 0x000001C1 System.Void BearMatch.GameplayUi::NextLevel()
extern void GameplayUi_NextLevel_mC41BDB81C686BABB8CDFE93A1AB7A893017FFA87 (void);
// 0x000001C2 System.Void BearMatch.GameplayUi::.ctor()
extern void GameplayUi__ctor_m0BBAE7BF4044520413024F21FE9DB9F57569172C (void);
// 0x000001C3 System.Void BearMatch.GridItem::InitializeNewGame(UnityEngine.Vector3)
extern void GridItem_InitializeNewGame_mF918C75057B3013A4108DD921E699F03799B284A (void);
// 0x000001C4 System.Void BearMatch.GridItem::ClearDecal(System.Boolean)
extern void GridItem_ClearDecal_mDF706A3E7C63BE07F2E89ACBFF6FDCA46817F657 (void);
// 0x000001C5 System.Void BearMatch.GridItem::ChangeDecal(System.Int32,UnityEngine.Sprite)
extern void GridItem_ChangeDecal_m6BBC232DBB6616A925C9472F116579F31BC588C3 (void);
// 0x000001C6 System.Void BearMatch.GridItem::MoveDown(System.Int32,System.Int32&)
extern void GridItem_MoveDown_mAC292214479CC5FD19D41436F0A3D6B55225D37C (void);
// 0x000001C7 System.Void BearMatch.GridItem::ShowArrowTarget(System.Int32)
extern void GridItem_ShowArrowTarget_m3F0A1FB4B205D8A920ABF25C16924A028FFF2142 (void);
// 0x000001C8 System.Void BearMatch.GridItem::ClearDecalForNextTurn()
extern void GridItem_ClearDecalForNextTurn_m1D5A1E6CA68637BF205098FD7ABDD4AFF643DD68 (void);
// 0x000001C9 System.Void BearMatch.GridItem::GridItemMoveUpDone()
extern void GridItem_GridItemMoveUpDone_m246DA44FFF388145E7961E0D176534A374981263 (void);
// 0x000001CA System.Void BearMatch.GridItem::.ctor()
extern void GridItem__ctor_m2CFCF86A7372B7271C2DB543CE2BD38A24A79E59 (void);
// 0x000001CB System.Collections.IEnumerator BearMatch.Loading::Start()
extern void Loading_Start_m998640A617BC3A44E3DE0D20B2498943C988AC29 (void);
// 0x000001CC System.Void BearMatch.Loading::.ctor()
extern void Loading__ctor_m43279C8AAC4675B5EE1BB5D6267BDF729A717A46 (void);
// 0x000001CD System.Void BearMatch.Loading/<Start>d__1::.ctor(System.Int32)
extern void U3CStartU3Ed__1__ctor_m864F5B8B33AAC4DEA6CF421DE2F5A75483E9B1E9 (void);
// 0x000001CE System.Void BearMatch.Loading/<Start>d__1::System.IDisposable.Dispose()
extern void U3CStartU3Ed__1_System_IDisposable_Dispose_m7F24EDF6096F489C112C5CBCB0A137680B45E4E5 (void);
// 0x000001CF System.Boolean BearMatch.Loading/<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_mD65854DB289F93F3C72DA860C2D5C49A8035A192 (void);
// 0x000001D0 System.Object BearMatch.Loading/<Start>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEBE57B8DA077676C34D1CA125FC9AD4248D00169 (void);
// 0x000001D1 System.Void BearMatch.Loading/<Start>d__1::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m26669B3362F9DB43F6AC567BDF7C14C8ADE86535 (void);
// 0x000001D2 System.Object BearMatch.Loading/<Start>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_m4779BD9000DD699B0DCC0E12A636242FEF626CEE (void);
// 0x000001D3 BearMatch.PlayerController BearMatch.PlayerController::get_Instance()
extern void PlayerController_get_Instance_mB19A099B11BE20743A0BB9F41C3D639F1A6BFA70 (void);
// 0x000001D4 System.Void BearMatch.PlayerController::set_Instance(BearMatch.PlayerController)
extern void PlayerController_set_Instance_m4E00BA49BA02B0A9B4D32971BDA08D139D8A1034 (void);
// 0x000001D5 UnityEngine.Transform BearMatch.PlayerController::get_Center()
extern void PlayerController_get_Center_m4B62D63D0AAEAFD6955D15326B2B765629DDF6AA (void);
// 0x000001D6 System.Void BearMatch.PlayerController::Awake()
extern void PlayerController_Awake_m5DFAF4F3BFA3B5311F241FA25EE1793A4BC131BE (void);
// 0x000001D7 System.Void BearMatch.PlayerController::Start()
extern void PlayerController_Start_m9B0DA31712414AD3D20CF3B59564BD724B2C9A89 (void);
// 0x000001D8 System.Void BearMatch.PlayerController::OnDestroy()
extern void PlayerController_OnDestroy_m5CAC8BE95C1DF6A3F1FC73F8DD4A73BCE93BD018 (void);
// 0x000001D9 System.Void BearMatch.PlayerController::OnValidate()
extern void PlayerController_OnValidate_mDC0A235B16751A2C7DB8BBD189D2607AF2E590FF (void);
// 0x000001DA System.Void BearMatch.PlayerController::InitializeNewGame()
extern void PlayerController_InitializeNewGame_m929DB965BC52AC34D8E3B70F9BB7D73FC981FC28 (void);
// 0x000001DB System.Collections.IEnumerator BearMatch.PlayerController::IECheckGroundAndLockBody()
extern void PlayerController_IECheckGroundAndLockBody_m9BBA76E0F715A10948D4086063331E0E1EF4C667 (void);
// 0x000001DC System.Void BearMatch.PlayerController::StartNewLevel()
extern void PlayerController_StartNewLevel_m4520BAB3F2D1C5D82AFEB6AA5C07F6E322A5E8DB (void);
// 0x000001DD System.Void BearMatch.PlayerController::Update()
extern void PlayerController_Update_mE70BAEE8A63A25396D1777AEB9A5FF963A3D243E (void);
// 0x000001DE System.Void BearMatch.PlayerController::StartMovePlayer()
extern void PlayerController_StartMovePlayer_m17A42485CF162C5D48B8A7B87598A6B6619CB657 (void);
// 0x000001DF System.Void BearMatch.PlayerController::UpdatePlayerVelocity(UnityEngine.Vector2)
extern void PlayerController_UpdatePlayerVelocity_m1D655B28440C6B61D955BB9F485BC6D17B6FAAC8 (void);
// 0x000001E0 System.Void BearMatch.PlayerController::EndMovePlayer()
extern void PlayerController_EndMovePlayer_m3518E29720576DAA6ACE90A19F20874F199E8A0F (void);
// 0x000001E1 System.Void BearMatch.PlayerController::TriggerUnderground()
extern void PlayerController_TriggerUnderground_m4648108AFA8E4A593A1579A9B9B5407A381052A0 (void);
// 0x000001E2 System.Void BearMatch.PlayerController::ToggleRagdoll(System.Boolean)
extern void PlayerController_ToggleRagdoll_m4439CBB2AD4D8D3FD516138AB096FEAC4762BB0E (void);
// 0x000001E3 System.Void BearMatch.PlayerController::OnPlayerVictory(System.Object)
extern void PlayerController_OnPlayerVictory_m826D1089F1EEAE07D12DBA7FCBD9EC76AB5AB702 (void);
// 0x000001E4 System.Void BearMatch.PlayerController::.ctor()
extern void PlayerController__ctor_mE41EF85DE4B7C1221BFCE9A0EB905589DA3AF0FB (void);
// 0x000001E5 System.Void BearMatch.PlayerController/<IECheckGroundAndLockBody>d__30::.ctor(System.Int32)
extern void U3CIECheckGroundAndLockBodyU3Ed__30__ctor_mDE7250DF56AC98F83DB7D346A045B8B9577EF446 (void);
// 0x000001E6 System.Void BearMatch.PlayerController/<IECheckGroundAndLockBody>d__30::System.IDisposable.Dispose()
extern void U3CIECheckGroundAndLockBodyU3Ed__30_System_IDisposable_Dispose_m7EEF19C46D50BEB22CBB0FD550CC4AED62D39069 (void);
// 0x000001E7 System.Boolean BearMatch.PlayerController/<IECheckGroundAndLockBody>d__30::MoveNext()
extern void U3CIECheckGroundAndLockBodyU3Ed__30_MoveNext_m5A1E34E7ABC24B60DC7C46D4D5C692395112D055 (void);
// 0x000001E8 System.Object BearMatch.PlayerController/<IECheckGroundAndLockBody>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIECheckGroundAndLockBodyU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0279C253745248631D16A0148B54958D9126AE3 (void);
// 0x000001E9 System.Void BearMatch.PlayerController/<IECheckGroundAndLockBody>d__30::System.Collections.IEnumerator.Reset()
extern void U3CIECheckGroundAndLockBodyU3Ed__30_System_Collections_IEnumerator_Reset_m02741FFAF6CA0649E6B2F146837A0D7CF0782C3E (void);
// 0x000001EA System.Object BearMatch.PlayerController/<IECheckGroundAndLockBody>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CIECheckGroundAndLockBodyU3Ed__30_System_Collections_IEnumerator_get_Current_m22CA11B05636CF5FD1B42510FBF70ADA7EDA5D88 (void);
// 0x000001EB System.Void BearMatch.GameOverPopup::Show(BearMatch.GamePopup,System.Boolean,System.Object,UnityEngine.Events.UnityAction,System.Boolean)
extern void GameOverPopup_Show_m4C50E277EF71C53499804F7AAFE296EF0FD630CB (void);
// 0x000001EC System.Void BearMatch.GameOverPopup::Click_Replay()
extern void GameOverPopup_Click_Replay_mE002AA6CDD7ABE1E16D442F3253FD6746E7EFCF2 (void);
// 0x000001ED System.Void BearMatch.GameOverPopup::.ctor()
extern void GameOverPopup__ctor_mEA81834FF30817B803C82CBA1BD62FAEC3226B92 (void);
// 0x000001EE System.Void BearMatch.NewSkinPopup::Show(BearMatch.GamePopup,System.Boolean,System.Object,UnityEngine.Events.UnityAction,System.Boolean)
extern void NewSkinPopup_Show_m5AE038DA4E2B542040BD07199407031B4DE80CFF (void);
// 0x000001EF System.Void BearMatch.NewSkinPopup::ShowNoThankText()
extern void NewSkinPopup_ShowNoThankText_mD3D3E92CA8C91AB92CBBA16BC640C015F9E87CE6 (void);
// 0x000001F0 System.Void BearMatch.NewSkinPopup::Click_Claim()
extern void NewSkinPopup_Click_Claim_m352284211152DB4BDCBE21B1A6247F2E4F71BA80 (void);
// 0x000001F1 System.Void BearMatch.NewSkinPopup::WatchVideoDone()
extern void NewSkinPopup_WatchVideoDone_mE615A786DA4BA174EE2751F5C5C188F03B41ACF8 (void);
// 0x000001F2 System.Void BearMatch.NewSkinPopup::WatchVideoFail()
extern void NewSkinPopup_WatchVideoFail_m9B764907FF45824D12DECA559DF638B792EACED3 (void);
// 0x000001F3 System.Void BearMatch.NewSkinPopup::.ctor()
extern void NewSkinPopup__ctor_m41E0332BBA22AC9156FAA82B3314C27042ADC093 (void);
// 0x000001F4 System.Void BearMatch.NoInternetPopup::Show(BearMatch.GamePopup,System.Boolean,System.Object,UnityEngine.Events.UnityAction,System.Boolean)
extern void NoInternetPopup_Show_mF63C5B2A3D87A13C4AE869C2D0506904C513A98B (void);
// 0x000001F5 System.Void BearMatch.NoInternetPopup::Update()
extern void NoInternetPopup_Update_mDFCF21D3F9B5664C339CA71BCA32FF439E319ACD (void);
// 0x000001F6 System.Void BearMatch.NoInternetPopup::.ctor()
extern void NoInternetPopup__ctor_m801EAC154D86FC15E87DEDC8A5E98E269B59C2C4 (void);
// 0x000001F7 System.Void BearMatch.RatePopup::Show(BearMatch.GamePopup,System.Boolean,System.Object,UnityEngine.Events.UnityAction,System.Boolean)
extern void RatePopup_Show_mA124C20F7A78CDAC96C4DDC5C93923C61E5B78C3 (void);
// 0x000001F8 System.Void BearMatch.RatePopup::Click_Rate(System.Int32)
extern void RatePopup_Click_Rate_m78E55570D11330EFB1E6AA108AB5FCD610282528 (void);
// 0x000001F9 System.Collections.IEnumerator BearMatch.RatePopup::IEShowRate()
extern void RatePopup_IEShowRate_m9DF447E40E270CB3B62FF9BA4BD76983C30D2584 (void);
// 0x000001FA System.Void BearMatch.RatePopup::RateAndClosePopup()
extern void RatePopup_RateAndClosePopup_m395DECB6EE1108AC40B226421F0105FE30EFDD5D (void);
// 0x000001FB System.Void BearMatch.RatePopup::.ctor()
extern void RatePopup__ctor_mE0160FC549251FF019952A7BAB75FBDEF56D4E10 (void);
// 0x000001FC System.Void BearMatch.RatePopup/<IEShowRate>d__4::.ctor(System.Int32)
extern void U3CIEShowRateU3Ed__4__ctor_m0C70538881DDC968226D492A1EA2B49D4A5E2C5F (void);
// 0x000001FD System.Void BearMatch.RatePopup/<IEShowRate>d__4::System.IDisposable.Dispose()
extern void U3CIEShowRateU3Ed__4_System_IDisposable_Dispose_m8F42D05A9AA9DFDF1E3B156693237C5FEDC73FDD (void);
// 0x000001FE System.Boolean BearMatch.RatePopup/<IEShowRate>d__4::MoveNext()
extern void U3CIEShowRateU3Ed__4_MoveNext_mBE8B7317A5434405025524F4D255CBDB89387AD0 (void);
// 0x000001FF System.Object BearMatch.RatePopup/<IEShowRate>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIEShowRateU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m84021406DD61193AA3F4D90454712EA90E74DDD2 (void);
// 0x00000200 System.Void BearMatch.RatePopup/<IEShowRate>d__4::System.Collections.IEnumerator.Reset()
extern void U3CIEShowRateU3Ed__4_System_Collections_IEnumerator_Reset_m6D8D467F4F520687B7499331D06CE598B72F6818 (void);
// 0x00000201 System.Object BearMatch.RatePopup/<IEShowRate>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CIEShowRateU3Ed__4_System_Collections_IEnumerator_get_Current_m7ADBC6AD92621B5983D0ABAEB5C846166B9B60D1 (void);
// 0x00000202 System.Void BearMatch.ShopItem::Initialized(BearMatch.ShopPopup,System.Int32)
extern void ShopItem_Initialized_mF2AA21BA8418A2E1922DF0EB2A36A31A14201ABC (void);
// 0x00000203 System.Void BearMatch.ShopItem::Click_ChooseItem()
extern void ShopItem_Click_ChooseItem_m809640D6D614F272C132B026DD78C4D469D26013 (void);
// 0x00000204 System.Void BearMatch.ShopItem::ResetItem()
extern void ShopItem_ResetItem_mEB82CAF476854B1DB354293C0B29DDBB09ED059D (void);
// 0x00000205 System.Void BearMatch.ShopItem::ShowHighlight(System.Boolean)
extern void ShopItem_ShowHighlight_m2B516807C4C9D083D431533165A0332E5304E6BE (void);
// 0x00000206 System.Void BearMatch.ShopItem::.ctor()
extern void ShopItem__ctor_mCE0B80E94DD60C0494E156C359F4CC00FBF6DE77 (void);
// 0x00000207 System.Void BearMatch.ShopPopup::Start()
extern void ShopPopup_Start_m18CAD558301B3FF56ED9936C9CB4063D655396F2 (void);
// 0x00000208 System.Void BearMatch.ShopPopup::Show(BearMatch.GamePopup,System.Boolean,System.Object,UnityEngine.Events.UnityAction,System.Boolean)
extern void ShopPopup_Show_m517068DD5002B62C74973E9E6F3B5303188E11D7 (void);
// 0x00000209 System.Void BearMatch.ShopPopup::Hide(System.Boolean)
extern void ShopPopup_Hide_mD52C1A5CF350CD3C72B3CE93E3986ED5AB7E156B (void);
// 0x0000020A System.Void BearMatch.ShopPopup::Click_Reset()
extern void ShopPopup_Click_Reset_m2519A632D269F487D1E9EF5AD3090026B74BBF14 (void);
// 0x0000020B System.Void BearMatch.ShopPopup::Click_Tab(System.Int32)
extern void ShopPopup_Click_Tab_mCABDFB88C8DF924D7F0BF12C3530C947393F1D61 (void);
// 0x0000020C System.Void BearMatch.ShopPopup::Click_WatchVideoGetCoin()
extern void ShopPopup_Click_WatchVideoGetCoin_mFA2EFC397CDF5A6BE17AD69DC3F6D142C54A8FA4 (void);
// 0x0000020D System.Void BearMatch.ShopPopup::WatchVideoDone()
extern void ShopPopup_WatchVideoDone_m5980D708D3956DCC22C632A977A62AB2454260B7 (void);
// 0x0000020E System.Void BearMatch.ShopPopup::WatchVideoFail()
extern void ShopPopup_WatchVideoFail_m846F571CF28AE93B15C1DD83A818AD85EECCB719 (void);
// 0x0000020F System.Void BearMatch.ShopPopup::Click_BuyRandom()
extern void ShopPopup_Click_BuyRandom_m4313C69A461A048A87B599BE2B90A63CCF6CC2F4 (void);
// 0x00000210 System.Collections.IEnumerator BearMatch.ShopPopup::IEBuyRandom()
extern void ShopPopup_IEBuyRandom_mE77A7939BE76363AE5F8D496A0AADDB28307C450 (void);
// 0x00000211 System.Void BearMatch.ShopPopup::UnlockItem(System.Int32)
extern void ShopPopup_UnlockItem_m6C936424880731F10904649B93C39EEC845EFD4F (void);
// 0x00000212 System.Void BearMatch.ShopPopup::PreviewCharacter(System.Int32)
extern void ShopPopup_PreviewCharacter_m42EB6EE757B1FAA9DB328157FDC20F13CAAF3FA9 (void);
// 0x00000213 System.Void BearMatch.ShopPopup::SelectCharacter()
extern void ShopPopup_SelectCharacter_m3C3BC7137173419574E6D35F2FE18F6F19F1CCD3 (void);
// 0x00000214 System.Void BearMatch.ShopPopup::.ctor()
extern void ShopPopup__ctor_m562F3F0DDC0486385AB877B08B548D96CF6FBCB4 (void);
// 0x00000215 System.Void BearMatch.ShopPopup/<IEBuyRandom>d__18::.ctor(System.Int32)
extern void U3CIEBuyRandomU3Ed__18__ctor_m4B86F3C18AD6C00B0E6DE1EE7CB5B1557A921CC0 (void);
// 0x00000216 System.Void BearMatch.ShopPopup/<IEBuyRandom>d__18::System.IDisposable.Dispose()
extern void U3CIEBuyRandomU3Ed__18_System_IDisposable_Dispose_m6FAC56AFF731B71AF672C03A50232FCA06F5F68E (void);
// 0x00000217 System.Boolean BearMatch.ShopPopup/<IEBuyRandom>d__18::MoveNext()
extern void U3CIEBuyRandomU3Ed__18_MoveNext_m009A399A8C3F26C7B37B78950A709845B67F5ECC (void);
// 0x00000218 System.Object BearMatch.ShopPopup/<IEBuyRandom>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIEBuyRandomU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73BEE0F53A7E6FF34C9EB6D8D75CC73D0766BB25 (void);
// 0x00000219 System.Void BearMatch.ShopPopup/<IEBuyRandom>d__18::System.Collections.IEnumerator.Reset()
extern void U3CIEBuyRandomU3Ed__18_System_Collections_IEnumerator_Reset_m50765866E0B08791487FA8D9EB9BB8AF8BD33BDF (void);
// 0x0000021A System.Object BearMatch.ShopPopup/<IEBuyRandom>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CIEBuyRandomU3Ed__18_System_Collections_IEnumerator_get_Current_m75225B6474DE175624A6153E4E5BE3463F29C376 (void);
// 0x0000021B System.Void BearMatch.StartGamePopup::Show(BearMatch.GamePopup,System.Boolean,System.Object,UnityEngine.Events.UnityAction,System.Boolean)
extern void StartGamePopup_Show_m9D3A3E84E7AAA431ADEC66B5D18C02436DA2EF02 (void);
// 0x0000021C System.Void BearMatch.StartGamePopup::Click_BuyNoAds()
extern void StartGamePopup_Click_BuyNoAds_mE9675077EBB815350A312987E0B40B6C2BECBE86 (void);
// 0x0000021D System.Void BearMatch.StartGamePopup::OnBuyNoAdsCallback(System.Boolean,System.String)
extern void StartGamePopup_OnBuyNoAdsCallback_m38A876B480D745053DD0F97A9929788D784A1C48 (void);
// 0x0000021E System.Void BearMatch.StartGamePopup::Click_Shop()
extern void StartGamePopup_Click_Shop_m0D4EF64547AAE1016563CF857BC22238294E15C1 (void);
// 0x0000021F System.Void BearMatch.StartGamePopup::Click_StartGame()
extern void StartGamePopup_Click_StartGame_mC53F962E8B12718B8B13633CAF7DC20AE5A35F9B (void);
// 0x00000220 System.Void BearMatch.StartGamePopup::.ctor()
extern void StartGamePopup__ctor_mE3D9B27A2F684C73B872298D109783EC7A7A0AF5 (void);
// 0x00000221 System.Void BearMatch.VictoryPopup::Show(BearMatch.GamePopup,System.Boolean,System.Object,UnityEngine.Events.UnityAction,System.Boolean)
extern void VictoryPopup_Show_mEE2B82411B70722D51B9F1FABE015A9C2C3B7242 (void);
// 0x00000222 System.Void BearMatch.VictoryPopup::ShowNewSkinPopup()
extern void VictoryPopup_ShowNewSkinPopup_m41797589F8368BA514E5951B5BDD750D044F234B (void);
// 0x00000223 System.Void BearMatch.VictoryPopup::UpdateCoinWatchVideoText()
extern void VictoryPopup_UpdateCoinWatchVideoText_m587F0B9BCCFD63119513B2C5F2CAA8553E0CCA0A (void);
// 0x00000224 System.Void BearMatch.VictoryPopup::Click_WatchVideo()
extern void VictoryPopup_Click_WatchVideo_m210BD7155AEAD1014401DE44F97D0438354FCCAC (void);
// 0x00000225 System.Void BearMatch.VictoryPopup::WatchVideoDone()
extern void VictoryPopup_WatchVideoDone_m1F389515E858F56B3A2CFDE9ABAA2133B965C657 (void);
// 0x00000226 System.Void BearMatch.VictoryPopup::WatchVideoFail()
extern void VictoryPopup_WatchVideoFail_m159E8151A3C02662329FC1C685AAD64092806F39 (void);
// 0x00000227 System.Void BearMatch.VictoryPopup::Click_NextLevel()
extern void VictoryPopup_Click_NextLevel_m936378422A06DB094591F2D3F9E1241ABAB04C4D (void);
// 0x00000228 System.Void BearMatch.VictoryPopup::NextLevel()
extern void VictoryPopup_NextLevel_m299BC898825785C6696BE78E4EC0E0ED369474FB (void);
// 0x00000229 System.Collections.IEnumerator BearMatch.VictoryPopup::IEShowCoinFly()
extern void VictoryPopup_IEShowCoinFly_mF4EC2FBD1FCA111047595602E3D90CE6F0F397E0 (void);
// 0x0000022A System.Void BearMatch.VictoryPopup::.ctor()
extern void VictoryPopup__ctor_mBB98EBC2C4F653C4E66C7EFB0BA72812DCB403F4 (void);
// 0x0000022B System.Void BearMatch.VictoryPopup::<ShowNewSkinPopup>b__25_0()
extern void VictoryPopup_U3CShowNewSkinPopupU3Eb__25_0_m669F7E97415A3F449A38B01C245A566E47508701 (void);
// 0x0000022C System.Void BearMatch.VictoryPopup/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_mDF2145ABD22C021106B4502508F0E631159A080F (void);
// 0x0000022D System.Void BearMatch.VictoryPopup/<>c__DisplayClass32_0::<IEShowCoinFly>b__2()
extern void U3CU3Ec__DisplayClass32_0_U3CIEShowCoinFlyU3Eb__2_mC6272C40CD1D56277DB6B9ED9B030523815A3D44 (void);
// 0x0000022E System.Boolean BearMatch.VictoryPopup/<>c__DisplayClass32_0::<IEShowCoinFly>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CIEShowCoinFlyU3Eb__0_mE66117767C5DF2160EC5620D9C655C8ACBE6B382 (void);
// 0x0000022F System.Void BearMatch.VictoryPopup/<>c__DisplayClass32_0::<IEShowCoinFly>b__3()
extern void U3CU3Ec__DisplayClass32_0_U3CIEShowCoinFlyU3Eb__3_mF57D4E231715DC507C0E82BC72D63C49CD548F36 (void);
// 0x00000230 System.Boolean BearMatch.VictoryPopup/<>c__DisplayClass32_0::<IEShowCoinFly>b__1()
extern void U3CU3Ec__DisplayClass32_0_U3CIEShowCoinFlyU3Eb__1_m5F1FD923588400E27F61B48B5966FB8A2CA5AB24 (void);
// 0x00000231 System.Void BearMatch.VictoryPopup/<IEShowCoinFly>d__32::.ctor(System.Int32)
extern void U3CIEShowCoinFlyU3Ed__32__ctor_m75E10487EC1DD27238E40A287D945E64267E57BF (void);
// 0x00000232 System.Void BearMatch.VictoryPopup/<IEShowCoinFly>d__32::System.IDisposable.Dispose()
extern void U3CIEShowCoinFlyU3Ed__32_System_IDisposable_Dispose_m7F45A091903276769DE528507A8F082705D7D1F2 (void);
// 0x00000233 System.Boolean BearMatch.VictoryPopup/<IEShowCoinFly>d__32::MoveNext()
extern void U3CIEShowCoinFlyU3Ed__32_MoveNext_mBECD9442FD86363B3F2F6A8A8FB954377BD21A5A (void);
// 0x00000234 System.Object BearMatch.VictoryPopup/<IEShowCoinFly>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIEShowCoinFlyU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m00DB0CFC4F42158A1D2FF7276F05FDDACC26038E (void);
// 0x00000235 System.Void BearMatch.VictoryPopup/<IEShowCoinFly>d__32::System.Collections.IEnumerator.Reset()
extern void U3CIEShowCoinFlyU3Ed__32_System_Collections_IEnumerator_Reset_mEB6631672C7E07722BB51DB65CB8C6204B4B5F8C (void);
// 0x00000236 System.Object BearMatch.VictoryPopup/<IEShowCoinFly>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CIEShowCoinFlyU3Ed__32_System_Collections_IEnumerator_get_Current_m0A39B2305BEACE7C43E67A53F5F5AD18EF911775 (void);
// 0x00000237 System.Void BearMatch.WarningPopup::Show(BearMatch.GamePopup,System.Boolean,System.Object,UnityEngine.Events.UnityAction,System.Boolean)
extern void WarningPopup_Show_m55EEA34542F592AC7CDD8C42AD88681EF0D028D0 (void);
// 0x00000238 System.Void BearMatch.WarningPopup::ShowText(System.String)
extern void WarningPopup_ShowText_m98D05CAC6E2CD6C06E179016CFE51A443E9256DF (void);
// 0x00000239 System.Void BearMatch.WarningPopup::.ctor()
extern void WarningPopup__ctor_mE88F18EDAF6F651D78FFD7E84F1F667B57F6ACBD (void);
// 0x0000023A System.Void BearMatch.PushBackBot::OnCollisionEnter(UnityEngine.Collision)
extern void PushBackBot_OnCollisionEnter_m2E5723663A2788594B3C0E5136794157CF14477A (void);
// 0x0000023B System.Void BearMatch.PushBackBot::.ctor()
extern void PushBackBot__ctor_m117D2B4D47BC1C0C1E9FA13690D69BC6FCD99C9A (void);
// 0x0000023C System.Void BearMatch.UnderGround::OnCollisionEnter(UnityEngine.Collision)
extern void UnderGround_OnCollisionEnter_mB39C382988D1E423C61977026E3A8CB81DC66269 (void);
// 0x0000023D System.Void BearMatch.UnderGround::.ctor()
extern void UnderGround__ctor_m961AF27EA4A52547B682D9D46A5E2C8976BFF420 (void);
// 0x0000023E System.Void com.adjust.sdk.JSONNode::Add(System.String,com.adjust.sdk.JSONNode)
extern void JSONNode_Add_mA73D290ED9AFFE3F42861A87D20DD066CBF02BA3 (void);
// 0x0000023F com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::get_Item(System.Int32)
extern void JSONNode_get_Item_mD7BBCD16DB5D0500676559AFB2A2594B0103D8F3 (void);
// 0x00000240 System.Void com.adjust.sdk.JSONNode::set_Item(System.Int32,com.adjust.sdk.JSONNode)
extern void JSONNode_set_Item_mD4F5BEDADBDB66141EC0EB6F3E8E0C04A481CD6B (void);
// 0x00000241 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::get_Item(System.String)
extern void JSONNode_get_Item_m85A1C5F8B26938A5BDD5CBA14C84E4A37B3D4ACF (void);
// 0x00000242 System.Void com.adjust.sdk.JSONNode::set_Item(System.String,com.adjust.sdk.JSONNode)
extern void JSONNode_set_Item_mD9F8F18C2B5F9054A722B4CFD8129E2C48A4BE5D (void);
// 0x00000243 System.String com.adjust.sdk.JSONNode::get_Value()
extern void JSONNode_get_Value_m422474020628AD90572986DC9999B5E33D51044F (void);
// 0x00000244 System.Void com.adjust.sdk.JSONNode::set_Value(System.String)
extern void JSONNode_set_Value_mF7DD7A968B9CE3DE2DEC99CFBB8F6A0035DA0BBC (void);
// 0x00000245 System.Int32 com.adjust.sdk.JSONNode::get_Count()
extern void JSONNode_get_Count_m6A898D357D50A5C2E8042AFCB3D0E795B3F0FB25 (void);
// 0x00000246 System.Void com.adjust.sdk.JSONNode::Add(com.adjust.sdk.JSONNode)
extern void JSONNode_Add_m8EE67AF49568BB6CC3DD0968C5E7E206EF3F4A99 (void);
// 0x00000247 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::Remove(System.String)
extern void JSONNode_Remove_mCC44CF5159F5A8661D1906F9444477EF5C990B71 (void);
// 0x00000248 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::Remove(System.Int32)
extern void JSONNode_Remove_m404F68BE1FB04E8F4B50EADF3490CAC177BB57FA (void);
// 0x00000249 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::Remove(com.adjust.sdk.JSONNode)
extern void JSONNode_Remove_m68BB8C3076B8DE3EB84D43AC8FEFA4057F2973A1 (void);
// 0x0000024A System.Collections.Generic.IEnumerable`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONNode::get_Childs()
extern void JSONNode_get_Childs_m041DDDFC000548A689CE29D3288D53AA643C3482 (void);
// 0x0000024B System.Collections.Generic.IEnumerable`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONNode::get_DeepChilds()
extern void JSONNode_get_DeepChilds_m44F560506E902A0B0DABB6B82562D127259FEFC5 (void);
// 0x0000024C System.String com.adjust.sdk.JSONNode::ToString()
extern void JSONNode_ToString_mCE094AA189C11D45C5915FA3D229C9FD1076A6E1 (void);
// 0x0000024D System.String com.adjust.sdk.JSONNode::ToString(System.String)
extern void JSONNode_ToString_m729A82FD7DD7412637589CDE0158F98B5E7C14C2 (void);
// 0x0000024E System.Int32 com.adjust.sdk.JSONNode::get_AsInt()
extern void JSONNode_get_AsInt_m06D351315628BE80806748B43605699EBD2724C7 (void);
// 0x0000024F System.Void com.adjust.sdk.JSONNode::set_AsInt(System.Int32)
extern void JSONNode_set_AsInt_m351063CC07FA09FFBF85FB543D40ADC1F90B751A (void);
// 0x00000250 System.Single com.adjust.sdk.JSONNode::get_AsFloat()
extern void JSONNode_get_AsFloat_m18888F5DF88D1ABD784270641319B7ABDDDBE9E6 (void);
// 0x00000251 System.Void com.adjust.sdk.JSONNode::set_AsFloat(System.Single)
extern void JSONNode_set_AsFloat_mD796198844BF326B7288864EB6C4D5AD4E510E9C (void);
// 0x00000252 System.Double com.adjust.sdk.JSONNode::get_AsDouble()
extern void JSONNode_get_AsDouble_m3CBB3A48ADA115310C2187857B1C71ABC7806752 (void);
// 0x00000253 System.Void com.adjust.sdk.JSONNode::set_AsDouble(System.Double)
extern void JSONNode_set_AsDouble_mA5DA00CF265864C946AA28CCBAC8A7FF4D22BA76 (void);
// 0x00000254 System.Boolean com.adjust.sdk.JSONNode::get_AsBool()
extern void JSONNode_get_AsBool_m8D64DD4B3FC875095E283DBE5AC867003123635F (void);
// 0x00000255 System.Void com.adjust.sdk.JSONNode::set_AsBool(System.Boolean)
extern void JSONNode_set_AsBool_mB969B70E7AF09CCF88B79BCF1FBFC03D9F8DB6AD (void);
// 0x00000256 com.adjust.sdk.JSONArray com.adjust.sdk.JSONNode::get_AsArray()
extern void JSONNode_get_AsArray_m7B937E045E18295E048B40D1B7C3A58E01F710F3 (void);
// 0x00000257 com.adjust.sdk.JSONClass com.adjust.sdk.JSONNode::get_AsObject()
extern void JSONNode_get_AsObject_m09F640638EA27AE9A75A66D413209E1B0A4C1700 (void);
// 0x00000258 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::op_Implicit(System.String)
extern void JSONNode_op_Implicit_mC14E8B965CA6EBB9FFF12F7808330C926C5B1C18 (void);
// 0x00000259 System.String com.adjust.sdk.JSONNode::op_Implicit(com.adjust.sdk.JSONNode)
extern void JSONNode_op_Implicit_m8ECC47F8ACF595260FD729E531D9E38998604EDD (void);
// 0x0000025A System.Boolean com.adjust.sdk.JSONNode::op_Equality(com.adjust.sdk.JSONNode,System.Object)
extern void JSONNode_op_Equality_m70E93DF433C90502AAC76D837057CEF1FE04F7C6 (void);
// 0x0000025B System.Boolean com.adjust.sdk.JSONNode::op_Inequality(com.adjust.sdk.JSONNode,System.Object)
extern void JSONNode_op_Inequality_m886F271FA8B6F834DC1DE8E5B3F2EDF1C88D90DC (void);
// 0x0000025C System.Boolean com.adjust.sdk.JSONNode::Equals(System.Object)
extern void JSONNode_Equals_m024E691480E99CC5FCAC978BEA06E3C89AC936BE (void);
// 0x0000025D System.Int32 com.adjust.sdk.JSONNode::GetHashCode()
extern void JSONNode_GetHashCode_m205B8B106B815AF3CFC5FFAC4A4E253452BC08E5 (void);
// 0x0000025E System.String com.adjust.sdk.JSONNode::Escape(System.String)
extern void JSONNode_Escape_m2E11BD8B7B358628AC0E369DD34DB2FF35FE47C0 (void);
// 0x0000025F com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::Parse(System.String)
extern void JSONNode_Parse_m3FD7EA73C960EEDE36C94D065292C1B1885174CE (void);
// 0x00000260 System.Void com.adjust.sdk.JSONNode::Serialize(System.IO.BinaryWriter)
extern void JSONNode_Serialize_m7B9E652622D50367BB8DB62252A0ADDBD2A02A4F (void);
// 0x00000261 System.Void com.adjust.sdk.JSONNode::SaveToStream(System.IO.Stream)
extern void JSONNode_SaveToStream_mAAF83974E2DB60FD527965E28263B1202E195C27 (void);
// 0x00000262 System.Void com.adjust.sdk.JSONNode::SaveToCompressedStream(System.IO.Stream)
extern void JSONNode_SaveToCompressedStream_mDFE8495C0252DA781F53FB62A4E580D9267594B1 (void);
// 0x00000263 System.Void com.adjust.sdk.JSONNode::SaveToCompressedFile(System.String)
extern void JSONNode_SaveToCompressedFile_m5EE5A35989E44A29862AC7BA21F5C78179BCBD71 (void);
// 0x00000264 System.String com.adjust.sdk.JSONNode::SaveToCompressedBase64()
extern void JSONNode_SaveToCompressedBase64_mC5D7414CD46FEBB67BB8567FDA778D453B87FA7B (void);
// 0x00000265 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::Deserialize(System.IO.BinaryReader)
extern void JSONNode_Deserialize_m330FFC061D9EDF8F293DD532D9B3BEF6AA6F2D99 (void);
// 0x00000266 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::LoadFromCompressedFile(System.String)
extern void JSONNode_LoadFromCompressedFile_m631041CF9B4776DDE4BC0701EAEE4D6AA3A846AD (void);
// 0x00000267 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::LoadFromCompressedStream(System.IO.Stream)
extern void JSONNode_LoadFromCompressedStream_m0416ACF556E3858060290AFD998C1A2B2F9523D4 (void);
// 0x00000268 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::LoadFromCompressedBase64(System.String)
extern void JSONNode_LoadFromCompressedBase64_m8F4558A2A4D57BD495A1226DD3B4DAEC58102CC9 (void);
// 0x00000269 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::LoadFromStream(System.IO.Stream)
extern void JSONNode_LoadFromStream_m82D468E397895F05A4799A6F217B68D0FC80D964 (void);
// 0x0000026A com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::LoadFromBase64(System.String)
extern void JSONNode_LoadFromBase64_m34262C804F04DF3ABC9FE04AC775F42EDEF37EBD (void);
// 0x0000026B System.Void com.adjust.sdk.JSONNode::.ctor()
extern void JSONNode__ctor_mCC776C44E5B26CB440CD077D35CF116A45EAE5EA (void);
// 0x0000026C System.Void com.adjust.sdk.JSONNode/<get_Childs>d__17::.ctor(System.Int32)
extern void U3Cget_ChildsU3Ed__17__ctor_mF797A836A6E3340B1BAAAF50AEEC10DFA89DAE73 (void);
// 0x0000026D System.Void com.adjust.sdk.JSONNode/<get_Childs>d__17::System.IDisposable.Dispose()
extern void U3Cget_ChildsU3Ed__17_System_IDisposable_Dispose_m94877C8D1DA5BE6EA246389F10F4C9C7AF366A74 (void);
// 0x0000026E System.Boolean com.adjust.sdk.JSONNode/<get_Childs>d__17::MoveNext()
extern void U3Cget_ChildsU3Ed__17_MoveNext_mE6F86033D5481E4036487E92E0F64B9840CB794B (void);
// 0x0000026F com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode/<get_Childs>d__17::System.Collections.Generic.IEnumerator<com.adjust.sdk.JSONNode>.get_Current()
extern void U3Cget_ChildsU3Ed__17_System_Collections_Generic_IEnumeratorU3Ccom_adjust_sdk_JSONNodeU3E_get_Current_m54B690F8FA694EE02627B97D48386D9095E229E4 (void);
// 0x00000270 System.Void com.adjust.sdk.JSONNode/<get_Childs>d__17::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildsU3Ed__17_System_Collections_IEnumerator_Reset_m0366D0A88F5A1FBA81AE3F732F855C0CAE045788 (void);
// 0x00000271 System.Object com.adjust.sdk.JSONNode/<get_Childs>d__17::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildsU3Ed__17_System_Collections_IEnumerator_get_Current_m38B1B4A7F33CF7834BB44D72C8FED6220D1C75BD (void);
// 0x00000272 System.Collections.Generic.IEnumerator`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONNode/<get_Childs>d__17::System.Collections.Generic.IEnumerable<com.adjust.sdk.JSONNode>.GetEnumerator()
extern void U3Cget_ChildsU3Ed__17_System_Collections_Generic_IEnumerableU3Ccom_adjust_sdk_JSONNodeU3E_GetEnumerator_m071E822DD6D0A22882CDAA62B63D9660FF1776C0 (void);
// 0x00000273 System.Collections.IEnumerator com.adjust.sdk.JSONNode/<get_Childs>d__17::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildsU3Ed__17_System_Collections_IEnumerable_GetEnumerator_mA892F96D61A7E4D40CBA9B2CF48F34079F726849 (void);
// 0x00000274 System.Void com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::.ctor(System.Int32)
extern void U3Cget_DeepChildsU3Ed__19__ctor_m046016CC55A4C7B9C1FE5AE10DD72E4050826BAC (void);
// 0x00000275 System.Void com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::System.IDisposable.Dispose()
extern void U3Cget_DeepChildsU3Ed__19_System_IDisposable_Dispose_mDCF0787A78165575446D46E5C1CD1A8A10FF7018 (void);
// 0x00000276 System.Boolean com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::MoveNext()
extern void U3Cget_DeepChildsU3Ed__19_MoveNext_m9F51E57FD6F93E690C029855DAA59D591E773022 (void);
// 0x00000277 System.Void com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::<>m__Finally1()
extern void U3Cget_DeepChildsU3Ed__19_U3CU3Em__Finally1_m44FF129641F4B9A288B22CC405CFDB01813AF413 (void);
// 0x00000278 System.Void com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::<>m__Finally2()
extern void U3Cget_DeepChildsU3Ed__19_U3CU3Em__Finally2_m50CD9A7591C11FE67D4AE24614BD3B979B030C7F (void);
// 0x00000279 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::System.Collections.Generic.IEnumerator<com.adjust.sdk.JSONNode>.get_Current()
extern void U3Cget_DeepChildsU3Ed__19_System_Collections_Generic_IEnumeratorU3Ccom_adjust_sdk_JSONNodeU3E_get_Current_mB46DB5D77ECC4B15D6B4BA4ED0A03F21CE95D25D (void);
// 0x0000027A System.Void com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::System.Collections.IEnumerator.Reset()
extern void U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerator_Reset_m2862E593874F11BCC026302EFC96AFDE00AB5A06 (void);
// 0x0000027B System.Object com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::System.Collections.IEnumerator.get_Current()
extern void U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerator_get_Current_mABF1413C57FA5A3C1651355AB4A5E4C0402E27DA (void);
// 0x0000027C System.Collections.Generic.IEnumerator`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::System.Collections.Generic.IEnumerable<com.adjust.sdk.JSONNode>.GetEnumerator()
extern void U3Cget_DeepChildsU3Ed__19_System_Collections_Generic_IEnumerableU3Ccom_adjust_sdk_JSONNodeU3E_GetEnumerator_mA1AC5268F717D42575E2B41A69ED09A0C60071B0 (void);
// 0x0000027D System.Collections.IEnumerator com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerable_GetEnumerator_m481F9219BE5FB9B09E8DFC02714ED34393641102 (void);
// 0x0000027E com.adjust.sdk.JSONNode com.adjust.sdk.JSONArray::get_Item(System.Int32)
extern void JSONArray_get_Item_m755E3E8E6725A4FBF408E5BD1A8432C419410503 (void);
// 0x0000027F System.Void com.adjust.sdk.JSONArray::set_Item(System.Int32,com.adjust.sdk.JSONNode)
extern void JSONArray_set_Item_mC4EF1F71394FFDFAE7153C0AC8E698EE4AA5B9F5 (void);
// 0x00000280 com.adjust.sdk.JSONNode com.adjust.sdk.JSONArray::get_Item(System.String)
extern void JSONArray_get_Item_m70C52946E9D9B5DE99DBE66423561B152ECCB454 (void);
// 0x00000281 System.Void com.adjust.sdk.JSONArray::set_Item(System.String,com.adjust.sdk.JSONNode)
extern void JSONArray_set_Item_m2AB7F7C0919BABC722D5262F62960F58A0BCCFDE (void);
// 0x00000282 System.Int32 com.adjust.sdk.JSONArray::get_Count()
extern void JSONArray_get_Count_m810A578C961B778B760E51A3D0111CFB8152864B (void);
// 0x00000283 System.Void com.adjust.sdk.JSONArray::Add(System.String,com.adjust.sdk.JSONNode)
extern void JSONArray_Add_m11ED297B0CE157395B41622C3A70577A4CCDEE1B (void);
// 0x00000284 com.adjust.sdk.JSONNode com.adjust.sdk.JSONArray::Remove(System.Int32)
extern void JSONArray_Remove_m294856B07209D367010855B3B71CEACD50B8F6AF (void);
// 0x00000285 com.adjust.sdk.JSONNode com.adjust.sdk.JSONArray::Remove(com.adjust.sdk.JSONNode)
extern void JSONArray_Remove_m6C5A0F04019B3D0DC675038B5B04AE38079B786A (void);
// 0x00000286 System.Collections.Generic.IEnumerable`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONArray::get_Childs()
extern void JSONArray_get_Childs_mC749B8F046AD011544966486F60CA9C0C217A874 (void);
// 0x00000287 System.Collections.IEnumerator com.adjust.sdk.JSONArray::GetEnumerator()
extern void JSONArray_GetEnumerator_m4E4EA5BBF194BA5E068CA738E573C73DD1ED5635 (void);
// 0x00000288 System.String com.adjust.sdk.JSONArray::ToString()
extern void JSONArray_ToString_m58F4AFD74189F06A5380F48EB94330B1F62787AB (void);
// 0x00000289 System.String com.adjust.sdk.JSONArray::ToString(System.String)
extern void JSONArray_ToString_m97E2C58394C45CEB1C54965B96C6E9CB19B8005B (void);
// 0x0000028A System.Void com.adjust.sdk.JSONArray::Serialize(System.IO.BinaryWriter)
extern void JSONArray_Serialize_m85EA6F504087F2B91019ADACE0AF84BE56C977DF (void);
// 0x0000028B System.Void com.adjust.sdk.JSONArray::.ctor()
extern void JSONArray__ctor_m6ECA2300A22DEFC3387A72AF03FEC3355B150C4E (void);
// 0x0000028C System.Void com.adjust.sdk.JSONArray/<get_Childs>d__13::.ctor(System.Int32)
extern void U3Cget_ChildsU3Ed__13__ctor_m6474546C7E49FF6A548EFAB592D82CD4C52A3E2B (void);
// 0x0000028D System.Void com.adjust.sdk.JSONArray/<get_Childs>d__13::System.IDisposable.Dispose()
extern void U3Cget_ChildsU3Ed__13_System_IDisposable_Dispose_mE5E9FC725A9C7CB1A97AE4BCF844F2F773000283 (void);
// 0x0000028E System.Boolean com.adjust.sdk.JSONArray/<get_Childs>d__13::MoveNext()
extern void U3Cget_ChildsU3Ed__13_MoveNext_m93338AB8CC1520C49B28F7BA37D0216F8DFE3894 (void);
// 0x0000028F System.Void com.adjust.sdk.JSONArray/<get_Childs>d__13::<>m__Finally1()
extern void U3Cget_ChildsU3Ed__13_U3CU3Em__Finally1_mC2A2DBA3A3B18AB12F36800AD2129AF9CE3B4865 (void);
// 0x00000290 com.adjust.sdk.JSONNode com.adjust.sdk.JSONArray/<get_Childs>d__13::System.Collections.Generic.IEnumerator<com.adjust.sdk.JSONNode>.get_Current()
extern void U3Cget_ChildsU3Ed__13_System_Collections_Generic_IEnumeratorU3Ccom_adjust_sdk_JSONNodeU3E_get_Current_m939A44A15A892D3328145314D6F3BD3180E600F0 (void);
// 0x00000291 System.Void com.adjust.sdk.JSONArray/<get_Childs>d__13::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildsU3Ed__13_System_Collections_IEnumerator_Reset_m9BBCABEA39F9133BC5E7188E8E6346538077025F (void);
// 0x00000292 System.Object com.adjust.sdk.JSONArray/<get_Childs>d__13::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildsU3Ed__13_System_Collections_IEnumerator_get_Current_mAAA548D9FF3C67A7AEF373A19FFAB5C9C3A97C5E (void);
// 0x00000293 System.Collections.Generic.IEnumerator`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONArray/<get_Childs>d__13::System.Collections.Generic.IEnumerable<com.adjust.sdk.JSONNode>.GetEnumerator()
extern void U3Cget_ChildsU3Ed__13_System_Collections_Generic_IEnumerableU3Ccom_adjust_sdk_JSONNodeU3E_GetEnumerator_m3C0A4675A2FFA0A86616539694B4392F7A86B4D6 (void);
// 0x00000294 System.Collections.IEnumerator com.adjust.sdk.JSONArray/<get_Childs>d__13::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildsU3Ed__13_System_Collections_IEnumerable_GetEnumerator_m208AD3C417C7CAABDDEABD4EE49670B1E6403926 (void);
// 0x00000295 System.Void com.adjust.sdk.JSONArray/<GetEnumerator>d__14::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__14__ctor_m0695479939904B620F4C38973708FCF84941755E (void);
// 0x00000296 System.Void com.adjust.sdk.JSONArray/<GetEnumerator>d__14::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__14_System_IDisposable_Dispose_mDCC768ED829C36509C1AE08EC89EDA457099995C (void);
// 0x00000297 System.Boolean com.adjust.sdk.JSONArray/<GetEnumerator>d__14::MoveNext()
extern void U3CGetEnumeratorU3Ed__14_MoveNext_mE03BAD590A4A72ADD88274F87F144FA61C677CD2 (void);
// 0x00000298 System.Void com.adjust.sdk.JSONArray/<GetEnumerator>d__14::<>m__Finally1()
extern void U3CGetEnumeratorU3Ed__14_U3CU3Em__Finally1_m4F72145277B7B879E05365A241A55E1FA2DCC478 (void);
// 0x00000299 System.Object com.adjust.sdk.JSONArray/<GetEnumerator>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetEnumeratorU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC44ED8F1DA1967025AB91E813D3F5E8CAB273B4A (void);
// 0x0000029A System.Void com.adjust.sdk.JSONArray/<GetEnumerator>d__14::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__14_System_Collections_IEnumerator_Reset_m5FF5CA385FB75BA70CEF94B6FAE71C700ABA5A6A (void);
// 0x0000029B System.Object com.adjust.sdk.JSONArray/<GetEnumerator>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__14_System_Collections_IEnumerator_get_Current_m4BE072CF6B24FCA546848441F442FA00F8049498 (void);
// 0x0000029C com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass::get_Item(System.String)
extern void JSONClass_get_Item_m49CF33B11E4FCA72EBC0D1E89BFA1F64D1C82D93 (void);
// 0x0000029D System.Void com.adjust.sdk.JSONClass::set_Item(System.String,com.adjust.sdk.JSONNode)
extern void JSONClass_set_Item_m887D13A167F244972827C10AD304E445D273B72E (void);
// 0x0000029E com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass::get_Item(System.Int32)
extern void JSONClass_get_Item_m332F023B545862E9766531549B9A5D8276645551 (void);
// 0x0000029F System.Void com.adjust.sdk.JSONClass::set_Item(System.Int32,com.adjust.sdk.JSONNode)
extern void JSONClass_set_Item_m8E3A9CA1DEDAFDA4165B2D5CEE425BC01B829B71 (void);
// 0x000002A0 System.Int32 com.adjust.sdk.JSONClass::get_Count()
extern void JSONClass_get_Count_mD26CAC564152CABFB3D996FE189D7184F6C9FB3C (void);
// 0x000002A1 System.Void com.adjust.sdk.JSONClass::Add(System.String,com.adjust.sdk.JSONNode)
extern void JSONClass_Add_m8016B5271985A574BAC8E7745DE5F950299FFF38 (void);
// 0x000002A2 com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass::Remove(System.String)
extern void JSONClass_Remove_mC59E957355660E1DE1D528916D23F86B3EB5F680 (void);
// 0x000002A3 com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass::Remove(System.Int32)
extern void JSONClass_Remove_m3A246109A77359EAAF01B6324AD2F436608DA680 (void);
// 0x000002A4 com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass::Remove(com.adjust.sdk.JSONNode)
extern void JSONClass_Remove_mDE17444DFA5339F564FBB7071E7913D9C1B0C877 (void);
// 0x000002A5 System.Collections.Generic.IEnumerable`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONClass::get_Childs()
extern void JSONClass_get_Childs_m7ED5CDF26A34F041E5CBF93B9EC616E940AEC925 (void);
// 0x000002A6 System.Collections.IEnumerator com.adjust.sdk.JSONClass::GetEnumerator()
extern void JSONClass_GetEnumerator_mC63BF81FA10F977320F60BFF46CDA748A7F09DAD (void);
// 0x000002A7 System.String com.adjust.sdk.JSONClass::ToString()
extern void JSONClass_ToString_m2851DE91AA6840330E1A2A64F5803D702AD3BF37 (void);
// 0x000002A8 System.String com.adjust.sdk.JSONClass::ToString(System.String)
extern void JSONClass_ToString_m82935C8A4E9EB9D8E5693077AB9603452F71B027 (void);
// 0x000002A9 System.Void com.adjust.sdk.JSONClass::Serialize(System.IO.BinaryWriter)
extern void JSONClass_Serialize_mBA8EAE5D8ADB2413DE2A726447B0037B5686BFF8 (void);
// 0x000002AA System.Void com.adjust.sdk.JSONClass::.ctor()
extern void JSONClass__ctor_m02C51CEB21719D911A19C729D02745553A1EB446 (void);
// 0x000002AB System.Void com.adjust.sdk.JSONClass/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m54EAB9A9ED470825466B363E8EF656B51E99F746 (void);
// 0x000002AC System.Boolean com.adjust.sdk.JSONClass/<>c__DisplayClass12_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<System.String,com.adjust.sdk.JSONNode>)
extern void U3CU3Ec__DisplayClass12_0_U3CRemoveU3Eb__0_m919E74F0EFF672D2C2B58FE1D21F705C7A3DD3B8 (void);
// 0x000002AD System.Void com.adjust.sdk.JSONClass/<get_Childs>d__14::.ctor(System.Int32)
extern void U3Cget_ChildsU3Ed__14__ctor_m2A7CD01CA103381FBF4FB8D70B1FD26083894793 (void);
// 0x000002AE System.Void com.adjust.sdk.JSONClass/<get_Childs>d__14::System.IDisposable.Dispose()
extern void U3Cget_ChildsU3Ed__14_System_IDisposable_Dispose_m31D2FA345366CAF0D63CB243DCBE67745AA52783 (void);
// 0x000002AF System.Boolean com.adjust.sdk.JSONClass/<get_Childs>d__14::MoveNext()
extern void U3Cget_ChildsU3Ed__14_MoveNext_m3C0CFA00C04821A51C6A3C0A5808DFDE9ECE5A5E (void);
// 0x000002B0 System.Void com.adjust.sdk.JSONClass/<get_Childs>d__14::<>m__Finally1()
extern void U3Cget_ChildsU3Ed__14_U3CU3Em__Finally1_m9FA71F2E785602FDD64125BD5BE3FCE3561532D7 (void);
// 0x000002B1 com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass/<get_Childs>d__14::System.Collections.Generic.IEnumerator<com.adjust.sdk.JSONNode>.get_Current()
extern void U3Cget_ChildsU3Ed__14_System_Collections_Generic_IEnumeratorU3Ccom_adjust_sdk_JSONNodeU3E_get_Current_mB0226A6E5AC71AEBA38F19684C7758964A705388 (void);
// 0x000002B2 System.Void com.adjust.sdk.JSONClass/<get_Childs>d__14::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildsU3Ed__14_System_Collections_IEnumerator_Reset_m65E72C29D884A8DEDBBAAD79C3035EFB6A106ACB (void);
// 0x000002B3 System.Object com.adjust.sdk.JSONClass/<get_Childs>d__14::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildsU3Ed__14_System_Collections_IEnumerator_get_Current_m78FA739F5BE9ACDFE18DD77640515DA4B2BF4576 (void);
// 0x000002B4 System.Collections.Generic.IEnumerator`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONClass/<get_Childs>d__14::System.Collections.Generic.IEnumerable<com.adjust.sdk.JSONNode>.GetEnumerator()
extern void U3Cget_ChildsU3Ed__14_System_Collections_Generic_IEnumerableU3Ccom_adjust_sdk_JSONNodeU3E_GetEnumerator_mF561773E4A39039E3DAFEE4082F0FF2E39DE7F2C (void);
// 0x000002B5 System.Collections.IEnumerator com.adjust.sdk.JSONClass/<get_Childs>d__14::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildsU3Ed__14_System_Collections_IEnumerable_GetEnumerator_m80BE9E5871675B635E6737823E81E269BDFF7359 (void);
// 0x000002B6 System.Void com.adjust.sdk.JSONClass/<GetEnumerator>d__15::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__15__ctor_m71795B1B109474BBF1C793AE265C8218EE46A317 (void);
// 0x000002B7 System.Void com.adjust.sdk.JSONClass/<GetEnumerator>d__15::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__15_System_IDisposable_Dispose_m92BA0C0FE94BF8EA461E8D2985DCADF217B29B3A (void);
// 0x000002B8 System.Boolean com.adjust.sdk.JSONClass/<GetEnumerator>d__15::MoveNext()
extern void U3CGetEnumeratorU3Ed__15_MoveNext_mB78E9C334CD336419DCE6EAFEF6FFE51FD51947E (void);
// 0x000002B9 System.Void com.adjust.sdk.JSONClass/<GetEnumerator>d__15::<>m__Finally1()
extern void U3CGetEnumeratorU3Ed__15_U3CU3Em__Finally1_m909727106AE5FE5AA12C727A654659A639A11F9F (void);
// 0x000002BA System.Object com.adjust.sdk.JSONClass/<GetEnumerator>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetEnumeratorU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D7E2674085894FC3011FFCE567CFA21DA8C30D0 (void);
// 0x000002BB System.Void com.adjust.sdk.JSONClass/<GetEnumerator>d__15::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__15_System_Collections_IEnumerator_Reset_m9877E5F955C30C1CE2C2FEB43B2608137528BCE4 (void);
// 0x000002BC System.Object com.adjust.sdk.JSONClass/<GetEnumerator>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__15_System_Collections_IEnumerator_get_Current_mDB2FE16291B3B81162572DC31EE732BA957BD162 (void);
// 0x000002BD System.String com.adjust.sdk.JSONData::get_Value()
extern void JSONData_get_Value_m3ADA5A705F607FB7C35D4605EA832E89599BE425 (void);
// 0x000002BE System.Void com.adjust.sdk.JSONData::set_Value(System.String)
extern void JSONData_set_Value_m217DDEC42EA58FCFA6CBC6EFE473BD448A16DBE2 (void);
// 0x000002BF System.Void com.adjust.sdk.JSONData::.ctor(System.String)
extern void JSONData__ctor_mF07078A36644CD1C44FD4394482FFF67BCCEEAC5 (void);
// 0x000002C0 System.Void com.adjust.sdk.JSONData::.ctor(System.Single)
extern void JSONData__ctor_m0BA2DCE6697B2DF087570A042E0BD1D48B1B7AD2 (void);
// 0x000002C1 System.Void com.adjust.sdk.JSONData::.ctor(System.Double)
extern void JSONData__ctor_m2EDE89D6B666DDFB6A4D2399A8462D82EC448AEE (void);
// 0x000002C2 System.Void com.adjust.sdk.JSONData::.ctor(System.Boolean)
extern void JSONData__ctor_m0AB47342A4FDA49AF5B0775BE663DCE5A697B350 (void);
// 0x000002C3 System.Void com.adjust.sdk.JSONData::.ctor(System.Int32)
extern void JSONData__ctor_m312352D90C0634BDBABD84416086C56157235C3F (void);
// 0x000002C4 System.String com.adjust.sdk.JSONData::ToString()
extern void JSONData_ToString_m4C8593075AFF9E56B99908CEEA6988EB2AE61580 (void);
// 0x000002C5 System.String com.adjust.sdk.JSONData::ToString(System.String)
extern void JSONData_ToString_m67FDD9B98DF0DD19D397B6E210603DF3194840FD (void);
// 0x000002C6 System.Void com.adjust.sdk.JSONData::Serialize(System.IO.BinaryWriter)
extern void JSONData_Serialize_m509455814FED79C97E0C05F354839F51488D9D91 (void);
// 0x000002C7 System.Void com.adjust.sdk.JSONLazyCreator::.ctor(com.adjust.sdk.JSONNode)
extern void JSONLazyCreator__ctor_mF3FA6877067F4F9086006389F0910D541AA27059 (void);
// 0x000002C8 System.Void com.adjust.sdk.JSONLazyCreator::.ctor(com.adjust.sdk.JSONNode,System.String)
extern void JSONLazyCreator__ctor_m8C48C27CBB58E5F6990AFCA554803B7E191A3D46 (void);
// 0x000002C9 System.Void com.adjust.sdk.JSONLazyCreator::Set(com.adjust.sdk.JSONNode)
extern void JSONLazyCreator_Set_m9FCCD3D9059234381ACD61296F3B33ABB5392630 (void);
// 0x000002CA com.adjust.sdk.JSONNode com.adjust.sdk.JSONLazyCreator::get_Item(System.Int32)
extern void JSONLazyCreator_get_Item_m8A8A9C17EDC287BFF118DDED313F4107F4072A22 (void);
// 0x000002CB System.Void com.adjust.sdk.JSONLazyCreator::set_Item(System.Int32,com.adjust.sdk.JSONNode)
extern void JSONLazyCreator_set_Item_m19485C7273CDACCE9F7B1834D56FB74E19777E12 (void);
// 0x000002CC com.adjust.sdk.JSONNode com.adjust.sdk.JSONLazyCreator::get_Item(System.String)
extern void JSONLazyCreator_get_Item_mF8E012F6005145846304C594EACF1584CA5DDA9A (void);
// 0x000002CD System.Void com.adjust.sdk.JSONLazyCreator::set_Item(System.String,com.adjust.sdk.JSONNode)
extern void JSONLazyCreator_set_Item_m33B036AAA008F596ADB3F455D2021D1EF025B49A (void);
// 0x000002CE System.Void com.adjust.sdk.JSONLazyCreator::Add(com.adjust.sdk.JSONNode)
extern void JSONLazyCreator_Add_m59C51EC772277124A17D87DA5C74707E3EFD6AFB (void);
// 0x000002CF System.Void com.adjust.sdk.JSONLazyCreator::Add(System.String,com.adjust.sdk.JSONNode)
extern void JSONLazyCreator_Add_mBACDDB1A14FCE6CEA2674C9B4EDA816F21DF9E29 (void);
// 0x000002D0 System.Boolean com.adjust.sdk.JSONLazyCreator::op_Equality(com.adjust.sdk.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Equality_m1AB247D90A51923A9C6EA40FFB3F81E0D97E3576 (void);
// 0x000002D1 System.Boolean com.adjust.sdk.JSONLazyCreator::op_Inequality(com.adjust.sdk.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Inequality_mCBCE4C31D43C15D08157DFA42D40BB149AB22191 (void);
// 0x000002D2 System.Boolean com.adjust.sdk.JSONLazyCreator::Equals(System.Object)
extern void JSONLazyCreator_Equals_m586E6D9E990915F11F72E099E203D60723BEBC48 (void);
// 0x000002D3 System.Int32 com.adjust.sdk.JSONLazyCreator::GetHashCode()
extern void JSONLazyCreator_GetHashCode_m9CC90F8845B9E57BB6913BF83FB3C5D89FA61A35 (void);
// 0x000002D4 System.String com.adjust.sdk.JSONLazyCreator::ToString()
extern void JSONLazyCreator_ToString_m49AE6E6F07C738C9FB3F7ABC86EBBA8C0828996C (void);
// 0x000002D5 System.String com.adjust.sdk.JSONLazyCreator::ToString(System.String)
extern void JSONLazyCreator_ToString_mD794E0057AC95367420ED3D100817F019BD77A91 (void);
// 0x000002D6 System.Int32 com.adjust.sdk.JSONLazyCreator::get_AsInt()
extern void JSONLazyCreator_get_AsInt_m3D1F120A6A54087B34B638943D2168F10CF58BA8 (void);
// 0x000002D7 System.Void com.adjust.sdk.JSONLazyCreator::set_AsInt(System.Int32)
extern void JSONLazyCreator_set_AsInt_m78FF9DC114F9731546230F760A5048069378B32F (void);
// 0x000002D8 System.Single com.adjust.sdk.JSONLazyCreator::get_AsFloat()
extern void JSONLazyCreator_get_AsFloat_m1BF100B3129E3B67B00EB12BAB1C599198E60D87 (void);
// 0x000002D9 System.Void com.adjust.sdk.JSONLazyCreator::set_AsFloat(System.Single)
extern void JSONLazyCreator_set_AsFloat_mE15D7CE3B2B38E9E5EDC079D339A8AB5FAF674D6 (void);
// 0x000002DA System.Double com.adjust.sdk.JSONLazyCreator::get_AsDouble()
extern void JSONLazyCreator_get_AsDouble_m40DD7A463621DD10526570D5430173F7AE7771B5 (void);
// 0x000002DB System.Void com.adjust.sdk.JSONLazyCreator::set_AsDouble(System.Double)
extern void JSONLazyCreator_set_AsDouble_m275BB96935FAE5F2CD8A8328EAC9FE887B5A3377 (void);
// 0x000002DC System.Boolean com.adjust.sdk.JSONLazyCreator::get_AsBool()
extern void JSONLazyCreator_get_AsBool_mAF53CC7D86CFCE9E85413B126CFD79470BCD6B30 (void);
// 0x000002DD System.Void com.adjust.sdk.JSONLazyCreator::set_AsBool(System.Boolean)
extern void JSONLazyCreator_set_AsBool_m05997EDED19E2297BFE7D45A1062CB7CA52F1ED8 (void);
// 0x000002DE com.adjust.sdk.JSONArray com.adjust.sdk.JSONLazyCreator::get_AsArray()
extern void JSONLazyCreator_get_AsArray_mAEBD138BCA37CA30F180A221CC7872FE240BFA62 (void);
// 0x000002DF com.adjust.sdk.JSONClass com.adjust.sdk.JSONLazyCreator::get_AsObject()
extern void JSONLazyCreator_get_AsObject_m52BB6F39E712E7E3ED988842537FEFF804B543C7 (void);
// 0x000002E0 com.adjust.sdk.JSONNode com.adjust.sdk.JSON::Parse(System.String)
extern void JSON_Parse_m3D7BC20285304E72A9991F6B6A70F09341A6BF08 (void);
// 0x000002E1 System.Void com.adjust.sdk.AdjustAndroid::Start(com.adjust.sdk.AdjustConfig)
extern void AdjustAndroid_Start_mD01129DAB4F78C21F0E0B206FEA18A984182C9B8 (void);
// 0x000002E2 System.Void com.adjust.sdk.AdjustAndroid::TrackEvent(com.adjust.sdk.AdjustEvent)
extern void AdjustAndroid_TrackEvent_mA39AD43D87F02F2082A7E23AF706966E70B3E744 (void);
// 0x000002E3 System.Boolean com.adjust.sdk.AdjustAndroid::IsEnabled()
extern void AdjustAndroid_IsEnabled_m4CA2407E6DCD137D539F66594338B04FB99935F5 (void);
// 0x000002E4 System.Void com.adjust.sdk.AdjustAndroid::SetEnabled(System.Boolean)
extern void AdjustAndroid_SetEnabled_m2FC31E0639E78170CF580A32851BC6D3C67CD973 (void);
// 0x000002E5 System.Void com.adjust.sdk.AdjustAndroid::SetOfflineMode(System.Boolean)
extern void AdjustAndroid_SetOfflineMode_m1051441F52D74D8442772723D4115B1F220A4937 (void);
// 0x000002E6 System.Void com.adjust.sdk.AdjustAndroid::SendFirstPackages()
extern void AdjustAndroid_SendFirstPackages_mB506A548A21E861BA08EDF637143B59BC73BFDA1 (void);
// 0x000002E7 System.Void com.adjust.sdk.AdjustAndroid::SetDeviceToken(System.String)
extern void AdjustAndroid_SetDeviceToken_m088C0FFE619D4A66905DF78EE4E617DC2897DAB1 (void);
// 0x000002E8 System.String com.adjust.sdk.AdjustAndroid::GetAdid()
extern void AdjustAndroid_GetAdid_m009AA802086EF711FEB5CF462DFDF07770487142 (void);
// 0x000002E9 System.Void com.adjust.sdk.AdjustAndroid::GdprForgetMe()
extern void AdjustAndroid_GdprForgetMe_mED6BB5AE19466332DB83B3A5944E16786467778D (void);
// 0x000002EA System.Void com.adjust.sdk.AdjustAndroid::DisableThirdPartySharing()
extern void AdjustAndroid_DisableThirdPartySharing_m5DF591771C8DBB1FAA7FC4F0A92343925702DD64 (void);
// 0x000002EB com.adjust.sdk.AdjustAttribution com.adjust.sdk.AdjustAndroid::GetAttribution()
extern void AdjustAndroid_GetAttribution_m61053B5E336CF6BF8AB30AE23709246C1DBDFAA1 (void);
// 0x000002EC System.Void com.adjust.sdk.AdjustAndroid::AddSessionPartnerParameter(System.String,System.String)
extern void AdjustAndroid_AddSessionPartnerParameter_m7FC9C6A549452ABE82B0D0C2C512683DE2612E8A (void);
// 0x000002ED System.Void com.adjust.sdk.AdjustAndroid::AddSessionCallbackParameter(System.String,System.String)
extern void AdjustAndroid_AddSessionCallbackParameter_mB1F27189544F2C0812816C3467EC0F12C507E1B1 (void);
// 0x000002EE System.Void com.adjust.sdk.AdjustAndroid::RemoveSessionPartnerParameter(System.String)
extern void AdjustAndroid_RemoveSessionPartnerParameter_m4A778D326B5A6D93A6A383DF584E65DDACF8F63B (void);
// 0x000002EF System.Void com.adjust.sdk.AdjustAndroid::RemoveSessionCallbackParameter(System.String)
extern void AdjustAndroid_RemoveSessionCallbackParameter_m0310DC529C4A1CC9373F11CBB9271449EAF0FD37 (void);
// 0x000002F0 System.Void com.adjust.sdk.AdjustAndroid::ResetSessionPartnerParameters()
extern void AdjustAndroid_ResetSessionPartnerParameters_m2DDF83690EA07EE49777B363AF9E4A4E80E96A3D (void);
// 0x000002F1 System.Void com.adjust.sdk.AdjustAndroid::ResetSessionCallbackParameters()
extern void AdjustAndroid_ResetSessionCallbackParameters_m4F9C27D13AF336DA7C9E7ACDF0C9DCEC51C0248A (void);
// 0x000002F2 System.Void com.adjust.sdk.AdjustAndroid::AppWillOpenUrl(System.String)
extern void AdjustAndroid_AppWillOpenUrl_m3C2C1D2ED45A19E0B190CBD8AA4CDFE65AB77565 (void);
// 0x000002F3 System.Void com.adjust.sdk.AdjustAndroid::TrackAdRevenue(System.String,System.String)
extern void AdjustAndroid_TrackAdRevenue_m89D588AA54628906ADDD884FB1489E99BEB0BAC4 (void);
// 0x000002F4 System.Void com.adjust.sdk.AdjustAndroid::TrackAdRevenue(com.adjust.sdk.AdjustAdRevenue)
extern void AdjustAndroid_TrackAdRevenue_mAA058F26F32A71C3DD851C4C5D0AAA30BF5BFAAE (void);
// 0x000002F5 System.Void com.adjust.sdk.AdjustAndroid::TrackPlayStoreSubscription(com.adjust.sdk.AdjustPlayStoreSubscription)
extern void AdjustAndroid_TrackPlayStoreSubscription_mCB14505E4907C5E247D20C54AE2E21E4061D9126 (void);
// 0x000002F6 System.Void com.adjust.sdk.AdjustAndroid::TrackThirdPartySharing(com.adjust.sdk.AdjustThirdPartySharing)
extern void AdjustAndroid_TrackThirdPartySharing_mE13C5DFC1869E87290F7BD3A6133680A8F9A4669 (void);
// 0x000002F7 System.Void com.adjust.sdk.AdjustAndroid::TrackMeasurementConsent(System.Boolean)
extern void AdjustAndroid_TrackMeasurementConsent_m991B157AD636A840943A5959AF07CECCEA53E24C (void);
// 0x000002F8 System.Void com.adjust.sdk.AdjustAndroid::OnPause()
extern void AdjustAndroid_OnPause_m03F7F32D01A177BE0E39D6000AEE0C0013A6E3A2 (void);
// 0x000002F9 System.Void com.adjust.sdk.AdjustAndroid::OnResume()
extern void AdjustAndroid_OnResume_mB0166C82E08BF116FCF516416D16436DD3FC5D43 (void);
// 0x000002FA System.Void com.adjust.sdk.AdjustAndroid::SetReferrer(System.String)
extern void AdjustAndroid_SetReferrer_m1FA7992941C79022A5CBE0CA62D01EBED329AB6B (void);
// 0x000002FB System.Void com.adjust.sdk.AdjustAndroid::GetGoogleAdId(System.Action`1<System.String>)
extern void AdjustAndroid_GetGoogleAdId_mB10ABF7D147D322BDC45C5E03431D011A1E3C2B2 (void);
// 0x000002FC System.String com.adjust.sdk.AdjustAndroid::GetAmazonAdId()
extern void AdjustAndroid_GetAmazonAdId_mC89BBCC14BBDCD74BAB16BE200FF234BF8ABEB1D (void);
// 0x000002FD System.String com.adjust.sdk.AdjustAndroid::GetSdkVersion()
extern void AdjustAndroid_GetSdkVersion_mBFA4A689BDECA8CAE549F1D530894085C6E72476 (void);
// 0x000002FE System.Void com.adjust.sdk.AdjustAndroid::SetTestOptions(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustAndroid_SetTestOptions_m8CDAF0631752C444D36FB96610046B04B58FB4BB (void);
// 0x000002FF System.Boolean com.adjust.sdk.AdjustAndroid::IsAppSecretSet(com.adjust.sdk.AdjustConfig)
extern void AdjustAndroid_IsAppSecretSet_mF94D1EDF229B5033FD9034AEC49AC655AFCACDE9 (void);
// 0x00000300 System.Void com.adjust.sdk.AdjustAndroid::.ctor()
extern void AdjustAndroid__ctor_m0035BEC1E1826590313CEAF840550DF637DF1268 (void);
// 0x00000301 System.Void com.adjust.sdk.AdjustAndroid::.cctor()
extern void AdjustAndroid__cctor_m2A3DC4B0CBC9C1862D7BFD3D3AF7522DA9FA0612 (void);
// 0x00000302 System.Void com.adjust.sdk.AdjustAndroid/AttributionChangeListener::.ctor(System.Action`1<com.adjust.sdk.AdjustAttribution>)
extern void AttributionChangeListener__ctor_mA6205D1D3A3CBA8F7FD10B413B0FD68FFFA548B8 (void);
// 0x00000303 System.Void com.adjust.sdk.AdjustAndroid/AttributionChangeListener::onAttributionChanged(UnityEngine.AndroidJavaObject)
extern void AttributionChangeListener_onAttributionChanged_m8EEEC19136B64A9315CCB266EF17028994504EB8 (void);
// 0x00000304 System.Void com.adjust.sdk.AdjustAndroid/DeferredDeeplinkListener::.ctor(System.Action`1<System.String>)
extern void DeferredDeeplinkListener__ctor_m9AFF50C9019C550FE14B914675EE675B36EE0582 (void);
// 0x00000305 System.Boolean com.adjust.sdk.AdjustAndroid/DeferredDeeplinkListener::launchReceivedDeeplink(UnityEngine.AndroidJavaObject)
extern void DeferredDeeplinkListener_launchReceivedDeeplink_mE6D35176E2C2AADC10C881FF8F3927AB528C5F3B (void);
// 0x00000306 System.Void com.adjust.sdk.AdjustAndroid/EventTrackingSucceededListener::.ctor(System.Action`1<com.adjust.sdk.AdjustEventSuccess>)
extern void EventTrackingSucceededListener__ctor_m2A4ED2670A9A651245F8D5E344BDBFB5B3E3D43D (void);
// 0x00000307 System.Void com.adjust.sdk.AdjustAndroid/EventTrackingSucceededListener::onFinishedEventTrackingSucceeded(UnityEngine.AndroidJavaObject)
extern void EventTrackingSucceededListener_onFinishedEventTrackingSucceeded_m202DFEA1323BD584F04E02C0A8323870A1959460 (void);
// 0x00000308 System.Void com.adjust.sdk.AdjustAndroid/EventTrackingFailedListener::.ctor(System.Action`1<com.adjust.sdk.AdjustEventFailure>)
extern void EventTrackingFailedListener__ctor_m7D5E6FA8F17AAC33B50DFC25A1ACA5183FCE6545 (void);
// 0x00000309 System.Void com.adjust.sdk.AdjustAndroid/EventTrackingFailedListener::onFinishedEventTrackingFailed(UnityEngine.AndroidJavaObject)
extern void EventTrackingFailedListener_onFinishedEventTrackingFailed_m4FB36E7E27EF57535C67940BC6F148293CF366C0 (void);
// 0x0000030A System.Void com.adjust.sdk.AdjustAndroid/SessionTrackingSucceededListener::.ctor(System.Action`1<com.adjust.sdk.AdjustSessionSuccess>)
extern void SessionTrackingSucceededListener__ctor_mD524E8A2ACB053F1100FC8BD428D97E256EEAAF1 (void);
// 0x0000030B System.Void com.adjust.sdk.AdjustAndroid/SessionTrackingSucceededListener::onFinishedSessionTrackingSucceeded(UnityEngine.AndroidJavaObject)
extern void SessionTrackingSucceededListener_onFinishedSessionTrackingSucceeded_m641A6B81E4C67260BB84A35FC9803D11AA33102A (void);
// 0x0000030C System.Void com.adjust.sdk.AdjustAndroid/SessionTrackingFailedListener::.ctor(System.Action`1<com.adjust.sdk.AdjustSessionFailure>)
extern void SessionTrackingFailedListener__ctor_mEC4AC95C995754842BC483D6D7AA0381A508BA6B (void);
// 0x0000030D System.Void com.adjust.sdk.AdjustAndroid/SessionTrackingFailedListener::onFinishedSessionTrackingFailed(UnityEngine.AndroidJavaObject)
extern void SessionTrackingFailedListener_onFinishedSessionTrackingFailed_m2494A8E23D515011E6014DEB2630F9F3A88F8D69 (void);
// 0x0000030E System.Void com.adjust.sdk.AdjustAndroid/DeviceIdsReadListener::.ctor(System.Action`1<System.String>)
extern void DeviceIdsReadListener__ctor_mDF6EF7D21335FCB0B244E975C68B5A7CC8F1E39D (void);
// 0x0000030F System.Void com.adjust.sdk.AdjustAndroid/DeviceIdsReadListener::onGoogleAdIdRead(System.String)
extern void DeviceIdsReadListener_onGoogleAdIdRead_m82DB269D37125FB1A6A3667454CF5B408AC179DD (void);
// 0x00000310 System.Void com.adjust.sdk.AdjustAndroid/DeviceIdsReadListener::onGoogleAdIdRead(UnityEngine.AndroidJavaObject)
extern void DeviceIdsReadListener_onGoogleAdIdRead_m46B72AE61C8F5B9408B0FA1374F0D34557C6D16A (void);
// 0x00000311 System.Void com.adjust.sdk.Adjust::Awake()
extern void Adjust_Awake_m1B0E9298029BFF09C771F77DF8181CADFCB63BA8 (void);
// 0x00000312 System.Void com.adjust.sdk.Adjust::OnApplicationPause(System.Boolean)
extern void Adjust_OnApplicationPause_m3ADC342D8050B80840CB85B003EBCF689C8E4012 (void);
// 0x00000313 System.Void com.adjust.sdk.Adjust::start(com.adjust.sdk.AdjustConfig)
extern void Adjust_start_mEDA99971143DC57743841647E3EFF994DA9F573B (void);
// 0x00000314 System.Void com.adjust.sdk.Adjust::trackEvent(com.adjust.sdk.AdjustEvent)
extern void Adjust_trackEvent_mDC05176792C1AE9200857EB70116183B2244B33F (void);
// 0x00000315 System.Void com.adjust.sdk.Adjust::setEnabled(System.Boolean)
extern void Adjust_setEnabled_m8004D89A6F03FB0854AF642D8FB5EB969C610285 (void);
// 0x00000316 System.Boolean com.adjust.sdk.Adjust::isEnabled()
extern void Adjust_isEnabled_mB3E5BC11C9E09FB38DBA830323AA47CADA21B12F (void);
// 0x00000317 System.Void com.adjust.sdk.Adjust::setOfflineMode(System.Boolean)
extern void Adjust_setOfflineMode_m9CB0BEFA63D15D269D5C687021B248540E272C92 (void);
// 0x00000318 System.Void com.adjust.sdk.Adjust::setDeviceToken(System.String)
extern void Adjust_setDeviceToken_mB4DBFAB4BC5F577F1E6300F3B49F590663FF0F5C (void);
// 0x00000319 System.Void com.adjust.sdk.Adjust::gdprForgetMe()
extern void Adjust_gdprForgetMe_m294348DF8F8CD4628B9FE5056D24DF698A94ED40 (void);
// 0x0000031A System.Void com.adjust.sdk.Adjust::disableThirdPartySharing()
extern void Adjust_disableThirdPartySharing_m998A3653A5E2048A14F1E2EC78B87BCD8323840C (void);
// 0x0000031B System.Void com.adjust.sdk.Adjust::appWillOpenUrl(System.String)
extern void Adjust_appWillOpenUrl_m3392AEF212AEAFA41D7E0A079BFE02DA08768468 (void);
// 0x0000031C System.Void com.adjust.sdk.Adjust::sendFirstPackages()
extern void Adjust_sendFirstPackages_m1303552D006125FBA6636CAA4E9CE88E0BEB06DB (void);
// 0x0000031D System.Void com.adjust.sdk.Adjust::addSessionPartnerParameter(System.String,System.String)
extern void Adjust_addSessionPartnerParameter_mF7169C2B398C189CBFF42ABB579FAC21B248421A (void);
// 0x0000031E System.Void com.adjust.sdk.Adjust::addSessionCallbackParameter(System.String,System.String)
extern void Adjust_addSessionCallbackParameter_m521FF9F49898C1F8D1FA7EC9C0FEA68D00FF9B5C (void);
// 0x0000031F System.Void com.adjust.sdk.Adjust::removeSessionPartnerParameter(System.String)
extern void Adjust_removeSessionPartnerParameter_m1EF1A935AB3EF747A5D0866F303C72AC6AEC8030 (void);
// 0x00000320 System.Void com.adjust.sdk.Adjust::removeSessionCallbackParameter(System.String)
extern void Adjust_removeSessionCallbackParameter_m060BD40FC083FE9FFCEFDB9F55A88501B8021A09 (void);
// 0x00000321 System.Void com.adjust.sdk.Adjust::resetSessionPartnerParameters()
extern void Adjust_resetSessionPartnerParameters_m10B6EB7AF8E27B76E3C8B1E4C8BF0EE8A347732F (void);
// 0x00000322 System.Void com.adjust.sdk.Adjust::resetSessionCallbackParameters()
extern void Adjust_resetSessionCallbackParameters_m57E4C9DCACC3F2AA8238DF296B3B9841D6C368E3 (void);
// 0x00000323 System.Void com.adjust.sdk.Adjust::trackAdRevenue(System.String,System.String)
extern void Adjust_trackAdRevenue_mDCE9DF27677C8C3E7DA49D0C7E8CD30E99D214D6 (void);
// 0x00000324 System.Void com.adjust.sdk.Adjust::trackAdRevenue(com.adjust.sdk.AdjustAdRevenue)
extern void Adjust_trackAdRevenue_m91EDB1E714C5383380F3A7887013C101458754E8 (void);
// 0x00000325 System.Void com.adjust.sdk.Adjust::trackAppStoreSubscription(com.adjust.sdk.AdjustAppStoreSubscription)
extern void Adjust_trackAppStoreSubscription_m655F4FE561AEC8C06448EB92CE18C7094B3C969B (void);
// 0x00000326 System.Void com.adjust.sdk.Adjust::trackPlayStoreSubscription(com.adjust.sdk.AdjustPlayStoreSubscription)
extern void Adjust_trackPlayStoreSubscription_mE286132E839D5082BFA4B91DDAF8069EEB983905 (void);
// 0x00000327 System.Void com.adjust.sdk.Adjust::trackThirdPartySharing(com.adjust.sdk.AdjustThirdPartySharing)
extern void Adjust_trackThirdPartySharing_m2F0AD03FD18B95A9FE0126E2F778DE9810049E8D (void);
// 0x00000328 System.Void com.adjust.sdk.Adjust::trackMeasurementConsent(System.Boolean)
extern void Adjust_trackMeasurementConsent_mCEB66814F0EB4100DC32D37AE6E3C43BBC6B848E (void);
// 0x00000329 System.Void com.adjust.sdk.Adjust::requestTrackingAuthorizationWithCompletionHandler(System.Action`1<System.Int32>,System.String)
extern void Adjust_requestTrackingAuthorizationWithCompletionHandler_m8A29912EA80AEA67AE5B37EA6C54D568C6E31A19 (void);
// 0x0000032A System.Void com.adjust.sdk.Adjust::updateConversionValue(System.Int32)
extern void Adjust_updateConversionValue_m50493AE8F6851EECF90B739F3ED31562A18339FA (void);
// 0x0000032B System.Int32 com.adjust.sdk.Adjust::getAppTrackingAuthorizationStatus()
extern void Adjust_getAppTrackingAuthorizationStatus_mE88B444AE6B989C06278E8ECA3B6A4386626F83F (void);
// 0x0000032C System.String com.adjust.sdk.Adjust::getAdid()
extern void Adjust_getAdid_m7C420247243DDA0F96ABF44B11A73BB1C186B6E4 (void);
// 0x0000032D com.adjust.sdk.AdjustAttribution com.adjust.sdk.Adjust::getAttribution()
extern void Adjust_getAttribution_m84EA3EB40F0B9AE5E85A880086A540B64FC54D82 (void);
// 0x0000032E System.String com.adjust.sdk.Adjust::getWinAdid()
extern void Adjust_getWinAdid_m3B6A40CE6D17494A00196E683EE3033674865A6B (void);
// 0x0000032F System.String com.adjust.sdk.Adjust::getIdfa()
extern void Adjust_getIdfa_mA4FEEC84C6D9A8CA25ADFDC852D5156E0B07A6AD (void);
// 0x00000330 System.String com.adjust.sdk.Adjust::getSdkVersion()
extern void Adjust_getSdkVersion_m8DAFBADADD1CF185060E065944FF352332149B35 (void);
// 0x00000331 System.Void com.adjust.sdk.Adjust::setReferrer(System.String)
extern void Adjust_setReferrer_m48CB96C45237A51DBE34F3D143AAE600CC7FA141 (void);
// 0x00000332 System.Void com.adjust.sdk.Adjust::getGoogleAdId(System.Action`1<System.String>)
extern void Adjust_getGoogleAdId_m0A776FBE1A723C47F4E90AFE03E99E503BEF043C (void);
// 0x00000333 System.String com.adjust.sdk.Adjust::getAmazonAdId()
extern void Adjust_getAmazonAdId_mA5FD7EED2F33449DA0BCC2710E1C8C98AEBCB73A (void);
// 0x00000334 System.Boolean com.adjust.sdk.Adjust::IsEditor()
extern void Adjust_IsEditor_m95F617F8E2F50FD12BC18D741EC7D466F3A8DDF4 (void);
// 0x00000335 System.Void com.adjust.sdk.Adjust::SetTestOptions(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void Adjust_SetTestOptions_m9C0018EB8C448EDA4F2D8B2D2A13B7F0076F0E26 (void);
// 0x00000336 System.Void com.adjust.sdk.Adjust::.ctor()
extern void Adjust__ctor_m3303F268C76843435B868BB9D9E307FBD20A8F0B (void);
// 0x00000337 System.Void com.adjust.sdk.AdjustAdRevenue::.ctor(System.String)
extern void AdjustAdRevenue__ctor_m4C94C4313766148F6D9DC7451483B6EC847EEFB8 (void);
// 0x00000338 System.Void com.adjust.sdk.AdjustAdRevenue::setRevenue(System.Double,System.String)
extern void AdjustAdRevenue_setRevenue_mB37B06AC7FE6C0D6FF8BF3DEDD7C5E2A58E3E3A7 (void);
// 0x00000339 System.Void com.adjust.sdk.AdjustAdRevenue::setAdImpressionsCount(System.Int32)
extern void AdjustAdRevenue_setAdImpressionsCount_m3181A0D66506FA4D9971B18CC8E3DDB921EB1115 (void);
// 0x0000033A System.Void com.adjust.sdk.AdjustAdRevenue::setAdRevenueNetwork(System.String)
extern void AdjustAdRevenue_setAdRevenueNetwork_m500036ED01D100B35A12B3DD99AA9E754EA72B25 (void);
// 0x0000033B System.Void com.adjust.sdk.AdjustAdRevenue::setAdRevenueUnit(System.String)
extern void AdjustAdRevenue_setAdRevenueUnit_mF46B42441260BED2E68D98661A8D90D4F202C856 (void);
// 0x0000033C System.Void com.adjust.sdk.AdjustAdRevenue::setAdRevenuePlacement(System.String)
extern void AdjustAdRevenue_setAdRevenuePlacement_m1C1843A4ED920DDBAD223DFCD78131655804CC0B (void);
// 0x0000033D System.Void com.adjust.sdk.AdjustAdRevenue::addCallbackParameter(System.String,System.String)
extern void AdjustAdRevenue_addCallbackParameter_m2B3A25714F44C6FBA06C09FB6ABD9F703EC9335C (void);
// 0x0000033E System.Void com.adjust.sdk.AdjustAdRevenue::addPartnerParameter(System.String,System.String)
extern void AdjustAdRevenue_addPartnerParameter_mAF18DE2CE37C15D2179C77ADF244D1AB260D32D3 (void);
// 0x0000033F System.Void com.adjust.sdk.AdjustAppStoreSubscription::.ctor(System.String,System.String,System.String,System.String)
extern void AdjustAppStoreSubscription__ctor_m0D3482433734BA539F0A09252DE3659D21FD1536 (void);
// 0x00000340 System.Void com.adjust.sdk.AdjustAppStoreSubscription::setTransactionDate(System.String)
extern void AdjustAppStoreSubscription_setTransactionDate_mD1BA71DA15248006C26B22602D1BF4A83B0ACC0C (void);
// 0x00000341 System.Void com.adjust.sdk.AdjustAppStoreSubscription::setSalesRegion(System.String)
extern void AdjustAppStoreSubscription_setSalesRegion_m0E1646795FA1466592F7E7A7D14B04EC02D6E39B (void);
// 0x00000342 System.Void com.adjust.sdk.AdjustAppStoreSubscription::addCallbackParameter(System.String,System.String)
extern void AdjustAppStoreSubscription_addCallbackParameter_mD67B08D11C9DCD410CB8966744F3962905E8AA70 (void);
// 0x00000343 System.Void com.adjust.sdk.AdjustAppStoreSubscription::addPartnerParameter(System.String,System.String)
extern void AdjustAppStoreSubscription_addPartnerParameter_m6B639A50999BE4CC82D58CCCE7D1F50536D62019 (void);
// 0x00000344 System.String com.adjust.sdk.AdjustAttribution::get_adid()
extern void AdjustAttribution_get_adid_m7FEE4DDFADFF7764690922FE17064A8475DCC159 (void);
// 0x00000345 System.Void com.adjust.sdk.AdjustAttribution::set_adid(System.String)
extern void AdjustAttribution_set_adid_m8FF9650D73A3B30569FA924D09F2A1B5841800F6 (void);
// 0x00000346 System.String com.adjust.sdk.AdjustAttribution::get_network()
extern void AdjustAttribution_get_network_m8430B735848CDEF80E9054A358E1147FBD19AEE3 (void);
// 0x00000347 System.Void com.adjust.sdk.AdjustAttribution::set_network(System.String)
extern void AdjustAttribution_set_network_m68ED3E4E1E6850226D667FDE9829B402AF120D20 (void);
// 0x00000348 System.String com.adjust.sdk.AdjustAttribution::get_adgroup()
extern void AdjustAttribution_get_adgroup_m15DAB5440B779D12C1BD8BCF9C47B20F14692416 (void);
// 0x00000349 System.Void com.adjust.sdk.AdjustAttribution::set_adgroup(System.String)
extern void AdjustAttribution_set_adgroup_m04EB13F0176574C01F8E233A15E6E7AB71CDEBFB (void);
// 0x0000034A System.String com.adjust.sdk.AdjustAttribution::get_campaign()
extern void AdjustAttribution_get_campaign_mB839E1C4DD4EC624B6C46E9444F1A9D868EA0750 (void);
// 0x0000034B System.Void com.adjust.sdk.AdjustAttribution::set_campaign(System.String)
extern void AdjustAttribution_set_campaign_m29AC5BBED526925450C7D081A5A656E9A71470E9 (void);
// 0x0000034C System.String com.adjust.sdk.AdjustAttribution::get_creative()
extern void AdjustAttribution_get_creative_mC15C380B618E220C2143920CCB88EBAF8A864B36 (void);
// 0x0000034D System.Void com.adjust.sdk.AdjustAttribution::set_creative(System.String)
extern void AdjustAttribution_set_creative_mF0F350C3D8521BBC5D841A28428210CD9CF41183 (void);
// 0x0000034E System.String com.adjust.sdk.AdjustAttribution::get_clickLabel()
extern void AdjustAttribution_get_clickLabel_m45D150F891EF508E44F219A4CBE768A05BCA866D (void);
// 0x0000034F System.Void com.adjust.sdk.AdjustAttribution::set_clickLabel(System.String)
extern void AdjustAttribution_set_clickLabel_mAAFCDD0362AFE2EF2F6AEC66E6973B65B75692DE (void);
// 0x00000350 System.String com.adjust.sdk.AdjustAttribution::get_trackerName()
extern void AdjustAttribution_get_trackerName_mEA8576F240393B289A3C0CC66F9D7F2E965EEB52 (void);
// 0x00000351 System.Void com.adjust.sdk.AdjustAttribution::set_trackerName(System.String)
extern void AdjustAttribution_set_trackerName_m731697B9763F60A9FC502CC6A1A27BDBD2574876 (void);
// 0x00000352 System.String com.adjust.sdk.AdjustAttribution::get_trackerToken()
extern void AdjustAttribution_get_trackerToken_mB2CB9686A8CC7243A6C4391F4728F1BA8197F64A (void);
// 0x00000353 System.Void com.adjust.sdk.AdjustAttribution::set_trackerToken(System.String)
extern void AdjustAttribution_set_trackerToken_m6093F9C8CC27B2425BB1373F51EDFA26B9E2103F (void);
// 0x00000354 System.String com.adjust.sdk.AdjustAttribution::get_costType()
extern void AdjustAttribution_get_costType_m94B271C6C975D4C945D5912D7879C411BB2F25C6 (void);
// 0x00000355 System.Void com.adjust.sdk.AdjustAttribution::set_costType(System.String)
extern void AdjustAttribution_set_costType_m2B994A60E50367E752D803F431BE9B010BE784B0 (void);
// 0x00000356 System.Nullable`1<System.Double> com.adjust.sdk.AdjustAttribution::get_costAmount()
extern void AdjustAttribution_get_costAmount_m570856A2EFDAE1646AB3EBE61E9D11FC7A872182 (void);
// 0x00000357 System.Void com.adjust.sdk.AdjustAttribution::set_costAmount(System.Nullable`1<System.Double>)
extern void AdjustAttribution_set_costAmount_m8C20F2BD1C52F1109660D5A965B5159BA4DC5647 (void);
// 0x00000358 System.String com.adjust.sdk.AdjustAttribution::get_costCurrency()
extern void AdjustAttribution_get_costCurrency_m746AD16AC39C41F680D4420B830529EAF595E999 (void);
// 0x00000359 System.Void com.adjust.sdk.AdjustAttribution::set_costCurrency(System.String)
extern void AdjustAttribution_set_costCurrency_m4C83141F90E118ADEA5CCA620335B9FDD0C38D51 (void);
// 0x0000035A System.Void com.adjust.sdk.AdjustAttribution::.ctor()
extern void AdjustAttribution__ctor_m36B38620BB1475A4ACE1EDB1CCA466AB2F754307 (void);
// 0x0000035B System.Void com.adjust.sdk.AdjustAttribution::.ctor(System.String)
extern void AdjustAttribution__ctor_m8274D1B29F0C4D4D99E2067269DBF55161E3B98A (void);
// 0x0000035C System.Void com.adjust.sdk.AdjustAttribution::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustAttribution__ctor_mE3820E52AF63417CE1FF2ADAAE8B1BFA701344C9 (void);
// 0x0000035D System.Void com.adjust.sdk.AdjustConfig::.ctor(System.String,com.adjust.sdk.AdjustEnvironment)
extern void AdjustConfig__ctor_m718373AA152F4C6F3AB5E805B4630AB008A32395 (void);
// 0x0000035E System.Void com.adjust.sdk.AdjustConfig::.ctor(System.String,com.adjust.sdk.AdjustEnvironment,System.Boolean)
extern void AdjustConfig__ctor_m96C4907B142108F8818BEBC52EDC03D90B5C6EA7 (void);
// 0x0000035F System.Void com.adjust.sdk.AdjustConfig::setLogLevel(com.adjust.sdk.AdjustLogLevel)
extern void AdjustConfig_setLogLevel_mDA93163BE7A5E536C670CCDC0CCF7C93B9B3E54F (void);
// 0x00000360 System.Void com.adjust.sdk.AdjustConfig::setDefaultTracker(System.String)
extern void AdjustConfig_setDefaultTracker_mA67C3195A19A5E9AA2B5AF9E071336CA9E1AB724 (void);
// 0x00000361 System.Void com.adjust.sdk.AdjustConfig::setExternalDeviceId(System.String)
extern void AdjustConfig_setExternalDeviceId_m5AA54126D0A69091B9573F3A530BD2AF8B450FDF (void);
// 0x00000362 System.Void com.adjust.sdk.AdjustConfig::setLaunchDeferredDeeplink(System.Boolean)
extern void AdjustConfig_setLaunchDeferredDeeplink_m8D6806307929E8E3AE2F01CE3C08BF96DDCD526F (void);
// 0x00000363 System.Void com.adjust.sdk.AdjustConfig::setSendInBackground(System.Boolean)
extern void AdjustConfig_setSendInBackground_m039AABBAF2DB300CE62F8CBF78DA3A5E36604317 (void);
// 0x00000364 System.Void com.adjust.sdk.AdjustConfig::setEventBufferingEnabled(System.Boolean)
extern void AdjustConfig_setEventBufferingEnabled_mBB81E8C7A41ABCA6326F518EE53905C327B1F982 (void);
// 0x00000365 System.Void com.adjust.sdk.AdjustConfig::setNeedsCost(System.Boolean)
extern void AdjustConfig_setNeedsCost_m27ACE0EB3E57AECBD640B2A1B4510BCFBE8553DD (void);
// 0x00000366 System.Void com.adjust.sdk.AdjustConfig::setDelayStart(System.Double)
extern void AdjustConfig_setDelayStart_m5E3583922F84F6E2B9988052D54ABECE6113B0B6 (void);
// 0x00000367 System.Void com.adjust.sdk.AdjustConfig::setUserAgent(System.String)
extern void AdjustConfig_setUserAgent_mDD4FFFE5044037A2BC22003F631A9989361DFA1D (void);
// 0x00000368 System.Void com.adjust.sdk.AdjustConfig::setIsDeviceKnown(System.Boolean)
extern void AdjustConfig_setIsDeviceKnown_mAD1C556F14A0DBAED60254F330EF9625F3AB6EDA (void);
// 0x00000369 System.Void com.adjust.sdk.AdjustConfig::setUrlStrategy(System.String)
extern void AdjustConfig_setUrlStrategy_m43C184E9915977FC7955F22A086111B7836E2263 (void);
// 0x0000036A System.Void com.adjust.sdk.AdjustConfig::deactivateSKAdNetworkHandling()
extern void AdjustConfig_deactivateSKAdNetworkHandling_m9E3A12F2125AE97AF898E7AC49DBCE9085D93B9E (void);
// 0x0000036B System.Void com.adjust.sdk.AdjustConfig::setDeferredDeeplinkDelegate(System.Action`1<System.String>,System.String)
extern void AdjustConfig_setDeferredDeeplinkDelegate_m0434CB6325F267D824956505E38F55C1BC69F750 (void);
// 0x0000036C System.Action`1<System.String> com.adjust.sdk.AdjustConfig::getDeferredDeeplinkDelegate()
extern void AdjustConfig_getDeferredDeeplinkDelegate_m5E71CF0E1CD8ED86E14052643073B2B34A19E574 (void);
// 0x0000036D System.Void com.adjust.sdk.AdjustConfig::setAttributionChangedDelegate(System.Action`1<com.adjust.sdk.AdjustAttribution>,System.String)
extern void AdjustConfig_setAttributionChangedDelegate_m16311DC0B297069CC826AB0CEE81C747C47B7054 (void);
// 0x0000036E System.Action`1<com.adjust.sdk.AdjustAttribution> com.adjust.sdk.AdjustConfig::getAttributionChangedDelegate()
extern void AdjustConfig_getAttributionChangedDelegate_m0B91F876BC47C733C887A0C674C69C7A2AAE859E (void);
// 0x0000036F System.Void com.adjust.sdk.AdjustConfig::setEventSuccessDelegate(System.Action`1<com.adjust.sdk.AdjustEventSuccess>,System.String)
extern void AdjustConfig_setEventSuccessDelegate_mC93D376662090A2A7D5341FCB0EB6F5D47034C00 (void);
// 0x00000370 System.Action`1<com.adjust.sdk.AdjustEventSuccess> com.adjust.sdk.AdjustConfig::getEventSuccessDelegate()
extern void AdjustConfig_getEventSuccessDelegate_m803B0AF83809209BDCA4FD72ADCD37A3D6525AAE (void);
// 0x00000371 System.Void com.adjust.sdk.AdjustConfig::setEventFailureDelegate(System.Action`1<com.adjust.sdk.AdjustEventFailure>,System.String)
extern void AdjustConfig_setEventFailureDelegate_mDF106AB503D7AE6A0EF9FC23C86FDB561C53D919 (void);
// 0x00000372 System.Action`1<com.adjust.sdk.AdjustEventFailure> com.adjust.sdk.AdjustConfig::getEventFailureDelegate()
extern void AdjustConfig_getEventFailureDelegate_m55B097E3E827DAA9D0A03C3827815990DEEFAA73 (void);
// 0x00000373 System.Void com.adjust.sdk.AdjustConfig::setSessionSuccessDelegate(System.Action`1<com.adjust.sdk.AdjustSessionSuccess>,System.String)
extern void AdjustConfig_setSessionSuccessDelegate_m38873292BB0382A5A82272A971C2C8FB32EE97ED (void);
// 0x00000374 System.Action`1<com.adjust.sdk.AdjustSessionSuccess> com.adjust.sdk.AdjustConfig::getSessionSuccessDelegate()
extern void AdjustConfig_getSessionSuccessDelegate_mDD3BD6C6F62AF59330E60B1570D2FC3D42DE20C1 (void);
// 0x00000375 System.Void com.adjust.sdk.AdjustConfig::setSessionFailureDelegate(System.Action`1<com.adjust.sdk.AdjustSessionFailure>,System.String)
extern void AdjustConfig_setSessionFailureDelegate_m3BEF1CB7417F8E3E12E59E610DBE1FEA8584E2AC (void);
// 0x00000376 System.Action`1<com.adjust.sdk.AdjustSessionFailure> com.adjust.sdk.AdjustConfig::getSessionFailureDelegate()
extern void AdjustConfig_getSessionFailureDelegate_mB847ACF06A571D19D85DD18BA596E78F646AED66 (void);
// 0x00000377 System.Void com.adjust.sdk.AdjustConfig::setConversionValueUpdatedDelegate(System.Action`1<System.Int32>,System.String)
extern void AdjustConfig_setConversionValueUpdatedDelegate_m944853EAA8941CDC4ECEA27C4C9CAD01279639B4 (void);
// 0x00000378 System.Action`1<System.Int32> com.adjust.sdk.AdjustConfig::getConversionValueUpdatedDelegate()
extern void AdjustConfig_getConversionValueUpdatedDelegate_mA8286519D143FC8FA6AA32373F2169099ABEEE23 (void);
// 0x00000379 System.Void com.adjust.sdk.AdjustConfig::setAppSecret(System.Int64,System.Int64,System.Int64,System.Int64,System.Int64)
extern void AdjustConfig_setAppSecret_mCF9AAAE31F6A695F806709B8599E319706BE15DE (void);
// 0x0000037A System.Void com.adjust.sdk.AdjustConfig::setAllowiAdInfoReading(System.Boolean)
extern void AdjustConfig_setAllowiAdInfoReading_mBCAE2AC7ED0E99E915648114A3424E985EFE469C (void);
// 0x0000037B System.Void com.adjust.sdk.AdjustConfig::setAllowAdServicesInfoReading(System.Boolean)
extern void AdjustConfig_setAllowAdServicesInfoReading_m232716609D173872EF41FD5837A9D0133419C4C1 (void);
// 0x0000037C System.Void com.adjust.sdk.AdjustConfig::setAllowIdfaReading(System.Boolean)
extern void AdjustConfig_setAllowIdfaReading_m439C9CAB2FDE23F534F838B3BEAC30B917E483CA (void);
// 0x0000037D System.Void com.adjust.sdk.AdjustConfig::setProcessName(System.String)
extern void AdjustConfig_setProcessName_mA05E8249BDBEECED54C503BAAE53011D4EF18E53 (void);
// 0x0000037E System.Void com.adjust.sdk.AdjustConfig::setReadMobileEquipmentIdentity(System.Boolean)
extern void AdjustConfig_setReadMobileEquipmentIdentity_m60C524B45682B362D3A43D8EA2AAB5E324F3D16C (void);
// 0x0000037F System.Void com.adjust.sdk.AdjustConfig::setPreinstallTrackingEnabled(System.Boolean)
extern void AdjustConfig_setPreinstallTrackingEnabled_m50FF6E90421C467AAB8D1668E426E2F2F5B15BDA (void);
// 0x00000380 System.Void com.adjust.sdk.AdjustConfig::setLogDelegate(System.Action`1<System.String>)
extern void AdjustConfig_setLogDelegate_mCD5A0B2CC87D71A6618CB76ED218FFDB346D487C (void);
// 0x00000381 System.String com.adjust.sdk.AdjustEnvironmentExtension::ToLowercaseString(com.adjust.sdk.AdjustEnvironment)
extern void AdjustEnvironmentExtension_ToLowercaseString_m64A2BF7A9C9E8D34F18E1164E8C16D663513BB1F (void);
// 0x00000382 System.Void com.adjust.sdk.AdjustEvent::.ctor(System.String)
extern void AdjustEvent__ctor_mB6F2EEAE794AF0DBA97B384BD745A06235288C03 (void);
// 0x00000383 System.Void com.adjust.sdk.AdjustEvent::setRevenue(System.Double,System.String)
extern void AdjustEvent_setRevenue_mA8B68C9A56C7A90FDD61D07D6E9B527EA4BAEB49 (void);
// 0x00000384 System.Void com.adjust.sdk.AdjustEvent::addCallbackParameter(System.String,System.String)
extern void AdjustEvent_addCallbackParameter_m69836E8BCB0600E59592F2226886F7E3717267DC (void);
// 0x00000385 System.Void com.adjust.sdk.AdjustEvent::addPartnerParameter(System.String,System.String)
extern void AdjustEvent_addPartnerParameter_m5C8A9B71C8E3668F18D2A7107128C2AA7F60115B (void);
// 0x00000386 System.Void com.adjust.sdk.AdjustEvent::setTransactionId(System.String)
extern void AdjustEvent_setTransactionId_mD82CAE578CF9FBBB0F73937723AE9679D33AA254 (void);
// 0x00000387 System.Void com.adjust.sdk.AdjustEvent::setCallbackId(System.String)
extern void AdjustEvent_setCallbackId_m77A43125761431059046C8BD038A4090A6F67A98 (void);
// 0x00000388 System.Void com.adjust.sdk.AdjustEvent::setReceipt(System.String,System.String)
extern void AdjustEvent_setReceipt_m0DA7BC506BF585B0EFDD3E0FEDC51EECE0406BFD (void);
// 0x00000389 System.String com.adjust.sdk.AdjustEventFailure::get_Adid()
extern void AdjustEventFailure_get_Adid_m63A229A1E387D51BA76FD857843A30909472F4E9 (void);
// 0x0000038A System.Void com.adjust.sdk.AdjustEventFailure::set_Adid(System.String)
extern void AdjustEventFailure_set_Adid_m1C9E862F9EE373D5F36B28D07F944581B4733FCC (void);
// 0x0000038B System.String com.adjust.sdk.AdjustEventFailure::get_Message()
extern void AdjustEventFailure_get_Message_m39E32498366357A63414ACBF2D829D67E378435C (void);
// 0x0000038C System.Void com.adjust.sdk.AdjustEventFailure::set_Message(System.String)
extern void AdjustEventFailure_set_Message_m67C166B4D02AD43A8835555633ED6A41B6470472 (void);
// 0x0000038D System.String com.adjust.sdk.AdjustEventFailure::get_Timestamp()
extern void AdjustEventFailure_get_Timestamp_m8AD7E740ED2BAD647DF69D3E9E20DA10AEA7894C (void);
// 0x0000038E System.Void com.adjust.sdk.AdjustEventFailure::set_Timestamp(System.String)
extern void AdjustEventFailure_set_Timestamp_m144FA4FAB62F3AE2D92C8A729A4D80C78129FC8F (void);
// 0x0000038F System.String com.adjust.sdk.AdjustEventFailure::get_EventToken()
extern void AdjustEventFailure_get_EventToken_m790B0C32B96810DB063845DB41C7EA5392511E0F (void);
// 0x00000390 System.Void com.adjust.sdk.AdjustEventFailure::set_EventToken(System.String)
extern void AdjustEventFailure_set_EventToken_m0107E2C7300ECD415209E1F64A6B8AD04F33798E (void);
// 0x00000391 System.String com.adjust.sdk.AdjustEventFailure::get_CallbackId()
extern void AdjustEventFailure_get_CallbackId_m7C6B49AB5A6AE7A9287E309C85E4DDC8B6E01F6F (void);
// 0x00000392 System.Void com.adjust.sdk.AdjustEventFailure::set_CallbackId(System.String)
extern void AdjustEventFailure_set_CallbackId_mE4D4EE9B87B3B947F952C7BC539A177AA609B0FD (void);
// 0x00000393 System.Boolean com.adjust.sdk.AdjustEventFailure::get_WillRetry()
extern void AdjustEventFailure_get_WillRetry_m437C69AED2629C0A51F93160CF269ECB51C48138 (void);
// 0x00000394 System.Void com.adjust.sdk.AdjustEventFailure::set_WillRetry(System.Boolean)
extern void AdjustEventFailure_set_WillRetry_m4C79E145286998F97FFFC7106C792794C06669E9 (void);
// 0x00000395 System.Collections.Generic.Dictionary`2<System.String,System.Object> com.adjust.sdk.AdjustEventFailure::get_JsonResponse()
extern void AdjustEventFailure_get_JsonResponse_mB7A9E1270C3CA4F577552217E4FDB3CCFB32852A (void);
// 0x00000396 System.Void com.adjust.sdk.AdjustEventFailure::set_JsonResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustEventFailure_set_JsonResponse_mC129C66E6BD3773556DD9984F8A9B41987A480EE (void);
// 0x00000397 System.Void com.adjust.sdk.AdjustEventFailure::.ctor()
extern void AdjustEventFailure__ctor_m528922562AC18ADE49AC59EFECDF9DDDF06D9827 (void);
// 0x00000398 System.Void com.adjust.sdk.AdjustEventFailure::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustEventFailure__ctor_mD35BD0B33754A00AF01D005F17CE529500281A14 (void);
// 0x00000399 System.Void com.adjust.sdk.AdjustEventFailure::.ctor(System.String)
extern void AdjustEventFailure__ctor_mE44FDD70724F8F42E19DE705B7A0771C23BE0284 (void);
// 0x0000039A System.Void com.adjust.sdk.AdjustEventFailure::BuildJsonResponseFromString(System.String)
extern void AdjustEventFailure_BuildJsonResponseFromString_mFC779A74C66E513EC19EF86F780AE363B25A828A (void);
// 0x0000039B System.String com.adjust.sdk.AdjustEventFailure::GetJsonResponse()
extern void AdjustEventFailure_GetJsonResponse_m4A9D1FDB6FF13C9F955E00C64A4996F5826A31FD (void);
// 0x0000039C System.String com.adjust.sdk.AdjustEventSuccess::get_Adid()
extern void AdjustEventSuccess_get_Adid_m9107BA449922578E0F9B8CB8B4541FE26A6C56C5 (void);
// 0x0000039D System.Void com.adjust.sdk.AdjustEventSuccess::set_Adid(System.String)
extern void AdjustEventSuccess_set_Adid_mF832EF6F1DC6FE8156A132AD42AA1060E539A7AD (void);
// 0x0000039E System.String com.adjust.sdk.AdjustEventSuccess::get_Message()
extern void AdjustEventSuccess_get_Message_m5B29D1C7B3CF3C7CED972991740A888131931DE2 (void);
// 0x0000039F System.Void com.adjust.sdk.AdjustEventSuccess::set_Message(System.String)
extern void AdjustEventSuccess_set_Message_m38D9A47DB181615424C49B59C6E4A562B3E5F89F (void);
// 0x000003A0 System.String com.adjust.sdk.AdjustEventSuccess::get_Timestamp()
extern void AdjustEventSuccess_get_Timestamp_m193EB4EBA0B8DA8CF0863D1DF75FEF141B1D3B10 (void);
// 0x000003A1 System.Void com.adjust.sdk.AdjustEventSuccess::set_Timestamp(System.String)
extern void AdjustEventSuccess_set_Timestamp_m0CCE0BEF1E47ACA8E07187A73BBE9ACFEEC6586B (void);
// 0x000003A2 System.String com.adjust.sdk.AdjustEventSuccess::get_EventToken()
extern void AdjustEventSuccess_get_EventToken_m5784EFFBAE4463DA0ECFF6A537731DC98E286A3E (void);
// 0x000003A3 System.Void com.adjust.sdk.AdjustEventSuccess::set_EventToken(System.String)
extern void AdjustEventSuccess_set_EventToken_mAF539927077C6E4B98FC29622DE5D26C3A5F2C64 (void);
// 0x000003A4 System.String com.adjust.sdk.AdjustEventSuccess::get_CallbackId()
extern void AdjustEventSuccess_get_CallbackId_m3D7D77C8EF5837C5EAAB45998FD4C7A02C04D983 (void);
// 0x000003A5 System.Void com.adjust.sdk.AdjustEventSuccess::set_CallbackId(System.String)
extern void AdjustEventSuccess_set_CallbackId_mA49D8F4F34D8A1C9FB36A15EFB7572AC187A28C9 (void);
// 0x000003A6 System.Collections.Generic.Dictionary`2<System.String,System.Object> com.adjust.sdk.AdjustEventSuccess::get_JsonResponse()
extern void AdjustEventSuccess_get_JsonResponse_mC1ED1F8BC320A1BE406D403D15DB0EA699A01A75 (void);
// 0x000003A7 System.Void com.adjust.sdk.AdjustEventSuccess::set_JsonResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustEventSuccess_set_JsonResponse_mCA8F4E6DE391C1D4B8BCEEFB437BA5EE1E717D90 (void);
// 0x000003A8 System.Void com.adjust.sdk.AdjustEventSuccess::.ctor()
extern void AdjustEventSuccess__ctor_m8E95350D1027E90E42E4A890D5D8F6C683C1388C (void);
// 0x000003A9 System.Void com.adjust.sdk.AdjustEventSuccess::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustEventSuccess__ctor_m3AF21839E90ADA4ACF33D117311F354A788FFE1B (void);
// 0x000003AA System.Void com.adjust.sdk.AdjustEventSuccess::.ctor(System.String)
extern void AdjustEventSuccess__ctor_m572E2ED470E4819DFF8462F86CD0A35EE856DE75 (void);
// 0x000003AB System.Void com.adjust.sdk.AdjustEventSuccess::BuildJsonResponseFromString(System.String)
extern void AdjustEventSuccess_BuildJsonResponseFromString_mB45093E3AE421B1E1C210318F2081EB7016C065C (void);
// 0x000003AC System.String com.adjust.sdk.AdjustEventSuccess::GetJsonResponse()
extern void AdjustEventSuccess_GetJsonResponse_mC8F1B778DCD86E0CFCE0A7F34D2AE30E440E465B (void);
// 0x000003AD System.String com.adjust.sdk.AdjustLogLevelExtension::ToLowercaseString(com.adjust.sdk.AdjustLogLevel)
extern void AdjustLogLevelExtension_ToLowercaseString_m34762F532C1EFC081ACEDDC382441EF5B124BD13 (void);
// 0x000003AE System.String com.adjust.sdk.AdjustLogLevelExtension::ToUppercaseString(com.adjust.sdk.AdjustLogLevel)
extern void AdjustLogLevelExtension_ToUppercaseString_mBDB79054591AE2DA26F4C0139CBC670095675033 (void);
// 0x000003AF System.Void com.adjust.sdk.AdjustPlayStoreSubscription::.ctor(System.String,System.String,System.String,System.String,System.String,System.String)
extern void AdjustPlayStoreSubscription__ctor_m8FAA1BDF8B8C354B18FB090ACB1EF65E0B381EA1 (void);
// 0x000003B0 System.Void com.adjust.sdk.AdjustPlayStoreSubscription::setPurchaseTime(System.String)
extern void AdjustPlayStoreSubscription_setPurchaseTime_mFB33C90CFC1A515912E08927A41E27DEB80094F4 (void);
// 0x000003B1 System.Void com.adjust.sdk.AdjustPlayStoreSubscription::addCallbackParameter(System.String,System.String)
extern void AdjustPlayStoreSubscription_addCallbackParameter_m384EC847C6931509BE14FF2275D6AB32F493F9A4 (void);
// 0x000003B2 System.Void com.adjust.sdk.AdjustPlayStoreSubscription::addPartnerParameter(System.String,System.String)
extern void AdjustPlayStoreSubscription_addPartnerParameter_m864989B749FAE715C806A3772FC7B968FFD4A5F4 (void);
// 0x000003B3 System.String com.adjust.sdk.AdjustSessionFailure::get_Adid()
extern void AdjustSessionFailure_get_Adid_m55CBA752E653E41BB100CA0666E984AC41A1C986 (void);
// 0x000003B4 System.Void com.adjust.sdk.AdjustSessionFailure::set_Adid(System.String)
extern void AdjustSessionFailure_set_Adid_m9D52E417E29F03D868D2A5C1BA50578FAE232BC7 (void);
// 0x000003B5 System.String com.adjust.sdk.AdjustSessionFailure::get_Message()
extern void AdjustSessionFailure_get_Message_m7FB5952110E6198593306F2D2206C87878241071 (void);
// 0x000003B6 System.Void com.adjust.sdk.AdjustSessionFailure::set_Message(System.String)
extern void AdjustSessionFailure_set_Message_m84D2E372880BCEAB77F55A2D5E3228A2D0179835 (void);
// 0x000003B7 System.String com.adjust.sdk.AdjustSessionFailure::get_Timestamp()
extern void AdjustSessionFailure_get_Timestamp_m16815BDDD78D3DC8836D6929D7ECA0287567E1C9 (void);
// 0x000003B8 System.Void com.adjust.sdk.AdjustSessionFailure::set_Timestamp(System.String)
extern void AdjustSessionFailure_set_Timestamp_m4620F96554EF0DBF543BF574C3B9E2CBEA0BF46E (void);
// 0x000003B9 System.Boolean com.adjust.sdk.AdjustSessionFailure::get_WillRetry()
extern void AdjustSessionFailure_get_WillRetry_mDC6EF21BB9ED54A38E87A437F25B3E1ABFB64CB7 (void);
// 0x000003BA System.Void com.adjust.sdk.AdjustSessionFailure::set_WillRetry(System.Boolean)
extern void AdjustSessionFailure_set_WillRetry_m891830EFFC0F200C979980F639EF51F2357E6BCF (void);
// 0x000003BB System.Collections.Generic.Dictionary`2<System.String,System.Object> com.adjust.sdk.AdjustSessionFailure::get_JsonResponse()
extern void AdjustSessionFailure_get_JsonResponse_m3CC10F98CEFA48F10203B4B21CA8B7F48313E337 (void);
// 0x000003BC System.Void com.adjust.sdk.AdjustSessionFailure::set_JsonResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustSessionFailure_set_JsonResponse_m9697C8316211570DED147C08CA044DB7A9626B6E (void);
// 0x000003BD System.Void com.adjust.sdk.AdjustSessionFailure::.ctor()
extern void AdjustSessionFailure__ctor_m55084005614B14B05358BFC8D8093D0E1BA5D577 (void);
// 0x000003BE System.Void com.adjust.sdk.AdjustSessionFailure::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustSessionFailure__ctor_mC8D3BF875D5D8A394B38A08DA6FD82FE78D65AB2 (void);
// 0x000003BF System.Void com.adjust.sdk.AdjustSessionFailure::.ctor(System.String)
extern void AdjustSessionFailure__ctor_mF96CCCD25D8F54F5FE37C1532E5A7D5B1FADEB3F (void);
// 0x000003C0 System.Void com.adjust.sdk.AdjustSessionFailure::BuildJsonResponseFromString(System.String)
extern void AdjustSessionFailure_BuildJsonResponseFromString_m2D4F30200FC6361CACC4417A512F8E14FF9C38A6 (void);
// 0x000003C1 System.String com.adjust.sdk.AdjustSessionFailure::GetJsonResponse()
extern void AdjustSessionFailure_GetJsonResponse_mE5D4C31B41ED1899C26AB32CD2648ADEFDE09351 (void);
// 0x000003C2 System.String com.adjust.sdk.AdjustSessionSuccess::get_Adid()
extern void AdjustSessionSuccess_get_Adid_m647C0D4B4E911D6C8BE1634A171F548461180414 (void);
// 0x000003C3 System.Void com.adjust.sdk.AdjustSessionSuccess::set_Adid(System.String)
extern void AdjustSessionSuccess_set_Adid_m4393AA9B18910CE351BB43D1C510132B4F971573 (void);
// 0x000003C4 System.String com.adjust.sdk.AdjustSessionSuccess::get_Message()
extern void AdjustSessionSuccess_get_Message_m86BB21FF8BEC5DA95055C3A12413D7CEAF1731EA (void);
// 0x000003C5 System.Void com.adjust.sdk.AdjustSessionSuccess::set_Message(System.String)
extern void AdjustSessionSuccess_set_Message_mD680D8861FD8EE269D0994D51498AC2210694E99 (void);
// 0x000003C6 System.String com.adjust.sdk.AdjustSessionSuccess::get_Timestamp()
extern void AdjustSessionSuccess_get_Timestamp_mE2D213502F0F03A341B1E39DC4152AEF5C68F813 (void);
// 0x000003C7 System.Void com.adjust.sdk.AdjustSessionSuccess::set_Timestamp(System.String)
extern void AdjustSessionSuccess_set_Timestamp_m2ED4611CC016044E197BF515B3A7C81C27B207EA (void);
// 0x000003C8 System.Collections.Generic.Dictionary`2<System.String,System.Object> com.adjust.sdk.AdjustSessionSuccess::get_JsonResponse()
extern void AdjustSessionSuccess_get_JsonResponse_m13404EAE48C660945ED5BBC50A26E9AB2E4B8595 (void);
// 0x000003C9 System.Void com.adjust.sdk.AdjustSessionSuccess::set_JsonResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustSessionSuccess_set_JsonResponse_mCFFE1E0F01BD95837EE0A4E9D89CE5913C3E0FBC (void);
// 0x000003CA System.Void com.adjust.sdk.AdjustSessionSuccess::.ctor()
extern void AdjustSessionSuccess__ctor_m5D4F0E9806EDCE8130DE98471E7ECA654B744F9A (void);
// 0x000003CB System.Void com.adjust.sdk.AdjustSessionSuccess::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustSessionSuccess__ctor_m468034512A1D2682AA0F15926CE8CA80F239C31D (void);
// 0x000003CC System.Void com.adjust.sdk.AdjustSessionSuccess::.ctor(System.String)
extern void AdjustSessionSuccess__ctor_mFD79CF038E807DE1559B54362B6E87EFAEFCD542 (void);
// 0x000003CD System.Void com.adjust.sdk.AdjustSessionSuccess::BuildJsonResponseFromString(System.String)
extern void AdjustSessionSuccess_BuildJsonResponseFromString_m2CA7E40EDAD331AE6DEDF385D364682D7AC8ACCE (void);
// 0x000003CE System.String com.adjust.sdk.AdjustSessionSuccess::GetJsonResponse()
extern void AdjustSessionSuccess_GetJsonResponse_m22B1531644212867F4EFF412E5B90CC8F7A15C5D (void);
// 0x000003CF System.Void com.adjust.sdk.AdjustThirdPartySharing::.ctor(System.Nullable`1<System.Boolean>)
extern void AdjustThirdPartySharing__ctor_mD050F802304C5E3A20E88D7C1F8AE85586641A82 (void);
// 0x000003D0 System.Void com.adjust.sdk.AdjustThirdPartySharing::addGranularOption(System.String,System.String,System.String)
extern void AdjustThirdPartySharing_addGranularOption_m430DCE18F822237234F208C6FFD6C7837A2A1A77 (void);
// 0x000003D1 System.Int32 com.adjust.sdk.AdjustUtils::ConvertLogLevel(System.Nullable`1<com.adjust.sdk.AdjustLogLevel>)
extern void AdjustUtils_ConvertLogLevel_m0FCC0CC22C6B6C6F22D5B15A9D3AE79782DC582A (void);
// 0x000003D2 System.Int32 com.adjust.sdk.AdjustUtils::ConvertBool(System.Nullable`1<System.Boolean>)
extern void AdjustUtils_ConvertBool_m1EF79C9F1C7B11BC5F240653329F039AD770AAF9 (void);
// 0x000003D3 System.Double com.adjust.sdk.AdjustUtils::ConvertDouble(System.Nullable`1<System.Double>)
extern void AdjustUtils_ConvertDouble_mF0BA3CCACECD87FDA363512371270A8989F6CCB4 (void);
// 0x000003D4 System.Int32 com.adjust.sdk.AdjustUtils::ConvertInt(System.Nullable`1<System.Int32>)
extern void AdjustUtils_ConvertInt_m10AC63AC85A6C9BC94013B6677A8358BFF2B489D (void);
// 0x000003D5 System.Int64 com.adjust.sdk.AdjustUtils::ConvertLong(System.Nullable`1<System.Int64>)
extern void AdjustUtils_ConvertLong_m73EFABDE60849F9C2F456F267ABAF7AF9DDFB438 (void);
// 0x000003D6 System.String com.adjust.sdk.AdjustUtils::ConvertListToJson(System.Collections.Generic.List`1<System.String>)
extern void AdjustUtils_ConvertListToJson_mD7777D5BA7CDEC21713D8E898B349459CE08F8B6 (void);
// 0x000003D7 System.String com.adjust.sdk.AdjustUtils::GetJsonResponseCompact(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustUtils_GetJsonResponseCompact_m6524D9E659857DDE6E5A40C5E9FD20FC63AE4175 (void);
// 0x000003D8 System.String com.adjust.sdk.AdjustUtils::GetJsonString(com.adjust.sdk.JSONNode,System.String)
extern void AdjustUtils_GetJsonString_m3BCF0F70953B40ACC0609048179E44C810FFDC3E (void);
// 0x000003D9 System.Void com.adjust.sdk.AdjustUtils::WriteJsonResponseDictionary(com.adjust.sdk.JSONClass,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustUtils_WriteJsonResponseDictionary_mBD703AD06DD17B0777ECE4322F39126C4DFB687F (void);
// 0x000003DA System.String com.adjust.sdk.AdjustUtils::TryGetValue(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.String)
extern void AdjustUtils_TryGetValue_mB22F0EC045510DED75ECA87B697F24A4B3A049E5 (void);
// 0x000003DB UnityEngine.AndroidJavaObject com.adjust.sdk.AdjustUtils::TestOptionsMap2AndroidJavaObject(System.Collections.Generic.Dictionary`2<System.String,System.String>,UnityEngine.AndroidJavaObject)
extern void AdjustUtils_TestOptionsMap2AndroidJavaObject_mEE341272AC54FF3806A16A4C04D1690CB8538A19 (void);
// 0x000003DC System.Void com.adjust.sdk.AdjustUtils::.ctor()
extern void AdjustUtils__ctor_mEE74F3B9A26BAE12B3C426FF63604FD7396544A2 (void);
// 0x000003DD System.Void com.adjust.sdk.AdjustUtils::.cctor()
extern void AdjustUtils__cctor_m1BEC2F6F63E6AFF40E12F581D676AC0132398CEE (void);
static Il2CppMethodPointer s_methodPointers[989] = 
{
	ExampleGUI_OnGUI_m7F98D722B7B9B570BBAA43D6E62AA1C9ADAB9514,
	ExampleGUI_ShowGUI_m3A21CF5132CCE304624990EEB9311B27FC974279,
	ExampleGUI_HandleGooglePlayId_m3E5E31CFA1DACF07F0E799F5C9D79A0AAE15ABE0,
	ExampleGUI_AttributionChangedCallback_mF94C25FEB4A93C591437B1B7B980C4FEA040BA73,
	ExampleGUI_EventSuccessCallback_m31F74382525AD79CE9078C9E3DBA80527C24124D,
	ExampleGUI_EventFailureCallback_m3E6FA431F4BD7E4B85E03C02B841950D78AF35EE,
	ExampleGUI_SessionSuccessCallback_m0900AAA8351E1B38AA866EC6BC8D138165E2146C,
	ExampleGUI_SessionFailureCallback_mE56B4A25ADA21A2A3CE51C1CC1B9DED1CDC93059,
	ExampleGUI_DeferredDeeplinkCallback_m0490F45A5F0CAB4623C4DE98C9056D175511544B,
	ExampleGUI__ctor_m781F1113ADEDC46C79DCD9CDEF57E6A92867A83D,
	U3CU3Ec__cctor_m822E5D6A5457A73E7B23238FFDDFDE17EE74C196,
	U3CU3Ec__ctor_m4795792C3C133DF92B1D9761F0AF23E6BE3D3FB6,
	U3CU3Ec_U3COnGUIU3Eb__6_0_m44C8CA7B767C4F1F34EBEA0E08A25658315F0219,
	JoystickPlayerExample_FixedUpdate_m9AEDBA111F95D67A006A5D3821956048224541B7,
	JoystickPlayerExample__ctor_m702422E0AE29402330CF41FDDBEE76F0506342E2,
	JoystickSetterExample_ModeChanged_m35AF30EE3E6C8CEBF064A7AB80F5795E9AF06D23,
	JoystickSetterExample_AxisChanged_m5CA220FEA14E06BD8A445E31C5B66E4601C5E404,
	JoystickSetterExample_SnapX_m25A77C3DE4C6DBBE3B4A58E2DE8CD44B1773D6A1,
	JoystickSetterExample_SnapY_m54FE8DCB2CE8D2BF5D2CDF84C68DE263F0E25B1B,
	JoystickSetterExample_Update_m99B2432D22FE669B4DC3209696AD4A62096C7D41,
	JoystickSetterExample__ctor_m2A3D66E05BCDF78D0F116348094717BEBA73EC91,
	Joystick_get_Horizontal_m78CF4472B86063E54254AC8AE0A52126E4008AFA,
	Joystick_get_Vertical_mA2B0917896CF9CE47A6D342D1734E43441C3D4BE,
	Joystick_get_Direction_m52502695D42BDAB6075089BDE442ABE72EAC81EC,
	Joystick_get_HandleRange_mB38F0D3B6ADE2D1557D7A3D6759C469F17509D77,
	Joystick_set_HandleRange_m686B579A1F02EFCD4878BEA27EA606FC23CD2505,
	Joystick_get_DeadZone_mCE52B635A8CF24F6DD867C14E34094515DE6AEFC,
	Joystick_set_DeadZone_mD5699A14E5284026F303C8AF8D3457DFA9920F19,
	Joystick_get_AxisOptions_mA74F5FEE31C158E5179F0B108272ED28A661E388,
	Joystick_set_AxisOptions_m541692280806ECF76F7C2C710973AF9D8AB334C6,
	Joystick_get_SnapX_m51CAFDCC399606BA82986908700AAA45668A0F40,
	Joystick_set_SnapX_mB2090989F6AC933B30823751D74E799FC8D9B87A,
	Joystick_get_SnapY_m35AFC1AD1DF5EDE5FCE8BAFEBE91AD51D7451613,
	Joystick_set_SnapY_m7419D5EB972285A9E5E446CD668BEC266D11CD86,
	Joystick_Start_m5E46F090910AB69BE9838BFDB91A4F6E6934480C,
	Joystick_OnEnable_m9DAE2F453BA576C3E2248CEF1990F60D3E6A5970,
	Joystick_OnPointerDown_mF176903D532D9129C90BBBD00FD7714BA3A0D8E6,
	Joystick_OnBeginDrag_m61F6E3409DA1D701467A10355CD99D20415E5387,
	Joystick_OnDrag_m39E69636AEDC0E471EAD1371A99F4694ECDBA1E9,
	Joystick_HandleInput_m15A4E86369A1AF0A4A5727DEC0FD93212A99901C,
	Joystick_FormatInput_mDDF7AF40138CF227F0106811C8749180FBF45342,
	Joystick_SnapFloat_mADE5AF21C67A2298A08CD12F9A8ED73AFA866987,
	Joystick_OnPointerUp_mEDED4DA77D954CBAC11CF82B57AB7A4DBFCDE22C,
	Joystick_ScreenPointToAnchoredPosition_mC1EB7560E844BF682674E4E7BD640604BC12B024,
	Joystick__ctor_m9BBE494CA4714F24227F33CB54C10B4DA78BF06B,
	DynamicJoystick_get_MoveThreshold_m16C670C1DA0A45E83F6F87C4304F459EDDEEDD5A,
	DynamicJoystick_set_MoveThreshold_m000C1AE325C0B9C33172E4202F2AFB59820517F9,
	DynamicJoystick_Start_mFE16C6CE0B753F08E79A2AEC75782DEEF3B96F72,
	DynamicJoystick_OnPointerDown_mBFA3026A0DA4A6B53C0E747A1E892CBC7F43E136,
	DynamicJoystick_OnPointerUp_m10389907A9D3362F6B75FDC5F35AF37A5DD5AE7C,
	DynamicJoystick_HandleInput_m3F157F4825BE6682228C8E135581C6404D268506,
	DynamicJoystick__ctor_m9DDA6263314BD7B97679DF14A4664358BD3E58CB,
	FixedJoystick__ctor_m8C8BB74E5EA8CA2C3DD2AE084301EC91F519AD24,
	FloatingJoystick_Start_mB22059CD82AF77A8F94AC72E81A8BAE969399E81,
	FloatingJoystick_OnPointerDown_mFE5B4F54D5BBCA72F9729AB64765F558FA5C7A54,
	FloatingJoystick_OnPointerUp_m80ABA9C914E1953F91DBF74853CE84879352280D,
	FloatingJoystick__ctor_m6B72425996D46B025F9E9D22121E9D01BEC6BD3C,
	VariableJoystick_get_MoveThreshold_m8C9D3A63DB3B6CF1F0139C3504EC2E7AC4E7CF99,
	VariableJoystick_set_MoveThreshold_m23DC4187B405EB690D297379E2113568B40C705A,
	VariableJoystick_SetMode_mB9D0B9B6E2E4F431E36AED6F5AC989305ADDB1EE,
	VariableJoystick_Start_m21743760641EA0317ACCD95949B99825446FE74D,
	VariableJoystick_OnDisable_m28E1A01EC1C3E09951F78D24CEA99EEAED625E17,
	VariableJoystick_OnPointerDown_m8ABE5C8EFBC8DB3A2EE135FFF3C0F67C533AF4B5,
	VariableJoystick_OnPointerUp_m65296D82A6C2E1BDC2D622B9C922FAE8E4544526,
	VariableJoystick_HandleInput_mD1BCF9A068737A9C057EE8CEB7E6DEB682CC03AB,
	VariableJoystick__ctor_m6C7B41973BE20A94F16DB5DCC9AA805C5D8DF852,
	AppLovinClientFactory_AppLovinInstance_mE49124170E0B40B5A7DA90F722F4EDF3D95E2B0B,
	AppLovinClientFactory__ctor_mE4BF0A3809E04795759A668A1525FB5BE17308DE,
	AppLovinClient__ctor_m1F78AD3047E4A1BC42B0D09885C9DFBA745A4F4A,
	AppLovinClient_get_Instance_m299C432C799DE4D562F54BF166C7D5C7C6E8F349,
	AppLovinClient_Initialize_mA295EBBAE9C66A82395CB050D491F550E324C10A,
	AppLovinClient_SetHasUserConsent_m63C9B4AC68146CFD047A6C894C0897A0D0BFA2D8,
	AppLovinClient_SetIsAgeRestrictedUser_mDDABEF99C2B1E351CA04D19BAE18A6AC850E7F9A,
	AppLovinClient__cctor_m79901FDE51452FB2DF90D4BD26DD8F26B9676D9B,
	DummyClient__ctor_m4B3AAD2D317866316100150FE739319A27219D84,
	DummyClient_Initialize_m40E5DE4B66A169169654DCC99E5E1467063DF2EB,
	DummyClient_SetHasUserConsent_m4DD67FB305D5A20966A1B7F541C2B7AD844B85E3,
	DummyClient_SetIsAgeRestrictedUser_m112FF92EC432AA1C7DD2FC1FE2FF9754DE23F3D4,
	NULL,
	NULL,
	NULL,
	AppLovin_Initialize_mCE699071BCBC1C6F8BC78F0DF444C106AC2A0121,
	AppLovin_SetHasUserConsent_m46E30DA73CF2EE6864A308F43EED8A32D53743E0,
	AppLovin_SetIsAgeRestrictedUser_m9A370550AA1664C4B7683C13EE2FB3CB65E64323,
	AppLovin_GetAppLovinClient_m567E3A189A980D79B59337FB2859CDE43953EA0B,
	AppLovin__ctor_mEFDBAF3971E66989A5CF3031985C031EE32296B6,
	AppLovin__cctor_m26A8AE7E9F360DC5C80644CDFA79C6DE20BED908,
	ConsoleBase_get_ButtonHeight_mF81259A974C9E3C4B24AFB8D95A18F0A03EE3396,
	ConsoleBase_get_MainWindowWidth_m9D6DB2FC65DC5D7FA781E5CF2FDB945C760D3C1E,
	ConsoleBase_get_MainWindowFullWidth_m7238BF9690E4397727DF038E6B185AFC2F7A1BF8,
	ConsoleBase_get_MarginFix_m286762B25357CC839197F05CC557CCFA376B7DB3,
	ConsoleBase_get_MenuStack_mA1E88A5414EA46C243855D30F28FB58F507F7597,
	ConsoleBase_set_MenuStack_mDC687D0701EEDAD9D55A44AE9449326C5E6ADFDC,
	ConsoleBase_get_Status_m71AFE45D2F68B01846188F212D65923B5B341728,
	ConsoleBase_set_Status_mFD36D24FF2D3D6554F11859251BB325B750776D2,
	ConsoleBase_get_LastResponseTexture_m02F8A694917953E5B184AC489DBE600009451DCE,
	ConsoleBase_set_LastResponseTexture_m8ED57EA554542EB683B1484F5DDE13ED983A146D,
	ConsoleBase_get_LastResponse_m5BCC50E8189B285B0AE6244435A8A9F6E3D4CE85,
	ConsoleBase_set_LastResponse_m847D45A344585AFCFCE231CF8993E2A750C76547,
	ConsoleBase_get_ScrollPosition_m489494F56C1E8E36B530B9CEBF1973640D39A47D,
	ConsoleBase_set_ScrollPosition_m7C2D74B4FF2432875A98B2F5A70F4D02494A9C84,
	ConsoleBase_get_ScaleFactor_m54D2E9E038B321F36372AB6284799232475B37F2,
	ConsoleBase_get_FontSize_mBA25F85EA4D6916062FE3D049BDE8BF618D5189F,
	ConsoleBase_get_TextStyle_m08AB60C7174363AE850D9F15A5C606E73D766704,
	ConsoleBase_get_ButtonStyle_m28EF2E33654CE9FC915EBECA78D93778517322E2,
	ConsoleBase_get_TextInputStyle_m99CA0D3F64E54CCF5DED958ACB141D07EEFB0C13,
	ConsoleBase_get_LabelStyle_m97006D9788464D58A9BA885AD2348D784699C346,
	ConsoleBase_Awake_m8068C221C280D202F4051E0CBFBF91C77568CCD7,
	ConsoleBase_Button_m8787498ECF036B9B3B5EB6B2FF49AFA154282D10,
	ConsoleBase_LabelAndTextField_m72E1BBF75934CE747195CB87737A2D7EFC503A8A,
	ConsoleBase_IsHorizontalLayout_m90873C8D21DF82E9733E95CD43BFB72A54912B1D,
	ConsoleBase_SwitchMenu_mA4A6A237A2D4EE813268204D38391A9D2AB7BC34,
	ConsoleBase_GoBack_mA682B2317560D91C931680AA95C1AD390611304A,
	ConsoleBase__ctor_m2295736CFFF716F434C20994E95820657BA9C751,
	ConsoleBase__cctor_mE537FEF66506B08BAF97CD2B40D29210C530F6BB,
	LogView_AddLog_mF87B865A91948F6D3F59502861BF67B6354B4A0E,
	LogView_OnGUI_m6B78F78EC906C89DA0BE45F5D3C5571126542376,
	LogView__ctor_m23A534F52CC925EE10260A5EC32DECBDADE2F35D,
	LogView__cctor_m112A3FBD481C9FC1A99F57426E9D79AC9E36F226,
	NULL,
	MenuBase_ShowDialogModeSelector_mB52DA9E57B0AF80ED97B7951B1815424F939A5D6,
	MenuBase_ShowBackButton_m33BAD66D85A5FA9E564FF8CF85E8920842238BAE,
	MenuBase_HandleResult_m0CB446ED4B8BDA605B140E61A4A1C6B442765E65,
	MenuBase_HandleLimitedLoginResult_m5A42F3F12FA695D59588DCC12004870D55C4ECB9,
	MenuBase_OnGUI_m2EDD4E22A69914B99CBDF039142129959C363D97,
	MenuBase_AddStatus_mA9B44C51B6D228B6EB901EA6FDEC17B6524FF98E,
	MenuBase_AddBackButton_m546C0ED7A454CD1CC80C9BCE27E56D924E6FCDC6,
	MenuBase_AddLogButton_mDCB6003139C36CE1485D8F6C70C23BF7A13FB812,
	MenuBase_AddDialogModeButtons_mCEA0B4B8B4C88A0A8F9A7B4737B4595B0FE5B208,
	MenuBase_AddDialogModeButton_m8F24FD4F294616E73BEC80B082FADB4D73BF1AC2,
	MenuBase__ctor_mD1A153F6EBCE35B5990669A20787B391A5DEC0E2,
	AccessTokenMenu_GetGui_mAFFC7AADE6A3C5A549A15922B3933F868690FBB7,
	AccessTokenMenu__ctor_mA506557143C1CEB6F2D52ED9EECC44FB9058804C,
	AppEvents_GetGui_m4D6548C60C8A0B6B81EB16E4F20416013669AE09,
	AppEvents__ctor_m633F317D3E69F6B62705498B7790C36075F3C09B,
	AppLinks_GetGui_mEF6BABE28307C19F823198102718BF2DCBE91FC2,
	AppLinks__ctor_m2408DE5760F132BFD7437824410EE9FA14FE7F02,
	AppRequests_GetGui_mF21A92393C08E39EA4404B8AD9DE1076A42A7060,
	AppRequests_GetSelectedOGActionType_m350F98087C7EB39851FC4562007A9EC26261BA1A,
	AppRequests__ctor_m22F0E79DD38013C0846E20AC2E9F96FCCE6356D8,
	DialogShare_ShowDialogModeSelector_mCF0A9C2537886392CC684FBD4939F14C6B8A1F87,
	DialogShare_GetGui_m5A303622B0B9250A1C0E9570683CE6F5DC894783,
	DialogShare__ctor_m18343E0A2E25DD13D50F65E99F855EE5CBB4A44F,
	GraphRequest_GetGui_m04AC165B02447897B6210A4EEA01341C54A18E7E,
	GraphRequest_ProfilePhotoCallback_m377E44982A0745303405A4B2792BEAEA617DC962,
	GraphRequest_TakeScreenshot_mB6D07D0BD896A3FDA1B3A0CECD1F41A82BB71153,
	GraphRequest__ctor_m1893756902051EDCA6A648EFF3F8C79482D72F17,
	U3CTakeScreenshotU3Ed__4__ctor_mC5022BC2D6F58E67E8D34863FDDD01DB102192A9,
	U3CTakeScreenshotU3Ed__4_System_IDisposable_Dispose_m024CE39BF1690FA910DDA8A774BFC512D437756F,
	U3CTakeScreenshotU3Ed__4_MoveNext_m1BC8573BD27ED010119E112AA3BE6CA166C05D31,
	U3CTakeScreenshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF4F64034B62BD451ED5DB8EF972FC4B40806FC23,
	U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_Reset_m99D229E478D52F853F19ADCBCFFDAF25C148D899,
	U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_get_Current_m13E1B2CBD35D3D58C012334F0A4FD17B3930C030,
	MainMenu_ShowBackButton_m0AA293F10C340E46851503EF2B29E235FE61BBE8,
	MainMenu_GetGui_mBEAC140C4E2DC7BEE3B2F1FA3AA744770B6BE1BF,
	MainMenu_CallFBLogin_m8C94D27865F5C4197AF2C2A90D5E60BBA9D08269,
	MainMenu_CallFBLoginForPublish_m5519242C5D4241EF52CA37F78A5BCEEF18973A19,
	MainMenu_CallFBLogout_mEE96298E9614D3F79009BB034E4DB1047A7855C3,
	MainMenu_OnInitComplete_m36800A94B891CDDCC9C4DAED0CC5639190C2E207,
	MainMenu_OnHideUnity_mDE312A6C6A2FE6F756E72A64388E31F5BE626D84,
	MainMenu__ctor_m15C7560C4A0296BB1D433FA3D8B1C82568B7179F,
	Pay_GetGui_m38BDD0E07950F97403560C5AAD4D35F4ABE590AA,
	Pay_CallFBPay_m6C1243E4AA248F715366AA5E8996D25431AA2A58,
	Pay__ctor_mF524FF0E64FFA62C166094B9CFE640D276161A26,
	BotController_get_IsDied_m63415A22D604ABA1F5B2968E873A62C650FEF7F1,
	BotController_set_IsDied_m58AB128CD3900EE06594A77EF3DCB8DC4DD9882C,
	BotController_Start_mECFD2A31A4CF7B879A4D78BA6B0A3190D955EC55,
	BotController_OnDisable_m1BE5DD7FE3B0F170AE190439A1FD883906DDF706,
	BotController_OnValidate_m612D3A047D552240918EC20DD4425CAB0C23A9A9,
	BotController_InitializeNewGame_m926980AA77770138EB5F4CBD8A71EADB160593E8,
	BotController_StartRunToTargetPoint_m851299EAB633424DCFB90AD045DBDB6C949820E4,
	BotController_RandomTargetPosition_m05F8B41F1960585DEC4367BBFC970EB1F3351E35,
	BotController_Update_mED0D75A52B193110501D81E40611A53D3F979BDF,
	BotController_TriggerUnderground_mC7EBDDC4AB48468C70FD54D8B1A9437BC673F0F3,
	BotController_RunToGridResult_mA77C9006DE3BCB45361AEC623C72C4A804E8E2EC,
	BotController_OnPlayerDie_mE671BCCB097FCACEE46606A630E871120268EDE5,
	BotController_PushBack_mDA610250EBA5609413A8E25BF34F73A79BD09F9B,
	BotController_IEPushBack_m3AF2D1848E79185FFDDAC3D41D2A56B392387D61,
	BotController_ToggleRagdoll_mF3EB813D81F4394A366CDBB64C452F4E36A0F18B,
	BotController__ctor_m39D3F896495D9FEF2BAD2C8B2B33B974CB2A3EAB,
	U3CIEPushBackU3Ed__36__ctor_mEB5D3F0500DE66401C18D8FA98541EE13D880EC4,
	U3CIEPushBackU3Ed__36_System_IDisposable_Dispose_m70E4C09F1848215B1B0D4EFEA8DF4A8DF07648B7,
	U3CIEPushBackU3Ed__36_MoveNext_m0F95494B9A3A0561A7CB870F69B55D61E7084465,
	U3CIEPushBackU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87DF9A1E775B3564035D7623D59C88419CD9F687,
	U3CIEPushBackU3Ed__36_System_Collections_IEnumerator_Reset_m0E82361595DE77C97B44EAD650917D238AD6D5E9,
	U3CIEPushBackU3Ed__36_System_Collections_IEnumerator_get_Current_m3B511B8033F95E603EF7A93194B00E8D7F8BB27A,
	CameraController_get_Instance_mFD916D72E823F41D1BB917486DE0DA7E3601AFBB,
	CameraController_set_Instance_m8C6DD53EB4753EA7B8E1A51FB92F7A2D5E575E52,
	CameraController_Awake_m228DFEE758BBA7002ACB717A2DFED29959B64943,
	CameraController_Start_m1711365E7028C2B26425FCCB569E1527B83E8513,
	CameraController_OnDestroy_mF4CA27701B6AF26D2EF4F6327F30A84862F018C7,
	CameraController_InitializeNewGame_mE2510481FB56966A564EDAD54C1A9064AE58B3C2,
	CameraController_StartNewLevel_m02D6901CCFD058C0CA57B7AB881CFA9E90D2D41E,
	CameraController_LateUpdate_mE04957AF2CA40A36A4AB546E82EAB5E3F491DCE2,
	CameraController_OnPlayerVictory_m5AE2D204ED9D2D03E902535B19B69032531E08D1,
	CameraController__ctor_m3E4D0BC1FD1534718F6D4AD1FBADF0A24CD1ABBC,
	AdsManager_get_Instance_m89C7BFBC5CE29F77AB53A243F1BA80A6ABB636BD,
	AdsManager_set_Instance_mC7ECC2C57232DB9AAFD3E2DE6D87429DB4DF9A34,
	AdsManager_get_IsBannerLoaded_mC52A5A2A8217ECE54B22ECD045BD6ADAFF469A7D,
	AdsManager_set_IsBannerLoaded_mA04DB9C019C849AB487E11E0465F1BC9986BD815,
	AdsManager_get_IsInterstitialLoaded_m8AFF32ECA1FEC8AD5B94D250489A4340FE828C08,
	AdsManager_set_IsInterstitialLoaded_m0CC59AEB2A5785CDDFDA39E6409D236E4E0DA976,
	AdsManager_get_IsRewardVideoLoaded_mC22ECF4F9BE721BDCCFFA2E8594F529D1DDCF4F2,
	AdsManager_set_IsRewardVideoLoaded_m7ACD487A9E0EEEB3682D64A5A2EFE699D0DCC2F8,
	AdsManager_Awake_mF8BF8E9CA2461217CE98A394093C8C7644321843,
	AdsManager_Start_m26609DE96EE4F19DE80B214DF00308F998793585,
	AdsManager_InitMax_m8D467ADB40AC0CE9D0CAD61C5BF0DF4C3DCD4DB9,
	AdsManager_LoadAds_mA1C70597CAC67BAC92801EB50DCDB3270921FA50,
	AdsManager_OnBoughtNoAds_mF719B5A3A042512FE3BCFB39B0C35A44916A88E3,
	AdsManager_RequestBanner_mA8C7E65E97C95169E73E5CA2F03A5FE926BD7631,
	AdsManager_OnBannerAdLoadedEvent_mDF31E97ED7A9809CB132BE6ED5066E691BA9E78F,
	AdsManager_OnBannerAdFailedEvent_m06C360AF48A1F9413FF92DAA254682D3EC43B01E,
	AdsManager_OnBannerAdClickedEvent_m422D599DBC614E8A2722B15082BA5E3665E87A72,
	AdsManager_OnBannerAdRevenuePaidEvent_mD2085C5C50DF3362B0FF03AF15C568DAEB8BFC03,
	AdsManager_IEShowBanner_mA46D4B5B1D6CE4CE0F7E87CA34743555D16270EF,
	AdsManager_RequestInterstitial_m495BABEC3EC3CD01333D3A6FD3596F4830D4239B,
	AdsManager_LoadInterstitial_m80E4C77A45F2D9CB1B3247D292D52CE466F8A001,
	AdsManager_OnInterstitialLoadedEvent_m3C4A4FD6E3C6211D66FB2DE480BF09482C1100AC,
	AdsManager_InterstitialFailedToDisplayEvent_m9846B122B0E4D869F5C16073F723865E465F6B25,
	AdsManager_OnInterstitialFailedEvent_m16DF579ECCC9BD282D850DEED25BEAEF77C60FC7,
	AdsManager_OnInterstitialDismissedEvent_mF82B9884F8915D4767B5DD957BD98C62F3EC58EB,
	AdsManager_OnInterstitialClickedEvent_m1BE81BB0F12297A115BFD71BA5A8BCCA8A4D79D2,
	AdsManager_OnInterstitialRevenuePaidEvent_mE861343869179709D3F671BBDCE6B497F0E050B3,
	AdsManager_RequestRewardVideo_mB021A0C7F69D1B1298171799BEC802AA01D1F3F6,
	AdsManager_OnRewardedAdLoadedEvent_mE60652E69DB32F073156DFBD925DCBA6D5CF7098,
	AdsManager_OnRewardedAdFailedEvent_m6CF4A8BE667F7ECBCE64F47732D719548CD5100D,
	AdsManager_OnRewardedAdFailedToDisplayEvent_mA1405D0D50C49F2A2D4977642A450E2EBB9C793E,
	AdsManager_OnRewardedAdDisplayedEvent_m53F0186B452E9C28E2FCA6DF41425DB72D75A791,
	AdsManager_OnRewardedAdClickedEvent_m81ED193C031DC3945996E542A86CCF2AB6CA0FBC,
	AdsManager_OnRewardVideoOpened_m520618A2E440A4CD0F1695C40718942E5D185DC6,
	AdsManager_OnRewardedAdDismissedEvent_m793AF672292A9B05D3F4F28B4657CA9C20F3277E,
	AdsManager_OnRewardedAdReceivedRewardEvent_m77FC56B5A307E4A4A5554F05AA9BB565A6CD8975,
	AdsManager_OnRewardedAdRevenuePaidEvent_mBC41E4F65C3F586719C203C639A4A41DB42FDD11,
	AdsManager_IEDoRewardVideoRewardedCallback_m4D8A05812E25F41280794D5286F5DDCAD50BC2AF,
	AdsManager_IEDoRewardVideoClosedCallback_mCF6608A95685EBE846158817F6AA7161D054C9B5,
	AdsManager_LoadRewardedAd_mD815A3B22E2044D2956D2B0B40A3A8E9BF3C4C83,
	AdsManager_ShowInterstitialAd_m9E2C121CBE5CB80578EFF257FD04DA00D67BA6AE,
	AdsManager_ShowRewardVideo_m68ED33958B7FC3A76D21DEFBB7D768A42F2C48B4,
	AdsManager_OnApplicationPause_mAF1142F41624784E6407B3C52719AB8FB5A4AB60,
	AdsManager_TrackAdRevenue_m4FF79E040FC2F8B5AA06DF0A2B0981B0D7A0DD34,
	AdsManager_Requestbanner_mDEA572F6C9A0EAD55488E6BF1E47D6F06AFC310C,
	AdsManager_ShowBanner_mBE83916DAD40B7B9EB34079BA3C0E8DE692AD99E,
	AdsManager_InterstitialIsLoaded_m990B2C257ABFF88EFFDF411888099F4A69D2EAD8,
	AdsManager_VideoRewardIsLoaded_m93EEF85A59B42EFA939C81D6BCAF62BF67166A83,
	AdsManager__ctor_m8F03684B891950A46800EE2DC0707469D0B3FF7F,
	AdsManager_U3CInitMaxU3Eb__46_0_mF3422E16B740FDA20935BFE8B8E2A9D59FA23BBC,
	U3CIEShowBannerU3Ed__54__ctor_mE55F6BC876B5A446AC412BA26D62052EE64ECB89,
	U3CIEShowBannerU3Ed__54_System_IDisposable_Dispose_m24CAC7454DB6D0CC46B715B3D159A668166CDEBF,
	U3CIEShowBannerU3Ed__54_MoveNext_m107505AF578A310406DB47521A1554E82D10C11A,
	U3CIEShowBannerU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCEFEA230C2D3CB9E998A340F0377BF1C94916C54,
	U3CIEShowBannerU3Ed__54_System_Collections_IEnumerator_Reset_m2509B81FDED6AF0AED1315F2BEB9E216625463F3,
	U3CIEShowBannerU3Ed__54_System_Collections_IEnumerator_get_Current_mDA82D84F5A2DADA15888C7BC0E0AC32A3FC579F2,
	U3CIEDoRewardVideoRewardedCallbackU3Ed__73__ctor_m9110C0ADB8476971785C3E519D4EDC2221D547E9,
	U3CIEDoRewardVideoRewardedCallbackU3Ed__73_System_IDisposable_Dispose_m44EF9687E17F93CEB21B300BCED3D4EAEC1A4F52,
	U3CIEDoRewardVideoRewardedCallbackU3Ed__73_MoveNext_m82B2072D95948CAEAE3FE79A1E808E3E6DB7E20E,
	U3CIEDoRewardVideoRewardedCallbackU3Ed__73_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1F999B6B2D27D361338F586D9F8FD1FA35A5BFF7,
	U3CIEDoRewardVideoRewardedCallbackU3Ed__73_System_Collections_IEnumerator_Reset_m76D4FAA44521CCA25BE77E30DB848E5EE8577C79,
	U3CIEDoRewardVideoRewardedCallbackU3Ed__73_System_Collections_IEnumerator_get_Current_m7B3DFA1F48FE26F10649CF64402E8D7425A20693,
	U3CIEDoRewardVideoClosedCallbackU3Ed__74__ctor_m40418B31F69800DF23D1E7657FD1217F38EA91E0,
	U3CIEDoRewardVideoClosedCallbackU3Ed__74_System_IDisposable_Dispose_m8A2F41A17DD37CF86C8B39F443D74C2009A76CE8,
	U3CIEDoRewardVideoClosedCallbackU3Ed__74_MoveNext_m5A02997D2ADBD15586FB5DA3E6B4206396B1AD61,
	U3CIEDoRewardVideoClosedCallbackU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m529E9135EF65E6488A93D9622C0D074EB108DD37,
	U3CIEDoRewardVideoClosedCallbackU3Ed__74_System_Collections_IEnumerator_Reset_m2562181F949759F38E571607B6CB10ED231FF43E,
	U3CIEDoRewardVideoClosedCallbackU3Ed__74_System_Collections_IEnumerator_get_Current_m8BE854EDD23ECC0FB02974523FB2E0E9C6316E92,
	AutoRotation_Update_m2D83DCDC263AD25A68CFE5BD966A7C7A77840CB0,
	AutoRotation__ctor_m60EA534D955B452A3D2B4E8B5C0730855AEE6703,
	Constants__ctor_m46B3DF6C0B62C5925652A8D37CBDD11E3A57F8D5,
	Constants__cctor_mB80636127E24EB163DD67F6C97D52507E173BA55,
	EventDispatcher_get_Instance_m046F93FB07E8F0E75912861DE66BA0A65B52F3F5,
	EventDispatcher_set_Instance_m649143682312DB945292D7553B9E5D776DDF51CC,
	EventDispatcher_Awake_m8455EA8E92FF1DE15009CBBEC86AD071FFEA38E2,
	EventDispatcher_OnDestroy_mCCBEF07567F855D5D4B633E59AE243E48856CB48,
	EventDispatcher_AddListener_m8307E3C820300B29DC1A739F0CB1C214C48E8D14,
	EventDispatcher_PostEvent_mBAB648658AE85BC0DDD7F1F85216E6A40049BF76,
	EventDispatcher_RemoveListener_m869BC7202F8FA11ED18AF10BDF5A092D895D9DC5,
	EventDispatcher_RemoveAllListener_m35D3FF04596C9E905C19513A94ADB43537C8A936,
	EventDispatcher_ClearAllListener_mB99A53A0F9642321188094AA81C7784150305937,
	EventDispatcher__ctor_m173DB974E29B69AC3C07775F67AC78FF1052875F,
	ActionCallback__ctor_m00C66FE39E2F6E97C8D96C873DC01DF42B545D37,
	ActionCallback_Invoke_m1C3A2774148F9843BF87AE50288B94BC684874E6,
	ActionCallback_BeginInvoke_mF0491E24B2356315595C893D695C1E5473E67528,
	ActionCallback_EndInvoke_m9E2EF563D4131C58C84E1F48232C26285C819303,
	EventDispatcherExtension_PostEvent_m92BF43BC0FF6F17D2AEF94A2F14296C7196DF1CA,
	EventDispatcherExtension_AddListener_mD04C17591104474F01F7F4861EAB9C1E100546C9,
	EventDispatcherExtension_RemoveListener_m06BBEEF5F1559824CC42CB11B1699B847B9A3CB1,
	EventDispatcherExtension_RemoveAllListener_mF5692345464600A9F01DB031D97B34A5C2BB7CDE,
	EventDispatcherExtension_ClearAllListener_m66D02C1E36E2C25EB978AC7BC7237ED64337D090,
	FirebaseManager_get_TestGameValue_m04C9A77827BB3FBFC7E1A041E997C2211597FE53,
	FirebaseManager_set_TestGameValue_m710A5B19A3D01FACB94ACA61211CE9D8104DC41D,
	FirebaseManager_get_Instance_mDEB6781B7A6D8508DE5A88B092D6D69FB3B38DF2,
	FirebaseManager_set_Instance_m462121C1D44304C7059D224BCB1D75E69639AD4A,
	FirebaseManager_get_IsFirebaseReady_mF97530F5C4E97E9D502161D341BF1EDE49A3E476,
	FirebaseManager_set_IsFirebaseReady_mA33B3004BAD0575BF9CD348B108615A633F0F547,
	FirebaseManager_Awake_m7CE26934278B19D64C61B2B991477EA53DD03EC2,
	FirebaseManager_Start_m85C19F19BFED89551E1365FFC383C63DF1662CAA,
	FirebaseManager_InitializeFirebaseDone_mA64EE226D65A0891DE085A083A930E2937D08198,
	FirebaseManager_FetchFirebaseConfigDone_mF559AB5474414289E1208306C75AD553EC3D972B,
	FirebaseManager_LogEvent_m888365F2FA2964090AE6A3C7FA589414CAA15C69,
	FirebaseManager__ctor_mD38384E2549151C86E3EFACF2CB126FD4AC40DA0,
	FirebaseManager__cctor_m1EEB766C1B997FB960358DB42CD47DC51BFACFF4,
	FirebaseManager_U3CStartU3Eb__16_0_mD3658BF021506790330BB855AC753623DDD88086,
	FirebaseManager_U3CInitializeFirebaseDoneU3Eb__17_0_mD6BBE91BE5DBDFC634B27D8533DCD8DC85D8BEA4,
	FPSCounter_Start_m8D9BBC3654CFB2F7311CE1B006EB3545F4D799D5,
	FPSCounter_Update_mAC0E80A7667F828266DBD5ACC4547799F1988A28,
	FPSCounter__ctor_mDF524F4E7C59782F1CCEB236F28170E9189EE9ED,
	GamePopup_get_NumberPopupShow_m511AF17578AAE2F4293ED2A231229794D34E5F8A,
	GamePopup_get_IsHavePopupOpen_m8ADC1B3498F63C07EA28E09499DB323D43225370,
	GamePopup_get_RectTransform_mA44FD26BFB42F48F30F5EF04F3832635C1033087,
	GamePopup_get_CurrentPopup_mFA0A3C885D802F94A15CFD854967A07CDAADDF3F,
	GamePopup_Show_m91082F6455FD2DF47A99AB5619605584B2BDA3E5,
	GamePopup_Hide_m4A7FCABB603C0E335E13FCEB6446EF2C70380CE9,
	GamePopup_OnEscapeKeyClicked_m544E16A11C2E8D5D8F6168E0B0CABB04F82BA783,
	GamePopup_Click_DefaultClosePopup_mA2CB677911B46A439E3226ED0B2792444BBD4225,
	GamePopup_AnimationCall_Hide_m00E0678AEAA75BA3A5C0B3BEE5201DDA34504BEA,
	GamePopup_ClearAllPopup_mB04D0631AAE78200AA24321D1AED8F64053C2437,
	GamePopup__ctor_mE345689B603B61BF23CEFA75CA1E1A5D705834B2,
	GamePopup__cctor_m89986E3ECA697666E8B15DF65E15127BD9229D13,
	Logger_Log_m6F3EE6B05EF1CB73DECF048100777487B1CDD7A1,
	Logger_LogWarning_m28CD5C06586810B23CE56185D2215A638F75EA1F,
	Logger_LogError_m985236BF9496AF7746D9CD47050E7BBB86B6E6B4,
	SaveLoadData__ctor_m1FEFCB4735CEAE3FB1F6AC8B97B7676336A7325D,
	SaveLoadData_Md5Sum_m535D40EF0E7E6CD92B96E7D98020B6879565B261,
	SaveLoadData_UpdateDataToLatestVersion_m9F19121C8E3E920BB74864424605E99A5870FAA9,
	SaveLoadData_get_Data_mC66D57C2327DD2788B265D8751F01DA0B624FA28,
	SaveLoadData_set_Data_mB59DA1260C0EC6B33733B3FCF7F18DACD47B6BA3,
	SaveLoadData_LoadData_mF8AFA3998CFF7B6AE5F97E1FAD966C583D48AE1A,
	SaveLoadData_SaveData_mE87CF48DA01B6D624DB1C977ED354E7BB502A547,
	SaveLoadData_AddCoin_m992B8B74AE1F16E1B548D95CA4611AB6D47E729A,
	SaveLoadData_NextLevel_mC1F6C1CD83369373306C8BE267A79423E252C587,
	SaveLoadData_RandomIndexCharacterNeedUnlock_mA807994A9051556099226A0884D349944CBFCE66,
	U3CU3Ec__DisplayClass15_0__ctor_m2A052FBC6E3B96516E6F215B3596CD9E581AB22D,
	U3CU3Ec__DisplayClass15_0_U3CMd5SumU3Eb__0_mC1A89EA328E0E9F1EC7B466A6179DEA461F2F887,
	SoundManager_get_Instance_m3AEC930B80711741CC64E5DBBD46E884026E1A34,
	SoundManager_set_Instance_mB9B4EFC6B14C6880343E91E568F53821226081CE,
	SoundManager_Awake_mB231859CF84A895E30D233A88A288830C858C705,
	SoundManager_Play_ClickButton_m59BB00CCFC5CEF1C2E41E8B15ED95D792170978F,
	SoundManager_Play_Firework_m2C0A6626625138847CC3004C77201FCF218A52B3,
	SoundManager_Play_GetCoin_m4F0249D246BB2D2C4891C5AA1BF713D8A6B4B6A4,
	SoundManager_Play_HitBot_mE9F4B117042C50EAA7B61C9EAD738F42C9D40D29,
	SoundManager_Play_GameOver_m8D51C4FA5941934F4CCC56D1EDCA50A99FBBC09E,
	SoundManager_Play_Victory_m8378F266D7475B90EB13C16E533318CEE766C50C,
	SoundManager__ctor_mF50352EFDC43BCD2B58E8F0301C91C361C0AF1C6,
	StoreManager_get_Instance_mD8D3E05EFDD07AEE406FB4C9A397D8FC53887881,
	StoreManager_set_Instance_m1F1387F316149E0C835CFE29E55DF5DE837F8198,
	StoreManager_Awake_mCECB207C77BBBD79617DB4C17607CB0B30959412,
	StoreManager_Start_m7430C8F7627AE60462D6F59B4D3DEBFAD2D3EFFB,
	StoreManager_InitializePurchasing_m02DDF4E2B70879D39EE8B982535E9C8AD3891661,
	StoreManager_IsInitialized_m2CB2893D9CBB226FA95BE5D48EECA8526FD9D4B9,
	StoreManager_BuyItem_m45A9EBDAE616C06FADFA937E9D1099A1B0777AD6,
	StoreManager_GetPrice_mF0B76EEDB419BA8092131CE2CA5BAB4FAD413D5E,
	StoreManager_RestorePurchases_m056D3BB26AA2625C25DAA1DB889F892C9DB05C57,
	StoreManager_OnTransactionsRestored_m5D371CFB0B4CECCFB5FAFE1532D8E4A993F51DD0,
	StoreManager_OnInitialized_m206C6DC94709C9F5E49FB6151FC86D5BA13BA041,
	StoreManager_OnInitializeFailed_m640D5685F4BC7CD6F076717CC975661CE7B98357,
	StoreManager_ProcessPurchase_m8C29E4489FCBAA26A74AD634FED39DC994C5F2C2,
	StoreManager_OnPurchaseFailed_mC3055CAD71C86533AE30A26D8974477A918790EA,
	StoreManager__ctor_mB47881B0785F4B7B00809FD0A2CD3DC3A06E7992,
	SuperImage_get_mainTexture_mDE93967675478B3984E1D11AC7D2F4F5937339F5,
	SuperImage_OnPopulateMesh_m613343577F7F882145FFAACB63E6102A12CA7FDA,
	SuperImage_UpdateVertices_m1478012E3D36C2D55C2F67DC0F3747A0B99B9BC1,
	SuperImage_UpdateVertices_m483BD64D5AD07EC38F6CF88B5ADC03F1328F74E1,
	SuperImage__ctor_mA8A96EF0912B3272F52ED3A984B566F4D1D46107,
	UiManager_get_Instance_m0F69E0B2F3E56FB25404FD9695D1B37A0EE81870,
	UiManager_set_Instance_m158AB08B22E591609E59D166661A61F3F3AF2D15,
	UiManager_get_SuperImage_m29922B5B66B403C81C8FECA07FDD57DAC996F8AE,
	UiManager_get_RootCanvas_mEABA6028D8BBE0BADBEF3830D1EDB44F8F26585B,
	UiManager_get_SizeScale_m30D6A4BDDCF12558A32B3624E802A49FA7C58AD6,
	UiManager_set_SizeScale_mDC8928E8957908A14CB0352D1E950DF550B07570,
	UiManager_get_CoinPosition_mE33FE79367B35A2319D359C8AB1C1D02A0AA8A8F,
	UiManager_Awake_m66267834B7A3A6F590F1C5908CA6E533BD848BFD,
	UiManager_Start_m04854C3B25D17BB036BE8AC9BEFCF3C066D81F77,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UiManager_ShowSuperImage_mCAA23A83EFA7347E01D3BAE0AF75BD38E54E5925,
	UiManager_HideSuperImage_mACF1CC7B66DFBA12DFBF6EB2C6C473893D9AB3F3,
	UiManager_ShowCoin_m8994865C16CFFE20A28B4B93B03D1AAEEC3A08F0,
	UiManager__ctor_m483258F0D8DC96CD38FE86F254AE0F33FAA2F8A0,
	U3CStartU3Ed__23__ctor_mB1E0AA28F3BE6CC004BE229ADEA18831F3036196,
	U3CStartU3Ed__23_System_IDisposable_Dispose_m87B3CC49A5C4568140EC610F4A17DB16D733662E,
	U3CStartU3Ed__23_MoveNext_m03241495E437ACA94B4DF532A2998D6FFBD40857,
	U3CStartU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7247B803EA76009541937A2B21E7A1A5EF17398F,
	U3CStartU3Ed__23_System_Collections_IEnumerator_Reset_mC0C98B770819D6CFA5B63022A0914CE56E0EDF49,
	U3CStartU3Ed__23_System_Collections_IEnumerator_get_Current_m90F037AB99A992F6380DDE60F98AB9B4F94FCA3F,
	GameManager_get_Instance_mA33B8A03D44543B6721A6B4E6232FD3745FAEAC9,
	GameManager_set_Instance_m194FEBEF6D0DDE5F32F4818C90DAD1ADCF6ACBC7,
	GameManager_Awake_mF4EF1B6F6B1B260FDC5953909D67D0B6E906E1B8,
	GameManager__ctor_m6107A41B2117199E92B9A14649119414EE437789,
	GameplayController_get_Instance_m3AD44C84B6AAF71930D1779B4BA83098E9D42318,
	GameplayController_set_Instance_m226E911D6E1C4636D9A00BF26EBFB284B754F0D7,
	GameplayController_get_State_m79E017B68A5568FB4A42352BF6663FCBA6917D18,
	GameplayController_set_State_m274BC55B97FF08BAE18C49B66F2F277538BD8388,
	GameplayController_get_TestGameType_m3E3ACA8257CBCCCB1393BC0E5F548888105A5D53,
	GameplayController_set_TestGameType_m046CE058F92ADA3CBC2B80826B13CC383714AFED,
	GameplayController_Awake_m8796CAA118409CE6F767DD53A10184B860464DD9,
	GameplayController_Update_m932DAD3BD66DAF071603EF962FC1442241000326,
	GameplayController_SpawnPlayer_m32C4EB7D5E3BAF883AB616392BCA64F149039143,
	GameplayController_InitializeNewGame_mFF8CCB91A3F45CE3D9EF3F5CB38CA3493B17DC1B,
	GameplayController_StartNewLevel_m65D25F4F60B2A4F183CDDDF5EF5AEFE7F2A9F812,
	GameplayController_ClearAllGridItem_mA1049343BA8D9D1D793F587CE561A883CD0E3418,
	GameplayController_RandomDecal_m8F6F021279755B2B463D3EED3A2B3EE2FF20F266,
	GameplayController_ShowDecalResult_m9FEB502ACB8136F6ED6BD2C6A9DDAB1A198D4B2D,
	GameplayController_IECheckPlayerNearGridResult_m7B7C0289B5992953AD0FAEF6E132959AA91E655A,
	GameplayController_IECheckPushBot_mD9AB204331079A1273013E933CF49DA8AF86BEC9,
	GameplayController_OnCountdownDone_mB844D07ACBDECB7660DE9E8EC766D24A18C1CB34,
	GameplayController_GridItemMoveUpDone_m0DFBE29093B8AFB9C614F4B430D351AB32BF78EA,
	GameplayController_PlayerWin_m5AFC70381AC54F2842D584B8297ADD895B6AEB3C,
	GameplayController_OnPlayerDie_m51C5237CAD474B1E8FF2FCB2E64C29CE2D711A37,
	GameplayController_GetRandomGridItemActive_m441762DDCB9FB9E06E79F54D574744A6D62C8A70,
	GameplayController_HaveBotKilled_m82385C73D1018299B2D0A354FFEB520DAC761043,
	GameplayController__ctor_m3C8705B3712ED60635C1943848C8D04E02622626,
	U3CIECheckPlayerNearGridResultU3Ed__45__ctor_mC4F6C25B2DE9D2850EBC31B7B68D26512A23FB72,
	U3CIECheckPlayerNearGridResultU3Ed__45_System_IDisposable_Dispose_m31E332BECB03640F84E9A338967F49338BB88FC3,
	U3CIECheckPlayerNearGridResultU3Ed__45_MoveNext_mF68BFFA10CB7407795CADD1C8C04D845B6929343,
	U3CIECheckPlayerNearGridResultU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB826C1D388E9E12928B896852A772469677DF368,
	U3CIECheckPlayerNearGridResultU3Ed__45_System_Collections_IEnumerator_Reset_m6ADE6777E5672069452A8BFE6CB200C8E171A88D,
	U3CIECheckPlayerNearGridResultU3Ed__45_System_Collections_IEnumerator_get_Current_mA5F976BE4F1B7E36EDF27648D3122BF89A953F35,
	U3CIECheckPushBotU3Ed__46__ctor_m466EB7CCBF1335A1F55C38F0E34E46A59CE3C202,
	U3CIECheckPushBotU3Ed__46_System_IDisposable_Dispose_mA6D328E54083A4CFAD09680985FABA3B1253E861,
	U3CIECheckPushBotU3Ed__46_MoveNext_m4C59BE8407653B3ED1809F3F8E0C47FE5845B27B,
	U3CIECheckPushBotU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F1B7458C355DF0AED6A74E059707D90C5048A6D,
	U3CIECheckPushBotU3Ed__46_System_Collections_IEnumerator_Reset_mFCADB12534CA1CF277414BB659505F0DF152899D,
	U3CIECheckPushBotU3Ed__46_System_Collections_IEnumerator_get_Current_mFFF5A1A8B3F9CE0F471744A8F96B990A867B313E,
	U3CU3Ec__cctor_mC26489CA69666053EBAC7380439A312BE90535B1,
	U3CU3Ec__ctor_m88BC53F773CA9C6C2B2BC083ADBDA154769315E3,
	U3CU3Ec_U3CHaveBotKilledU3Eb__52_0_m89FCE30705FB9A437E9588F537D15FB8DC1C3531,
	GameplayUi_get_Instance_m2C600C696E55AA47C11A09C510D1ABE0C5F0D0E5,
	GameplayUi_set_Instance_m4CF915559AFA718E104720E2445D121F2A2E7652,
	GameplayUi_Awake_mAE3C793D7D39422BD42F63412409E7A4077D0E19,
	GameplayUi_OnDestroy_mC5BA36169A8C2E2434021CE9A9BE4AA80C2EB589,
	GameplayUi_Start_mD587709DC9812D10387A77EE0310D3A66880554F,
	GameplayUi_InitializeNewGame_mF18977DD6886D72A14E608518137491F12A8A8FA,
	GameplayUi_StartGame_mFAA3C1268CD47C7E5C2B2A02A0EA283512281697,
	GameplayUi_ShowCountdown_m294D751C20D2689F5DCF92620AD748B82CCD88D4,
	GameplayUi_Countdown_mCF8BC21DAD417831AC75EC2B4B70E298664B4846,
	GameplayUi_UpdateLevelText_m2EC07DE4432D90BBA4E1A9FE83B2E5145F6A4BE8,
	GameplayUi_OnPlayerDie_m50680823EF72045B9D73CEDB389FFF8EB4BBF861,
	GameplayUi_OnCoinChanged_m3B5BF036A0BB7FB74F3804DACEB9876375112C29,
	GameplayUi_TouchStartMovePlayer_mF90074BA38DF57285DD19456B15D106ECF5DD138,
	GameplayUi_StartMovePlayer_mF829C4B8255A7EE66FDC4D6A23917FADEAECC4EE,
	GameplayUi_UpdatePlayerVelocity_m1DCF9607722C9B76EE8ACE8DCCC973C4B0568593,
	GameplayUi_EndMovePlayer_m70EAA2F50F24E27901FD27FFC09A7B4C2FD344B2,
	GameplayUi_ShowGameOverPopup_m0225E33C23CB41215BF9945AC5CF1F68E0D5C7BB,
	GameplayUi_ReplayLevel_m7232C8598301DA77BF360E12CE7425260DF9057A,
	GameplayUi_ShowVictory_m7A6E93997A80B9B6A7586390347B290FAA447366,
	GameplayUi_ShowVictoryPopup_m8E5F28579A032EDDDA300BFFA5BC0A2E6AD88F0F,
	GameplayUi_NextLevel_mC41BDB81C686BABB8CDFE93A1AB7A893017FFA87,
	GameplayUi__ctor_m0BBAE7BF4044520413024F21FE9DB9F57569172C,
	GridItem_InitializeNewGame_mF918C75057B3013A4108DD921E699F03799B284A,
	GridItem_ClearDecal_mDF706A3E7C63BE07F2E89ACBFF6FDCA46817F657,
	GridItem_ChangeDecal_m6BBC232DBB6616A925C9472F116579F31BC588C3,
	GridItem_MoveDown_mAC292214479CC5FD19D41436F0A3D6B55225D37C,
	GridItem_ShowArrowTarget_m3F0A1FB4B205D8A920ABF25C16924A028FFF2142,
	GridItem_ClearDecalForNextTurn_m1D5A1E6CA68637BF205098FD7ABDD4AFF643DD68,
	GridItem_GridItemMoveUpDone_m246DA44FFF388145E7961E0D176534A374981263,
	GridItem__ctor_m2CFCF86A7372B7271C2DB543CE2BD38A24A79E59,
	Loading_Start_m998640A617BC3A44E3DE0D20B2498943C988AC29,
	Loading__ctor_m43279C8AAC4675B5EE1BB5D6267BDF729A717A46,
	U3CStartU3Ed__1__ctor_m864F5B8B33AAC4DEA6CF421DE2F5A75483E9B1E9,
	U3CStartU3Ed__1_System_IDisposable_Dispose_m7F24EDF6096F489C112C5CBCB0A137680B45E4E5,
	U3CStartU3Ed__1_MoveNext_mD65854DB289F93F3C72DA860C2D5C49A8035A192,
	U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEBE57B8DA077676C34D1CA125FC9AD4248D00169,
	U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m26669B3362F9DB43F6AC567BDF7C14C8ADE86535,
	U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_m4779BD9000DD699B0DCC0E12A636242FEF626CEE,
	PlayerController_get_Instance_mB19A099B11BE20743A0BB9F41C3D639F1A6BFA70,
	PlayerController_set_Instance_m4E00BA49BA02B0A9B4D32971BDA08D139D8A1034,
	PlayerController_get_Center_m4B62D63D0AAEAFD6955D15326B2B765629DDF6AA,
	PlayerController_Awake_m5DFAF4F3BFA3B5311F241FA25EE1793A4BC131BE,
	PlayerController_Start_m9B0DA31712414AD3D20CF3B59564BD724B2C9A89,
	PlayerController_OnDestroy_m5CAC8BE95C1DF6A3F1FC73F8DD4A73BCE93BD018,
	PlayerController_OnValidate_mDC0A235B16751A2C7DB8BBD189D2607AF2E590FF,
	PlayerController_InitializeNewGame_m929DB965BC52AC34D8E3B70F9BB7D73FC981FC28,
	PlayerController_IECheckGroundAndLockBody_m9BBA76E0F715A10948D4086063331E0E1EF4C667,
	PlayerController_StartNewLevel_m4520BAB3F2D1C5D82AFEB6AA5C07F6E322A5E8DB,
	PlayerController_Update_mE70BAEE8A63A25396D1777AEB9A5FF963A3D243E,
	PlayerController_StartMovePlayer_m17A42485CF162C5D48B8A7B87598A6B6619CB657,
	PlayerController_UpdatePlayerVelocity_m1D655B28440C6B61D955BB9F485BC6D17B6FAAC8,
	PlayerController_EndMovePlayer_m3518E29720576DAA6ACE90A19F20874F199E8A0F,
	PlayerController_TriggerUnderground_m4648108AFA8E4A593A1579A9B9B5407A381052A0,
	PlayerController_ToggleRagdoll_m4439CBB2AD4D8D3FD516138AB096FEAC4762BB0E,
	PlayerController_OnPlayerVictory_m826D1089F1EEAE07D12DBA7FCBD9EC76AB5AB702,
	PlayerController__ctor_mE41EF85DE4B7C1221BFCE9A0EB905589DA3AF0FB,
	U3CIECheckGroundAndLockBodyU3Ed__30__ctor_mDE7250DF56AC98F83DB7D346A045B8B9577EF446,
	U3CIECheckGroundAndLockBodyU3Ed__30_System_IDisposable_Dispose_m7EEF19C46D50BEB22CBB0FD550CC4AED62D39069,
	U3CIECheckGroundAndLockBodyU3Ed__30_MoveNext_m5A1E34E7ABC24B60DC7C46D4D5C692395112D055,
	U3CIECheckGroundAndLockBodyU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0279C253745248631D16A0148B54958D9126AE3,
	U3CIECheckGroundAndLockBodyU3Ed__30_System_Collections_IEnumerator_Reset_m02741FFAF6CA0649E6B2F146837A0D7CF0782C3E,
	U3CIECheckGroundAndLockBodyU3Ed__30_System_Collections_IEnumerator_get_Current_m22CA11B05636CF5FD1B42510FBF70ADA7EDA5D88,
	GameOverPopup_Show_m4C50E277EF71C53499804F7AAFE296EF0FD630CB,
	GameOverPopup_Click_Replay_mE002AA6CDD7ABE1E16D442F3253FD6746E7EFCF2,
	GameOverPopup__ctor_mEA81834FF30817B803C82CBA1BD62FAEC3226B92,
	NewSkinPopup_Show_m5AE038DA4E2B542040BD07199407031B4DE80CFF,
	NewSkinPopup_ShowNoThankText_mD3D3E92CA8C91AB92CBBA16BC640C015F9E87CE6,
	NewSkinPopup_Click_Claim_m352284211152DB4BDCBE21B1A6247F2E4F71BA80,
	NewSkinPopup_WatchVideoDone_mE615A786DA4BA174EE2751F5C5C188F03B41ACF8,
	NewSkinPopup_WatchVideoFail_m9B764907FF45824D12DECA559DF638B792EACED3,
	NewSkinPopup__ctor_m41E0332BBA22AC9156FAA82B3314C27042ADC093,
	NoInternetPopup_Show_mF63C5B2A3D87A13C4AE869C2D0506904C513A98B,
	NoInternetPopup_Update_mDFCF21D3F9B5664C339CA71BCA32FF439E319ACD,
	NoInternetPopup__ctor_m801EAC154D86FC15E87DEDC8A5E98E269B59C2C4,
	RatePopup_Show_mA124C20F7A78CDAC96C4DDC5C93923C61E5B78C3,
	RatePopup_Click_Rate_m78E55570D11330EFB1E6AA108AB5FCD610282528,
	RatePopup_IEShowRate_m9DF447E40E270CB3B62FF9BA4BD76983C30D2584,
	RatePopup_RateAndClosePopup_m395DECB6EE1108AC40B226421F0105FE30EFDD5D,
	RatePopup__ctor_mE0160FC549251FF019952A7BAB75FBDEF56D4E10,
	U3CIEShowRateU3Ed__4__ctor_m0C70538881DDC968226D492A1EA2B49D4A5E2C5F,
	U3CIEShowRateU3Ed__4_System_IDisposable_Dispose_m8F42D05A9AA9DFDF1E3B156693237C5FEDC73FDD,
	U3CIEShowRateU3Ed__4_MoveNext_mBE8B7317A5434405025524F4D255CBDB89387AD0,
	U3CIEShowRateU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m84021406DD61193AA3F4D90454712EA90E74DDD2,
	U3CIEShowRateU3Ed__4_System_Collections_IEnumerator_Reset_m6D8D467F4F520687B7499331D06CE598B72F6818,
	U3CIEShowRateU3Ed__4_System_Collections_IEnumerator_get_Current_m7ADBC6AD92621B5983D0ABAEB5C846166B9B60D1,
	ShopItem_Initialized_mF2AA21BA8418A2E1922DF0EB2A36A31A14201ABC,
	ShopItem_Click_ChooseItem_m809640D6D614F272C132B026DD78C4D469D26013,
	ShopItem_ResetItem_mEB82CAF476854B1DB354293C0B29DDBB09ED059D,
	ShopItem_ShowHighlight_m2B516807C4C9D083D431533165A0332E5304E6BE,
	ShopItem__ctor_mCE0B80E94DD60C0494E156C359F4CC00FBF6DE77,
	ShopPopup_Start_m18CAD558301B3FF56ED9936C9CB4063D655396F2,
	ShopPopup_Show_m517068DD5002B62C74973E9E6F3B5303188E11D7,
	ShopPopup_Hide_mD52C1A5CF350CD3C72B3CE93E3986ED5AB7E156B,
	ShopPopup_Click_Reset_m2519A632D269F487D1E9EF5AD3090026B74BBF14,
	ShopPopup_Click_Tab_mCABDFB88C8DF924D7F0BF12C3530C947393F1D61,
	ShopPopup_Click_WatchVideoGetCoin_mFA2EFC397CDF5A6BE17AD69DC3F6D142C54A8FA4,
	ShopPopup_WatchVideoDone_m5980D708D3956DCC22C632A977A62AB2454260B7,
	ShopPopup_WatchVideoFail_m846F571CF28AE93B15C1DD83A818AD85EECCB719,
	ShopPopup_Click_BuyRandom_m4313C69A461A048A87B599BE2B90A63CCF6CC2F4,
	ShopPopup_IEBuyRandom_mE77A7939BE76363AE5F8D496A0AADDB28307C450,
	ShopPopup_UnlockItem_m6C936424880731F10904649B93C39EEC845EFD4F,
	ShopPopup_PreviewCharacter_m42EB6EE757B1FAA9DB328157FDC20F13CAAF3FA9,
	ShopPopup_SelectCharacter_m3C3BC7137173419574E6D35F2FE18F6F19F1CCD3,
	ShopPopup__ctor_m562F3F0DDC0486385AB877B08B548D96CF6FBCB4,
	U3CIEBuyRandomU3Ed__18__ctor_m4B86F3C18AD6C00B0E6DE1EE7CB5B1557A921CC0,
	U3CIEBuyRandomU3Ed__18_System_IDisposable_Dispose_m6FAC56AFF731B71AF672C03A50232FCA06F5F68E,
	U3CIEBuyRandomU3Ed__18_MoveNext_m009A399A8C3F26C7B37B78950A709845B67F5ECC,
	U3CIEBuyRandomU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73BEE0F53A7E6FF34C9EB6D8D75CC73D0766BB25,
	U3CIEBuyRandomU3Ed__18_System_Collections_IEnumerator_Reset_m50765866E0B08791487FA8D9EB9BB8AF8BD33BDF,
	U3CIEBuyRandomU3Ed__18_System_Collections_IEnumerator_get_Current_m75225B6474DE175624A6153E4E5BE3463F29C376,
	StartGamePopup_Show_m9D3A3E84E7AAA431ADEC66B5D18C02436DA2EF02,
	StartGamePopup_Click_BuyNoAds_mE9675077EBB815350A312987E0B40B6C2BECBE86,
	StartGamePopup_OnBuyNoAdsCallback_m38A876B480D745053DD0F97A9929788D784A1C48,
	StartGamePopup_Click_Shop_m0D4EF64547AAE1016563CF857BC22238294E15C1,
	StartGamePopup_Click_StartGame_mC53F962E8B12718B8B13633CAF7DC20AE5A35F9B,
	StartGamePopup__ctor_mE3D9B27A2F684C73B872298D109783EC7A7A0AF5,
	VictoryPopup_Show_mEE2B82411B70722D51B9F1FABE015A9C2C3B7242,
	VictoryPopup_ShowNewSkinPopup_m41797589F8368BA514E5951B5BDD750D044F234B,
	VictoryPopup_UpdateCoinWatchVideoText_m587F0B9BCCFD63119513B2C5F2CAA8553E0CCA0A,
	VictoryPopup_Click_WatchVideo_m210BD7155AEAD1014401DE44F97D0438354FCCAC,
	VictoryPopup_WatchVideoDone_m1F389515E858F56B3A2CFDE9ABAA2133B965C657,
	VictoryPopup_WatchVideoFail_m159E8151A3C02662329FC1C685AAD64092806F39,
	VictoryPopup_Click_NextLevel_m936378422A06DB094591F2D3F9E1241ABAB04C4D,
	VictoryPopup_NextLevel_m299BC898825785C6696BE78E4EC0E0ED369474FB,
	VictoryPopup_IEShowCoinFly_mF4EC2FBD1FCA111047595602E3D90CE6F0F397E0,
	VictoryPopup__ctor_mBB98EBC2C4F653C4E66C7EFB0BA72812DCB403F4,
	VictoryPopup_U3CShowNewSkinPopupU3Eb__25_0_m669F7E97415A3F449A38B01C245A566E47508701,
	U3CU3Ec__DisplayClass32_0__ctor_mDF2145ABD22C021106B4502508F0E631159A080F,
	U3CU3Ec__DisplayClass32_0_U3CIEShowCoinFlyU3Eb__2_mC6272C40CD1D56277DB6B9ED9B030523815A3D44,
	U3CU3Ec__DisplayClass32_0_U3CIEShowCoinFlyU3Eb__0_mE66117767C5DF2160EC5620D9C655C8ACBE6B382,
	U3CU3Ec__DisplayClass32_0_U3CIEShowCoinFlyU3Eb__3_mF57D4E231715DC507C0E82BC72D63C49CD548F36,
	U3CU3Ec__DisplayClass32_0_U3CIEShowCoinFlyU3Eb__1_m5F1FD923588400E27F61B48B5966FB8A2CA5AB24,
	U3CIEShowCoinFlyU3Ed__32__ctor_m75E10487EC1DD27238E40A287D945E64267E57BF,
	U3CIEShowCoinFlyU3Ed__32_System_IDisposable_Dispose_m7F45A091903276769DE528507A8F082705D7D1F2,
	U3CIEShowCoinFlyU3Ed__32_MoveNext_mBECD9442FD86363B3F2F6A8A8FB954377BD21A5A,
	U3CIEShowCoinFlyU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m00DB0CFC4F42158A1D2FF7276F05FDDACC26038E,
	U3CIEShowCoinFlyU3Ed__32_System_Collections_IEnumerator_Reset_mEB6631672C7E07722BB51DB65CB8C6204B4B5F8C,
	U3CIEShowCoinFlyU3Ed__32_System_Collections_IEnumerator_get_Current_m0A39B2305BEACE7C43E67A53F5F5AD18EF911775,
	WarningPopup_Show_m55EEA34542F592AC7CDD8C42AD88681EF0D028D0,
	WarningPopup_ShowText_m98D05CAC6E2CD6C06E179016CFE51A443E9256DF,
	WarningPopup__ctor_mE88F18EDAF6F651D78FFD7E84F1F667B57F6ACBD,
	PushBackBot_OnCollisionEnter_m2E5723663A2788594B3C0E5136794157CF14477A,
	PushBackBot__ctor_m117D2B4D47BC1C0C1E9FA13690D69BC6FCD99C9A,
	UnderGround_OnCollisionEnter_mB39C382988D1E423C61977026E3A8CB81DC66269,
	UnderGround__ctor_m961AF27EA4A52547B682D9D46A5E2C8976BFF420,
	JSONNode_Add_mA73D290ED9AFFE3F42861A87D20DD066CBF02BA3,
	JSONNode_get_Item_mD7BBCD16DB5D0500676559AFB2A2594B0103D8F3,
	JSONNode_set_Item_mD4F5BEDADBDB66141EC0EB6F3E8E0C04A481CD6B,
	JSONNode_get_Item_m85A1C5F8B26938A5BDD5CBA14C84E4A37B3D4ACF,
	JSONNode_set_Item_mD9F8F18C2B5F9054A722B4CFD8129E2C48A4BE5D,
	JSONNode_get_Value_m422474020628AD90572986DC9999B5E33D51044F,
	JSONNode_set_Value_mF7DD7A968B9CE3DE2DEC99CFBB8F6A0035DA0BBC,
	JSONNode_get_Count_m6A898D357D50A5C2E8042AFCB3D0E795B3F0FB25,
	JSONNode_Add_m8EE67AF49568BB6CC3DD0968C5E7E206EF3F4A99,
	JSONNode_Remove_mCC44CF5159F5A8661D1906F9444477EF5C990B71,
	JSONNode_Remove_m404F68BE1FB04E8F4B50EADF3490CAC177BB57FA,
	JSONNode_Remove_m68BB8C3076B8DE3EB84D43AC8FEFA4057F2973A1,
	JSONNode_get_Childs_m041DDDFC000548A689CE29D3288D53AA643C3482,
	JSONNode_get_DeepChilds_m44F560506E902A0B0DABB6B82562D127259FEFC5,
	JSONNode_ToString_mCE094AA189C11D45C5915FA3D229C9FD1076A6E1,
	JSONNode_ToString_m729A82FD7DD7412637589CDE0158F98B5E7C14C2,
	JSONNode_get_AsInt_m06D351315628BE80806748B43605699EBD2724C7,
	JSONNode_set_AsInt_m351063CC07FA09FFBF85FB543D40ADC1F90B751A,
	JSONNode_get_AsFloat_m18888F5DF88D1ABD784270641319B7ABDDDBE9E6,
	JSONNode_set_AsFloat_mD796198844BF326B7288864EB6C4D5AD4E510E9C,
	JSONNode_get_AsDouble_m3CBB3A48ADA115310C2187857B1C71ABC7806752,
	JSONNode_set_AsDouble_mA5DA00CF265864C946AA28CCBAC8A7FF4D22BA76,
	JSONNode_get_AsBool_m8D64DD4B3FC875095E283DBE5AC867003123635F,
	JSONNode_set_AsBool_mB969B70E7AF09CCF88B79BCF1FBFC03D9F8DB6AD,
	JSONNode_get_AsArray_m7B937E045E18295E048B40D1B7C3A58E01F710F3,
	JSONNode_get_AsObject_m09F640638EA27AE9A75A66D413209E1B0A4C1700,
	JSONNode_op_Implicit_mC14E8B965CA6EBB9FFF12F7808330C926C5B1C18,
	JSONNode_op_Implicit_m8ECC47F8ACF595260FD729E531D9E38998604EDD,
	JSONNode_op_Equality_m70E93DF433C90502AAC76D837057CEF1FE04F7C6,
	JSONNode_op_Inequality_m886F271FA8B6F834DC1DE8E5B3F2EDF1C88D90DC,
	JSONNode_Equals_m024E691480E99CC5FCAC978BEA06E3C89AC936BE,
	JSONNode_GetHashCode_m205B8B106B815AF3CFC5FFAC4A4E253452BC08E5,
	JSONNode_Escape_m2E11BD8B7B358628AC0E369DD34DB2FF35FE47C0,
	JSONNode_Parse_m3FD7EA73C960EEDE36C94D065292C1B1885174CE,
	JSONNode_Serialize_m7B9E652622D50367BB8DB62252A0ADDBD2A02A4F,
	JSONNode_SaveToStream_mAAF83974E2DB60FD527965E28263B1202E195C27,
	JSONNode_SaveToCompressedStream_mDFE8495C0252DA781F53FB62A4E580D9267594B1,
	JSONNode_SaveToCompressedFile_m5EE5A35989E44A29862AC7BA21F5C78179BCBD71,
	JSONNode_SaveToCompressedBase64_mC5D7414CD46FEBB67BB8567FDA778D453B87FA7B,
	JSONNode_Deserialize_m330FFC061D9EDF8F293DD532D9B3BEF6AA6F2D99,
	JSONNode_LoadFromCompressedFile_m631041CF9B4776DDE4BC0701EAEE4D6AA3A846AD,
	JSONNode_LoadFromCompressedStream_m0416ACF556E3858060290AFD998C1A2B2F9523D4,
	JSONNode_LoadFromCompressedBase64_m8F4558A2A4D57BD495A1226DD3B4DAEC58102CC9,
	JSONNode_LoadFromStream_m82D468E397895F05A4799A6F217B68D0FC80D964,
	JSONNode_LoadFromBase64_m34262C804F04DF3ABC9FE04AC775F42EDEF37EBD,
	JSONNode__ctor_mCC776C44E5B26CB440CD077D35CF116A45EAE5EA,
	U3Cget_ChildsU3Ed__17__ctor_mF797A836A6E3340B1BAAAF50AEEC10DFA89DAE73,
	U3Cget_ChildsU3Ed__17_System_IDisposable_Dispose_m94877C8D1DA5BE6EA246389F10F4C9C7AF366A74,
	U3Cget_ChildsU3Ed__17_MoveNext_mE6F86033D5481E4036487E92E0F64B9840CB794B,
	U3Cget_ChildsU3Ed__17_System_Collections_Generic_IEnumeratorU3Ccom_adjust_sdk_JSONNodeU3E_get_Current_m54B690F8FA694EE02627B97D48386D9095E229E4,
	U3Cget_ChildsU3Ed__17_System_Collections_IEnumerator_Reset_m0366D0A88F5A1FBA81AE3F732F855C0CAE045788,
	U3Cget_ChildsU3Ed__17_System_Collections_IEnumerator_get_Current_m38B1B4A7F33CF7834BB44D72C8FED6220D1C75BD,
	U3Cget_ChildsU3Ed__17_System_Collections_Generic_IEnumerableU3Ccom_adjust_sdk_JSONNodeU3E_GetEnumerator_m071E822DD6D0A22882CDAA62B63D9660FF1776C0,
	U3Cget_ChildsU3Ed__17_System_Collections_IEnumerable_GetEnumerator_mA892F96D61A7E4D40CBA9B2CF48F34079F726849,
	U3Cget_DeepChildsU3Ed__19__ctor_m046016CC55A4C7B9C1FE5AE10DD72E4050826BAC,
	U3Cget_DeepChildsU3Ed__19_System_IDisposable_Dispose_mDCF0787A78165575446D46E5C1CD1A8A10FF7018,
	U3Cget_DeepChildsU3Ed__19_MoveNext_m9F51E57FD6F93E690C029855DAA59D591E773022,
	U3Cget_DeepChildsU3Ed__19_U3CU3Em__Finally1_m44FF129641F4B9A288B22CC405CFDB01813AF413,
	U3Cget_DeepChildsU3Ed__19_U3CU3Em__Finally2_m50CD9A7591C11FE67D4AE24614BD3B979B030C7F,
	U3Cget_DeepChildsU3Ed__19_System_Collections_Generic_IEnumeratorU3Ccom_adjust_sdk_JSONNodeU3E_get_Current_mB46DB5D77ECC4B15D6B4BA4ED0A03F21CE95D25D,
	U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerator_Reset_m2862E593874F11BCC026302EFC96AFDE00AB5A06,
	U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerator_get_Current_mABF1413C57FA5A3C1651355AB4A5E4C0402E27DA,
	U3Cget_DeepChildsU3Ed__19_System_Collections_Generic_IEnumerableU3Ccom_adjust_sdk_JSONNodeU3E_GetEnumerator_mA1AC5268F717D42575E2B41A69ED09A0C60071B0,
	U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerable_GetEnumerator_m481F9219BE5FB9B09E8DFC02714ED34393641102,
	JSONArray_get_Item_m755E3E8E6725A4FBF408E5BD1A8432C419410503,
	JSONArray_set_Item_mC4EF1F71394FFDFAE7153C0AC8E698EE4AA5B9F5,
	JSONArray_get_Item_m70C52946E9D9B5DE99DBE66423561B152ECCB454,
	JSONArray_set_Item_m2AB7F7C0919BABC722D5262F62960F58A0BCCFDE,
	JSONArray_get_Count_m810A578C961B778B760E51A3D0111CFB8152864B,
	JSONArray_Add_m11ED297B0CE157395B41622C3A70577A4CCDEE1B,
	JSONArray_Remove_m294856B07209D367010855B3B71CEACD50B8F6AF,
	JSONArray_Remove_m6C5A0F04019B3D0DC675038B5B04AE38079B786A,
	JSONArray_get_Childs_mC749B8F046AD011544966486F60CA9C0C217A874,
	JSONArray_GetEnumerator_m4E4EA5BBF194BA5E068CA738E573C73DD1ED5635,
	JSONArray_ToString_m58F4AFD74189F06A5380F48EB94330B1F62787AB,
	JSONArray_ToString_m97E2C58394C45CEB1C54965B96C6E9CB19B8005B,
	JSONArray_Serialize_m85EA6F504087F2B91019ADACE0AF84BE56C977DF,
	JSONArray__ctor_m6ECA2300A22DEFC3387A72AF03FEC3355B150C4E,
	U3Cget_ChildsU3Ed__13__ctor_m6474546C7E49FF6A548EFAB592D82CD4C52A3E2B,
	U3Cget_ChildsU3Ed__13_System_IDisposable_Dispose_mE5E9FC725A9C7CB1A97AE4BCF844F2F773000283,
	U3Cget_ChildsU3Ed__13_MoveNext_m93338AB8CC1520C49B28F7BA37D0216F8DFE3894,
	U3Cget_ChildsU3Ed__13_U3CU3Em__Finally1_mC2A2DBA3A3B18AB12F36800AD2129AF9CE3B4865,
	U3Cget_ChildsU3Ed__13_System_Collections_Generic_IEnumeratorU3Ccom_adjust_sdk_JSONNodeU3E_get_Current_m939A44A15A892D3328145314D6F3BD3180E600F0,
	U3Cget_ChildsU3Ed__13_System_Collections_IEnumerator_Reset_m9BBCABEA39F9133BC5E7188E8E6346538077025F,
	U3Cget_ChildsU3Ed__13_System_Collections_IEnumerator_get_Current_mAAA548D9FF3C67A7AEF373A19FFAB5C9C3A97C5E,
	U3Cget_ChildsU3Ed__13_System_Collections_Generic_IEnumerableU3Ccom_adjust_sdk_JSONNodeU3E_GetEnumerator_m3C0A4675A2FFA0A86616539694B4392F7A86B4D6,
	U3Cget_ChildsU3Ed__13_System_Collections_IEnumerable_GetEnumerator_m208AD3C417C7CAABDDEABD4EE49670B1E6403926,
	U3CGetEnumeratorU3Ed__14__ctor_m0695479939904B620F4C38973708FCF84941755E,
	U3CGetEnumeratorU3Ed__14_System_IDisposable_Dispose_mDCC768ED829C36509C1AE08EC89EDA457099995C,
	U3CGetEnumeratorU3Ed__14_MoveNext_mE03BAD590A4A72ADD88274F87F144FA61C677CD2,
	U3CGetEnumeratorU3Ed__14_U3CU3Em__Finally1_m4F72145277B7B879E05365A241A55E1FA2DCC478,
	U3CGetEnumeratorU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC44ED8F1DA1967025AB91E813D3F5E8CAB273B4A,
	U3CGetEnumeratorU3Ed__14_System_Collections_IEnumerator_Reset_m5FF5CA385FB75BA70CEF94B6FAE71C700ABA5A6A,
	U3CGetEnumeratorU3Ed__14_System_Collections_IEnumerator_get_Current_m4BE072CF6B24FCA546848441F442FA00F8049498,
	JSONClass_get_Item_m49CF33B11E4FCA72EBC0D1E89BFA1F64D1C82D93,
	JSONClass_set_Item_m887D13A167F244972827C10AD304E445D273B72E,
	JSONClass_get_Item_m332F023B545862E9766531549B9A5D8276645551,
	JSONClass_set_Item_m8E3A9CA1DEDAFDA4165B2D5CEE425BC01B829B71,
	JSONClass_get_Count_mD26CAC564152CABFB3D996FE189D7184F6C9FB3C,
	JSONClass_Add_m8016B5271985A574BAC8E7745DE5F950299FFF38,
	JSONClass_Remove_mC59E957355660E1DE1D528916D23F86B3EB5F680,
	JSONClass_Remove_m3A246109A77359EAAF01B6324AD2F436608DA680,
	JSONClass_Remove_mDE17444DFA5339F564FBB7071E7913D9C1B0C877,
	JSONClass_get_Childs_m7ED5CDF26A34F041E5CBF93B9EC616E940AEC925,
	JSONClass_GetEnumerator_mC63BF81FA10F977320F60BFF46CDA748A7F09DAD,
	JSONClass_ToString_m2851DE91AA6840330E1A2A64F5803D702AD3BF37,
	JSONClass_ToString_m82935C8A4E9EB9D8E5693077AB9603452F71B027,
	JSONClass_Serialize_mBA8EAE5D8ADB2413DE2A726447B0037B5686BFF8,
	JSONClass__ctor_m02C51CEB21719D911A19C729D02745553A1EB446,
	U3CU3Ec__DisplayClass12_0__ctor_m54EAB9A9ED470825466B363E8EF656B51E99F746,
	U3CU3Ec__DisplayClass12_0_U3CRemoveU3Eb__0_m919E74F0EFF672D2C2B58FE1D21F705C7A3DD3B8,
	U3Cget_ChildsU3Ed__14__ctor_m2A7CD01CA103381FBF4FB8D70B1FD26083894793,
	U3Cget_ChildsU3Ed__14_System_IDisposable_Dispose_m31D2FA345366CAF0D63CB243DCBE67745AA52783,
	U3Cget_ChildsU3Ed__14_MoveNext_m3C0CFA00C04821A51C6A3C0A5808DFDE9ECE5A5E,
	U3Cget_ChildsU3Ed__14_U3CU3Em__Finally1_m9FA71F2E785602FDD64125BD5BE3FCE3561532D7,
	U3Cget_ChildsU3Ed__14_System_Collections_Generic_IEnumeratorU3Ccom_adjust_sdk_JSONNodeU3E_get_Current_mB0226A6E5AC71AEBA38F19684C7758964A705388,
	U3Cget_ChildsU3Ed__14_System_Collections_IEnumerator_Reset_m65E72C29D884A8DEDBBAAD79C3035EFB6A106ACB,
	U3Cget_ChildsU3Ed__14_System_Collections_IEnumerator_get_Current_m78FA739F5BE9ACDFE18DD77640515DA4B2BF4576,
	U3Cget_ChildsU3Ed__14_System_Collections_Generic_IEnumerableU3Ccom_adjust_sdk_JSONNodeU3E_GetEnumerator_mF561773E4A39039E3DAFEE4082F0FF2E39DE7F2C,
	U3Cget_ChildsU3Ed__14_System_Collections_IEnumerable_GetEnumerator_m80BE9E5871675B635E6737823E81E269BDFF7359,
	U3CGetEnumeratorU3Ed__15__ctor_m71795B1B109474BBF1C793AE265C8218EE46A317,
	U3CGetEnumeratorU3Ed__15_System_IDisposable_Dispose_m92BA0C0FE94BF8EA461E8D2985DCADF217B29B3A,
	U3CGetEnumeratorU3Ed__15_MoveNext_mB78E9C334CD336419DCE6EAFEF6FFE51FD51947E,
	U3CGetEnumeratorU3Ed__15_U3CU3Em__Finally1_m909727106AE5FE5AA12C727A654659A639A11F9F,
	U3CGetEnumeratorU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D7E2674085894FC3011FFCE567CFA21DA8C30D0,
	U3CGetEnumeratorU3Ed__15_System_Collections_IEnumerator_Reset_m9877E5F955C30C1CE2C2FEB43B2608137528BCE4,
	U3CGetEnumeratorU3Ed__15_System_Collections_IEnumerator_get_Current_mDB2FE16291B3B81162572DC31EE732BA957BD162,
	JSONData_get_Value_m3ADA5A705F607FB7C35D4605EA832E89599BE425,
	JSONData_set_Value_m217DDEC42EA58FCFA6CBC6EFE473BD448A16DBE2,
	JSONData__ctor_mF07078A36644CD1C44FD4394482FFF67BCCEEAC5,
	JSONData__ctor_m0BA2DCE6697B2DF087570A042E0BD1D48B1B7AD2,
	JSONData__ctor_m2EDE89D6B666DDFB6A4D2399A8462D82EC448AEE,
	JSONData__ctor_m0AB47342A4FDA49AF5B0775BE663DCE5A697B350,
	JSONData__ctor_m312352D90C0634BDBABD84416086C56157235C3F,
	JSONData_ToString_m4C8593075AFF9E56B99908CEEA6988EB2AE61580,
	JSONData_ToString_m67FDD9B98DF0DD19D397B6E210603DF3194840FD,
	JSONData_Serialize_m509455814FED79C97E0C05F354839F51488D9D91,
	JSONLazyCreator__ctor_mF3FA6877067F4F9086006389F0910D541AA27059,
	JSONLazyCreator__ctor_m8C48C27CBB58E5F6990AFCA554803B7E191A3D46,
	JSONLazyCreator_Set_m9FCCD3D9059234381ACD61296F3B33ABB5392630,
	JSONLazyCreator_get_Item_m8A8A9C17EDC287BFF118DDED313F4107F4072A22,
	JSONLazyCreator_set_Item_m19485C7273CDACCE9F7B1834D56FB74E19777E12,
	JSONLazyCreator_get_Item_mF8E012F6005145846304C594EACF1584CA5DDA9A,
	JSONLazyCreator_set_Item_m33B036AAA008F596ADB3F455D2021D1EF025B49A,
	JSONLazyCreator_Add_m59C51EC772277124A17D87DA5C74707E3EFD6AFB,
	JSONLazyCreator_Add_mBACDDB1A14FCE6CEA2674C9B4EDA816F21DF9E29,
	JSONLazyCreator_op_Equality_m1AB247D90A51923A9C6EA40FFB3F81E0D97E3576,
	JSONLazyCreator_op_Inequality_mCBCE4C31D43C15D08157DFA42D40BB149AB22191,
	JSONLazyCreator_Equals_m586E6D9E990915F11F72E099E203D60723BEBC48,
	JSONLazyCreator_GetHashCode_m9CC90F8845B9E57BB6913BF83FB3C5D89FA61A35,
	JSONLazyCreator_ToString_m49AE6E6F07C738C9FB3F7ABC86EBBA8C0828996C,
	JSONLazyCreator_ToString_mD794E0057AC95367420ED3D100817F019BD77A91,
	JSONLazyCreator_get_AsInt_m3D1F120A6A54087B34B638943D2168F10CF58BA8,
	JSONLazyCreator_set_AsInt_m78FF9DC114F9731546230F760A5048069378B32F,
	JSONLazyCreator_get_AsFloat_m1BF100B3129E3B67B00EB12BAB1C599198E60D87,
	JSONLazyCreator_set_AsFloat_mE15D7CE3B2B38E9E5EDC079D339A8AB5FAF674D6,
	JSONLazyCreator_get_AsDouble_m40DD7A463621DD10526570D5430173F7AE7771B5,
	JSONLazyCreator_set_AsDouble_m275BB96935FAE5F2CD8A8328EAC9FE887B5A3377,
	JSONLazyCreator_get_AsBool_mAF53CC7D86CFCE9E85413B126CFD79470BCD6B30,
	JSONLazyCreator_set_AsBool_m05997EDED19E2297BFE7D45A1062CB7CA52F1ED8,
	JSONLazyCreator_get_AsArray_mAEBD138BCA37CA30F180A221CC7872FE240BFA62,
	JSONLazyCreator_get_AsObject_m52BB6F39E712E7E3ED988842537FEFF804B543C7,
	JSON_Parse_m3D7BC20285304E72A9991F6B6A70F09341A6BF08,
	AdjustAndroid_Start_mD01129DAB4F78C21F0E0B206FEA18A984182C9B8,
	AdjustAndroid_TrackEvent_mA39AD43D87F02F2082A7E23AF706966E70B3E744,
	AdjustAndroid_IsEnabled_m4CA2407E6DCD137D539F66594338B04FB99935F5,
	AdjustAndroid_SetEnabled_m2FC31E0639E78170CF580A32851BC6D3C67CD973,
	AdjustAndroid_SetOfflineMode_m1051441F52D74D8442772723D4115B1F220A4937,
	AdjustAndroid_SendFirstPackages_mB506A548A21E861BA08EDF637143B59BC73BFDA1,
	AdjustAndroid_SetDeviceToken_m088C0FFE619D4A66905DF78EE4E617DC2897DAB1,
	AdjustAndroid_GetAdid_m009AA802086EF711FEB5CF462DFDF07770487142,
	AdjustAndroid_GdprForgetMe_mED6BB5AE19466332DB83B3A5944E16786467778D,
	AdjustAndroid_DisableThirdPartySharing_m5DF591771C8DBB1FAA7FC4F0A92343925702DD64,
	AdjustAndroid_GetAttribution_m61053B5E336CF6BF8AB30AE23709246C1DBDFAA1,
	AdjustAndroid_AddSessionPartnerParameter_m7FC9C6A549452ABE82B0D0C2C512683DE2612E8A,
	AdjustAndroid_AddSessionCallbackParameter_mB1F27189544F2C0812816C3467EC0F12C507E1B1,
	AdjustAndroid_RemoveSessionPartnerParameter_m4A778D326B5A6D93A6A383DF584E65DDACF8F63B,
	AdjustAndroid_RemoveSessionCallbackParameter_m0310DC529C4A1CC9373F11CBB9271449EAF0FD37,
	AdjustAndroid_ResetSessionPartnerParameters_m2DDF83690EA07EE49777B363AF9E4A4E80E96A3D,
	AdjustAndroid_ResetSessionCallbackParameters_m4F9C27D13AF336DA7C9E7ACDF0C9DCEC51C0248A,
	AdjustAndroid_AppWillOpenUrl_m3C2C1D2ED45A19E0B190CBD8AA4CDFE65AB77565,
	AdjustAndroid_TrackAdRevenue_m89D588AA54628906ADDD884FB1489E99BEB0BAC4,
	AdjustAndroid_TrackAdRevenue_mAA058F26F32A71C3DD851C4C5D0AAA30BF5BFAAE,
	AdjustAndroid_TrackPlayStoreSubscription_mCB14505E4907C5E247D20C54AE2E21E4061D9126,
	AdjustAndroid_TrackThirdPartySharing_mE13C5DFC1869E87290F7BD3A6133680A8F9A4669,
	AdjustAndroid_TrackMeasurementConsent_m991B157AD636A840943A5959AF07CECCEA53E24C,
	AdjustAndroid_OnPause_m03F7F32D01A177BE0E39D6000AEE0C0013A6E3A2,
	AdjustAndroid_OnResume_mB0166C82E08BF116FCF516416D16436DD3FC5D43,
	AdjustAndroid_SetReferrer_m1FA7992941C79022A5CBE0CA62D01EBED329AB6B,
	AdjustAndroid_GetGoogleAdId_mB10ABF7D147D322BDC45C5E03431D011A1E3C2B2,
	AdjustAndroid_GetAmazonAdId_mC89BBCC14BBDCD74BAB16BE200FF234BF8ABEB1D,
	AdjustAndroid_GetSdkVersion_mBFA4A689BDECA8CAE549F1D530894085C6E72476,
	AdjustAndroid_SetTestOptions_m8CDAF0631752C444D36FB96610046B04B58FB4BB,
	AdjustAndroid_IsAppSecretSet_mF94D1EDF229B5033FD9034AEC49AC655AFCACDE9,
	AdjustAndroid__ctor_m0035BEC1E1826590313CEAF840550DF637DF1268,
	AdjustAndroid__cctor_m2A3DC4B0CBC9C1862D7BFD3D3AF7522DA9FA0612,
	AttributionChangeListener__ctor_mA6205D1D3A3CBA8F7FD10B413B0FD68FFFA548B8,
	AttributionChangeListener_onAttributionChanged_m8EEEC19136B64A9315CCB266EF17028994504EB8,
	DeferredDeeplinkListener__ctor_m9AFF50C9019C550FE14B914675EE675B36EE0582,
	DeferredDeeplinkListener_launchReceivedDeeplink_mE6D35176E2C2AADC10C881FF8F3927AB528C5F3B,
	EventTrackingSucceededListener__ctor_m2A4ED2670A9A651245F8D5E344BDBFB5B3E3D43D,
	EventTrackingSucceededListener_onFinishedEventTrackingSucceeded_m202DFEA1323BD584F04E02C0A8323870A1959460,
	EventTrackingFailedListener__ctor_m7D5E6FA8F17AAC33B50DFC25A1ACA5183FCE6545,
	EventTrackingFailedListener_onFinishedEventTrackingFailed_m4FB36E7E27EF57535C67940BC6F148293CF366C0,
	SessionTrackingSucceededListener__ctor_mD524E8A2ACB053F1100FC8BD428D97E256EEAAF1,
	SessionTrackingSucceededListener_onFinishedSessionTrackingSucceeded_m641A6B81E4C67260BB84A35FC9803D11AA33102A,
	SessionTrackingFailedListener__ctor_mEC4AC95C995754842BC483D6D7AA0381A508BA6B,
	SessionTrackingFailedListener_onFinishedSessionTrackingFailed_m2494A8E23D515011E6014DEB2630F9F3A88F8D69,
	DeviceIdsReadListener__ctor_mDF6EF7D21335FCB0B244E975C68B5A7CC8F1E39D,
	DeviceIdsReadListener_onGoogleAdIdRead_m82DB269D37125FB1A6A3667454CF5B408AC179DD,
	DeviceIdsReadListener_onGoogleAdIdRead_m46B72AE61C8F5B9408B0FA1374F0D34557C6D16A,
	Adjust_Awake_m1B0E9298029BFF09C771F77DF8181CADFCB63BA8,
	Adjust_OnApplicationPause_m3ADC342D8050B80840CB85B003EBCF689C8E4012,
	Adjust_start_mEDA99971143DC57743841647E3EFF994DA9F573B,
	Adjust_trackEvent_mDC05176792C1AE9200857EB70116183B2244B33F,
	Adjust_setEnabled_m8004D89A6F03FB0854AF642D8FB5EB969C610285,
	Adjust_isEnabled_mB3E5BC11C9E09FB38DBA830323AA47CADA21B12F,
	Adjust_setOfflineMode_m9CB0BEFA63D15D269D5C687021B248540E272C92,
	Adjust_setDeviceToken_mB4DBFAB4BC5F577F1E6300F3B49F590663FF0F5C,
	Adjust_gdprForgetMe_m294348DF8F8CD4628B9FE5056D24DF698A94ED40,
	Adjust_disableThirdPartySharing_m998A3653A5E2048A14F1E2EC78B87BCD8323840C,
	Adjust_appWillOpenUrl_m3392AEF212AEAFA41D7E0A079BFE02DA08768468,
	Adjust_sendFirstPackages_m1303552D006125FBA6636CAA4E9CE88E0BEB06DB,
	Adjust_addSessionPartnerParameter_mF7169C2B398C189CBFF42ABB579FAC21B248421A,
	Adjust_addSessionCallbackParameter_m521FF9F49898C1F8D1FA7EC9C0FEA68D00FF9B5C,
	Adjust_removeSessionPartnerParameter_m1EF1A935AB3EF747A5D0866F303C72AC6AEC8030,
	Adjust_removeSessionCallbackParameter_m060BD40FC083FE9FFCEFDB9F55A88501B8021A09,
	Adjust_resetSessionPartnerParameters_m10B6EB7AF8E27B76E3C8B1E4C8BF0EE8A347732F,
	Adjust_resetSessionCallbackParameters_m57E4C9DCACC3F2AA8238DF296B3B9841D6C368E3,
	Adjust_trackAdRevenue_mDCE9DF27677C8C3E7DA49D0C7E8CD30E99D214D6,
	Adjust_trackAdRevenue_m91EDB1E714C5383380F3A7887013C101458754E8,
	Adjust_trackAppStoreSubscription_m655F4FE561AEC8C06448EB92CE18C7094B3C969B,
	Adjust_trackPlayStoreSubscription_mE286132E839D5082BFA4B91DDAF8069EEB983905,
	Adjust_trackThirdPartySharing_m2F0AD03FD18B95A9FE0126E2F778DE9810049E8D,
	Adjust_trackMeasurementConsent_mCEB66814F0EB4100DC32D37AE6E3C43BBC6B848E,
	Adjust_requestTrackingAuthorizationWithCompletionHandler_m8A29912EA80AEA67AE5B37EA6C54D568C6E31A19,
	Adjust_updateConversionValue_m50493AE8F6851EECF90B739F3ED31562A18339FA,
	Adjust_getAppTrackingAuthorizationStatus_mE88B444AE6B989C06278E8ECA3B6A4386626F83F,
	Adjust_getAdid_m7C420247243DDA0F96ABF44B11A73BB1C186B6E4,
	Adjust_getAttribution_m84EA3EB40F0B9AE5E85A880086A540B64FC54D82,
	Adjust_getWinAdid_m3B6A40CE6D17494A00196E683EE3033674865A6B,
	Adjust_getIdfa_mA4FEEC84C6D9A8CA25ADFDC852D5156E0B07A6AD,
	Adjust_getSdkVersion_m8DAFBADADD1CF185060E065944FF352332149B35,
	Adjust_setReferrer_m48CB96C45237A51DBE34F3D143AAE600CC7FA141,
	Adjust_getGoogleAdId_m0A776FBE1A723C47F4E90AFE03E99E503BEF043C,
	Adjust_getAmazonAdId_mA5FD7EED2F33449DA0BCC2710E1C8C98AEBCB73A,
	Adjust_IsEditor_m95F617F8E2F50FD12BC18D741EC7D466F3A8DDF4,
	Adjust_SetTestOptions_m9C0018EB8C448EDA4F2D8B2D2A13B7F0076F0E26,
	Adjust__ctor_m3303F268C76843435B868BB9D9E307FBD20A8F0B,
	AdjustAdRevenue__ctor_m4C94C4313766148F6D9DC7451483B6EC847EEFB8,
	AdjustAdRevenue_setRevenue_mB37B06AC7FE6C0D6FF8BF3DEDD7C5E2A58E3E3A7,
	AdjustAdRevenue_setAdImpressionsCount_m3181A0D66506FA4D9971B18CC8E3DDB921EB1115,
	AdjustAdRevenue_setAdRevenueNetwork_m500036ED01D100B35A12B3DD99AA9E754EA72B25,
	AdjustAdRevenue_setAdRevenueUnit_mF46B42441260BED2E68D98661A8D90D4F202C856,
	AdjustAdRevenue_setAdRevenuePlacement_m1C1843A4ED920DDBAD223DFCD78131655804CC0B,
	AdjustAdRevenue_addCallbackParameter_m2B3A25714F44C6FBA06C09FB6ABD9F703EC9335C,
	AdjustAdRevenue_addPartnerParameter_mAF18DE2CE37C15D2179C77ADF244D1AB260D32D3,
	AdjustAppStoreSubscription__ctor_m0D3482433734BA539F0A09252DE3659D21FD1536,
	AdjustAppStoreSubscription_setTransactionDate_mD1BA71DA15248006C26B22602D1BF4A83B0ACC0C,
	AdjustAppStoreSubscription_setSalesRegion_m0E1646795FA1466592F7E7A7D14B04EC02D6E39B,
	AdjustAppStoreSubscription_addCallbackParameter_mD67B08D11C9DCD410CB8966744F3962905E8AA70,
	AdjustAppStoreSubscription_addPartnerParameter_m6B639A50999BE4CC82D58CCCE7D1F50536D62019,
	AdjustAttribution_get_adid_m7FEE4DDFADFF7764690922FE17064A8475DCC159,
	AdjustAttribution_set_adid_m8FF9650D73A3B30569FA924D09F2A1B5841800F6,
	AdjustAttribution_get_network_m8430B735848CDEF80E9054A358E1147FBD19AEE3,
	AdjustAttribution_set_network_m68ED3E4E1E6850226D667FDE9829B402AF120D20,
	AdjustAttribution_get_adgroup_m15DAB5440B779D12C1BD8BCF9C47B20F14692416,
	AdjustAttribution_set_adgroup_m04EB13F0176574C01F8E233A15E6E7AB71CDEBFB,
	AdjustAttribution_get_campaign_mB839E1C4DD4EC624B6C46E9444F1A9D868EA0750,
	AdjustAttribution_set_campaign_m29AC5BBED526925450C7D081A5A656E9A71470E9,
	AdjustAttribution_get_creative_mC15C380B618E220C2143920CCB88EBAF8A864B36,
	AdjustAttribution_set_creative_mF0F350C3D8521BBC5D841A28428210CD9CF41183,
	AdjustAttribution_get_clickLabel_m45D150F891EF508E44F219A4CBE768A05BCA866D,
	AdjustAttribution_set_clickLabel_mAAFCDD0362AFE2EF2F6AEC66E6973B65B75692DE,
	AdjustAttribution_get_trackerName_mEA8576F240393B289A3C0CC66F9D7F2E965EEB52,
	AdjustAttribution_set_trackerName_m731697B9763F60A9FC502CC6A1A27BDBD2574876,
	AdjustAttribution_get_trackerToken_mB2CB9686A8CC7243A6C4391F4728F1BA8197F64A,
	AdjustAttribution_set_trackerToken_m6093F9C8CC27B2425BB1373F51EDFA26B9E2103F,
	AdjustAttribution_get_costType_m94B271C6C975D4C945D5912D7879C411BB2F25C6,
	AdjustAttribution_set_costType_m2B994A60E50367E752D803F431BE9B010BE784B0,
	AdjustAttribution_get_costAmount_m570856A2EFDAE1646AB3EBE61E9D11FC7A872182,
	AdjustAttribution_set_costAmount_m8C20F2BD1C52F1109660D5A965B5159BA4DC5647,
	AdjustAttribution_get_costCurrency_m746AD16AC39C41F680D4420B830529EAF595E999,
	AdjustAttribution_set_costCurrency_m4C83141F90E118ADEA5CCA620335B9FDD0C38D51,
	AdjustAttribution__ctor_m36B38620BB1475A4ACE1EDB1CCA466AB2F754307,
	AdjustAttribution__ctor_m8274D1B29F0C4D4D99E2067269DBF55161E3B98A,
	AdjustAttribution__ctor_mE3820E52AF63417CE1FF2ADAAE8B1BFA701344C9,
	AdjustConfig__ctor_m718373AA152F4C6F3AB5E805B4630AB008A32395,
	AdjustConfig__ctor_m96C4907B142108F8818BEBC52EDC03D90B5C6EA7,
	AdjustConfig_setLogLevel_mDA93163BE7A5E536C670CCDC0CCF7C93B9B3E54F,
	AdjustConfig_setDefaultTracker_mA67C3195A19A5E9AA2B5AF9E071336CA9E1AB724,
	AdjustConfig_setExternalDeviceId_m5AA54126D0A69091B9573F3A530BD2AF8B450FDF,
	AdjustConfig_setLaunchDeferredDeeplink_m8D6806307929E8E3AE2F01CE3C08BF96DDCD526F,
	AdjustConfig_setSendInBackground_m039AABBAF2DB300CE62F8CBF78DA3A5E36604317,
	AdjustConfig_setEventBufferingEnabled_mBB81E8C7A41ABCA6326F518EE53905C327B1F982,
	AdjustConfig_setNeedsCost_m27ACE0EB3E57AECBD640B2A1B4510BCFBE8553DD,
	AdjustConfig_setDelayStart_m5E3583922F84F6E2B9988052D54ABECE6113B0B6,
	AdjustConfig_setUserAgent_mDD4FFFE5044037A2BC22003F631A9989361DFA1D,
	AdjustConfig_setIsDeviceKnown_mAD1C556F14A0DBAED60254F330EF9625F3AB6EDA,
	AdjustConfig_setUrlStrategy_m43C184E9915977FC7955F22A086111B7836E2263,
	AdjustConfig_deactivateSKAdNetworkHandling_m9E3A12F2125AE97AF898E7AC49DBCE9085D93B9E,
	AdjustConfig_setDeferredDeeplinkDelegate_m0434CB6325F267D824956505E38F55C1BC69F750,
	AdjustConfig_getDeferredDeeplinkDelegate_m5E71CF0E1CD8ED86E14052643073B2B34A19E574,
	AdjustConfig_setAttributionChangedDelegate_m16311DC0B297069CC826AB0CEE81C747C47B7054,
	AdjustConfig_getAttributionChangedDelegate_m0B91F876BC47C733C887A0C674C69C7A2AAE859E,
	AdjustConfig_setEventSuccessDelegate_mC93D376662090A2A7D5341FCB0EB6F5D47034C00,
	AdjustConfig_getEventSuccessDelegate_m803B0AF83809209BDCA4FD72ADCD37A3D6525AAE,
	AdjustConfig_setEventFailureDelegate_mDF106AB503D7AE6A0EF9FC23C86FDB561C53D919,
	AdjustConfig_getEventFailureDelegate_m55B097E3E827DAA9D0A03C3827815990DEEFAA73,
	AdjustConfig_setSessionSuccessDelegate_m38873292BB0382A5A82272A971C2C8FB32EE97ED,
	AdjustConfig_getSessionSuccessDelegate_mDD3BD6C6F62AF59330E60B1570D2FC3D42DE20C1,
	AdjustConfig_setSessionFailureDelegate_m3BEF1CB7417F8E3E12E59E610DBE1FEA8584E2AC,
	AdjustConfig_getSessionFailureDelegate_mB847ACF06A571D19D85DD18BA596E78F646AED66,
	AdjustConfig_setConversionValueUpdatedDelegate_m944853EAA8941CDC4ECEA27C4C9CAD01279639B4,
	AdjustConfig_getConversionValueUpdatedDelegate_mA8286519D143FC8FA6AA32373F2169099ABEEE23,
	AdjustConfig_setAppSecret_mCF9AAAE31F6A695F806709B8599E319706BE15DE,
	AdjustConfig_setAllowiAdInfoReading_mBCAE2AC7ED0E99E915648114A3424E985EFE469C,
	AdjustConfig_setAllowAdServicesInfoReading_m232716609D173872EF41FD5837A9D0133419C4C1,
	AdjustConfig_setAllowIdfaReading_m439C9CAB2FDE23F534F838B3BEAC30B917E483CA,
	AdjustConfig_setProcessName_mA05E8249BDBEECED54C503BAAE53011D4EF18E53,
	AdjustConfig_setReadMobileEquipmentIdentity_m60C524B45682B362D3A43D8EA2AAB5E324F3D16C,
	AdjustConfig_setPreinstallTrackingEnabled_m50FF6E90421C467AAB8D1668E426E2F2F5B15BDA,
	AdjustConfig_setLogDelegate_mCD5A0B2CC87D71A6618CB76ED218FFDB346D487C,
	AdjustEnvironmentExtension_ToLowercaseString_m64A2BF7A9C9E8D34F18E1164E8C16D663513BB1F,
	AdjustEvent__ctor_mB6F2EEAE794AF0DBA97B384BD745A06235288C03,
	AdjustEvent_setRevenue_mA8B68C9A56C7A90FDD61D07D6E9B527EA4BAEB49,
	AdjustEvent_addCallbackParameter_m69836E8BCB0600E59592F2226886F7E3717267DC,
	AdjustEvent_addPartnerParameter_m5C8A9B71C8E3668F18D2A7107128C2AA7F60115B,
	AdjustEvent_setTransactionId_mD82CAE578CF9FBBB0F73937723AE9679D33AA254,
	AdjustEvent_setCallbackId_m77A43125761431059046C8BD038A4090A6F67A98,
	AdjustEvent_setReceipt_m0DA7BC506BF585B0EFDD3E0FEDC51EECE0406BFD,
	AdjustEventFailure_get_Adid_m63A229A1E387D51BA76FD857843A30909472F4E9,
	AdjustEventFailure_set_Adid_m1C9E862F9EE373D5F36B28D07F944581B4733FCC,
	AdjustEventFailure_get_Message_m39E32498366357A63414ACBF2D829D67E378435C,
	AdjustEventFailure_set_Message_m67C166B4D02AD43A8835555633ED6A41B6470472,
	AdjustEventFailure_get_Timestamp_m8AD7E740ED2BAD647DF69D3E9E20DA10AEA7894C,
	AdjustEventFailure_set_Timestamp_m144FA4FAB62F3AE2D92C8A729A4D80C78129FC8F,
	AdjustEventFailure_get_EventToken_m790B0C32B96810DB063845DB41C7EA5392511E0F,
	AdjustEventFailure_set_EventToken_m0107E2C7300ECD415209E1F64A6B8AD04F33798E,
	AdjustEventFailure_get_CallbackId_m7C6B49AB5A6AE7A9287E309C85E4DDC8B6E01F6F,
	AdjustEventFailure_set_CallbackId_mE4D4EE9B87B3B947F952C7BC539A177AA609B0FD,
	AdjustEventFailure_get_WillRetry_m437C69AED2629C0A51F93160CF269ECB51C48138,
	AdjustEventFailure_set_WillRetry_m4C79E145286998F97FFFC7106C792794C06669E9,
	AdjustEventFailure_get_JsonResponse_mB7A9E1270C3CA4F577552217E4FDB3CCFB32852A,
	AdjustEventFailure_set_JsonResponse_mC129C66E6BD3773556DD9984F8A9B41987A480EE,
	AdjustEventFailure__ctor_m528922562AC18ADE49AC59EFECDF9DDDF06D9827,
	AdjustEventFailure__ctor_mD35BD0B33754A00AF01D005F17CE529500281A14,
	AdjustEventFailure__ctor_mE44FDD70724F8F42E19DE705B7A0771C23BE0284,
	AdjustEventFailure_BuildJsonResponseFromString_mFC779A74C66E513EC19EF86F780AE363B25A828A,
	AdjustEventFailure_GetJsonResponse_m4A9D1FDB6FF13C9F955E00C64A4996F5826A31FD,
	AdjustEventSuccess_get_Adid_m9107BA449922578E0F9B8CB8B4541FE26A6C56C5,
	AdjustEventSuccess_set_Adid_mF832EF6F1DC6FE8156A132AD42AA1060E539A7AD,
	AdjustEventSuccess_get_Message_m5B29D1C7B3CF3C7CED972991740A888131931DE2,
	AdjustEventSuccess_set_Message_m38D9A47DB181615424C49B59C6E4A562B3E5F89F,
	AdjustEventSuccess_get_Timestamp_m193EB4EBA0B8DA8CF0863D1DF75FEF141B1D3B10,
	AdjustEventSuccess_set_Timestamp_m0CCE0BEF1E47ACA8E07187A73BBE9ACFEEC6586B,
	AdjustEventSuccess_get_EventToken_m5784EFFBAE4463DA0ECFF6A537731DC98E286A3E,
	AdjustEventSuccess_set_EventToken_mAF539927077C6E4B98FC29622DE5D26C3A5F2C64,
	AdjustEventSuccess_get_CallbackId_m3D7D77C8EF5837C5EAAB45998FD4C7A02C04D983,
	AdjustEventSuccess_set_CallbackId_mA49D8F4F34D8A1C9FB36A15EFB7572AC187A28C9,
	AdjustEventSuccess_get_JsonResponse_mC1ED1F8BC320A1BE406D403D15DB0EA699A01A75,
	AdjustEventSuccess_set_JsonResponse_mCA8F4E6DE391C1D4B8BCEEFB437BA5EE1E717D90,
	AdjustEventSuccess__ctor_m8E95350D1027E90E42E4A890D5D8F6C683C1388C,
	AdjustEventSuccess__ctor_m3AF21839E90ADA4ACF33D117311F354A788FFE1B,
	AdjustEventSuccess__ctor_m572E2ED470E4819DFF8462F86CD0A35EE856DE75,
	AdjustEventSuccess_BuildJsonResponseFromString_mB45093E3AE421B1E1C210318F2081EB7016C065C,
	AdjustEventSuccess_GetJsonResponse_mC8F1B778DCD86E0CFCE0A7F34D2AE30E440E465B,
	AdjustLogLevelExtension_ToLowercaseString_m34762F532C1EFC081ACEDDC382441EF5B124BD13,
	AdjustLogLevelExtension_ToUppercaseString_mBDB79054591AE2DA26F4C0139CBC670095675033,
	AdjustPlayStoreSubscription__ctor_m8FAA1BDF8B8C354B18FB090ACB1EF65E0B381EA1,
	AdjustPlayStoreSubscription_setPurchaseTime_mFB33C90CFC1A515912E08927A41E27DEB80094F4,
	AdjustPlayStoreSubscription_addCallbackParameter_m384EC847C6931509BE14FF2275D6AB32F493F9A4,
	AdjustPlayStoreSubscription_addPartnerParameter_m864989B749FAE715C806A3772FC7B968FFD4A5F4,
	AdjustSessionFailure_get_Adid_m55CBA752E653E41BB100CA0666E984AC41A1C986,
	AdjustSessionFailure_set_Adid_m9D52E417E29F03D868D2A5C1BA50578FAE232BC7,
	AdjustSessionFailure_get_Message_m7FB5952110E6198593306F2D2206C87878241071,
	AdjustSessionFailure_set_Message_m84D2E372880BCEAB77F55A2D5E3228A2D0179835,
	AdjustSessionFailure_get_Timestamp_m16815BDDD78D3DC8836D6929D7ECA0287567E1C9,
	AdjustSessionFailure_set_Timestamp_m4620F96554EF0DBF543BF574C3B9E2CBEA0BF46E,
	AdjustSessionFailure_get_WillRetry_mDC6EF21BB9ED54A38E87A437F25B3E1ABFB64CB7,
	AdjustSessionFailure_set_WillRetry_m891830EFFC0F200C979980F639EF51F2357E6BCF,
	AdjustSessionFailure_get_JsonResponse_m3CC10F98CEFA48F10203B4B21CA8B7F48313E337,
	AdjustSessionFailure_set_JsonResponse_m9697C8316211570DED147C08CA044DB7A9626B6E,
	AdjustSessionFailure__ctor_m55084005614B14B05358BFC8D8093D0E1BA5D577,
	AdjustSessionFailure__ctor_mC8D3BF875D5D8A394B38A08DA6FD82FE78D65AB2,
	AdjustSessionFailure__ctor_mF96CCCD25D8F54F5FE37C1532E5A7D5B1FADEB3F,
	AdjustSessionFailure_BuildJsonResponseFromString_m2D4F30200FC6361CACC4417A512F8E14FF9C38A6,
	AdjustSessionFailure_GetJsonResponse_mE5D4C31B41ED1899C26AB32CD2648ADEFDE09351,
	AdjustSessionSuccess_get_Adid_m647C0D4B4E911D6C8BE1634A171F548461180414,
	AdjustSessionSuccess_set_Adid_m4393AA9B18910CE351BB43D1C510132B4F971573,
	AdjustSessionSuccess_get_Message_m86BB21FF8BEC5DA95055C3A12413D7CEAF1731EA,
	AdjustSessionSuccess_set_Message_mD680D8861FD8EE269D0994D51498AC2210694E99,
	AdjustSessionSuccess_get_Timestamp_mE2D213502F0F03A341B1E39DC4152AEF5C68F813,
	AdjustSessionSuccess_set_Timestamp_m2ED4611CC016044E197BF515B3A7C81C27B207EA,
	AdjustSessionSuccess_get_JsonResponse_m13404EAE48C660945ED5BBC50A26E9AB2E4B8595,
	AdjustSessionSuccess_set_JsonResponse_mCFFE1E0F01BD95837EE0A4E9D89CE5913C3E0FBC,
	AdjustSessionSuccess__ctor_m5D4F0E9806EDCE8130DE98471E7ECA654B744F9A,
	AdjustSessionSuccess__ctor_m468034512A1D2682AA0F15926CE8CA80F239C31D,
	AdjustSessionSuccess__ctor_mFD79CF038E807DE1559B54362B6E87EFAEFCD542,
	AdjustSessionSuccess_BuildJsonResponseFromString_m2CA7E40EDAD331AE6DEDF385D364682D7AC8ACCE,
	AdjustSessionSuccess_GetJsonResponse_m22B1531644212867F4EFF412E5B90CC8F7A15C5D,
	AdjustThirdPartySharing__ctor_mD050F802304C5E3A20E88D7C1F8AE85586641A82,
	AdjustThirdPartySharing_addGranularOption_m430DCE18F822237234F208C6FFD6C7837A2A1A77,
	AdjustUtils_ConvertLogLevel_m0FCC0CC22C6B6C6F22D5B15A9D3AE79782DC582A,
	AdjustUtils_ConvertBool_m1EF79C9F1C7B11BC5F240653329F039AD770AAF9,
	AdjustUtils_ConvertDouble_mF0BA3CCACECD87FDA363512371270A8989F6CCB4,
	AdjustUtils_ConvertInt_m10AC63AC85A6C9BC94013B6677A8358BFF2B489D,
	AdjustUtils_ConvertLong_m73EFABDE60849F9C2F456F267ABAF7AF9DDFB438,
	AdjustUtils_ConvertListToJson_mD7777D5BA7CDEC21713D8E898B349459CE08F8B6,
	AdjustUtils_GetJsonResponseCompact_m6524D9E659857DDE6E5A40C5E9FD20FC63AE4175,
	AdjustUtils_GetJsonString_m3BCF0F70953B40ACC0609048179E44C810FFDC3E,
	AdjustUtils_WriteJsonResponseDictionary_mBD703AD06DD17B0777ECE4322F39126C4DFB687F,
	AdjustUtils_TryGetValue_mB22F0EC045510DED75ECA87B697F24A4B3A049E5,
	AdjustUtils_TestOptionsMap2AndroidJavaObject_mEE341272AC54FF3806A16A4C04D1690CB8538A19,
	AdjustUtils__ctor_mEE74F3B9A26BAE12B3C426FF63604FD7396544A2,
	AdjustUtils__cctor_m1BEC2F6F63E6AFF40E12F581D676AC0132398CEE,
};
static const int32_t s_InvokerIndices[989] = 
{
	5935,
	4749,
	4775,
	4775,
	4775,
	4775,
	4775,
	4775,
	4775,
	5935,
	11082,
	5935,
	4775,
	5935,
	5935,
	4749,
	4749,
	4684,
	4684,
	5935,
	5935,
	5868,
	5868,
	5923,
	5868,
	4828,
	5868,
	4828,
	5788,
	4749,
	5728,
	4684,
	5728,
	4684,
	5935,
	5935,
	4775,
	4775,
	4775,
	1029,
	5935,
	2193,
	4775,
	4444,
	5935,
	5868,
	4828,
	5935,
	4775,
	4775,
	1029,
	5935,
	5935,
	5935,
	4775,
	4775,
	5935,
	5868,
	4828,
	4749,
	5935,
	5935,
	4775,
	4775,
	1029,
	5935,
	11043,
	5935,
	5935,
	11043,
	5935,
	4684,
	4684,
	11082,
	5935,
	5935,
	4684,
	4684,
	0,
	0,
	0,
	11082,
	10248,
	10248,
	11043,
	5935,
	11082,
	11036,
	11036,
	11036,
	11036,
	11043,
	10256,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5923,
	4879,
	5868,
	5788,
	5814,
	5814,
	5814,
	5814,
	5935,
	3459,
	2711,
	5728,
	4775,
	5935,
	5935,
	11082,
	10256,
	5935,
	5935,
	11082,
	0,
	5728,
	5728,
	4775,
	4775,
	5935,
	5935,
	5935,
	5935,
	5935,
	4749,
	5935,
	5935,
	5935,
	5935,
	5935,
	5935,
	5935,
	5935,
	5646,
	5935,
	5728,
	5935,
	5935,
	5935,
	4775,
	5814,
	5935,
	4749,
	5935,
	5728,
	5814,
	5935,
	5814,
	5728,
	5935,
	2496,
	5935,
	5935,
	5935,
	4684,
	5935,
	5935,
	5935,
	5935,
	5728,
	4684,
	5935,
	5935,
	5935,
	4881,
	5935,
	5935,
	5935,
	5935,
	4881,
	4775,
	2777,
	4287,
	4684,
	5935,
	4749,
	5935,
	5728,
	5814,
	5935,
	5814,
	11043,
	10256,
	5935,
	5935,
	5935,
	5935,
	5935,
	5935,
	4775,
	5935,
	11043,
	10256,
	11019,
	10248,
	11019,
	10248,
	11019,
	10248,
	5935,
	5935,
	5935,
	5935,
	4775,
	5935,
	2730,
	2730,
	2730,
	2730,
	5814,
	5935,
	5935,
	2730,
	1478,
	2730,
	2730,
	2730,
	2730,
	5935,
	2730,
	2730,
	1478,
	2730,
	2730,
	2730,
	2730,
	1496,
	2730,
	5814,
	5814,
	5935,
	4775,
	1478,
	4684,
	4775,
	5935,
	5935,
	5728,
	5728,
	5935,
	4775,
	4749,
	5935,
	5728,
	5814,
	5935,
	5814,
	4749,
	5935,
	5728,
	5814,
	5935,
	5814,
	4749,
	5935,
	5728,
	5814,
	5935,
	5814,
	5935,
	5935,
	5935,
	11082,
	11043,
	10256,
	5935,
	5935,
	2519,
	2519,
	2519,
	4749,
	5935,
	5935,
	2726,
	4775,
	1236,
	4775,
	8910,
	8910,
	8910,
	10252,
	11082,
	11043,
	10256,
	11043,
	10256,
	11019,
	10248,
	5935,
	5935,
	5935,
	5935,
	10256,
	5935,
	11082,
	4775,
	4775,
	5935,
	5935,
	5935,
	11036,
	11019,
	5814,
	11043,
	498,
	4684,
	5935,
	5935,
	5935,
	11082,
	5935,
	11082,
	10256,
	10256,
	10256,
	5935,
	9990,
	11082,
	11043,
	10256,
	11082,
	11082,
	10252,
	11082,
	11082,
	5935,
	4684,
	11043,
	10256,
	5935,
	4828,
	4828,
	4828,
	4828,
	4828,
	4828,
	5935,
	11043,
	10256,
	5935,
	5935,
	5935,
	5728,
	2730,
	4271,
	5935,
	4684,
	2730,
	4749,
	4055,
	2723,
	5935,
	5814,
	4775,
	5935,
	2747,
	5935,
	11043,
	10256,
	5814,
	5814,
	5868,
	4828,
	5925,
	5935,
	5814,
	0,
	0,
	0,
	0,
	0,
	2747,
	5935,
	5935,
	5935,
	4749,
	5935,
	5728,
	5814,
	5935,
	5814,
	11043,
	10256,
	5935,
	5935,
	11043,
	10256,
	11036,
	10252,
	11036,
	10252,
	5935,
	5935,
	5935,
	5935,
	5935,
	5935,
	5935,
	5935,
	5814,
	5814,
	5935,
	5935,
	5935,
	4775,
	5925,
	5935,
	5935,
	4749,
	5935,
	5728,
	5814,
	5935,
	5814,
	4749,
	5935,
	5728,
	5814,
	5935,
	5814,
	11082,
	5935,
	1957,
	11043,
	10256,
	5935,
	5935,
	5935,
	5935,
	5935,
	4775,
	5935,
	5935,
	4775,
	4775,
	5935,
	5935,
	4879,
	5935,
	5935,
	5935,
	5935,
	5935,
	5935,
	5935,
	4881,
	4684,
	2519,
	2452,
	4749,
	5935,
	5935,
	5935,
	5814,
	5935,
	4749,
	5935,
	5728,
	5814,
	5935,
	5814,
	11043,
	10256,
	5814,
	5935,
	5935,
	5935,
	5935,
	5935,
	5814,
	5935,
	5935,
	5935,
	4879,
	5935,
	5935,
	4684,
	4775,
	5935,
	4749,
	5935,
	5728,
	5814,
	5935,
	5814,
	498,
	5935,
	5935,
	498,
	5935,
	5935,
	5935,
	5935,
	5935,
	498,
	5935,
	5935,
	498,
	4749,
	5814,
	5935,
	5935,
	4749,
	5935,
	5728,
	5814,
	5935,
	5814,
	2723,
	5935,
	5935,
	4684,
	5935,
	5935,
	498,
	4684,
	5935,
	4749,
	5935,
	5935,
	5935,
	5935,
	5814,
	4749,
	4749,
	5935,
	5935,
	4749,
	5935,
	5728,
	5814,
	5935,
	5814,
	498,
	5935,
	2265,
	5935,
	5935,
	5935,
	498,
	5935,
	5935,
	5935,
	5935,
	5935,
	5935,
	5935,
	5814,
	5935,
	5935,
	5935,
	5935,
	5728,
	5935,
	5728,
	4749,
	5935,
	5728,
	5814,
	5935,
	5814,
	498,
	4775,
	5935,
	4775,
	5935,
	4775,
	5935,
	2730,
	4267,
	2519,
	4271,
	2730,
	5814,
	4775,
	5788,
	4775,
	4271,
	4267,
	4271,
	5814,
	5814,
	5814,
	4271,
	5788,
	4749,
	5868,
	4828,
	5755,
	4715,
	5728,
	4684,
	5814,
	5814,
	9990,
	9990,
	8354,
	8354,
	3459,
	5788,
	9990,
	9990,
	4775,
	4775,
	4775,
	4775,
	5814,
	9990,
	9990,
	9990,
	9990,
	9990,
	9990,
	5935,
	4749,
	5935,
	5728,
	5814,
	5935,
	5814,
	5814,
	5814,
	4749,
	5935,
	5728,
	5935,
	5935,
	5814,
	5935,
	5814,
	5814,
	5814,
	4267,
	2519,
	4271,
	2730,
	5788,
	2730,
	4267,
	4271,
	5814,
	5814,
	5814,
	4271,
	4775,
	5935,
	4749,
	5935,
	5728,
	5935,
	5814,
	5935,
	5814,
	5814,
	5814,
	4749,
	5935,
	5728,
	5935,
	5814,
	5935,
	5814,
	4271,
	2730,
	4267,
	2519,
	5788,
	2730,
	4271,
	4267,
	4271,
	5814,
	5814,
	5814,
	4271,
	4775,
	5935,
	5935,
	3200,
	4749,
	5935,
	5728,
	5935,
	5814,
	5935,
	5814,
	5814,
	5814,
	4749,
	5935,
	5728,
	5935,
	5814,
	5935,
	5814,
	5814,
	4775,
	4775,
	4828,
	4715,
	4684,
	4749,
	5814,
	4271,
	4775,
	4775,
	2730,
	4775,
	4267,
	2519,
	4271,
	2730,
	4775,
	2730,
	8354,
	8354,
	3459,
	5788,
	5814,
	4271,
	5788,
	4749,
	5868,
	4828,
	5755,
	4715,
	5728,
	4684,
	5814,
	5814,
	9990,
	10256,
	10256,
	11019,
	10248,
	10248,
	11082,
	10256,
	11043,
	11082,
	11082,
	11043,
	8977,
	8977,
	10256,
	10256,
	11082,
	11082,
	10256,
	8977,
	10256,
	10256,
	10256,
	10248,
	11082,
	11082,
	10256,
	10256,
	11043,
	11043,
	10256,
	9762,
	5935,
	11082,
	4775,
	4775,
	4775,
	3459,
	4775,
	4775,
	4775,
	4775,
	4775,
	4775,
	4775,
	4775,
	4775,
	4775,
	4775,
	5935,
	4684,
	10256,
	10256,
	10248,
	11019,
	10248,
	10256,
	11082,
	11082,
	10256,
	11082,
	8977,
	8977,
	10256,
	10256,
	11082,
	11082,
	8977,
	10256,
	10256,
	10256,
	10256,
	10248,
	8977,
	10252,
	11036,
	11043,
	11043,
	11043,
	11043,
	11043,
	10256,
	10256,
	11043,
	11019,
	10256,
	5935,
	4775,
	2282,
	4749,
	4775,
	4775,
	4775,
	2730,
	2730,
	990,
	4775,
	4775,
	2730,
	2730,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5642,
	4602,
	5814,
	4775,
	5935,
	4775,
	4775,
	2723,
	1451,
	4749,
	4775,
	4775,
	4684,
	4684,
	4684,
	4684,
	4715,
	4775,
	4684,
	4775,
	5935,
	2730,
	5814,
	2730,
	5814,
	2730,
	5814,
	2730,
	5814,
	2730,
	5814,
	2730,
	5814,
	2730,
	5814,
	492,
	4684,
	4684,
	4684,
	4775,
	4684,
	4684,
	4775,
	9987,
	4775,
	2282,
	2730,
	2730,
	4775,
	4775,
	2730,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5728,
	4684,
	5814,
	4775,
	5935,
	4775,
	4775,
	4775,
	5814,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5935,
	4775,
	4775,
	4775,
	5814,
	9987,
	9987,
	303,
	4775,
	2730,
	2730,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5728,
	4684,
	5814,
	4775,
	5935,
	4775,
	4775,
	4775,
	5814,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5814,
	4775,
	5935,
	4775,
	4775,
	4775,
	5814,
	4600,
	1478,
	9871,
	9872,
	9821,
	9873,
	9908,
	9990,
	9990,
	8637,
	8977,
	8637,
	8637,
	5935,
	11082,
};
static const Il2CppTokenRangePair s_rgctxIndices[5] = 
{
	{ 0x06000174, { 0, 2 } },
	{ 0x06000175, { 2, 2 } },
	{ 0x06000176, { 4, 1 } },
	{ 0x06000177, { 5, 2 } },
	{ 0x06000178, { 7, 1 } },
};
extern const uint32_t g_rgctx_T_tDD48826E9560B9F82AC8FD61DD5AB231ED194E04;
extern const uint32_t g_rgctx_T_tDD48826E9560B9F82AC8FD61DD5AB231ED194E04;
extern const uint32_t g_rgctx_T_t55FB7897D0D3CE0E6C39A65423D0906D2E0985C4;
extern const uint32_t g_rgctx_T_t55FB7897D0D3CE0E6C39A65423D0906D2E0985C4;
extern const uint32_t g_rgctx_T_tBF53114EB54F3D242B472108F68CFD2C7A51CFF8;
extern const uint32_t g_rgctx_T_t1EE558A3754D67AFEAB7E05C921028B7218A9776;
extern const uint32_t g_rgctx_T_t1EE558A3754D67AFEAB7E05C921028B7218A9776;
extern const uint32_t g_rgctx_T_tE3A8C5E0F2E745E21DA0A13EF5BC131CB21A13B6;
static const Il2CppRGCTXDefinition s_rgctxValues[8] = 
{
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tDD48826E9560B9F82AC8FD61DD5AB231ED194E04 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tDD48826E9560B9F82AC8FD61DD5AB231ED194E04 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t55FB7897D0D3CE0E6C39A65423D0906D2E0985C4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t55FB7897D0D3CE0E6C39A65423D0906D2E0985C4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tBF53114EB54F3D242B472108F68CFD2C7A51CFF8 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t1EE558A3754D67AFEAB7E05C921028B7218A9776 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t1EE558A3754D67AFEAB7E05C921028B7218A9776 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tE3A8C5E0F2E745E21DA0A13EF5BC131CB21A13B6 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	989,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	5,
	s_rgctxIndices,
	8,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
