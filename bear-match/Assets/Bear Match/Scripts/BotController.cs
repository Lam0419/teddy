using System.Collections;
using UnityEngine;

namespace BearMatch
{
    public class BotController : MonoBehaviour
    {
        [Header("Move")]
        [SerializeField] private Rigidbody body;
        [SerializeField] private CapsuleCollider moveCollider;
        [SerializeField] private CapsuleCollider blockPlayerCollider;
        [SerializeField] private float moveSpeed;

        [Header("Ragdoll")]
        [SerializeField] private Animator animator;
        [SerializeField] private Rigidbody[] ragdollBodies;
        [SerializeField] private Transform rootBody;
        [SerializeField] private float pushBackTime = 2f;
        [SerializeField] private GameObject hitEffect;
        [SerializeField] private GameObject bloodFx;


        public bool IsDied { get; private set; } = false;


        private const string AnimationParameter_Move = "Move";
        private const string AnimationParameter_Die = "Die";


        private PhysicMaterial slippyPhysics;

        private Vector3 targetPosition = Vector3.zero;
        private Vector3 targetForward = Vector3.zero;
        private Vector3 currentVelocity;

        private bool canRun = false;
        private float timeStartChooseTarget = -1;
        private float timeComeToTarget = -1;
        private bool isWaitToNextTurn = false;

        private bool isRagdoll = false;

        private void Start()
        {
            Physics.IgnoreCollision(moveCollider, blockPlayerCollider, true);

            slippyPhysics = new PhysicMaterial
            {
                name = "slippyPhysics",
                staticFriction = 0f,
                dynamicFriction = 0f,
                frictionCombine = PhysicMaterialCombine.Minimum
            };

            moveCollider.material = slippyPhysics;
            blockPlayerCollider.material = slippyPhysics;

            EventDispatcherExtension.AddListener(EventID.Player_Die, OnPlayerDie);

            hitEffect.SetActive(false);
            bloodFx.SetActive(false);
        }

        private void OnDisable()
        {
            EventDispatcherExtension.RemoveListener(EventID.Player_Die, OnPlayerDie);
        }

        private void OnValidate()
        {
            body = GetComponent<Rigidbody>();
            moveCollider = transform.GetChild(0).GetComponent<CapsuleCollider>();
            blockPlayerCollider = transform.GetChild(1).GetComponent<CapsuleCollider>();

            animator = transform.GetChild(2).GetComponent<Animator>();
            ragdollBodies = animator.GetComponentsInChildren<Rigidbody>();
            rootBody = animator.transform.GetChild(0);

            hitEffect = transform.GetChild(3).gameObject;
            bloodFx = transform.GetChild(4).gameObject;

            foreach (var ragdoll in ragdollBodies)
            {
                ragdoll.tag = Constants.BOT_TAG;
            }
        }

        public void InitializeNewGame(Vector3 spawnPoint)
        {
            StopAllCoroutines();

            IsDied = false;
            isWaitToNextTurn = false;

            currentVelocity = Vector3.zero;
            ToggleRagdoll(true);

            //RandomTargetPosition();

            transform.position = spawnPoint;
            transform.forward = Vector3.forward;

            animator.SetBool(AnimationParameter_Move, false);

            canRun = false;
            Invoke(nameof(StartRunToTargetPoint), 1);
        }

        public void StartRunToTargetPoint()
        {
            canRun = true;
            isWaitToNextTurn = false;

            RandomTargetPosition();
        }

        private void RandomTargetPosition()
        {
            if (isWaitToNextTurn) return;

            targetPosition = GameplayController.Instance.GetRandomGridItemActive();

            targetForward.x = targetPosition.x - transform.position.x;
            targetForward.z = targetPosition.z - transform.position.z;

            timeStartChooseTarget = Time.time;
            timeComeToTarget = -1;
        }

        private void Update()
        {
            if (IsDied || GameplayController.State == GameplayState.Game_Over || isRagdoll
                || GameplayController.State == GameplayState.Victory) return;

            if (!canRun)
            {
                currentVelocity = Vector3.zero;
                currentVelocity.y = body.velocity.y;
                body.velocity = currentVelocity;
            }
            else
            {
                if (timeComeToTarget < 0)
                {
                    currentVelocity = (targetPosition - transform.position).normalized * moveSpeed;

                    targetForward.x = currentVelocity.x;
                    targetForward.z = currentVelocity.z;
                    transform.forward = Vector3.Lerp(transform.forward, targetForward, 0.1f);

                    currentVelocity.y = body.velocity.y;
                    body.velocity = currentVelocity;
                }
                else
                {
                    currentVelocity = Vector3.zero;
                    currentVelocity.y = body.velocity.y;
                    body.velocity = currentVelocity;
                }

                if (!isWaitToNextTurn && Time.time - timeStartChooseTarget >= 2)
                {
                    RandomTargetPosition();
                }
                else
                {
                    // if come to target, run to next point
                    if (Vector3.Distance(transform.position, targetPosition) <= 0.45f)
                    {
                        if (timeComeToTarget < 0)
                            timeComeToTarget = Time.time;

                        if (timeComeToTarget > 0 && Time.time - timeComeToTarget >= 0.5f)
                            RandomTargetPosition();
                    }
                    else
                    {
                        timeComeToTarget = -1;
                    }
                }

                animator.SetBool(AnimationParameter_Move, timeComeToTarget < 0);
            }
        }

        public void TriggerUnderground()
        {
            if (IsDied) return;

            Logger.Log($"Kill {gameObject.name}");

            StopAllCoroutines();
            IsDied = true;

            bloodFx.SetActive(false);
            bloodFx.SetActive(true);

            animator.SetBool(AnimationParameter_Move, false);

            ToggleRagdoll(false);

            GameplayController.Instance.HaveBotKilled();
        }

        public void RunToGridResult(Vector3 position)
        {
            targetPosition = position;

            targetForward.x = targetPosition.x - transform.position.x;
            targetForward.z = targetPosition.z - transform.position.z;

            timeComeToTarget = -1;
            isWaitToNextTurn = true;
        }

        private void OnPlayerDie(object obj)
        {
            if (IsDied) return;

            StopAllCoroutines();
            animator.SetBool(AnimationParameter_Move, false);

            currentVelocity = Vector3.zero;
            body.velocity = currentVelocity;
            //body.isKinematic = true;
        }

        public void PushBack(float pushBackForce, float pushBackAngle)
        {
            if (isRagdoll) return;

            SoundManager.Instance.Play_HitBot();

            ToggleRagdoll(false);

            Vector3 explosionDirection = moveCollider.transform.position - PlayerController.Instance.transform.position;
            explosionDirection.y = Mathf.Sqrt(explosionDirection.x * explosionDirection.x + explosionDirection.z * explosionDirection.z) * Mathf.Tan(pushBackAngle * Mathf.Deg2Rad);
            explosionDirection = explosionDirection.normalized * pushBackForce;

            StartCoroutine(IEPushBack(explosionDirection));
        }

        private IEnumerator IEPushBack(Vector3 pushBackForce)
        {
            foreach (Rigidbody ragdollBone in ragdollBodies)
            {
                ragdollBone.AddForce(pushBackForce, ForceMode.Impulse);
            }

            hitEffect.SetActive(false);
            hitEffect.SetActive(true);

            yield return new WaitForSeconds(pushBackTime);
            transform.position = rootBody.position;
            ToggleRagdoll(true);
        }

        private void ToggleRagdoll(bool isDisableRagdoll)
        {
            isRagdoll = !isDisableRagdoll;

            moveCollider.enabled = isDisableRagdoll;
            blockPlayerCollider.enabled = isDisableRagdoll;

            body.isKinematic = !isDisableRagdoll;
            body.velocity = Vector3.zero;

            foreach (Rigidbody ragdollBone in ragdollBodies)
            {
                ragdollBone.isKinematic = isDisableRagdoll;
                ragdollBone.velocity = Vector3.zero;
            }

            animator.enabled = isDisableRagdoll;
            if (isDisableRagdoll)
            {
                animator.Play("idle", -1, 0);
            }
        }
    }
}
