﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace BearMatch
{
    public class GameplayUi : MonoBehaviour
    {
        [SerializeField] private GameObject gameplayControl;
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private TextMeshProUGUI coinText;

        [Header("Countdown"), Space(15)]
        [SerializeField] private TextMeshProUGUI countdownText;
        [SerializeField] private AudioSource countdownSoundEffect;

        [Header("Tutorial"), Space(15)]
        [SerializeField] private GameObject[] tutorialTexts;
        [SerializeField] private GameObject hand;


        public static GameplayUi Instance { get; private set; }

        private int countdownTime = 3;
        private UnityAction countdownCallback;
        private Animator countdownTextAnimator;

        private PlayerController player;
        private int countReplayLevel;
        private void Awake()
        {
            Instance = this;
            EventDispatcherExtension.AddListener(EventID.Player_Die, OnPlayerDie);
            EventDispatcherExtension.AddListener(EventID.Coin_Changed, OnCoinChanged);
        }

        private void OnDestroy()
        {
            EventDispatcherExtension.RemoveListener(EventID.Player_Die, OnPlayerDie);
            EventDispatcherExtension.RemoveListener(EventID.Coin_Changed, OnCoinChanged);
        }

        private void Start()
        {
            countdownTextAnimator = countdownText.gameObject.GetComponent<Animator>();

            InitializeNewGame();
        }

        public void InitializeNewGame()
        {
            GamePopup.ClearAllPopup();
            UiManager.Instance.ShowPanel<StartGamePopup>();

            gameplayControl.SetActive(false);
            player = PlayerController.Instance;

            GameplayController.Instance.InitializeNewGame();
            player.InitializeNewGame();
            CameraController.Instance.InitializeNewGame();

            UpdateLevelText();
            OnCoinChanged();

            hand.SetActive(true);

            switch (SaveLoadData.Data.CurrentLevel)
            {
                case 0:
                    for (int i = 0; i < tutorialTexts.Length; i++)
                    {
                        tutorialTexts[i].SetActive(i == 0);
                    }
                    break;

                case 1:
                    for (int i = 0; i < tutorialTexts.Length; i++)
                    {
                        tutorialTexts[i].SetActive(i == 1 && GameplayController.TestGameType == 0);
                    }
                    break;

                default:
                    for (int i = 0; i < tutorialTexts.Length; i++)
                    {
                        tutorialTexts[i].SetActive(false);
                    }
                    break;
            }
        }

        public void StartGame()
        {
#if !UNITY_EDITOR
            FirebaseManager.LogEvent($"start_{SaveLoadData.Data.CurrentLevel}");
#endif
            gameplayControl.SetActive(true);
            countdownText.gameObject.SetActive(false);

            GameplayController.Instance.StartNewLevel();
            player.StartNewLevel();
            CameraController.Instance.StartNewLevel();
        }

        public void ShowCountdown(UnityAction countdownCallback)
        {
            this.countdownCallback = countdownCallback;

            countdownSoundEffect.Play();
            countdownTime = 3;

            countdownText.gameObject.SetActive(true);
            countdownText.text = countdownTime.ToString();

            InvokeRepeating(nameof(Countdown), 1, 1);
        }

        private void Countdown()
        {
            countdownTime--;
            if (countdownTime <= 0)
            {
                countdownSoundEffect.Stop();

                countdownText.gameObject.SetActive(false);
                CancelInvoke(nameof(Countdown));

                countdownCallback?.Invoke();
            }
            else
            {
                countdownText.text = countdownTime.ToString();
                countdownTextAnimator.Play("Countdown Text Show", -1, 0);
            }
        }

        private void UpdateLevelText()
        {
            levelText.text = $"LEVEL {SaveLoadData.Data.CurrentLevel + 1:00}";
        }



        #region Event handler
        private void OnPlayerDie(object obj)
        {
            CancelInvoke();
            gameplayControl.SetActive(false);
            countdownSoundEffect.Stop();

            Invoke(nameof(ShowGameOverPopup), 1);
        }

        private void OnCoinChanged(object obj = null)
        {
            coinText.text = $"{SaveLoadData.Data.Coin}";
        }

        public void TouchStartMovePlayer()
        {
            hand.SetActive(false);
        }

        public void StartMovePlayer()
        {
            player.StartMovePlayer();
        }

        public void UpdatePlayerVelocity(Vector2 joystickDirection)
        {
            player.UpdatePlayerVelocity(joystickDirection);
        }

        public void EndMovePlayer()
        {
            player.EndMovePlayer();

            if (SaveLoadData.Data.CurrentLevel == 0)
            {
                hand.SetActive(true);
            }
        }

        #endregion



        private void ShowGameOverPopup()
        {
            UiManager.Instance.ShowPanel<GameOverPopup>();
        }

        public void ReplayLevel()
        {
            countReplayLevel++;
            Debug.Log("total count: " + countReplayLevel +"_firebase: " + FirebaseManager.countReplay);
            if(countReplayLevel == FirebaseManager.countReplay)
            {
                AdsManager.Instance.ShowInterstitialAd(InitializeNewGame);
                countReplayLevel = 0;
            }
            else
            {
                InitializeNewGame();
            }
        }

        public void ShowVictory()
        {
            CancelInvoke();
            gameplayControl.SetActive(false);
            SoundManager.Instance.Play_Victory();

            Invoke(nameof(ShowVictoryPopup), 3);
        }

        private void ShowVictoryPopup()
        {
            if (GameplayController.State == GameplayState.Victory)
                UiManager.Instance.ShowPanel<VictoryPopup>();
        }

        public void NextLevel()
        {
            SaveLoadData.NextLevel();
            InitializeNewGame();
        }
    }
}
