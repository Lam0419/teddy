﻿namespace BearMatch
{
    public enum EventID
    {
        Player_Die,
        Bought_No_Ads,
        Victory,
        Coin_Changed,
    }
}
