using UnityEngine;
using UnityEngine.Events;

namespace BearMatch
{
    public class NoInternetPopup : GamePopup
    {
        public override void Show(GamePopup popup, bool isHideLastPopup = true, object data = null, UnityAction closeCallback = null, bool isShowSuperImage = true)
        {
            base.Show(popup, isHideLastPopup: false, data, closeCallback, isShowSuperImage: false);
        }

        private void Update()
        {
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                Time.timeScale = 1;
                Hide();
            }
        }
    }
}
