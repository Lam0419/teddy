﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace BearMatch
{
    public enum GameplayState { None, Wait_To_Start, Playing, Game_Over, Victory }

    public class GameplayController : MonoBehaviour
    {
        [SerializeField] private Transform environment;
        [SerializeField] private Vector3[] environmentPositions;
        [SerializeField] private PlayerController[] playerPrefabs;

        [Header("Grid")]
        [SerializeField] private Transform gridParent;
        [SerializeField] private GridItem gridItemTemplate;

        [Header("Decal")]
        [SerializeField] private SpriteRenderer decalResult;
        [SerializeField] private GameObject arrow;
        [SerializeField] private Sprite[] decalSprites;

        [Header("Bot")]
        [SerializeField] private Transform spawnPoint;
        [SerializeField] private List<Transform> spawnPoints;
        [SerializeField] private Vector2Int minMaxBotPerLevel = new(3, 5);
        [SerializeField] private BotController[] botTemplates;

        public static GameplayController Instance { get; private set; }

        public static GameplayState State { get; private set; } = GameplayState.None;

        public static int TestGameType { get; private set; }


        // create grid
        private bool isGridCleared;
        private List<GridItem> gridItems = new();
        private int gridItemsSize = 0;
        //private int currentLength;
        //private int currentWidth;
        private int numberGridActive;
        private int numberGridItemMoveDown;

        // random decal
        private int numberGridHaveDecalSameResult;
        private int decalIndexResult = 0;
        private List<int> indexOfGridItemResults = new();


        // bots
        private List<int> indexSpawnPointNotUse;
        private List<BotController> bots = new();
        private int numberBotActive;

        private PlayerController player;
        private int currentLevel;

        private void Awake()
        {
            Instance = this;
            State = GameplayState.None;
            EventDispatcherExtension.AddListener(EventID.Player_Die, OnPlayerDie);

            SpawnPlayer();

            TestGameType = FirebaseManager.TestGameValue.Equals(FirebaseManager.FirebaseConfig_TestGameDefault) ? 0 : 1;

        }

        private void Update()
        {
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                if (!AdsManager.Instance.adsLoaded)
                {
                    return;
                }
                AdsManager.Instance.Requestbanner();
                AdsManager.Instance.ShowBanner();
                AdsManager.Instance.adsLoaded = false;
            }
            if (Application.internetReachability != NetworkReachability.NotReachable || GamePopup.CurrentPopup is NoInternetPopup) 
            {
                return;
            }

            Time.timeScale = 0;
            UiManager.Instance.ShowPopup<NoInternetPopup>();

        }

        public void SpawnPlayer()
        {
            if (PlayerController.Instance != null)
            {
                DestroyImmediate(PlayerController.Instance.gameObject);
            }

            Instantiate(playerPrefabs[SaveLoadData.Data.IndexCharacterUse]);
        }

        public void InitializeNewGame()
        {
            decalResult.DOFade(0, 0);
            arrow.SetActive(false);

            player = PlayerController.Instance;
            currentLevel = SaveLoadData.Data.CurrentLevel;


            // create grid
            int gridSize = SaveLoadData.Data.GridSize;
            numberGridActive = gridSize * gridSize;

            Vector3 startLocalPosition = new Vector3(-gridSize / 2f + 0.5f, 0, -gridSize / 2f + 0.5f);
            Vector3 currentLocalPosition = Vector3.zero;
            int gridItemIndex = 0;

            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    if (gridItemIndex >= gridItemsSize)
                    {
                        gridItems.Add(Instantiate(gridItemTemplate, gridParent));
                        gridItemsSize++;
                    }

                    currentLocalPosition.x = startLocalPosition.x + i;
                    currentLocalPosition.z = startLocalPosition.z + j;

                    gridItems[gridItemIndex].InitializeNewGame(currentLocalPosition);
                    gridItemIndex++;
                }
            }

            // hide all grid item not used
            for (; gridItemIndex < gridItemsSize; gridItemIndex++)
            {
                gridItems[gridItemIndex].gameObject.SetActive(false);
            }

            environment.position = gridSize switch
            {
                3 => environmentPositions[0],
                4 => environmentPositions[1],
                _ => environmentPositions[2],
            };



            if (currentLevel != 0)
            {
                // clear bot
                numberBotActive = bots.Count;
                for (int i = 0; i < numberBotActive; i++)
                {
                    Destroy(bots[i].gameObject);
                }
                bots.Clear();

                // create bot
                spawnPoint.localEulerAngles = new Vector3(0, Random.Range(0, 360f), 0);
                indexSpawnPointNotUse = new List<int>() { 0, 1, 2, 3, 4 };

                numberBotActive = currentLevel switch
                {
                    1 => TestGameType == 0 ? 1 : 3,
                    2 => 3,
                    _ => currentLevel >= 6 ? Random.Range(minMaxBotPerLevel.x, minMaxBotPerLevel.y + 1) : 4,
                };

                for (int i = 0; i < numberBotActive; i++)
                {
                    bots.Add(Instantiate(botTemplates[Random.Range(0, botTemplates.Length)]));

                    int randomIndex = Random.Range(0, indexSpawnPointNotUse.Count);
                    bots[i].InitializeNewGame(spawnPoints[indexSpawnPointNotUse[randomIndex]].position);
                    bots[i].gameObject.SetActive(true);
                    bots[i].gameObject.name = $"Bot {i}";

                    indexSpawnPointNotUse.RemoveAt(randomIndex);
                }
            }




            CancelInvoke();
            State = GameplayState.Wait_To_Start;
        }

        public void StartNewLevel()
        {
            State = GameplayState.Playing;

            ClearAllGridItem();

            if (TestGameType == 0)
            {
                switch (currentLevel)
                {
                    case 0:
                        RandomDecal();
                        break;

                    case 1:
                        bots[0].RunToGridResult(gridItems[3].transform.position);
                        StartCoroutine(IECheckPushBot());
                        break;

                    default:
                        Invoke(nameof(RandomDecal), 3);
                        break;
                }
            }
            else
            {
                switch (currentLevel)
                {
                    case 0:
                        RandomDecal();
                        break;

                    default:
                        Invoke(nameof(RandomDecal), 3);
                        break;
                }
            }
        }

        public void ClearAllGridItem()
        {
            if (State != GameplayState.Playing || isGridCleared) return;

            isGridCleared = true;
            foreach (var item in gridItems)
            {
                item.ClearDecal(true);
            }

            decalResult.DOFade(0, 0.2f);
            arrow.SetActive(false);
        }

        private void RandomDecal()
        {
            if (State != GameplayState.Playing) return;

            if (numberBotActive <= 0 && currentLevel != 0)
            {
                PlayerWin();
                return;
            }

            bool isGridResult = false;
            indexOfGridItemResults.Clear();

            numberGridHaveDecalSameResult = currentLevel != 0 ? Random.Range(1, Mathf.Clamp(numberGridActive / 3 + 1, 1, 4)) : 1;
            decalIndexResult = currentLevel != 0 ? Random.Range(0, decalSprites.Length) : 2;
            Logger.Log($"RandomDecal: number result: {numberGridHaveDecalSameResult} - Index Result: {decalIndexResult}");

            int decalIndexRandom = 0;
            for (int i = 0; i < numberGridActive; i++)
            {
                if (i >= numberGridActive - numberGridHaveDecalSameResult)
                {
                    isGridResult = true;
                }
                else
                {
                    isGridResult = numberGridHaveDecalSameResult > 0 ? Random.Range(0, 2) == 0 : false;
                }

                if (isGridResult)
                {
                    gridItems[i].ChangeDecal(decalIndexResult, decalSprites[decalIndexResult]);
                    indexOfGridItemResults.Add(i);
                    numberGridHaveDecalSameResult--;
                }
                else
                {
                    do
                    {
                        decalIndexRandom = Random.Range(-1, decalSprites.Length);

                    } while (decalIndexRandom == decalIndexResult);

                    gridItems[i].ChangeDecal(decalIndexRandom, decalIndexRandom != -1 ? decalSprites[decalIndexRandom] : null);
                }
            }

            numberGridHaveDecalSameResult = indexOfGridItemResults.Count;
            isGridCleared = false;

            if (currentLevel != 0)
            {
                Invoke(nameof(ShowDecalResult), 3);
            }
            else
            {
                ShowDecalResult();
            }
        }

        private void ShowDecalResult()
        {
            if (State != GameplayState.Playing) return;

            if (numberBotActive <= 0 && currentLevel != 0)
            {
                PlayerWin();
                return;
            }

            decalResult.DOFade(1, 0.2f);
            decalResult.sprite = decalSprites[decalIndexResult];

            if (TestGameType == 0)
            {
                if (currentLevel != 0)
                {
                    GameplayUi.Instance.ShowCountdown(OnCountdownDone);

                    // bot run to result
                    for (int i = 0; i < numberBotActive; i++)
                    {
                        bots[i].RunToGridResult(gridItems[indexOfGridItemResults[Random.Range(0, numberGridHaveDecalSameResult)]].transform.position);
                    }
                }
                else
                {
                    for (int i = 0; i < numberGridHaveDecalSameResult; i++)
                    {
                        gridItems[indexOfGridItemResults[i]].ShowArrowTarget(decalIndexResult);
                    }

                    StartCoroutine(IECheckPlayerNearGridResult());
                }
            }
            else
            {
                if (currentLevel != 0)
                {
                    GameplayUi.Instance.ShowCountdown(OnCountdownDone);

                    if (currentLevel <= 4)
                    {
                        if (currentLevel == 1 || currentLevel == 2)
                        {
                            for (int i = 0; i < numberGridHaveDecalSameResult; i++)
                            {
                                gridItems[indexOfGridItemResults[i]].ShowArrowTarget(decalIndexResult);
                            }

                            arrow.SetActive(true);
                        }
                    }
                    else
                    {
                        // bot run to result
                        for (int i = 0; i < numberBotActive; i++)
                        {
                            bots[i].RunToGridResult(gridItems[indexOfGridItemResults[Random.Range(0, numberGridHaveDecalSameResult)]].transform.position);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < numberGridHaveDecalSameResult; i++)
                    {
                        gridItems[indexOfGridItemResults[i]].ShowArrowTarget(decalIndexResult);
                    }

                    arrow.SetActive(true);
                    StartCoroutine(IECheckPlayerNearGridResult());
                }
            }
        }

        private IEnumerator IECheckPlayerNearGridResult()
        {
            while (true)
            {
                for (int i = 0; i < numberGridHaveDecalSameResult; i++)
                {
                    if (Vector3.Distance(player.transform.position, gridItems[indexOfGridItemResults[i]].transform.position) <= 0.25f)
                    {
                        PlayerWin();
                        yield break;
                    }
                }

                yield return null;
            }
        }

        private IEnumerator IECheckPushBot()
        {
            while (true)
            {
                if (numberBotActive <= 0)
                {
                    PlayerWin();
                    yield break;
                }

                yield return null;
            }
        }

        private void OnCountdownDone()
        {
            if (State != GameplayState.Playing) return;

            Logger.Log("OnCountdownDone - Move ground down");

            numberGridItemMoveDown = 0;
            for (int i = 0; i < numberGridActive; i++)
            {
                gridItems[i].MoveDown(decalIndexResult, ref numberGridItemMoveDown);
            }
        }

        public void GridItemMoveUpDone()
        {
            if (State != GameplayState.Playing) return;

            numberGridItemMoveDown--;

            if (numberGridItemMoveDown <= 0)
            {
                if (numberBotActive > 0)
                {
                    Invoke(nameof(RandomDecal), 2);

                    // bot run free
                    for (int i = 0; i < numberBotActive; i++)
                    {
                        bots[i].StartRunToTargetPoint();
                    }
                }
                else
                {
                    if (currentLevel != 0)
                        PlayerWin();
                }
            }
        }

        private void PlayerWin()
        {
            Logger.Log("----- WINNER -----");

            State = GameplayState.Victory;
            CancelInvoke();

            GameplayUi.Instance.ShowVictory();
            EventDispatcherExtension.PostEvent(EventID.Victory);

#if !UNITY_EDITOR
            FirebaseManager.LogEvent($"win_{currentLevel}");
#endif
        }

        private void OnPlayerDie(object obj)
        {
            State = GameplayState.Game_Over;
            CancelInvoke();
        }

        public Vector3 GetRandomGridItemActive()
        {
            return gridItems[Random.Range(0, numberGridActive)].transform.position;
        }

        public void HaveBotKilled()
        {
            numberBotActive--;
            bots.Sort((a, b) => a.IsDied.CompareTo(b.IsDied));
        }
    }
}
