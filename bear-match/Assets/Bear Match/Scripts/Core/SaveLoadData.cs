﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BearMatch
{
    [Serializable]
    public class SaveLoadData
    {
        public const int LatestDataVersion = 6;
        public int DataVersion = 0;


        /// //////////////// new data
        #region Data Version 0
        public int AppOpenTimes = 0;

        /// <summary>
        /// Current Level start at 0
        /// </summary>
        public int CurrentLevel = 0;

        public int GridSize = 3;

        public int Coin;
        public bool IsBoughtNoAds = false;

        public int IndexCharacterUse = 0;
        public bool[] IndexCharacterUnlocked;
        public bool[] IndexCharacterNew;
        public int IndexCharacterNeedUnlock = 1;

        public int NumberAdsShow = 0;
        public int NumberAdsClick = 0;

        public bool IsRated = false;
        #endregion


        public SaveLoadData()
        {
            DataVersion = LatestDataVersion;
            
            AppOpenTimes = 0;

            CurrentLevel = 0;
            GridSize = 3;

            Coin = 0;
            IsBoughtNoAds = false;

            IndexCharacterUse = 0;

            IndexCharacterNew = new bool[7];
            IndexCharacterUnlocked = new bool[7];
            for (int i = 0; i < 7; i++)
            {
                IndexCharacterUnlocked[i] = i == 0;
            }

            IndexCharacterNeedUnlock = UnityEngine.Random.Range(1, 7);

            NumberAdsShow = 0;
            NumberAdsClick = 0;

            IsRated = false;
        }

        private static string Md5Sum(string strToEncrypt)
        {
            System.Text.UTF8Encoding ue = new();
            byte[] bytes = ue.GetBytes(strToEncrypt);

            // encrypt bytes
            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new();
            byte[] hashBytes = md5.ComputeHash(bytes);

            // Convert the encrypted bytes back to a string (base 16)
            string hashString = "";
            Array.ForEach(hashBytes, (hash) => hashString += Convert.ToString(hash, 16).PadLeft(2, '0'));

            return hashString.PadLeft(32, '0');
        }

        private static void UpdateDataToLatestVersion()
        {
            Logger.Log("Update Data To Latest Version");
            for (int i = Data.DataVersion + 1; i <= LatestDataVersion; i++)
            {
                switch (i)
                {
                    case LatestDataVersion:
                        break;
                }
            }

            Data.DataVersion = LatestDataVersion;
            SaveData();
        }

        #region Static Method
        public static SaveLoadData Data { get; set; }

        public static void LoadData()
        {
            string str = PlayerPrefs.GetString(Constants.USER_DATA_SAVE_NAME);

            if (string.IsNullOrEmpty(str) || string.IsNullOrWhiteSpace(str))
            {
                Data = new SaveLoadData();
                SaveData();
            }
            else
            {
                byte[] decodedStringToByte = Convert.FromBase64String(str);
                string jsonData = System.Text.Encoding.UTF8.GetString(decodedStringToByte);
                Data = JsonUtility.FromJson<SaveLoadData>(jsonData);

                // check hash to detect hack
                string hash = PlayerPrefs.GetString(Constants.USER_DATA_SAVE_HASH);
                string hash_check = Md5Sum(jsonData);
                if (!hash.Equals(hash_check))
                {
                    Logger.LogError("-------------------------------- Phat hien nghi van hack. Tao data moi. --------------------------------");
                    Data = new SaveLoadData();
                    SaveData();
                }
                else
                {
                    if (Data.DataVersion != LatestDataVersion)
                    {
                        UpdateDataToLatestVersion();
                    }
                }
            }
        }

        public static void SaveData()
        {
            string jsonData = JsonUtility.ToJson(Data);
            //Logger.Log(jsonData);

            byte[] toByteEncode = System.Text.Encoding.UTF8.GetBytes(jsonData);
            string encodedText = Convert.ToBase64String(toByteEncode);

            PlayerPrefs.SetString(Constants.USER_DATA_SAVE_HASH, Md5Sum(jsonData));
            PlayerPrefs.SetString(Constants.USER_DATA_SAVE_NAME, encodedText);
            PlayerPrefs.Save();
        }
        #endregion



        public static void AddCoin(int coinAdd)
        {
            Debug.Log($"You Got {coinAdd} coin!");

            Data.Coin += coinAdd;
            SaveData();

            EventDispatcherExtension.PostEvent(EventID.Coin_Changed);
        }

        public static void NextLevel()
        {
            Data.CurrentLevel++;
            Data.GridSize = Data.CurrentLevel >= 6 ? UnityEngine.Random.Range(3, 6) : 3;

            if (Data.CurrentLevel % 5 == 0)
            {
                RandomIndexCharacterNeedUnlock();
            }

            SaveData();
        }

        public static void RandomIndexCharacterNeedUnlock()
        {
            List<int> indexCharacterLocked = new();

            for (int i = 1; i < 7; i++)
            {
                if (!Data.IndexCharacterUnlocked[i])
                {
                    indexCharacterLocked.Add(i);
                }
            }

            if (indexCharacterLocked.Count > 0)
            {
                Data.IndexCharacterNeedUnlock = indexCharacterLocked[UnityEngine.Random.Range(0, indexCharacterLocked.Count)];
            }
            else
            {
                Data.IndexCharacterNeedUnlock = -1;
            }
        }
    }
}
