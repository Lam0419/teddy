using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace BearMatch
{
    public class Loading : MonoBehaviour
    {
        [SerializeField] private Slider loading;

        private IEnumerator Start()
        {
            float t = Time.time;
            float loadingTime = 5;

            while (Time.time - t < loadingTime)
            {
                loading.value = (Time.time - t) / loadingTime;
                yield return null;
            }

            SceneManager.LoadSceneAsync(1);
        }
    }
}
