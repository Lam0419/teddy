using UnityEngine;

namespace BearMatch
{
    public class PushBackBot : MonoBehaviour
    {
        [SerializeField] private float pushBackForce = 3;
        [SerializeField] private float pushBackAngle = 75;

        private void OnCollisionEnter(Collision collision)
        {
            if (!enabled) return;

            switch (collision.collider.tag)
            {
                case Constants.BOT_TAG:
                    if (collision.rigidbody != null && collision.rigidbody.TryGetComponent(out BotController bot) && bot)
                    {
                        bot.PushBack(pushBackForce, pushBackAngle);
                    }
                    break;
            }
        }
    }
}
