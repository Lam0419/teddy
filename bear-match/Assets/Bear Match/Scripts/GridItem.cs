using UnityEngine;
using DG.Tweening;

namespace BearMatch
{
    public class GridItem : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        [SerializeField] private SpriteRenderer decal;
        [SerializeField] private float fadeTime = 0.14f;
        [SerializeField] private GameObject arrowTarget;

        private int decalIndex = 0;

        public void InitializeNewGame(Vector3 currentLocalPosition)
        {
            gameObject.SetActive(true);
            animator.Play("Idle", -1, 0);
            transform.localPosition = currentLocalPosition;
            ClearDecal(false);
        }

        public void ClearDecal(bool isFadeOut)
        {
            decalIndex = -1;
            decal.DOFade(0, isFadeOut ? fadeTime : 0);
            arrowTarget.SetActive(false);
        }

        public void ChangeDecal(int decalIndexSetup, Sprite decalSprite)
        {
            decalIndex = decalIndexSetup;

            if (decalIndex != -1)
            {
                decal.DOFade(1, fadeTime);
                decal.sprite = decalSprite;
            }
        }

        public void MoveDown(int decalIndexResult, ref int numberGridItemMoveDown)
        {
            if (decalIndex == decalIndexResult) return;

            animator.Play("Grid Item Move Down", -1, 0);
            numberGridItemMoveDown++;
        }

        public void ShowArrowTarget(int decalIndexResult)
        {
            if (decalIndex != decalIndexResult) return;

            arrowTarget.SetActive(true);
        }

        // call by animator
        public void ClearDecalForNextTurn()
        {
            GameplayController.Instance.ClearAllGridItem();
        }

        // call by animator
        public void GridItemMoveUpDone()
        {
            GameplayController.Instance.GridItemMoveUpDone();
        }
    }
}
