﻿using System.Collections;
using UnityEngine;

namespace BearMatch
{
    public class PlayerController : MonoBehaviour
    {
        [Header("Move")]
        [SerializeField] private Vector3 spawnPosition;
        [SerializeField] private Rigidbody body;
        [SerializeField] private CapsuleCollider moveCollider;
        [SerializeField] private CapsuleCollider blockPlayerCollider;
        [SerializeField] private float moveSpeed;
        [SerializeField] private GameObject smoke;
        [SerializeField] private GameObject bloodFx;

        [Header("Ground Check")]
        [SerializeField] private LayerMask groundLayer;
        [SerializeField] private float checkRadius;

        [Header("Animation")]
        [SerializeField] private Animator animator;
        [SerializeField] private Rigidbody[] ragdollBodies;
        [SerializeField] private PushBackBot pushBackBot;


        public static PlayerController Instance { get; private set; }
        public Transform Center => moveCollider.transform;

        private const string AnimationParameter_Move = "Move";
        private const string AnimationParameter_Victory = "Victory";


        private PhysicMaterial slippyPhysics;

        private Vector3 inputVelocity;
        private Vector3 currentVelocity;
        private bool isMoving;

        private bool isDied = false;

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            Physics.IgnoreCollision(moveCollider, blockPlayerCollider, true);

            slippyPhysics = new PhysicMaterial
            {
                name = "slippyPhysics",
                staticFriction = 0f,
                dynamicFriction = 0f,
                frictionCombine = PhysicMaterialCombine.Minimum
            };

            moveCollider.material = slippyPhysics;
            blockPlayerCollider.material = slippyPhysics;

            EventDispatcherExtension.AddListener(EventID.Victory, OnPlayerVictory);

            smoke.SetActive(false);
            bloodFx.SetActive(false);

            pushBackBot.enabled = false;
        }

        private void OnDestroy()
        {
            EventDispatcherExtension.RemoveListener(EventID.Victory, OnPlayerVictory);
        }

        private void OnValidate()
        {
            ragdollBodies = animator.GetComponentsInChildren<Rigidbody>();
        }

        public void InitializeNewGame()
        {
            isDied = false;

            isMoving = false;
            pushBackBot.enabled = false;

            inputVelocity = Vector3.zero;
            currentVelocity = Vector3.zero;

            ToggleRagdoll(true);

            animator.SetBool(AnimationParameter_Move, false);

            transform.position = spawnPosition;
            transform.forward = Vector3.forward;

            StartCoroutine(IECheckGroundAndLockBody());
        }

        private IEnumerator IECheckGroundAndLockBody()
        {
            while (!Physics.CheckSphere(transform.position, checkRadius, groundLayer, QueryTriggerInteraction.Ignore))
            {
                yield return null;
            }

            smoke.SetActive(false);
            smoke.SetActive(true);
            body.isKinematic = true;
        }

        public void StartNewLevel()
        {
            CancelInvoke();
            body.isKinematic = false;
        }

        private void Update()
        {
            if (isDied || GameplayController.State != GameplayState.Playing) return;

            if (isMoving)
            {
                currentVelocity = inputVelocity;
                currentVelocity.y = body.velocity.y;
                body.velocity = currentVelocity;
            }

            transform.forward = Vector3.Lerp(transform.forward, inputVelocity, 0.1f);
        }

        public void StartMovePlayer()
        {
            isMoving = true;
            animator.SetBool(AnimationParameter_Move, isMoving);

            pushBackBot.enabled = true;
        }

        public void UpdatePlayerVelocity(Vector2 joystickDirection)
        {
            if (isDied || joystickDirection == Vector2.zero) return;

            joystickDirection = joystickDirection.normalized;

            inputVelocity.x = joystickDirection.x * moveSpeed;
            inputVelocity.z = joystickDirection.y * moveSpeed;
        }

        public void EndMovePlayer()
        {
            isMoving = false;
            animator.SetBool(AnimationParameter_Move, isMoving);

            pushBackBot.enabled = false;

            currentVelocity = Vector3.zero;
            currentVelocity.y = body.velocity.y;
            body.velocity = currentVelocity;
        }

        public void TriggerUnderground()
        {
            if (isDied) return;

            Logger.Log($"Kill {gameObject.name}");

            isDied = true;
            ToggleRagdoll(false);
            SoundManager.Instance.Play_HitBot();

            bloodFx.SetActive(false);
            bloodFx.SetActive(true);

            EventDispatcherExtension.PostEvent(EventID.Player_Die);

#if !UNITY_EDITOR
            FirebaseManager.LogEvent($"lose_{SaveLoadData.Data.CurrentLevel}");
#endif
        }

        private void ToggleRagdoll(bool isDisableRagdoll)
        {
            moveCollider.enabled = isDisableRagdoll;
            blockPlayerCollider.enabled = isDisableRagdoll;

            body.isKinematic = !isDisableRagdoll;
            body.velocity = Vector3.zero;

            foreach (Rigidbody ragdollBone in ragdollBodies)
            {
                ragdollBone.isKinematic = isDisableRagdoll;
            }

            animator.enabled = isDisableRagdoll;
            if (isDisableRagdoll)
            {
                animator.Play("idle", -1, 0);
            }
        }

        private void OnPlayerVictory(object obj)
        {
            body.velocity = Vector3.zero;
            transform.forward = Vector3.back;

            animator.SetTrigger(AnimationParameter_Victory);
        }
    }
}
