﻿#define ENABLE_LOGS

using System.Diagnostics;

namespace BearMatch
{
    public static class Logger
    {
        [Conditional("ENABLE_LOGS")]
        public static void Log(string logMsg)
        {
            UnityEngine.Debug.Log(logMsg);
        }

        public static void LogWarning(string logMsg)
        {
            UnityEngine.Debug.LogWarning(logMsg);
        }

        public static void LogError(string logMsg)
        {
            UnityEngine.Debug.LogError(logMsg);
        }
    }
}
