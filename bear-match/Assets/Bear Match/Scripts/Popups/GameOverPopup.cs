using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace BearMatch
{
    public class GameOverPopup : GamePopup
    {
        [SerializeField] private TextMeshProUGUI levelText;

        public override void Show(GamePopup popup, bool isHideLastPopup = true, object data = null, UnityAction closeCallback = null, bool isShowSuperImage = true)
        {
            base.Show(popup, isHideLastPopup, data, closeCallback, isShowSuperImage: false);

            SoundManager.Instance.Play_GameOver();

            levelText.text = $"Level {SaveLoadData.Data.CurrentLevel + 1:00}";
            gameObject.SetActive(true);
        }

        public void Click_Replay()
        {
            SoundManager.Instance.Play_ClickButton();

            Hide();
            GameplayUi.Instance.ReplayLevel();
        }
    }
}
