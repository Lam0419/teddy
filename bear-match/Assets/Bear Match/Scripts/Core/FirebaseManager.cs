﻿#if !UNITY_EDITOR
using Firebase;
using Firebase.Analytics;
using Firebase.RemoteConfig;
using System;
using System.Collections.Generic;
#endif
using Firebase;
using Firebase.Analytics;
using Firebase.RemoteConfig;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BearMatch
{
    public class FirebaseManager : MonoBehaviour
    {
        public const string FirebaseConfig_TestGameKey = "test_game";
        public const string FirebaseConfig_TestGameDefault = "a";
        public static string TestGameValue { get; private set; } = FirebaseConfig_TestGameDefault;
        public static int countReplay;


        public static FirebaseManager Instance { get; private set; }

        public static bool IsFirebaseReady { get; private set; }



        private void Awake()
        {
            Logger.Log("Awake Firebase Manager");
            Instance = this;
        }

#if !UNITY_EDITOR
        private void Start()
        {
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                var dependencyStatus = task.Result;
                if (dependencyStatus == DependencyStatus.Available)
                {
                    InitializeFirebaseDone();
                }
                else
                {
                    // Firebase Unity SDK is not safe to use here.
                    Debug.LogError($"Could not resolve all Firebase dependencies: {dependencyStatus}");
                }
            });

        }

        private void InitializeFirebaseDone()
        {
            Logger.Log("Firebase is ready");
            FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventAppOpen);

            IsFirebaseReady = true;

            // init remote config
            Dictionary<string, object> defaults = new()
            {
                { FirebaseConfig_TestGameKey, FirebaseConfig_TestGameDefault }
            };
            FirebaseRemoteConfig.DefaultInstance.SetDefaultsAsync(defaults);

            FirebaseRemoteConfig.DefaultInstance.FetchAsync(TimeSpan.Zero).ContinueWith(fetchTask =>
            {
                if (fetchTask.IsCanceled)
                {
                    Logger.Log("Fetch canceled.");
                }
                else if (fetchTask.IsFaulted)
                {
                    Logger.Log("Fetch encountered an error.");
                }
                else if (fetchTask.IsCompleted)
                {
                    Logger.Log("FirebaseController Fetch completed successfully!");
                }

                var info = FirebaseRemoteConfig.DefaultInstance.Info;
                switch (info.LastFetchStatus)
                {
                    case LastFetchStatus.Success:
                        FirebaseRemoteConfig.DefaultInstance.ActivateAsync();
                        Logger.Log($"FirebaseController Remote data loaded and ready (last fetch time {info.FetchTime}.");
                        FetchFirebaseConfigDone();
                        break;

                    case LastFetchStatus.Failure:
                        switch (info.LastFetchFailureReason)
                        {
                            case FetchFailureReason.Error:
                                Logger.Log("FirebaseController Fetch failed for unknown reason");
                                break;
                            case FetchFailureReason.Throttled:
                                Logger.Log("FirebaseController Fetch throttled until " + info.ThrottledEndTime);
                                break;
                        }
                        break;

                    case LastFetchStatus.Pending:
                        Logger.Log("FirebaseController Latest Fetch call still pending.");
                        break;
                }
            });

        }

        private void FetchFirebaseConfigDone()
        {
            Logger.Log("Fetch Firebase Config Done");

            TestGameValue = FirebaseRemoteConfig.DefaultInstance.GetValue(FirebaseConfig_TestGameKey).StringValue;
            var countReplayAds = FirebaseRemoteConfig.DefaultInstance.GetValue("count_replay").LongValue;
            countReplay = (int)countReplayAds;
            Logger.Log($"TestGameValue: {TestGameValue}");
        }

        public static void LogEvent(string name)
        {
            if (IsFirebaseReady)
                FirebaseAnalytics.LogEvent(name);
        }
#endif
    }
}
