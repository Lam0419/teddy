using System;
using UnityEngine;
using UnityEngine.Events;

namespace BearMatch
{
    public class StartGamePopup : GamePopup
    {
        [SerializeField] private GameObject buyAdsButton;

        public override void Show(GamePopup popup, bool isHideLastPopup = true, object data = null, UnityAction closeCallback = null, bool isShowSuperImage = true)
        {
            base.Show(popup, isHideLastPopup, data, closeCallback, isShowSuperImage: false);

            buyAdsButton.SetActive(!SaveLoadData.Data.IsBoughtNoAds);
        }

        public void Click_BuyNoAds()
        {
            SoundManager.Instance.Play_ClickButton();

            buyAdsButton.SetActive(false);
            StoreManager.Instance.BuyItem(StoreManager.product_no_ad, OnBuyNoAdsCallback);
        }

        private void OnBuyNoAdsCallback(bool isSuccess, string productId)
        {
            if (!productId.Equals(StoreManager.product_no_ad, StringComparison.Ordinal)) return;

            buyAdsButton.SetActive(!isSuccess);
        }

        public void Click_Shop()
        {
            SoundManager.Instance.Play_ClickButton();

            UiManager.Instance.ShowPanel<ShopPopup>();
        }

        public void Click_StartGame()
        {
            SoundManager.Instance.Play_ClickButton();

            if (SaveLoadData.Data.CurrentLevel >= 100)
            {
                UiManager.Instance.ShowPopup<WarningPopup>(isHideLastPopup: false);
            }
            else
            {
                Hide();
                GameplayUi.Instance.StartGame();
            }
        }
    }
}
