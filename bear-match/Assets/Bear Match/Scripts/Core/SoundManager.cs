﻿using UnityEngine;

namespace BearMatch
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private AudioSource soundEffect;
        [SerializeField] private AudioClip clickButton;
        [SerializeField] private AudioClip firework;
        [SerializeField] private AudioClip getCoin;
        [SerializeField] private AudioClip hitBot;
        [SerializeField] private AudioClip gameOver;
        [SerializeField] private AudioClip victory;

        public static SoundManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance != null)
            {
                return;
            }

            Debug.unityLogger.Log("Awake Sound Manager");
            Instance = this;
        }

        public void Play_ClickButton(float volume = 1)
        {
            soundEffect.PlayOneShot(clickButton, volume);
        }

        public void Play_Firework(float volume = 1)
        {
            soundEffect.PlayOneShot(firework, volume);
        }

        public void Play_GetCoin(float volume = 1)
        {
            soundEffect.PlayOneShot(getCoin, volume);
        }

        public void Play_HitBot(float volume = 1)
        {
            soundEffect.PlayOneShot(hitBot, volume);
        }

        public void Play_GameOver(float volume = 1)
        {
            soundEffect.PlayOneShot(gameOver, volume);
        }

        public void Play_Victory(float volume = 1)
        {
            soundEffect.PlayOneShot(victory, volume);
        }
    }
}
