﻿using UnityEngine;

namespace BearMatch
{
    public class AutoRotation : MonoBehaviour
    {
        [SerializeField] private float rotationSpeed = 5;

        private void Update()
        {
            transform.Rotate(0, 0, Time.deltaTime * rotationSpeed);
        }
    }
}
