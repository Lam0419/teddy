﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BearMatch
{
    public class GamePopup : MonoBehaviour
    {
        public static int NumberPopupShow => currentPopups.Count;
        public static bool IsHavePopupOpen => currentPopups.Count != 0;

        public RectTransform RectTransform
        {
            get
            {
                if (rectTransform == null)
                    rectTransform = GetComponent<RectTransform>();

                return rectTransform;
            }
        }

        public static GamePopup CurrentPopup => currentPopups.Count != 0 ? currentPopups[0] : null;

        [SerializeField] protected Vector4 Margin = new(10, 10, 10, 20);
        [SerializeField] protected Animator animatorController;

        private static List<GamePopup> currentPopups = new();

        protected RectTransform rectTransform;
        protected UnityAction closeCallback;
        protected bool isShowSuperImage;
        protected bool isHideLastPopup;

        private string animationShow = "Gamepopup Show";

        #region Virtual Method
        public virtual void Show(GamePopup popup, bool isHideLastPopup = true, object data = null, UnityAction closeCallback = null, bool isShowSuperImage = true)
        {
            if (currentPopups.Count != 0)
            {
                if (currentPopups[0] == null)
                {
                    currentPopups.RemoveAt(0);
                }
                else
                {
                    currentPopups[0].gameObject.SetActive(!isHideLastPopup);
                }
            }

            if (!isHideLastPopup)
            {
                transform.SetAsLastSibling();
            }

            if (popup != null)
            {
                popup.gameObject.SetActive(true);
                popup.isShowSuperImage = isShowSuperImage;
                popup.isHideLastPopup = isHideLastPopup;

                if (popup.animatorController != null)
                    popup.animatorController.Play(animationShow, -1, 0);

                if (isShowSuperImage)
                {
                    UiManager.Instance.ShowSuperImage(popup.RectTransform, popup.Margin);
                }
                else if (isHideLastPopup)
                {
                    UiManager.Instance.HideSuperImage();
                }

                currentPopups.Insert(0, popup);
            }

            this.closeCallback = closeCallback;
        }

        public virtual void Hide(bool isStopCurrentSound = true)
        {
            bool isPlayAnimShow = true;

            if (currentPopups.Count != 0)
            {
                isPlayAnimShow = currentPopups[0].isHideLastPopup;
                currentPopups[0].gameObject.SetActive(false);
                UiManager.Instance.HideSuperImage();

                if (currentPopups[0] == this)
                {
                    currentPopups.RemoveAt(0);
                    closeCallback?.Invoke();
                }
                else
                {
                    currentPopups.RemoveAt(0);
                }
            }

            if (currentPopups.Count != 0)
            {
                currentPopups[0].gameObject.SetActive(true);

                if (isPlayAnimShow && currentPopups[0].animatorController != null)
                    currentPopups[0].animatorController.Play(animationShow, -1, 0);

                if (currentPopups[0].isShowSuperImage)
                {
                    UiManager.Instance.SuperImage.transform.SetAsLastSibling();
                    UiManager.Instance.ShowSuperImage(currentPopups[0].RectTransform, currentPopups[0].Margin);
                    currentPopups[0].transform.SetAsLastSibling();
                    UiManager.Instance.ShowCoin();
                }
            }
        }

        public virtual void OnEscapeKeyClicked()
        {
            //if (currentPopups.Count == 1 && currentPopups[0].animatorController != null)
            //{
            //    currentPopups[0].animatorController.SetBool("Close", true);
            //}
            //else
            //{
            //    Hide(true);
            //}
        }
        #endregion

        public void Click_DefaultClosePopup()
        {
            SoundManager.Instance.Play_ClickButton();

            if (currentPopups[0].animatorController != null)
            {
                currentPopups[0].animatorController.SetBool("Close", true);
            }
            else
            {
                Hide(true);
            }
        }

        public void AnimationCall_Hide()
        {
            if (currentPopups.Count > 0 && currentPopups[0].animatorController != null)
            {
                currentPopups[0].animatorController.SetBool("Close", false);
            }
            Hide(true);
        }

        public static void ClearAllPopup()
        {
            foreach (var popup in currentPopups)
            {
                if (popup != null)
                {
                    popup.gameObject.SetActive(false);
                }
            }
            currentPopups.Clear();
        }
    }
}
