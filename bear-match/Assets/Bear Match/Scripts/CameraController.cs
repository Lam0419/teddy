using UnityEngine;
using DG.Tweening;

namespace BearMatch
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private Camera mainCamera;
        [SerializeField] private float mainCameraAspectDefault = 720f / 1280;

        [SerializeField] private float fieldOfViewStartDefault = 40;
        [SerializeField] private float fieldOfViewPlayDefault = 80;
        [SerializeField] private float fieldOfViewVictoryDefault = 55;
        [SerializeField] private float zoomTime = 0.3f;
        //[SerializeField] private float smoothTime = 0.3f;

        [SerializeField] private Vector3 startPosition = new(0, 5, 0);
        [SerializeField] private GameObject firework;


        public static CameraController Instance { get; private set; }

        private float fieldOfViewStart = 35;
        private float fieldOfViewPlay = 60;
        private float fieldOfViewVictory = 60;

        private Transform targetTransform;

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            fieldOfViewStart = Mathf.Atan(mainCameraAspectDefault * Mathf.Tan(fieldOfViewStartDefault / 2 * Mathf.Deg2Rad) / mainCamera.aspect) * Mathf.Rad2Deg * 2;
            fieldOfViewPlay = Mathf.Atan(mainCameraAspectDefault * Mathf.Tan(fieldOfViewPlayDefault / 2 * Mathf.Deg2Rad) / mainCamera.aspect) * Mathf.Rad2Deg * 2;
            fieldOfViewVictory = Mathf.Atan(mainCameraAspectDefault * Mathf.Tan(fieldOfViewVictoryDefault / 2 * Mathf.Deg2Rad) / mainCamera.aspect) * Mathf.Rad2Deg * 2;

            mainCamera.fieldOfView = fieldOfViewStart;

            firework.SetActive(false);
            EventDispatcherExtension.AddListener(EventID.Victory, OnPlayerVictory);
        }

        private void OnDestroy()
        {
            EventDispatcherExtension.RemoveListener(EventID.Victory, OnPlayerVictory);
        }

        public void InitializeNewGame()
        {
            transform.position = startPosition;
            mainCamera.fieldOfView = fieldOfViewStart;

            targetTransform = PlayerController.Instance.Center;
        }

        public void StartNewLevel()
        {
            mainCamera.DOFieldOfView(fieldOfViewPlay, zoomTime);
        }

        private void LateUpdate()
        {
            if (GameplayController.State != GameplayState.Playing) return;

            transform.position = targetTransform.position;
        }

        private void OnPlayerVictory(object obj)
        {
            firework.SetActive(false);
            firework.SetActive(true);
            mainCamera.DOFieldOfView(fieldOfViewVictory, zoomTime);
        }
    }
}
