using UnityEngine.Events;
using UnityEngine;

namespace BearMatch
{
    public class NewSkinPopup : GamePopup
    {
        [SerializeField] private GameObject watchVideoButton;
        [SerializeField] private GameObject noThankText;
        [SerializeField] private GameObject[] characters;

        private int indexCharacterNeedUnlock;

        public override void Show(GamePopup popup, bool isHideLastPopup = true, object data = null, UnityAction closeCallback = null, bool isShowSuperImage = true)
        {
            base.Show(popup, isHideLastPopup, data, closeCallback, isShowSuperImage);

            watchVideoButton.SetActive(true);
            noThankText.SetActive(false);
            Invoke(nameof(ShowNoThankText), 3);

            indexCharacterNeedUnlock = SaveLoadData.Data.IndexCharacterNeedUnlock;

            for (int i = 0; i < characters.Length; i++)
            {
                characters[i].SetActive(i == indexCharacterNeedUnlock - 1);
            }
        }

        private void ShowNoThankText()
        {
            noThankText.SetActive(true);
        }

        public void Click_Claim()
        {
            SoundManager.Instance.Play_ClickButton();

            watchVideoButton.SetActive(false);
            AdsManager.Instance.ShowRewardVideo(WatchVideoDone, WatchVideoFail, WatchVideoFail);
        }

        private void WatchVideoDone()
        {
            SaveLoadData.Data.IndexCharacterUnlocked[indexCharacterNeedUnlock] = true;
            SaveLoadData.RandomIndexCharacterNeedUnlock();
            SaveLoadData.SaveData();

            Hide();
            UiManager.Instance.ShowPopup<WarningPopup>(data: Constants.NewSkin);
        }

        private void WatchVideoFail()
        {
            watchVideoButton.SetActive(true);
            UiManager.Instance.ShowPopup<WarningPopup>(data: Constants.ErrorWifi, isHideLastPopup: false);
        }
    }
}
