using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BearMatch
{
    public class ShopPopup : GamePopup
    {
        [SerializeField] private Image characterIcon;
        [SerializeField] private Sprite[] characterSprites;
        [SerializeField] private ShopItem[] shopItems;

        [SerializeField] private GameObject watchVideoButton;
        [SerializeField] private GameObject unlockRandomButton;
        [SerializeField] private float randomSkinTime = 2;
        [SerializeField] private float swapSkinTime = 0.1f;

        private List<int> indexCharacterLockeds = new();
        private int lastIndexCharacterUse;

        private void Start()
        {
            characterIcon.sprite = characterSprites[SaveLoadData.Data.IndexCharacterUse];

            for (int i = 0; i < shopItems.Length; i++)
            {
                shopItems[i].Initialized(this, i);
            }

            for (int i = 0; i < 7; i++)
            {
                if (!SaveLoadData.Data.IndexCharacterUnlocked[i])
                {
                    indexCharacterLockeds.Add(i);
                }
            }

            unlockRandomButton.SetActive(indexCharacterLockeds.Count != 0);
        }

        public override void Show(GamePopup popup, bool isHideLastPopup = true, object data = null, UnityAction closeCallback = null, bool isShowSuperImage = true)
        {
            base.Show(popup, isHideLastPopup, data, closeCallback, isShowSuperImage);

            foreach (var item in shopItems)
            {
                item.ResetItem();
            }

            lastIndexCharacterUse = SaveLoadData.Data.IndexCharacterUse;
        }

        public override void Hide(bool isStopCurrentSound = true)
        {
            base.Hide(isStopCurrentSound);

            if (lastIndexCharacterUse != SaveLoadData.Data.IndexCharacterUse)
            {
                GameplayController.Instance.SpawnPlayer();

                GameplayUi.Instance.InitializeNewGame();
            }
        }

        public void Click_Reset()
        {
            SoundManager.Instance.Play_ClickButton();

            SaveLoadData.Data.IndexCharacterUse = 0;
            characterIcon.sprite = characterSprites[SaveLoadData.Data.IndexCharacterUse];

            foreach (var item in shopItems)
            {
                item.ResetItem();
            }

            SaveLoadData.SaveData();
        }

        public void Click_Tab(int indexTabClicked)
        {
            SoundManager.Instance.Play_ClickButton();

            if (indexTabClicked > 0)
            {
                UiManager.Instance.ShowPopup<WarningPopup>(isHideLastPopup: false);
            }
        }

        public void Click_WatchVideoGetCoin()
        {
            SoundManager.Instance.Play_ClickButton();

            watchVideoButton.SetActive(false);
            AdsManager.Instance.ShowRewardVideo(WatchVideoDone, WatchVideoFail, WatchVideoFail);
        }

        private void WatchVideoDone()
        {
            watchVideoButton.SetActive(true);
            SaveLoadData.AddCoin(200);
        }

        private void WatchVideoFail()
        {
            watchVideoButton.SetActive(true);
            UiManager.Instance.ShowPopup<WarningPopup>(data: Constants.ErrorWifi, isHideLastPopup: false);
        }

        public void Click_BuyRandom()
        {
            SoundManager.Instance.Play_ClickButton();

            if (SaveLoadData.Data.Coin < 999)
            {
                UiManager.Instance.ShowPopup<WarningPopup>(data: Constants.NotEnoughCoin, isHideLastPopup: false);
            }
            else
            {
                SaveLoadData.AddCoin(-999);
                unlockRandomButton.SetActive(false);

                StartCoroutine(IEBuyRandom());
            }
        }

        private IEnumerator IEBuyRandom()
        {
            int count = indexCharacterLockeds.Count;

            if (count == 1)
            {
                UnlockItem(indexCharacterLockeds[0]);
            }
            else
            {
                for (int i = 0; i < count; i++)
                {
                    int index = UnityEngine.Random.Range(i, count);
                    int swap = indexCharacterLockeds[i];
                    indexCharacterLockeds[i] = indexCharacterLockeds[index];
                    indexCharacterLockeds[index] = swap;
                }

                WaitForSeconds waitForSeconds = new WaitForSeconds(swapSkinTime);
                for (int i = 0; i < randomSkinTime; i++)
                {
                    for (int j = 0; j < count; j++)
                    {
                        if (j > 0)
                            shopItems[indexCharacterLockeds[j - 1] - 1].ShowHighlight(false);

                        shopItems[indexCharacterLockeds[j] - 1].ShowHighlight(true);

                        yield return waitForSeconds;
                    }

                    shopItems[indexCharacterLockeds[count - 1] - 1].ShowHighlight(false);
                }

                UnlockItem(indexCharacterLockeds[count - 1]);
            }

            unlockRandomButton.SetActive(indexCharacterLockeds.Count != 0);
        }

        private void UnlockItem(int index)
        {
            SaveLoadData.Data.IndexCharacterUse = index;
            SaveLoadData.Data.IndexCharacterUnlocked[index] = true;
            characterIcon.sprite = characterSprites[SaveLoadData.Data.IndexCharacterUse];

            if (SaveLoadData.Data.IndexCharacterUse == SaveLoadData.Data.IndexCharacterNeedUnlock)
            {
                SaveLoadData.RandomIndexCharacterNeedUnlock();
            }

            foreach (var item in shopItems)
            {
                item.ResetItem();
            }

            SaveLoadData.SaveData();
            indexCharacterLockeds.Remove(index);
        }

        public void PreviewCharacter(int characterIndex)
        {
            characterIcon.sprite = characterSprites[characterIndex];
        }

        public void SelectCharacter()
        {
            foreach (var item in shopItems)
            {
                item.ResetItem();
            }

            characterIcon.sprite = characterSprites[SaveLoadData.Data.IndexCharacterUse];
        }
    }
}
