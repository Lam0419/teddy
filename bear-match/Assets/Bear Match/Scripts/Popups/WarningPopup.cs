using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace BearMatch
{
    public class WarningPopup : GamePopup
    {
        [SerializeField] private TextMeshProUGUI titleText;
        [SerializeField] private TextMeshProUGUI contentText;

        public override void Show(GamePopup popup, bool isHideLastPopup = true, object data = null, UnityAction closeCallback = null, bool isShowSuperImage = true)
        {
            base.Show(popup, isHideLastPopup: false, data, closeCallback, isShowSuperImage: false);

            if (data == null)
            {
                string[] datas = Constants.CommingSoon.Split("/");
                titleText.text = datas[0];
                contentText.text = datas[1];
            }
            else
            {
                string[] datas = data.ToString().Split("/");
                titleText.text = datas[0];
                contentText.text = datas[1];
            }
        }

        public void ShowText(string text)
        {
            string[] datas = text.Split("/");
            titleText.text = datas[0];
            contentText.text = datas[1];
            gameObject.SetActive(true);
        }
    }
}
