using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using Google.Play.Review;
using UnityEngine.UI;

namespace BearMatch
{
    public class RatePopup : GamePopup
    {
        [SerializeField] private Image[] stars;

        private bool isClickRate = false;

        public override void Show(GamePopup popup, bool isHideLastPopup = true, object data = null, UnityAction closeCallback = null, bool isShowSuperImage = true)
        {
            base.Show(popup, isHideLastPopup: false, data, closeCallback, isShowSuperImage: false);

            isClickRate = false;
            for (int i = 0; i < stars.Length; i++)
            {
                stars[i].color = Color.gray;
            }
        }

        public void Click_Rate(int starIndex)
        {
            if (isClickRate) return;

            isClickRate = true;
            for (int i = 0; i < stars.Length; i++)
            {
                stars[i].color = i <= starIndex ? Color.white : Color.gray;
            }

            if (starIndex <= 2)
            {
                RateAndClosePopup();
            }
            else
            {
                StartCoroutine(IEShowRate());
            }
        }

        private IEnumerator IEShowRate()
        {
            AdsManager.Instance.IsPausedBySystem = true;

            var reviewManager = new ReviewManager();
            var requestFlowOperation = reviewManager.RequestReviewFlow();

            yield return requestFlowOperation;

            if (requestFlowOperation.Error != ReviewErrorCode.NoError)
            {
                // Log error. For example, using requestFlowOperation.Error.ToString().
                yield break;
            }

            var launchFlowOperation = reviewManager.LaunchReviewFlow(requestFlowOperation.GetResult());

            yield return launchFlowOperation;

            if (launchFlowOperation.Error != ReviewErrorCode.NoError)
            {
                // Log error. For example, using requestFlowOperation.Error.ToString().
                yield break;
            }

            RateAndClosePopup();
        }

        private void RateAndClosePopup()
        {
            SaveLoadData.Data.IsRated = true;
            SaveLoadData.SaveData();
            Hide();
        }
    }
}
