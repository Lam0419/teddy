﻿using TMPro;
using UnityEngine;

namespace BearMatch
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class FPSCounter : MonoBehaviour
    {
        private const float fpsMeasurePeriod = 0.5f;
        private int m_FpsAccumulator = 0;
        private float m_FpsNextPeriod = 0;
        private int m_CurrentFps;
        private TextMeshProUGUI m_Text;

        private void Start()
        {
            //if (!GameManager.Instance.IsEnableTestGame)
            {
                Destroy(gameObject);
                return;
            }

            m_FpsNextPeriod = Time.realtimeSinceStartup + fpsMeasurePeriod;
            m_Text = GetComponent<TextMeshProUGUI>();
        }

        private void Update()
        {
            // measure average frames per second
            m_FpsAccumulator++;
            if (Time.realtimeSinceStartup > m_FpsNextPeriod)
            {
                m_CurrentFps = (int)(m_FpsAccumulator / fpsMeasurePeriod);
                m_FpsAccumulator = 0;
                m_FpsNextPeriod += fpsMeasurePeriod;
                m_Text.text = $"{m_CurrentFps} FPS";
                m_Text.color = m_CurrentFps >= 45 ? Color.green : m_CurrentFps >= 30 ? Color.yellow : Color.red;
            }
        }
    }
}
