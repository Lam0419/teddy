using System;
using UnityEngine;
using UnityEngine.UI;

namespace BearMatch
{
    public class ShopItem : MonoBehaviour
    {
        [SerializeField] private GameObject highlight;
        [SerializeField] private Image background;
        [SerializeField] private Sprite backgroundChooseSprite;
        [SerializeField] private Sprite backgroundDisableSprite;
        [SerializeField] private Sprite backgroundUnlockedSprite;

        [SerializeField] private Image characterIcon;
        [SerializeField] private Sprite[] characterSprites;

        [SerializeField] private GameObject lockIcon;
        [SerializeField] private GameObject newIcon;
        [SerializeField] private GameObject useIcon;

        private ShopPopup shopPopup;
        private int characterIndex = 0;

        public void Initialized(ShopPopup popup, int index)
        {
            shopPopup = popup;
            characterIndex = index + 1;

            characterIcon.sprite = characterSprites[index];

            ResetItem();
        }

        public void Click_ChooseItem()
        {
            SoundManager.Instance.Play_ClickButton();

            if (lockIcon.activeInHierarchy)
            {
                shopPopup.PreviewCharacter(characterIndex);
            }
            else
            {
                SaveLoadData.Data.IndexCharacterUse = characterIndex;
                SaveLoadData.SaveData();

                shopPopup.SelectCharacter();
            }
        }

        public void ResetItem()
        {
            if (SaveLoadData.Data.IndexCharacterUnlocked[characterIndex])
            {
                characterIcon.color = Color.white;

                if (characterIndex == SaveLoadData.Data.IndexCharacterUse)
                {
                    background.sprite = backgroundChooseSprite;
                    useIcon.SetActive(true);
                }
                else
                {
                    background.sprite = backgroundUnlockedSprite;
                    useIcon.SetActive(false);
                }

                lockIcon.SetActive(false);
                //newIcon.SetActive(SaveLoadData.Data.IndexCharacterNew[characterIndex]);
                newIcon.SetActive(false);
            }
            else
            {
                background.sprite = backgroundDisableSprite;
                characterIcon.color = Color.gray;

                lockIcon.SetActive(true);
                newIcon.SetActive(false);
                useIcon.SetActive(false);
            }

            highlight.SetActive(false);
        }

        public void ShowHighlight(bool isShow)
        {
            highlight.SetActive(isShow);
        }
    }
}
