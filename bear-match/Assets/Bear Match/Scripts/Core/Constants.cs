﻿namespace BearMatch
{
    public class Constants
    {
        public const string USER_DATA_SAVE_NAME = "dsfmnsdjfhsdjfsfd";
        public const string USER_DATA_SAVE_HASH = "ssajwehwjemcmsk";


        #region Scene Name
        public const string SCENE_MAIN_MENU = "Main_Menu";
        public const string SCENE_GAMEPLAY = "Gameplay";
        public static readonly string[] SCENE_MAP = { "Map 1 new", "Map 1 old", "Map 1" };
        #endregion


        ////#if UNITY_EDITOR
        //        //public static string APP_LINK = "";
        ////#elif UNITY_ANDROID
        //        public static string APP_LINK = $"https://play.google.com/store/apps/details?id={ Application.identifier }";
        ////#elif UNITY_IOS
        //        //public static string APP_LINK = "itms-apps://itunes.apple.com/app/id1577676680";
        ////#endif

        //        //public static string STORE_LINK = $"https://play.google.com/store/apps/developer?id=SuperPuzzle+Studio";

        //        //public static string POLICY_LINK = "https://play.google.com/store/apps/details?id=";


        // sound and music seting
        public const string SETTING_AUDIO_VOLUME = "Audio Volume";
        public const string SETTING_SOUND_EFFECT_VOLUME = "Sound Effect Volume";
        public const string SETTING_MUSIC_VOLUME = "Music Volume";


        // Gameplay Tag
        public const string PLAYER_TAG = "Player";
        public const string BOT_TAG = "Bot";

        public static string ErrorWifi = "Error!/Something went\nwrong!";
        public static string CommingSoon = "Warning!/This feature is\ncomming soon..!";
        public static string NotEnoughCoin = "Oops!/Your budget is not\nenough to buy this skin!";
        public static string NewSkin = "Congrats!/You’ve got new skin!";
        public static string NotHaveInternet = "Error!/Turn on your network\nconnection to play game!";
    }
}
