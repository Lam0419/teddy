using UnityEngine;

namespace BearMatch
{
    public class UnderGround : MonoBehaviour
    {
        private void OnCollisionEnter(Collision collision)
        {
            switch (collision.collider.tag)
            {
                case Constants.PLAYER_TAG:
                    if (collision.rigidbody != null && collision.rigidbody.TryGetComponent(out PlayerController player) && player)
                    {
                        player.TriggerUnderground();
                    }
                    break;

                case Constants.BOT_TAG:
                    if (collision.rigidbody != null && collision.rigidbody.TryGetComponent(out BotController bot) && bot)
                    {
                        bot.TriggerUnderground();
                    }
                    break;
            }
        }
    }
}
