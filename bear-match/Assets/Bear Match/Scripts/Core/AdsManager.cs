using System;
using System.Collections;
using com.adjust.sdk;
//using GoogleMobileAds.Api;
using UnityEngine;
using UnityEngine.Events;

namespace BearMatch
{
    public class AdsManager : MonoBehaviour
    {
        [SerializeField] private bool IsEnableTest = true;

        [Header("Test Android")]
        [SerializeField] private string AppIdTest = "ca-app-pub-3940256099942544~3347511713";
        [SerializeField] private string BannerIdTest = "ca-app-pub-3940256099942544/6300978111";
        [SerializeField] private string InterstitialIdTest = "ca-app-pub-3940256099942544/1033173712";
        [SerializeField] private string RewardVideoIdTest = "ca-app-pub-3940256099942544/5224354917";

        [Header("Android")]
        //admob
        //[SerializeField] private string AppIdAndroid = "ca-app-pub-3940256099942544~3347511713";
        //[SerializeField] private string BannerIdAndroid = "ca-app-pub-3940256099942544/6300978111";
        //[SerializeField] private string InterstitialIdAndroid = "ca-app-pub-3940256099942544/1033173712";
        //[SerializeField] private string RewardVideoIdAndroid = "ca-app-pub-3940256099942544/5224354917";

        //max
        private const string maxSdkKey = "3N4Mt8SNhOzkQnGb9oHsRRG1ItybcZDpJWN1fVAHLdRagxP-_k_ZXVaMAdMe5Otsmp6qJSXskfsrtakfRmPAGW";
        [SerializeField] private string AppIdAndroid = "ca-app-pub-6530974883137971~7086050855";
        [SerializeField] private string BannerIdAndroid = "40386a01dfe5fc97";
        [SerializeField] private string InterstitialIdAndroid = "ca-app-pub-6530974883137971/1833724172";
        [SerializeField] private string RewardVideoIdAndroid = "ca-app-pub-6530974883137971/3313438752";

        [Header("iOS")]
        [SerializeField] private string AppIdIos = "ca-app-pub-3940256099942544~1458002511";
        [SerializeField] private string BannerIdIos = "ca-app-pub-3940256099942544/2934735716";
        [SerializeField] private string InterstitialIdIos = "ca-app-pub-3940256099942544/4411468910";
        [SerializeField] private string RewardVideoIdIos = "ca-app-pub-3940256099942544/1712485313";

        public static AdsManager Instance { get; private set; }
        [HideInInspector] public bool IsPausedBySystem = true;

        public static bool IsBannerLoaded { get; private set; } = false;
        public static bool IsInterstitialLoaded { get; private set; } = false;
        public static bool IsRewardVideoLoaded { get; private set; } = false;

        private string bannerID;
        private const string adjustID = "apphj7ww9am8";
        //admob
        //private BannerView bannerViewLoaded;


        private string interstitialId;
        //admob
        //private InterstitialAd interstitial;
        private UnityAction onAdsFullClosed;


        private string rewardVideoID;
        //admob
        //private RewardedAd rewardVideo;
        private bool isRequestingRewardedVideo;
        private bool isRewardVideoRewarded = false;
        private UnityAction onRewardVideoRewarded;
        private UnityAction onRewardVideoClosed;

        private int rewardedRetryAttempt;
        private int interstitialRetryAttempt;

        public bool adsLoaded = true;
        public bool isGetCointReward=false;


        private void Awake()
        {
            if (Instance != null)
            {
                return;
            }

            Debug.unityLogger.Log("Awake Ads Controller");
            Instance = this;

            // Initialize the Google Mobile Ads SDK.
#if UNITY_IOS
            MobileAds.SetiOSAppPauseOnBackground(true);
            Debug.unityLogger.Log("AdsManager: iOS App Pause On Background");
#endif
            //MobileAds.Initialize(initStatus => { });
        }

        private void Start()
        {
            if (IsEnableTest)
            {
                bannerID = BannerIdTest;
                interstitialId = InterstitialIdTest;
                rewardVideoID = RewardVideoIdTest;
            }
            else
            {
#if UNITY_ANDROID // for release
                bannerID = BannerIdAndroid;
                interstitialId = InterstitialIdAndroid;
                rewardVideoID = RewardVideoIdAndroid;
#elif UNITY_IOS // for release
                bannerID = BannerIdIos;
                interstitialId = InterstitialIdIos;
                rewardVideoID = RewardVideoIdIos;
#endif
            }
            InitMax();
        }


        void InitMax()
        {
            MaxSdkCallbacks.OnSdkInitializedEvent += sdkConfiguration =>
            {
                //AppLovin SDK is initialized, configure and start loading ads.
                Debug.Log("MAX SDK Initialized");
                LoadAds();
            };

            //Initialize Adjust SDK
            AdjustConfig adjustConfig = new AdjustConfig(adjustID, AdjustEnvironment.Production);
            Adjust.start(adjustConfig);

            MaxSdk.SetSdkKey(maxSdkKey);
            MaxSdk.InitializeSdk();
        }


        public void LoadAds()
        {
            if (!SaveLoadData.Data.IsBoughtNoAds)
            {
                RequestRewardVideo();
                RequestBanner();
                RequestInterstitial();
                EventDispatcherExtension.AddListener(EventID.Bought_No_Ads, OnBoughtNoAds);
            }
            else
            {
                IsBannerLoaded = true;
                IsInterstitialLoaded = true;
            }
        }
        #region Admob
        //private void OnBoughtNoAds(object data = null)
        //{
        //    Debug.unityLogger.Log("AdsManager: OnBoughtNoAds");

        //    if (bannerViewLoaded != null)
        //        bannerViewLoaded.Hide();

        //    EventDispatcherExtension.RemoveListener(EventID.Bought_No_Ads, OnBoughtNoAds);
        //}
        #endregion
        #region MAx
        private void OnBoughtNoAds(object data = null)
        {
            Debug.unityLogger.Log("AdsManager: OnBoughtNoAds");

            MaxSdk.HideBanner(bannerID);

            EventDispatcherExtension.RemoveListener(EventID.Bought_No_Ads, OnBoughtNoAds);
        }
        #endregion

        #region Banner Admob
        //        private void RequestBanner()
        //        {
        //            if (SaveLoadData.Data.IsBoughtNoAds) return;

        //            IsBannerLoaded = false;
        //            // Clean up banner ad before creating a new one.
        //            bannerViewLoaded?.Destroy();

        //            //AdSize adaptiveSize = AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(AdSize.FullWidth);
        //            bannerViewLoaded = new BannerView(bannerID, AdSize.SmartBanner, AdPosition.Bottom);

        //            // Called when an ad request has successfully loaded.
        //            bannerViewLoaded.OnAdLoaded += OnBannerLoaded;
        //            // Called when an ad request failed to load.
        //            bannerViewLoaded.OnAdFailedToLoad += OnBannerFailedToLoad;
        //            // Called when an ad is clicked.
        //            bannerViewLoaded.OnAdOpening += OnBannerOpened;
        //            // Called when the user returned from the app after an ad click.
        //            bannerViewLoaded.OnAdClosed += OnBannerClosed;
        //            bannerViewLoaded.OnPaidEvent += HandleAdPaidEvent;
        //            AdRequest adRequest = new AdRequest.Builder().Build();

        //            // Load a banner ad.
        //            bannerViewLoaded.LoadAd(adRequest);
        //        }

        //        private void OnBannerLoaded(object sender, EventArgs args)
        //        {
        //            Debug.unityLogger.Log("OnBannerLoaded event received");
        //            StartCoroutine(IEShowBanner());

        //            IsBannerLoaded = true;
        //        }

        //        private void OnBannerFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        //        {
        //            Debug.unityLogger.Log("OnBannerFailedToLoad event received with message: " + args.LoadAdError.GetMessage());
        //            Invoke(nameof(RequestBanner), 5);
        //        }

        //        private void OnBannerOpened(object sender, EventArgs args)
        //        {
        //            Debug.unityLogger.Log("OnBannerOpened event received");
        //            IsPausedBySystem = true;

        //#if !UNITY_EDITOR
        //            SaveLoadData.Data.NumberAdsClick++;
        //            SaveLoadData.SaveData();
        //            FirebaseManager.LogEvent($"ads_click_{SaveLoadData.Data.NumberAdsClick}");
        //#endif
        //        }

        //        private void OnBannerClosed(object sender, EventArgs args)
        //        {
        //            Debug.unityLogger.Log("OnBannerClosed event received");
        //        }

        //        private IEnumerator IEShowBanner()
        //        {
        //            yield return null;

        //            if (SaveLoadData.Data.IsBoughtNoAds)
        //            {
        //                bannerViewLoaded.Hide();
        //            }
        //            else
        //            {
        //#if !UNITY_EDITOR
        //                SaveLoadData.Data.NumberAdsShow++;
        //                SaveLoadData.SaveData();
        //                FirebaseManager.LogEvent($"ads_show_{SaveLoadData.Data.NumberAdsShow}");
        //#endif
        //            }
        //        }
        #endregion
        #region Banner Max
        private void RequestBanner()
        {
            IsBannerLoaded = false;
            MaxSdkCallbacks.Banner.OnAdLoadedEvent += OnBannerAdLoadedEvent;
            MaxSdkCallbacks.Banner.OnAdLoadFailedEvent += OnBannerAdFailedEvent;
            MaxSdkCallbacks.Banner.OnAdClickedEvent += OnBannerAdClickedEvent;
            MaxSdkCallbacks.Banner.OnAdRevenuePaidEvent += OnBannerAdRevenuePaidEvent;

            // Banners are automatically sized to 320x50 on phones and 728x90 on tablets.
            // You may use the utility method `MaxSdkUtils.isTablet()` to help with view sizing adjustments.
            MaxSdk.CreateBanner(bannerID, MaxSdkBase.BannerPosition.BottomCenter);
            MaxSdk.SetBannerExtraParameter(bannerID, "adaptive_banner", "false");
            // Set background or background color for banners to be fully functional.
            MaxSdk.SetBannerBackgroundColor(bannerID, Color.white);
        }

        private void OnBannerAdLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            // Banner ad is ready to be shown.
            // If you have already called MaxSdk.ShowBanner(BannerAdUnitId) it will automatically be shown on the next ad refresh.
            Debug.Log("Banner ad loaded");
            //StartCoroutine(IEShowBanner());
            IsBannerLoaded = true;
        }

        private void OnBannerAdFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
        {
            // Banner ad failed to load. MAX will automatically try loading a new ad internally.
            Debug.Log("Banner ad failed to load with error code: " + errorInfo.Code);
            Invoke(nameof(RequestBanner), 5);
        }

        private void OnBannerAdClickedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            Debug.unityLogger.Log("OnBannerOpened event received");
            IsPausedBySystem = true;

#if !UNITY_EDITOR
            SaveLoadData.Data.NumberAdsClick++;
            SaveLoadData.SaveData();
            FirebaseManager.LogEvent($"ads_click_{SaveLoadData.Data.NumberAdsClick}");
#endif
        }

        private void OnBannerAdRevenuePaidEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            // Banner ad revenue paid. Use this callback to track user revenue.
            Debug.Log("Banner ad revenue paid");

            // Ad revenue
            double revenue = adInfo.Revenue;

            // Miscellaneous data
            string countryCode = MaxSdk.GetSdkConfiguration().CountryCode; // "US" for the United States, etc - Note: Do not confuse this with currency code which is "USD" in most cases!
            string networkName = adInfo.NetworkName; // Display name of the network that showed the ad (e.g. "AdColony")
            string adUnitIdentifier = adInfo.AdUnitIdentifier; // The MAX Ad Unit ID
            string placement = adInfo.Placement; // The placement this ad's postbacks are tied to

            TrackAdRevenue(adInfo);
        }

        #region IEShowBanner Admob
        //        private IEnumerator IEShowBanner()
        //        {
        //            yield return null;

        //            if (SaveLoadData.Data.IsBoughtNoAds)
        //            {
        //                bannerViewLoaded.Hide();
        //            }
        //            else
        //            {
        //#if !UNITY_EDITOR
        //                SaveLoadData.Data.NumberAdsShow++;
        //                SaveLoadData.SaveData();
        //                FirebaseManager.LogEvent($"ads_show_{SaveLoadData.Data.NumberAdsShow}");
        //#endif
        //            }
        //        }
        #endregion

        #region IEShowBanner MAx
        private IEnumerator IEShowBanner()
        {
            yield return null;

            if (SaveLoadData.Data.IsBoughtNoAds)
            {
                MaxSdk.HideBanner(bannerID);
            }
            else
            {
#if !UNITY_EDITOR
                SaveLoadData.Data.NumberAdsShow++;
                SaveLoadData.SaveData();
                FirebaseManager.LogEvent($"ads_show_{SaveLoadData.Data.NumberAdsShow}");
#endif
            }
        }
        #endregion
        #endregion


        #region Interstitial Admob
        //        private void RequestInterstitial()
        //        {
        //            if (SaveLoadData.Data.IsBoughtNoAds) return;

        //            IsInterstitialLoaded = false;
        //            // Clean up interstitial ad before creating a new one.
        //            interstitial?.Destroy();

        //            // Initialize an InterstitialAd.
        //            interstitial = new InterstitialAd(interstitialId);

        //            // Called when an ad request has successfully loaded.
        //            interstitial.OnAdLoaded += OnInterstitialLoaded;
        //            // Called when an ad request failed to load.
        //            interstitial.OnAdFailedToLoad += OnInterstitialFailedToLoad;
        //            // Called when an ad is shown.
        //            interstitial.OnAdOpening += OnInterstitialOpened;
        //            // Called when the ad is closed.
        //            interstitial.OnAdClosed += OnInterstitialClosed;
        //            interstitial.OnPaidEvent += HandleAdPaidEvent;

        //            // Create an empty ad request.
        //            AdRequest request = new AdRequest.Builder().Build();
        //            // Load the interstitial with the request.
        //            interstitial.LoadAd(request);
        //        }

        //        private void OnInterstitialLoaded(object sender, EventArgs args)
        //        {
        //            Debug.unityLogger.Log("Loaded event received");
        //            IsInterstitialLoaded = true;
        //        }

        //        private void OnInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        //        {
        //            Debug.unityLogger.Log("OnInterstitialFailedToLoad event received with message: " + args.LoadAdError.GetMessage());

        //            IsInterstitialLoaded = false;
        //            Invoke(nameof(RequestInterstitial), 5);
        //        }

        //        private void OnInterstitialOpened(object sender, EventArgs args)
        //        {
        //            Debug.unityLogger.Log("OnInterstitialOpened event received");
        //            IsPausedBySystem = true;

        //#if !UNITY_EDITOR
        //            SaveLoadData.Data.NumberAdsClick++;
        //            SaveLoadData.SaveData();
        //            FirebaseManager.LogEvent($"ads_click_{SaveLoadData.Data.NumberAdsClick}");
        //#endif
        //        }

        //        private void OnInterstitialClosed(object sender, EventArgs args)
        //        {
        //            Debug.unityLogger.Log("OnInterstitialClosed event received");
        //            StartCoroutine(IEDoInterstitialClosedCallback());
        //        }

        //        private IEnumerator IEDoInterstitialClosedCallback()
        //        {
        //            yield return null;
        //            onAdsFullClosed?.Invoke();
        //            onAdsFullClosed = null;
        //        }
        #endregion
        #region Interstitial Max
        private void RequestInterstitial()
        {
            if (SaveLoadData.Data.IsBoughtNoAds) return;

            IsInterstitialLoaded = false;

            MaxSdkCallbacks.Interstitial.OnAdLoadedEvent += OnInterstitialLoadedEvent;
            MaxSdkCallbacks.Interstitial.OnAdLoadFailedEvent += OnInterstitialFailedEvent;
            MaxSdkCallbacks.Interstitial.OnAdDisplayFailedEvent += InterstitialFailedToDisplayEvent;
            MaxSdkCallbacks.Interstitial.OnAdClickedEvent += OnInterstitialClickedEvent;
            MaxSdkCallbacks.Interstitial.OnAdHiddenEvent += OnInterstitialDismissedEvent;
            MaxSdkCallbacks.Interstitial.OnAdRevenuePaidEvent += OnInterstitialRevenuePaidEvent;

            // Load the first interstitial
            LoadInterstitial();
        }

        void LoadInterstitial()
        {
            MaxSdk.LoadInterstitial(interstitialId);
        }

        private void OnInterstitialLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            // Interstitial ad is ready to be shown. MaxSdk.IsInterstitialReady(interstitialAdUnitId) will now return 'true'
            Debug.Log("Interstitial loaded");
            IsInterstitialLoaded = true;
            // Reset retry attempt
            interstitialRetryAttempt = 0;
        }

        private void InterstitialFailedToDisplayEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo, MaxSdkBase.AdInfo adInfo)
        {
            // Interstitial ad failed to display. We recommend loading the next ad
            Debug.Log("Interstitial failed to display with error code: " + errorInfo.Code);
            LoadInterstitial();
        }

        private void OnInterstitialFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
        {
            // Interstitial ad failed to load. We recommend retrying with exponentially higher delays up to a maximum delay (in this case 64 seconds).
            interstitialRetryAttempt++;
            IsInterstitialLoaded = false;
            double retryDelay = Math.Pow(2, Math.Min(6, interstitialRetryAttempt));

            Debug.Log("Interstitial failed to load with error code: " + errorInfo.Code);

            Invoke("LoadInterstitial", (float)retryDelay);
        }

        private void OnInterstitialDismissedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            LoadInterstitial();
        }

        private void OnInterstitialClickedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo) 
        {
            Debug.unityLogger.Log("OnInterstitialOpened event received");
            IsPausedBySystem = true;

#if !UNITY_EDITOR
            SaveLoadData.Data.NumberAdsClick++;
            SaveLoadData.SaveData();
            FirebaseManager.LogEvent($"ads_click_{SaveLoadData.Data.NumberAdsClick}");
#endif
        }

        private void OnInterstitialRevenuePaidEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            // Interstitial ad revenue paid. Use this callback to track user revenue.
            Debug.Log("Interstitial revenue paid");

            // Ad revenue
            double revenue = adInfo.Revenue;

            // Miscellaneous data
            string countryCode = MaxSdk.GetSdkConfiguration().CountryCode; // "US" for the United States, etc - Note: Do not confuse this with currency code which is "USD" in most cases!
            string networkName = adInfo.NetworkName; // Display name of the network that showed the ad (e.g. "AdColony")
            string adUnitIdentifier = adInfo.AdUnitIdentifier; // The MAX Ad Unit ID
            string placement = adInfo.Placement; // The placement this ad's postbacks are tied to

            TrackAdRevenue(adInfo);
        }
        #endregion


        #region Reward Video Admob
        //        private void RequestRewardVideo()
        //        {
        //            isRewardVideoRewarded = false;
        //            isRequestingRewardedVideo = true;

        //            rewardVideo = new RewardedAd(rewardVideoID);

        //            // Called when an ad request has successfully loaded.
        //            rewardVideo.OnAdLoaded += OnRewardVideoLoaded;
        //            // Called when an ad request failed to load.
        //            rewardVideo.OnAdFailedToLoad += OnRewardVideoFailedToLoad;
        //            // Called when an ad is shown.
        //            rewardVideo.OnAdOpening += OnRewardVideoOpened;
        //            // Called when an ad request failed to show.
        //            rewardVideo.OnAdFailedToShow += OnRewardVideoFailedToShow;
        //            // Called when the user should be rewarded for interacting with the ad.
        //            rewardVideo.OnUserEarnedReward += OnRewardVideoRewarded;
        //            // Called when the ad is closed.
        //            rewardVideo.OnAdClosed += OnRewardVideoClosed;
        //            rewardVideo.OnPaidEvent += HandleAdPaidEvent;
        //            // Create an empty ad request.
        //            AdRequest request = new AdRequest.Builder().Build();
        //            // Load the rewarded ad with the request.
        //            rewardVideo.LoadAd(request);
        //        }

        //        private void OnRewardVideoLoaded(object sender, EventArgs args)
        //        {
        //            Debug.unityLogger.Log("OnRewardVideoLoaded event received");

        //            IsRewardVideoLoaded = true;
        //            isRequestingRewardedVideo = false;
        //        }

        //        private void OnRewardVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        //        {
        //            Debug.unityLogger.Log("OnRewardVideoFailedToLoad event received with message: " + args.LoadAdError.GetMessage());

        //            IsRewardVideoLoaded = false;
        //            isRequestingRewardedVideo = false;
        //            Invoke(nameof(RequestRewardVideo), 10);
        //        }

        //        private void OnRewardVideoOpened(object sender, EventArgs args)
        //        {
        //            Debug.unityLogger.Log("OnRewardVideoOpened event received");
        //            IsPausedBySystem = true;

        //#if !UNITY_EDITOR
        //            SaveLoadData.Data.NumberAdsClick++;
        //            SaveLoadData.SaveData();
        //            FirebaseManager.LogEvent($"ads_click_{SaveLoadData.Data.NumberAdsClick}");
        //#endif
        //        }

        //        private void OnRewardVideoFailedToShow(object sender, AdErrorEventArgs args)
        //        {
        //            Debug.unityLogger.Log("OnRewardVideoFailedToShow event received with message: " + args.AdError.GetMessage());
        //        }

        //        private void OnRewardVideoRewarded(object sender, Reward args)
        //        {
        //            Debug.unityLogger.Log($"OnRewardVideoRewarded event received for {args.Amount} {args.Type}");
        //            isRewardVideoRewarded = true;
        //        }

        //        private void OnRewardVideoClosed(object sender, EventArgs args)
        //        {
        //            Debug.unityLogger.Log("OnRewardVideoClosed event received");

        //            if (isRewardVideoRewarded)
        //            {
        //                StartCoroutine(IEDoRewardVideoRewardedCallback());
        //            }
        //            else
        //            {
        //                StartCoroutine(IEDoRewardVideoClosedCallback());
        //            }

        //            RequestRewardVideo();
        //        }

        //        private IEnumerator IEDoRewardVideoRewardedCallback()
        //        {
        //            yield return null;
        //            onRewardVideoRewarded?.Invoke();
        //            onRewardVideoRewarded = null;
        //        }

        //        private IEnumerator IEDoRewardVideoClosedCallback()
        //        {
        //            yield return null;
        //            onRewardVideoClosed?.Invoke();
        //            onRewardVideoClosed = null;
        //        }
        #endregion
        #region Reward Video Max
        private void RequestRewardVideo()
        {
            isRewardVideoRewarded = false;
            isRequestingRewardedVideo = true;

            MaxSdkCallbacks.Rewarded.OnAdLoadedEvent += OnRewardedAdLoadedEvent;
            MaxSdkCallbacks.Rewarded.OnAdLoadFailedEvent += OnRewardedAdFailedEvent;
            MaxSdkCallbacks.Rewarded.OnAdDisplayFailedEvent += OnRewardedAdFailedToDisplayEvent;
            MaxSdkCallbacks.Rewarded.OnAdDisplayedEvent += OnRewardedAdDisplayedEvent;
            MaxSdkCallbacks.Rewarded.OnAdClickedEvent += OnRewardedAdClickedEvent;
            //isRewardVideoRewarded = false;
            MaxSdkCallbacks.Rewarded.OnAdHiddenEvent += OnRewardedAdDismissedEvent;
            MaxSdkCallbacks.Rewarded.OnAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;
            MaxSdkCallbacks.Rewarded.OnAdRevenuePaidEvent += OnRewardedAdRevenuePaidEvent;
            // Load the first RewardedAd
            LoadRewardedAd();
        }

        private void OnRewardedAdLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            // Rewarded ad is ready to be shown. MaxSdk.IsRewardedAdReady(rewardedAdUnitId) will now return 'true'
            Debug.Log("Rewarded ad loaded");
            IsRewardVideoLoaded = true;
            isRequestingRewardedVideo = false;
            // Reset retry attempt
            rewardedRetryAttempt = 0;
        }

        private void OnRewardedAdFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
        {
            // Rewarded ad failed to load. We recommend retrying with exponentially higher delays up to a maximum delay (in this case 64 seconds).
            rewardedRetryAttempt++;
            IsRewardVideoLoaded = false;
            isRequestingRewardedVideo = false;

            double retryDelay = Math.Pow(2, Math.Min(6, rewardedRetryAttempt));

            Debug.Log("Rewarded ad failed to load with error code: " + errorInfo.Code);

            Invoke("LoadRewardedAd", (float)retryDelay);
        }

        private void OnRewardedAdFailedToDisplayEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo, MaxSdkBase.AdInfo adInfo)
        {
            // Rewarded ad failed to display. We recommend loading the next ad
            Debug.Log("Rewarded ad failed to display with error code: " + errorInfo.Code);
            IsPausedBySystem = true;
            LoadRewardedAd();
        }

        private void OnRewardedAdDisplayedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            Debug.Log("Rewarded ad displayed");
        }

        private void OnRewardedAdClickedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            Debug.Log("Rewarded ad clicked");
        }

        private void OnRewardVideoOpened(object sender, EventArgs args)
        {
            Debug.unityLogger.Log("OnRewardVideoOpened event received");
            IsPausedBySystem = true;

#if !UNITY_EDITOR
            SaveLoadData.Data.NumberAdsClick++;
            SaveLoadData.SaveData();
            FirebaseManager.LogEvent($"ads_click_{SaveLoadData.Data.NumberAdsClick}");
#endif
        }

        private void OnRewardedAdDismissedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            // Rewarded ad is hidden. Pre-load the next ad
            Debug.Log("Rewarded ad closed");
            if(isRewardVideoRewarded)
            {
                StartCoroutine(IEDoRewardVideoRewardedCallback());
                isRewardVideoRewarded = false;
            }
            else
            {
                StartCoroutine(IEDoRewardVideoClosedCallback());
            }

            LoadRewardedAd();
        }

        private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward, MaxSdkBase.AdInfo adInfo)
        {
            // Rewarded ad was displayed and user should receive the reward
            Debug.Log("Rewarded ad received reward");
            isRewardVideoRewarded = true;
            isGetCointReward = true;
        }

        private void OnRewardedAdRevenuePaidEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            // Rewarded ad revenue paid. Use this callback to track user revenue.
            Debug.Log("Rewarded ad revenue paid");

            // Ad revenue
            double revenue = adInfo.Revenue;

            // Miscellaneous data
            string countryCode = MaxSdk.GetSdkConfiguration().CountryCode; // "US" for the United States, etc - Note: Do not confuse this with currency code which is "USD" in most cases!
            string networkName = adInfo.NetworkName; // Display name of the network that showed the ad (e.g. "AdColony")
            string adUnitIdentifier = adInfo.AdUnitIdentifier; // The MAX Ad Unit ID
            string placement = adInfo.Placement; // The placement this ad's postbacks are tied to


            TrackAdRevenue(adInfo);
        }
        private IEnumerator IEDoRewardVideoRewardedCallback()
        {
            yield return null;
            onRewardVideoRewarded?.Invoke();
            onRewardVideoRewarded = null;
        }

        private IEnumerator IEDoRewardVideoClosedCallback()
        {
            yield return null;
            onRewardVideoClosed?.Invoke();
            onRewardVideoClosed = null;
        }
        private void LoadRewardedAd()
        {
            MaxSdk.LoadRewardedAd(RewardVideoIdAndroid);
        }
        #endregion


        #region Public Method
        //    public float GetBannerHeight()
        //    {
        //        return bannerView != null ? bannerView.GetHeightInPixels() : 0;
        //    }

        //public void HideBanner()
        //{
        //    Debug.unityLogger.Log("Hide Banner");
        //    if (bannerViewLoaded != null)
        //    {
        //        bannerViewLoaded.Hide();
        //    }
        //}

        //public void ShowBanner()
        //{
        //    Debug.unityLogger.Log("Show Banner");

        //    if (SaveLoadData.Data.IsBoughtNoAds) return;

        //    if (bannerViewLoaded != null)
        //    {
        //        bannerViewLoaded.Show();
        //    }
        //}
        #region ShowInter Admob
        //        public void ShowInterstitialAd(UnityAction interstitialClosedCallback = null)
        //        {
        //            Debug.unityLogger.Log("Show Interstitial");

        //            if (SaveLoadData.Data.IsBoughtNoAds)
        //            {
        //                interstitialClosedCallback?.Invoke();
        //                return;
        //            }

        //#if UNITY_EDITOR
        //            interstitialClosedCallback?.Invoke();
        //            return;
        //#else
        //            if (interstitial != null && interstitial.IsLoaded())
        //            {
        //                onAdsFullClosed = interstitialClosedCallback;
        //                interstitial.Show();
        //                RequestInterstitial();

        //                SaveLoadData.Data.NumberAdsShow++;
        //                SaveLoadData.SaveData();
        //                FirebaseManager.LogEvent($"ads_show_{SaveLoadData.Data.NumberAdsShow}");
        //            }
        //            else
        //            {
        //                interstitialClosedCallback?.Invoke();
        //                onAdsFullClosed = null;
        //            }
        //#endif
        //        }
        #endregion
        #region ShowInter MAx
        public void ShowInterstitialAd(UnityAction interstitialClosedCallback = null)
        {
            Debug.unityLogger.Log("Show Interstitial");

            if (SaveLoadData.Data.IsBoughtNoAds)
            {
                interstitialClosedCallback?.Invoke();
                return;
            }

#if UNITY_EDITOR
            interstitialClosedCallback?.Invoke();
            return;
#else
            if (InterstitialIsLoaded())
            {
                onAdsFullClosed = interstitialClosedCallback;
                MaxSdk.ShowInterstitial(interstitialId);
                RequestInterstitial();
                SaveLoadData.Data.NumberAdsShow++;
                SaveLoadData.SaveData();
                Firebase.Analytics.FirebaseAnalytics.LogEvent($"ads_show_{SaveLoadData.Data.NumberAdsShow}");
            }
            else
            {
                interstitialClosedCallback?.Invoke();
                onAdsFullClosed = null;
            }
#endif
        }
#endregion

#region ShowReward Admob
        //        public void ShowRewardVideo(UnityAction rewardedCallback = null, UnityAction onRewardVideoClosedCallback = null, UnityAction onRewardVideoFailedCallback = null)
        //        {
        //            Debug.unityLogger.Log("Show Reward Video");

        //#if UNITY_EDITOR
        //            rewardedCallback?.Invoke();
        //#else
        //            if (IsRewardVideoLoaded && rewardVideo != null && rewardVideo.IsLoaded())
        //            {
        //                rewardVideo.Show();
        //                onRewardVideoRewarded = rewardedCallback;
        //                onRewardVideoClosed = onRewardVideoClosedCallback;

        //                SaveLoadData.Data.NumberAdsShow++;
        //                SaveLoadData.SaveData();
        //                FirebaseManager.LogEvent($"ads_show_{SaveLoadData.Data.NumberAdsShow}");
        //            }
        //            else
        //            {
        //                //UiManager.Instance.Show<NoWifiPopup>();
        //                if (!isRequestingRewardedVideo)
        //                {
        //                    CancelInvoke();
        //                    RequestRewardVideo();
        //                }

        //                onRewardVideoFailedCallback?.Invoke();
        //            }
        //#endif
        //        }
#endregion
#region ShowReward MAx
        public void ShowRewardVideo(UnityAction rewardedCallback = null, UnityAction onRewardVideoClosedCallback = null, UnityAction onRewardVideoFailedCallback = null)
        {
            Debug.unityLogger.Log("Show Reward Video");

#if UNITY_EDITOR
            rewardedCallback?.Invoke();
#else
            if (VideoRewardIsLoaded() && IsRewardVideoLoaded)
            {
                MaxSdk.ShowRewardedAd(rewardVideoID);
                onRewardVideoRewarded = rewardedCallback;
                onRewardVideoClosed = onRewardVideoClosedCallback;
                SaveLoadData.Data.NumberAdsShow++;
                SaveLoadData.SaveData();
                Firebase.Analytics.FirebaseAnalytics.LogEvent($"ads_show_{SaveLoadData.Data.NumberAdsShow}");
                IsRewardVideoLoaded = false;
            }
            else
            {
                //UiManager.Instance.Show<NoWifiPopup>();
                if (!isRequestingRewardedVideo)
                {
                    CancelInvoke();
                    RequestRewardVideo();
                }

                onRewardVideoFailedCallback?.Invoke();
            }
#endif
        }
#endregion
#endregion


        //public void HandleAdPaidEvent(object sender, AdValueEventArgs args)
        //{
        //    Debug.Log("-------------------------------------------------- revenue");
        //    // ...
        //    AdValue adValue = args.AdValue;
        //    // send ad revenue info to Adjust
        //    AdjustAdRevenue adRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceAdMob);
        //    adRevenue.setRevenue(adValue.Value / 1000000f, adValue.CurrencyCode);
        //    Adjust.trackAdRevenue(adRevenue);
        //    Debug.Log("-------------------------------------------------- revenue : " + (adValue.Value / 1000000f).ToString());

        //}

        private void OnApplicationPause(bool pause)
        {
            if (!pause)
            {
                Debug.unityLogger.Log("App resume");
            }

            if (!SaveLoadData.Data.IsBoughtNoAds && !pause)
            {
                if (!IsPausedBySystem)
                {
                    ShowInterstitialAd(onAdsFullClosed);
                }
                else
                {
                    IsPausedBySystem = false;
                }
            }
        }
        private void TrackAdRevenue(MaxSdkBase.AdInfo adInfo)
        {
            AdjustAdRevenue adjustAdRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceAppLovinMAX);

            adjustAdRevenue.setRevenue(adInfo.Revenue, "USD");
            adjustAdRevenue.setAdRevenueNetwork(adInfo.NetworkName);
            adjustAdRevenue.setAdRevenueUnit(adInfo.AdUnitIdentifier);
            adjustAdRevenue.setAdRevenuePlacement(adInfo.Placement);

            Adjust.trackAdRevenue(adjustAdRevenue);
        }

        public void Requestbanner()
        {
            RequestBanner();
        }

        public void ShowBanner()
        {
#region Admob
            //Debug.unityLogger.Log("Show Banner");

            //if (SaveLoadData.Data.IsBoughtNoAds) return;

            //if (bannerViewLoaded != null)
            //{
            //    bannerViewLoaded.Show();
            //}
#endregion
#region Max
            if (SaveLoadData.Data.IsBoughtNoAds) return;
            MaxSdk.ShowBanner(bannerID);
            Camera.main.rect = new Rect(0f, 0.07f, 1f, 1f);
#endregion
        }
#region Max

#endregion
        public bool InterstitialIsLoaded()
        {
            return MaxSdk.IsInterstitialReady(interstitialId);
        }

        public bool VideoRewardIsLoaded()
        {
            return MaxSdk.IsRewardedAdReady(rewardVideoID);
        }


    }
}
