using UnityEngine;

namespace BearMatch
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Logger.Log("Awake Game Manager");
            DontDestroyOnLoad(gameObject);
            Instance = this;

            Application.targetFrameRate = 60;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            Input.multiTouchEnabled = false;

            SaveLoadData.LoadData();

            SaveLoadData.Data.AppOpenTimes++;
            SaveLoadData.SaveData();
        }
    }
}
